﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace serverHashes
{
    public static class WebSocketDictionary
    {
        public static string processDictionaryCommand(NetEventIO client, string cmd, string msg, ref FileStream fitxer)
        {
            switch (cmd)
            {
                case "get":
                    {
                        return llistarDiccionaris();
                    }
                case "upload":
                    {
                        return WebSocketUpload.putjarFitxerAlServidor(fitxer, client, msg);
                    }
                case "edit":
                    {
                        if (editDictionary(msg))
                            return "OK";
                        else
                            return "ERR";
                    }
                case "delete":
                    {
                        return deleteDictionary(msg);
                    }
            }
            return "";
        }

        private static string deleteDictionary(string msg)
        {
            if (!Guid.TryParse(msg, out _))
                return JsonConvert.SerializeObject(
                new DictionaryDeletionResult(DictionaryDeletionOperationResult.ERR));

            Dictionary d = Program.storage.getDictionary(msg);
            if (d == null)
                return JsonConvert.SerializeObject(
                new DictionaryDeletionResult(DictionaryDeletionOperationResult.ERR));

            List<Hash> hashesUsingDictionary;
            bool removed = Program.storage.removeDictionary(d, out hashesUsingDictionary);
            DictionaryDeletionResult result;
            if (removed)
            {
                result = new DictionaryDeletionResult(DictionaryDeletionOperationResult.OK);
                try
                {
                    File.Delete("./dictionary/" + d.uuid + "/" + d.fileName);
                    Directory.Delete("./dictionary/" + d.uuid);
                }
                catch (Exception)
                {
                    return JsonConvert.SerializeObject(
                        new DictionaryDeletionResult(DictionaryDeletionOperationResult.ERR));
                }
            }
            else
            {
                List<DictionaryDeletionHash> dictionaryDeletionHashes = new List<DictionaryDeletionHash>();
                foreach (Hash h in hashesUsingDictionary)
                {
                    dictionaryDeletionHashes.Add(new DictionaryDeletionHash(h));
                }
                result = new DictionaryDeletionResult(DictionaryDeletionOperationResult.ERR, dictionaryDeletionHashes);
            }
            return JsonConvert.SerializeObject(result);
        }


        private static bool editDictionary(string msg)
        {
            Dictionary<string, dynamic> editionData;
            try
            {
                editionData = (Dictionary<string, dynamic>)JsonConvert.DeserializeObject(msg, typeof(Dictionary<string, dynamic>));
            }
            catch (Exception)
            {
                return false;
            }

            dynamic uuid = null;
            if (editionData == null || !editionData.TryGetValue("uuid", out uuid) || uuid == null || !(uuid is string) || !Guid.TryParse(uuid, out Guid _))
            {
                return false;
            }

            Dictionary d = Program.storage.getDictionary(uuid);
            if (d == null) return false;
            d = new Dictionary(d);

            dynamic description = null;
            if (editionData.TryGetValue("description", out description) && description != null && description is string || description.length <= 3000)
            {
                d.description = description;
            }

            return Program.storage.updateDicionary(d);
        }

        private static string llistarDiccionaris()
        {
            return JsonConvert.SerializeObject(Program.storage.getAllDictionaries());
        }


    }
}
