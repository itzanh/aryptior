﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;

namespace serverHashes
{
    public static class WebSocketClients
    {
        public static void handleClients(string command, string message, NetEventIO.Callback callback, NetEventIO sender)
        {
            switch (command)
            {
                case "":
                    {
                        callback(llistarClients());
                        return;
                    }
                case "read":
                    {
                        getClientConfig(message, callback);
                        return;
                    }
                case "write":
                    {
                        setClientConfig(message, callback);
                        return;
                    }
                case "listBackup":
                    {
                        listClientBackups(message, callback);
                        return;
                    }
                case "configAll":
                    {
                        configureAllClients(message, sender, callback);
                        return;
                    }
                case "kick":
                    {
                        kickClient(message, callback);
                        return;
                    }
                case "ban":
                    {
                        banClient(message, callback);
                        return;
                    }
            }
            callback("ERR");
        }

        public static string llistarClients()
        {
            return JsonConvert.SerializeObject(Program.llistaClients);
        }

        public static void getClientConfig(string uuid, NetEventIO.Callback callback)
        {
            NetworkClient client;
            try
            {
                Program.llistaClients.TryGetValue(uuid, out client);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                callback("ERR");
                return;
            }

            if (client == null)
            {
                callback("ERR");
                return;
            }

            client.connection.emit("config", "read", "", (NetEventIO sender, string response) =>
            {
                if (response.Equals("ERR"))
                {
                    callback("ERR");
                    return;
                }
                try
                {
                    ClientSettings cs = (ClientSettings)JsonConvert.DeserializeObject(response, typeof(ClientSettings));
                    if (cs == null || !cs.isValid()) callback("ERR");
                    callback(response);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    callback("ERR");
                }
            });
        }

        public static void setClientConfig(string message, NetEventIO.Callback callback)
        {
            try
            {
                ClientNewSettings clientSettings = (ClientNewSettings)JsonConvert.DeserializeObject(message, typeof(ClientNewSettings));

                if (clientSettings == null || clientSettings.uuid == null
                    || clientSettings.newSettings == null || !Guid.TryParse(clientSettings.uuid, out _))
                {
                    callback("ERR");
                    return;
                }

                // validate the client settings before sending

                if (!clientSettings.newSettings.isValid())
                {
                    callback("ERR");
                    return;
                }

                // find the client by uuid on the list
                NetworkClient client;

                Program.llistaClients.TryGetValue(clientSettings.uuid, out client);
                if (client == null)
                {
                    callback("ERR");
                    return;
                }

                // emit the new settings to the found client and send to the web the response from it
                client.connection.emit("config", "write", JsonConvert.SerializeObject(clientSettings.newSettings),
                    (NetEventIO sender, string response) =>
                    {
                        if (!(response.Equals("OK") || response.Equals("ERR")))
                        {
                            callback("ERR");
                            return;
                        }
                        callback(response);
                    });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                callback("ERR");
                return;
            }
        }

        public static void listClientBackups(string uuid, NetEventIO.Callback callback)
        {
            NetworkClient client;
            try
            {
                Program.llistaClients.TryGetValue(uuid, out client);
            }
            catch (Exception)
            {
                callback("ERR");
                return;
            }

            if (client == null)
            {
                callback("ERR");
                return;
            }

            client.connection.emit("backup", "list", "", (NetEventIO sender, string response) =>
            {
                if (response.Equals("ERR"))
                {
                    callback("ERR");
                    return;
                }

                try
                {
                    List<BackupClient> backups = (List<BackupClient>)JsonConvert.DeserializeObject(response, typeof(List<BackupClient>));
                    if (backups == null || !verifyClientBackups(backups))
                    {
                        callback("ERR");
                        return;
                    }
                    callback(response);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    callback("ERR");
                }
            });
        }

        public static void configureAllClients(string message, NetEventIO sender, NetEventIO.Callback callback)
        {
            Dictionary<string, dynamic> keyValuePairs;
            try
            {
                keyValuePairs = (Dictionary<string, dynamic>)JsonConvert.DeserializeObject(message, typeof(Dictionary<string, dynamic>));
            }
            catch (Exception)
            {
                callback("ERR");
                return;
            }

            if (keyValuePairs == null || !ClientSettings.isValid(keyValuePairs))
            {
                callback("ERR");
                return;
            }

            Semaphore s = new Semaphore(0, 1);
            int current = 0;
            foreach (NetworkClient client in Program.llistaClients.Values)
            {
                client.connection.emit("configAll", "", message, (NetEventIO _, string response) =>
                {
                    s.Release();
                });
                s.WaitOne();
                current++;
                sender.emit("configAllProgress", "", current + ":" + Program.llistaClients.Count);
            }

            callback("OK");
        }

        private static bool verifyClientBackups(List<BackupClient> backups)
        {
            foreach (BackupClient backup in backups)
            {
                if (backup == null || !backup.isValid())
                    return false;

                if (Program.storage.mongo != null)
                {
                    Backup backupSel = Program.storage.mongo.getBackup(backup.uuid);
                    if (backupSel != null
                        && (!backupSel.hash.Equals(backup.hash)
                        || backupSel.size != backup.size
                        || backup.downloaded > backup.size)) return false;
                }
            }
            return true;
        }

        public static bool kickClient(string uuid, NetEventIO.Callback callback)
        {
            NetworkClient client;
            try
            {
                Program.llistaClients.TryGetValue(uuid, out client);
            }
            catch (Exception)
            {
                callback("ERR");
                return false;
            }
            if (client == null)
            {
                callback("ERR");
                return false;
            }

            Program.storage.onLog(LogType.ClientKick, new LogClient(client.connection.client.addr.Split(':')[0], client.connection.id));

            client.connection.client.disconnect();
            callback("OK");
            return true;
        }

        public static void banClient(string uuid, NetEventIO.Callback callback)
        {
            NetworkClient client;
            try
            {
                Program.llistaClients.TryGetValue(uuid, out client);
            }
            catch (Exception)
            {
                callback("ERR");
                return;
            }
            if (client == null)
            {
                callback("ERR");
                return;
            }

            Program.storage.onLog(LogType.ClientBan, client.connection.client.addr.Split(':')[0]);
            client.connection.client.disconnect();
            Program.CONFIG.socketSettings.addToBlacklist(client.addr.Split(':')[0]);
            Program.storage.ls.writeSettings(Program.CONFIG);
            callback("OK");
            return;
        }
    }
}
