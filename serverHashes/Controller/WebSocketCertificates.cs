﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading;

namespace serverHashes
{
    public static class WebSocketCertificates
    {
        public static void manageCert(NetEventIO sender, NetEventIO.Callback callback, string command, string message)
        {
            switch (command)
            {
                case "":
                case "list":
                    {
                        callback(listCertificates());
                        break;
                    }
                case "upload":
                    {
                        uploadCertificates(sender, message, callback);
                        break;
                    }
                case "remove":
                    {
                        callback(removeCertificate(message));
                        break;
                    }
                case "get":
                    {
                        callback(clientDownloadCertificate(sender, message));
                        break;
                    }
                default:
                    {
                        callback("ERR");
                        break;
                    }
            }
        }

        // TODO: Asegurarse de que existeix el directory i crearlo
        // TODO: Comprovar noms de fitxers incorrectes per sl FS
        // TODO: Comprovar les rutes per a '../',pero que no influsca en '.pfx'

        private static string listCertificates()
        {
            try
            {
                return JsonConvert.SerializeObject(Program.storage.listCertificates());
            }
            catch (Exception) { return "ERR"; }
        }

        private static void uploadCertificates(NetEventIO client, string msg, NetEventIO.Callback callback)
        {
            try
            {
                // check the file name
                if (msg.IndexOfAny(Path.GetInvalidPathChars()) > -1)
                {
                    callback("ERR");
                    return;
                }

                bool successfull = false;
                Thread t = new Thread(new ThreadStart(() =>
                {
                    client.on("certUpload", (NetEventIO sender, BinaryMessage message) =>
                    {
                        if (message == null || message.message == null
                        || message.message.Length > 1000000 || message.message.Length <= 0)
                        {
                            client.removeEvent("certUpload");
                            callback("ERR");
                            return;
                        }
                        CertificateInfo certificateInfo = new CertificateInfo(msg, message.message.Length);
                        if (Program.storage.insertCertificate(certificateInfo))
                            if (Program.storage.ls.writeCertificate(message.message, certificateInfo.uuid, false))
                                successfull = true;

                        client.removeEvent("certUpload");
                        callback(successfull ? "OK" : "ERR");
                    });

                    client.emit("certUpload");


                }));
                t.Start();

            }
            catch (Exception) { callback("ERR"); }
        }

        private static string removeCertificate(string message)
        {
            // search for the certificate in the database
            CertificateInfo certInfo = Program.storage.getCertificate(message);
            if (certInfo == null) return "ERR";

            // delete from the database
            if (!Program.storage.removeCertificate(certInfo.uuid)) return "ERR";

            // delete from disk
            if (!Program.storage.ls.deleteCertificate(certInfo.uuid)) return "ERR";

            return "OK";
        }

        private static string clientDownloadCertificate(NetEventIO client, string message)
        {
            // search for the certificate in the database
            CertificateInfo certInfo = Program.storage.getCertificate(message);
            if (certInfo == null) return "ERR";

            // read from disk
            byte[] readCert = Program.storage.ls.readCertificate(certInfo.uuid);
            if (readCert == null)
            {
                return "ERR";
            }

            new Thread(new ThreadStart(() =>
            {
                client.emit("certDownload", "", readCert ?? (new byte[0]));
            })).Start();

            return "OK";
        }
    }
}
