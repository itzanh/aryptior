﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading;

namespace serverHashes
{
    public static class WebSocketUpload
    {
        public static string putjarFitxerAlServidor(FileStream fitxer, NetEventIO client, string definicio)
        {
            Dictionary d;
            d = initializeUploadToServer(ref fitxer, definicio);
            if (d == null) return "ERR";
            // buscar si se ha de reanudar la putjada del fitxer al servidor
            long posInicial = 0;
            bool continuarPutjada = false;
            Program.storage.resumeDictionaryUploadToServer(ref d, ref posInicial, ref continuarPutjada);
            // marcar com a processant actualment
            d.isReady = false;
            // si se inicialitza de 0, hi ha que tornar a afegirlo
            if (!continuarPutjada)
            {
                d.uuid = Guid.NewGuid().ToString();
                if (!Program.storage.insertDictionary(d))
                    return "ERR";
            }

            new Thread(new ThreadStart(() =>
            {
                uploadChunkedData(client, d, posInicial);
                //d.uploadProgress = d.sizeInBytes;
                d.isReady = (d.uploadProgress == d.sizeInBytes);
                Program.storage.updateDicionary(d, false);
                if (fitxer != null)
                {
                    fitxer.Close();
                    fitxer.Close();
                    fitxer = null;
                }

            })).Start();

            return "OK";
        }

        private static void uploadChunkedData(NetEventIO client, Dictionary d, long posInicial)
        {
            int TAMANY_PART = pieceSize(d.sizeInBytes);
            string PATH = "./dictionary/" + d.uuid + "/" + d.uuid;
            // initialize directory structure
            try
            {
                if (!Directory.Exists("./dictionary")) Directory.CreateDirectory("./dictionary");
                if (!Directory.Exists("./dictionary/" + d.uuid)) Directory.CreateDirectory("./dictionary/" + d.uuid);
            }
            catch (Exception) { return; }
            // obrir el fitxer on escriure
            DictionaryUploadChunk p = new DictionaryUploadChunk(d.fileName, 0, TAMANY_PART);
            string mesDades = "";
            Semaphore demanar = new Semaphore(0, 1);

            for (long i = posInicial; i < d.sizeInBytes; i += TAMANY_PART)
            {
                long tamanyARebre = Math.Min((d.sizeInBytes - i), TAMANY_PART);
                // ajusar la part al progres actual
                p.i = i;
                p.chunkSize = tamanyARebre;
                d.uploadProgress = i;
                Program.storage.updateDicionary(d, false);
                Program.websocketPubSub.onPush("dictionary", SubscriptionChangeType.update, -1, JsonConvert.SerializeObject(d));
                // serialitzar
                mesDades = JsonConvert.SerializeObject(p);
                // demanar dades
                // esperar a rebre les dades demanades
                client.on("dictionaryChunkUpload", (NetEventIO _, BinaryMessage message) =>
                {
                    byte[] mesDadesRebudes = message.message;
                    // quant se hagen rebut, esciure a disc
                    if (mesDadesRebudes == null)
                    {
                        demanar.Release();
                        return;
                    }
                    Console.WriteLine("i " + i + " tamany " + mesDadesRebudes.Length);
                    if (mesDadesRebudes.Length < tamanyARebre)
                    {
                        demanar.Release();
                        return;
                    }
                    FileStream fitxer = new FileStream(PATH, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fitxer.Seek(0, SeekOrigin.End);
                    fitxer.Write(mesDadesRebudes, 0, mesDadesRebudes.Length);
                    fitxer.Flush();
                    fitxer.Close();
                    demanar.Release();
                }); //callback

                client.emit("dictionary", "chunkUpload", mesDades);
                Console.WriteLine("Demanat " + i + "/" + d.sizeInBytes);
                if (!demanar.WaitOne(60000)) break;

            } //for

            d.uploadProgress = p.i + Math.Min((d.sizeInBytes - p.i), TAMANY_PART);
            client.removeEvent("backupChunkUpload");
            Program.websocketPubSub.onPush("dictionary", SubscriptionChangeType.update, -1, JsonConvert.SerializeObject(d));
        }

        private static Dictionary initializeUploadToServer(ref FileStream fitxer, string definicio)
        {
            Dictionary d;
            // no permetre mes de una putjada al mateix temps per la mateixa connexio de websocket
            if (fitxer != null)
            {
                Console.WriteLine("Ja se esta putjant un fitxer");
                return null;
            }
            try
            {
                d = (Dictionary)JsonConvert.DeserializeObject(definicio, typeof(Dictionary));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
            if (d == null || !d.isValid()) return null;
            d.uploadProgress = 0;
            d.isReady = false;
            return d;
        }

        private static int pieceSize(long totalSize)
        {
            if (totalSize >= 0 && totalSize <= 350000000) // Between 0 and 350Mb, the piece size is 256Kb
            {
                return 256000;
            }
            else if (totalSize > 350000000 && totalSize <= 700000000) // Between 350Mb and 700Mb, the piece size is 512Kb
            {
                return 512000;
            }
            else if (totalSize > 700000000 && totalSize <= 4500000000) // Between 700Mb and 4.5Gb, the piece size is 2Mb
            {
                return 2000000;
            }
            else if (totalSize > 4500000000) // Higher than 4.5Gb, the piece size is 4Mb
            {
                return 4000000;
            }
            return 0;
        }
    }
}
