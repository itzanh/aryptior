﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading;

namespace serverHashes
{
    public static class WebSocketBackup
    {
        public static string processMessage(NetEventIO sender, string cmd, string msg)
        {
            switch (cmd)
            {
                case "list":
                    {
                        return listBackups();
                    }
                case "status":
                    {
                        return cronStatus();
                    }
                case "start":
                    {
                        if (startCronbackup())
                        {
                            return "OK";
                        }
                        else
                        {
                            return "ERR";
                        }
                    }
                case "stop":
                    {
                        if (stopCronBackup())
                            return "OK";
                        else
                            return "ERR";
                    }
                case "remove":
                    {
                        if (deleteBackup(msg))
                        {
                            return "OK";
                        }
                        else
                        {
                            return "ERR";
                        }
                    }
                case "upload":
                    {
                        return uploadBackup(sender, msg);
                    }
            }
            return "ERR";
        }

        private static bool stopCronBackup()
        {
            try
            {
                if (Program.backupCron != null)
                    return Program.backupCron.Stop();
            }
            catch (Exception) { }
            return false;
        }

        private static string uploadBackup(NetEventIO sender, string msg)
        {
            if (Program.storage.mongo == null) return "ERR";
            Backup b = new Backup();
            try
            {
                b.size = long.Parse(msg);
            }
            catch (Exception) { return "ERR"; }
            new Thread(new ThreadStart(() =>
            {
                long uploaded = uploadChunkedData(sender, b);
                if (uploaded == b.size) Program.storage.mongo.insertBackup(b);
            })).Start();
            return b.uuid;
        }

        private static long uploadChunkedData(NetEventIO client, Backup b)
        {
            const int TAMANY_PART = 65000;
            // obrir el fitxer on escriure
            BackupChunk p = new BackupChunk(b.uuid, 0, TAMANY_PART);
            string mesDades = "";
            Semaphore demanar = new Semaphore(0, 1);

            long i = 0;
            for (; i < b.size; i += TAMANY_PART)
            {
                long tamanyARebre = Math.Min((b.size - i), TAMANY_PART);
                // ajusar la part al progres actual
                p.i = i;
                p.tamanyPart = tamanyARebre;
                // serialitzar
                mesDades = JsonConvert.SerializeObject(p);
                // demanar dades
                // esperar a rebre les dades demanades
                client.on("backupChunkUpload", (NetEventIO _, BinaryMessage message) =>
                {
                    byte[] mesDadesRebudes = message.message;
                    // quant se hagen rebut, esciure a disc
                    Console.WriteLine("i " + i + " tamany " + mesDadesRebudes.Length);
                    FileStream fitxer = new FileStream(BackupCron.BACKUP_UPLOAD_DIR + b.toFileName(), FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fitxer.Seek(i, SeekOrigin.Begin);
                    fitxer.Write(mesDadesRebudes, 0, mesDadesRebudes.Length);
                    fitxer.Flush();
                    fitxer.Close();
                    demanar.Release();
                });

                client.emit("backup", "chunkUpload", mesDades);
                Console.WriteLine("Demanat " + i + "/" + b.size);
                demanar.WaitOne();

            } //for

            client.removeEvent("backupChunkUpload");
            return i;
        }

        private static bool deleteBackup(string msg)
        {
            if (!Program.CONFIG.backupSettings.doBackups) return false;
            return Program.backupCron.deleteBackup(msg);
        }

        private static bool startCronbackup()
        {
            return Program.backupCron.triggerBackup();
        }

        private static string listBackups()
        {
            if (Program.storage.mongo == null) return "ERR";
            return JsonConvert.SerializeObject(Program.storage.mongo.getBackups());
        }

        public static string cronStatus()
        {
            if (Program.backupCron == null || !Program.CONFIG.backupSettings.doBackups)
                return JsonConvert.SerializeObject(new BackupCronCurrentState(BackupCron.BackupCronState.stopped, DateTime.MinValue));
            else
                return JsonConvert.SerializeObject(new BackupCronCurrentState(Program.backupCron.cronState, Program.backupCron.nextBackup));
        }

    }
}
