﻿using Newtonsoft.Json;
using serverHashes.Models.Client;
using System;
using System.IO;
using System.Threading;

namespace serverHashes
{
    public static class Controller
    {
        /// <summary>
        /// Number of hashes per second.
        /// </summary>
        public static int nHashesS = 0;
        /// <summary>
        /// Counter of number of hashes processed in this period.
        /// </summary>
        public static int nHashesProcessed = 0;

        /// <summary>
        /// indica si ja es pot incrementar la combinacio o si esta en us, inicialitzat per defecte a 1 (en verd)
        /// </summary>
        public static readonly Semaphore semaforoCombinacio = new Semaphore(1, 1);

        /// <summary>
        /// Crida una funcio en resposta a un missatge especificat. 
        /// Funcio que s'encarrega de atendre els missatges que arriven per el socket dels client al servidor.
        /// </summary>
        /// <param name="clientIO"></param>        
        public static void processarMissatge(NetEventIO clientIO)
        {
            // initialization
            clientIO.on("initialization", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                callback(inicialitzacio());
            });

            clientIO.on("ping", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                callback(ping(e.message.command, sender));
            });

            // newPart
            clientIO.on("newPart", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                if (Program.estaIdle)
                {
                    callback("IDLE");
                    return;
                }
                Console.WriteLine("Enviant nova part...");
                NetworkClient client = Program.llistaClients[sender.id];
                Hash h = Program.hashActual;
                callback(novaPart(ref h, e.message.message, sender, ref client));
                Program.hashActual = h;
            });

            // informarPart
            clientIO.on("reportPart", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                NetworkClient client = Program.llistaClients[sender.id];
                informarPart(e.message.command, e.message.message, ref client);
                callback("OK");
            });

            // getRainbow
            clientIO.on("getRainbow", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                NetworkClient client = Program.llistaClients[sender.id];
                getRainbow(ref client, callback);
            });

            // reportRainbow
            clientIO.on("reportRainbow", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                NetworkClient client = Program.llistaClients[sender.id];
                if (reportRainbow(e.message.message, ref client))
                    callback("OK");
                else
                    callback("ERR");
            });
            clientIO.on("backup", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                processBackup(e.message.command, callback);
            });
            clientIO.on("backup", getBackup);
        }

        private static string ping(string cmd, NetEventIO sender)
        {
            if (cmd.Equals("interval"))
                return (
                    Program.CONFIG.socketSettings.socketOptions.networkTimeout > 0
                    ? Program.CONFIG.socketSettings.socketOptions.pingInterval.ToString()
                    : "0");
            else
                return "pong";
        }

        private static void getBackup(NetEventIO sender, OnMessageEventArgs e)
        {
            if (Program.storage.mongo == null)
            {
                sender.emit("backup", "err", new byte[0]);
                return;
            }
            Console.WriteLine(">> getting backup!");
            // get backup UUID
            if (!Guid.TryParse(e.message.message, out _))
            {
                sender.emit("backup", "err", new byte[0]);
                return;
            }

            Console.WriteLine(">> getting backup!");
            // get download block size and current index
            string[] metadata = e.message.command.Split('-');
            if (metadata.Length != 2)
            {
                sender.emit("backup", "err", new byte[0]);
                return;
            }
            Console.WriteLine(">> getting backup!");
            long blockSize;
            long index;
            try
            {
                blockSize = Int64.Parse(metadata[0]);
                index = Int64.Parse(metadata[1]);
            }
            catch (Exception)
            {
                sender.emit("backup", "err", new byte[0]);
                return;
            }

            Console.WriteLine(">> getting backup!");
            // get the backup object
            Backup b = Program.storage.mongo.getBackup(e.message.message);
            Console.WriteLine(">> getting backup! " + e.message.message);
            if (b == null || index >= b.size || blockSize <= 0 || index < 0)
            {
                Console.WriteLine(">> not getting backup! " + b.size + " " + index + " " + blockSize);
                sender.emit("backup", "err", new byte[0]);
                return;
            }

            Console.WriteLine(">> getting backup!");
            uploadBackupPart(sender, b, Math.Min(blockSize, (b.size - index)), index);
        }

        private static void uploadBackupPart(NetEventIO sender, Backup b, long chunkSize, long index)
        {
            Console.WriteLine(">> uploadinh backup!");
            FileStream fs = null;
            try
            {
                fs = new FileStream(Path.Combine(BackupCron.BACKUP_STORAGE_DIR, b.toFileName()), FileMode.Open);
                fs.Position = index;

                byte[] readData = new byte[chunkSize];
                fs.Read(readData, 0, readData.Length);

                Console.WriteLine(">> uploadinh backup!");
                if (!sender.emit("backup", chunkSize + "-" + index, readData)) Console.WriteLine(">> Error al enviar el mensajico");
            }
            catch (Exception r)
            {
                Console.WriteLine(">> not uploadinh backup! " + r.ToString());
                sender.emit("backup", "err", new byte[0]);
            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }

        private static void processBackup(string command, NetEventIO.Callback callback)
        {
            switch (command)
            {
                case "list":
                    {
                        if (Program.storage.mongo != null)
                        {
                            callback(JsonConvert.SerializeObject(Program.storage.mongo.getBackups()));
                            return;
                        }
                        break;
                    }
            }
            callback("ERR");
        }

        /// <summary>
        /// Inicialitzar un client amb el hash que se esta processant actualment en el servidor i el estat de este.
        /// Utilitza un pbjecte Estat, que serialitza a XML i envia per la connexio de xarxa per parametre.
        /// </summary>
        private static string inicialitzacio()
        {
            lock (Program.clientInitializationMutex)
            {
                return JsonConvert.SerializeObject(
                    new State(
                        Program.estaIdle,
                        Program.hashActual != null ? new ClientHash(Program.hashActual) : null
                        )
                    );
            }
        }

        private static bool reportRainbow(string missatge, ref NetworkClient client)
        {
            if (client.bruteForceChunk == null) return false;

            try
            {
                RainbowComputedPart computedPart = (RainbowComputedPart)JsonConvert.DeserializeObject(missatge, typeof(RainbowComputedPart));
                if (computedPart == null || !computedPart.isValid()) return false;

                Rainbow r = Program.storage.findRainbow(computedPart.id);
                if (r == null) return false;
                if (client.rainbowStorage == null) client.rainbowStorage = LevelDBStorage.getInstance(r.uuid);
                bool result = client.rainbowStorage.insertComputedHashes(computedPart.computedHashes);
                if (!result) return false;

                Program.incrementarBlocCombinacio(
                    ref client.bruteForceChunk.characters, r.hash.primerCaracterUnicode, r.hash.ultimCaracterUnicode, computedPart.computedHashes.Count);
                client.bruteForceChunk.charactersCount -= computedPart.computedHashes.Count;
                if (client.bruteForceChunk.charactersCount <= 0)
                {
                    client.lastReportedPart = DateTime.Now;
                    computeHashProcessingBlockSize(ref client);

                    client.processing = false;
                    client.processingUUID = null;
                    client.isProcessingRainbow = false;
                    client.bruteForceChunk = null;
                    client.dictionaryProcessingChunk = null;
                    if (client.rainbowStorage != null)
                    {
                        client.rainbowStorage = null;
                        client.lastReportedPart = DateTime.Now;
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR REPORTING RAINBOW " + e.ToString());
            }
            return false;
        }

        private static void getRainbow(ref NetworkClient client, NetEventIO.Callback callback)
        {
            if (client.processing || client.isProcessingRainbow)
            {
                callback(JsonConvert.SerializeObject(new RainbowPart("false", null, null)));
                return;
            }

            Rainbow r = Program.storage.getNextRainbow();
            if (r == null)
            {
                // if there are no rainbows, report that there is no work to do
                callback(JsonConvert.SerializeObject(new RainbowPart("false", null, null)));
                return;
            }

            // set the client as processing, and send a new part of the hash inside the rainbow
            client.processing = true;
            client.processingUUID = r.hash.uuid;
            client.isProcessingRainbow = true;
            BruteForceChunk bruteForceChunk = newRainbowPart(ref r, ref client);

            if (bruteForceChunk == null)
            {
                callback(JsonConvert.SerializeObject(new RainbowPart("false", null, null)));
            }
            else
            {
                callback(JsonConvert.SerializeObject(new RainbowPart(r.uuid, r.hash, bruteForceChunk)));
                Program.storage.updateRainbow(r);
            }
        }

        private static BruteForceChunk newRainbowPart(ref Rainbow r, ref NetworkClient client)
        {
            // esperar a poder modificar la part critica
            lock (semaforoCombinacio)
            {
                client.lastNewPartRequested = DateTime.Now;

                nHashesProcessed += (int)client.hashRatePerMs;

                if (r.hash.tipusBusqueda != TipusBusqueda.ForçaBruta)
                {
                    return null;
                }
                if (Program.combinacioAString(r.hash.caracters).Length > r.hash.caractersFinals
                    && r.hash.pendingCombinationCount() == 0)
                {
                    r.hash.estaResolt = true;
                    r.hash.dateFinished = DateTime.UtcNow;
                    r.hash.combinacionsPendents = null;
                    r.hash.lineesPendents = null;
                    Program.storage.updateRainbow(r);
                    return null;
                }
                else
                {
                    BruteForceChunk bruteForceChunk = novaPartCaracters(ref r.hash, ref client);

                    client.isProcessingRainbow = true;

                    return bruteForceChunk;
                }
            }
        }

        private static string novaPart(ref Hash hash, string msg, NetEventIO eventIO, ref NetworkClient client)
        {
            lock (semaforoCombinacio)
            {
                client.lastNewPartRequested = DateTime.Now;

                if (!msg.Equals(hash.uuid) || Program.estaIdle)
                {
                    return "IDLE";
                }

                if (client.hashRatePerMs > -1) nHashesProcessed += (int)client.hashRatePerMs;

                if (hash.tipusBusqueda == TipusBusqueda.ForçaBruta)
                {
                    // check the maximum length of the combination, to make sure this server desn't have to give up yet
                    if (Program.combinacioAString(hash.caracters).Length > hash.caractersFinals && hash.pendingCombinationCount() == 0)
                    {
                        Program.passwordNotFound();
                        return "IDLE";
                    }

                    Console.WriteLine("Augmentant combinacio a " + Program.combinacioAString());
                    return JsonConvert.SerializeObject(novaPartCaracters(ref hash, ref client));
                }
                else if (hash.tipusBusqueda == TipusBusqueda.Diccionari)
                {
                    Console.WriteLine("Augmentant combinacio a la linea " + hash.numLinea);
                    int dynamicCharCount = novaPartDiccionari(ref hash, eventIO, client);
                    return dynamicCharCount + "";
                }
                else
                {
                    return "ERR";
                }
            }
        }

        /// <summary>
        /// Donar a un client un rang (part) de combinacions a processar, 
        /// i modificar l'objecte del client per a guardar que esta processant actualment.
        /// </summary>
        /// <param name="client"></param>
        private static BruteForceChunk novaPartCaracters(ref Hash hash, ref NetworkClient client)
        {
            // PODEN QUEDAR COMBINACIONS PENDENTS DURANT EL PROCESSAMENT DEGUT A LA DESCONNEXIO DEL CLIENTS.
            // Per tant, es guarda per cada client si estan processant i que estan processant,
            // per a que al desconectarse, la combinacio s'enmagatzeme com a pendent.
            // Despres, quant es demane una nova combinacio, avans de augmentar la actual i seguir, hi ha que
            // donar les combinacions que han quedat pendents de altres clients.

            // comprovar si queden o no combinacions pendents
            if (hash.pendingCombinationCount() > 0)
            {
                client.bruteForceChunk = hash.getPendingCombination();
                Console.WriteLine("Restaurant combinacio anterior " + Program.combinacioAString(client.bruteForceChunk.characters));
            }
            else // si no queden enviar la combinacio actual i incrementar
            {
                newChunkCharactersIncrement(ref hash, client);
            }

            // modificarlo per a indicar que combincio esta processant per si es desconecta
            client.processing = true;
            client.processingUUID = hash.uuid;
            return client.bruteForceChunk;
        }

        private static void newChunkCharactersIncrement(ref Hash hash, NetworkClient client)
        {
            Console.WriteLine("Combinacio actual " + Program.combinacioAString(hash.caracters));

            int dynamicCharCount = (int)(client.hashRatePerMs * (float)hash.msProcessingCount);
            if (client.hashRatePerMs <= -1) dynamicCharCount = hash.initialTestCount;

            BruteForceChunk bruteForceChunk = new BruteForceChunk(dynamicCharCount, new int[hash.caracters.Length], dynamicCharCount);
            client.bruteForceChunk = bruteForceChunk;
            Array.Copy(hash.caracters, bruteForceChunk.characters, hash.caracters.Length);
            Console.WriteLine("Augmentar combinacio abans de continuar... " + client.bruteForceChunk.charactersCount);
            // incrementar la combinacio per al seguent
            Program.incrementarBlocCombinacio(ref hash, client.bruteForceChunk.charactersCount);
        }

        private static int novaPartDiccionari(ref Hash hash, NetEventIO eventIO, NetworkClient client)
        {
            int dynamicCharCount = (int)(client.hashRatePerMs * (float)hash.msProcessingCount);
            if (client.hashRatePerMs <= -1) dynamicCharCount = hash.initialTestCount;


            client.processing = true;
            client.processingUUID = hash.uuid;

            if (hash.pendingLinesCount() > 0)
            {
                client.dictionaryProcessingChunk = hash.getPendingLine();
            }
            else
            {
                client.dictionaryProcessingChunk = new DictionaryProcessingChunk(hash.numLinea, (hash.numLinea + dynamicCharCount), dynamicCharCount);
                hash.numLinea += (long)dynamicCharCount;

            }

            client.dictionaryReader = new StreamReader(
                            "./dictionary/" + hash.diccionari.uuid + "/" + hash.diccionari.uuid);

            for (int i = 0; i < client.dictionaryProcessingChunk.numLinea; i++)
            {
                _ = client.dictionaryReader.ReadLine();
            }

            newChunkDictionarySync(eventIO, client);

            Console.WriteLine("Enviant " + client.dictionaryProcessingChunk.linesCount + " combinacions. Enviant...");

            return dynamicCharCount;
        }

        private static void newChunkDictionarySync(NetEventIO eventIO, NetworkClient client)
        {
            string aux;
            eventIO.on("dictionaryChuck", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback sendChunk) =>
            {
                try
                {
                    long blockSize = Int64.Parse(e.message.message);
                    blockSize = Math.Min(blockSize, (client.dictionaryProcessingChunk.endNumLine - client.dictionaryProcessingChunk.numLinea));

                    if (client.dictionaryReader == null)
                    {
                        return;
                    }

                    string[] combinacionsAEnviar = new string[blockSize];
                    for (int i = 0; i < blockSize; i++)
                    {
                        aux = client.dictionaryReader.ReadLine();
                        combinacionsAEnviar[i] = aux;
                    }
                    client.dictionaryProcessingChunk.numLinea += blockSize;
                    sendChunk(JsonConvert.SerializeObject(combinacionsAEnviar));

                    if (client.dictionaryProcessingChunk.numLinea >= client.dictionaryProcessingChunk.endNumLine)
                    {
                        client.dictionaryReader.Dispose();
                        client.dictionaryReader = null;
                        eventIO.removeEvent("dictionaryChuck");
                    }
                }
                catch (Exception) { }
            });
        }

        /// <summary>
        /// Utilitzat per els clients per a informar del resultat que han tenit processant una part. El resulta pot ser:
        /// true: acompanyat de un text que es la contrasenya resolta
        /// false: no se ha trobat la contrasenya en la combinacio donada
        /// </summary>
        /// <param name="missatge">Missatge enviat per el client. P.Ex. "informarPart true 1234" si se ha trobat la contrasenya 1234, 
        /// o "informarPart false" si no se ha trobat.</param>
        /// <param name="client">Objecte que representa el client per a guardar que ja no esta processant.</param>
        private static void informarPart(string command, string missatge, ref NetworkClient client)
        {

            client.lastReportedPart = DateTime.Now;
            computeHashProcessingBlockSize(ref client);

            // marcar el client com a no processant actualment, ja que ha indicat que ha acabat la part
            client.processing = false;
            client.processingUUID = null;
            //client.characters = null;
            client.bruteForceChunk = null;
            client.dictionaryProcessingChunk = null;

            // llegir si el resultat el positiu
            if (command == "true")
            {
                Program.passwordFound(missatge);
            }

        }

        private static void computeHashProcessingBlockSize(ref NetworkClient client)
        {
            TimeSpan delta = client.lastReportedPart - client.lastNewPartRequested;

            Console.WriteLine("Processed in " + delta.TotalMilliseconds + "ms.");

            if (client.bruteForceChunk != null)
                client.hashRatePerMs = (client.bruteForceChunk.totalCharactersCount / delta.TotalMilliseconds);
            else if (client.dictionaryProcessingChunk != null)
                client.hashRatePerMs =
                    (client.dictionaryProcessingChunk.linesCount / delta.TotalMilliseconds);

            Console.WriteLine("Hash rate is " + client.hashRatePerMs + ".");

        }

    } //class Controller
} //namespace
