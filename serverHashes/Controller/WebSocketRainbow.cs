﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace serverHashes
{
    public static class WebSocketRainbow
    {

        public static string processRainbow(string cmd, string message)
        {
            switch (cmd)
            {
                case "list":
                    {
                        return listRainbows(message);
                    }
                case "new":
                    {
                        if (addRainbow(message))
                        {
                            return "OK";
                        }
                        else
                        {
                            return "ERR";
                        }
                    }
                case "remove":
                    {
                        if (removeRainbow(message))
                        {
                            return "OK";
                        }
                        else
                        {
                            return "ERR";
                        }
                    }
                case "toggle":
                    {
                        if (toggleRainbow(message))
                        {
                            return "OK";
                        }
                        else
                        {
                            return "ERR";
                        }
                    }
                case "search":
                    {
                        return findPwdInRainbow(message);
                    }
                case "edit":
                    {
                        if (editRainbow(message))
                        {
                            return "OK";
                        }
                        else
                        {
                            return "ERR";
                        }
                    }
            }
            return "ERR";
        }

        private static string listRainbows(string msg)
        {
            List<Rainbow> list;
            int totalLength;
            if (msg.Equals(String.Empty))
                list = Program.storage.getAllRainbows(out totalLength);
            else
            {
                WebSocketQuery query;
                try
                {
                    query = (WebSocketQuery)JsonConvert.DeserializeObject(msg, typeof(WebSocketQuery));
                }
                catch (Exception)
                {
                    return "ERR";
                }
                if (query == null) return "ERR";
                list = Program.storage.getAllRainbows(out totalLength, query.offset, query.limit);
            }
            return JsonConvert.SerializeObject(new WebSocketQueryResponse<Rainbow>(totalLength, list));
        }

        public static bool addRainbow(string data)
        {
            try
            {
                Rainbow r = (Rainbow)JsonConvert.DeserializeObject(data, typeof(Rainbow));
                if (r == null || !r.isValid()) return false;

                r.uuid = Guid.NewGuid().ToString();
                r.hash.uuid = Guid.NewGuid().ToString();
                r.enabled = true;
                r.hash.surrender = false;
                r.hash.estaResolt = false;
                r.hash.solucio = null;
                r.hash.dateCreated = DateTime.UtcNow;
                r.hash.dateFinished = DateTime.MinValue;
                r.hash.numLinea = 0;
                r.hash.combinacionsPendents = new List<BruteForceChunk>();
                r.hash.lineesPendents = new List<DictionaryProcessingChunk>();
                r.hash.tipusBusqueda = TipusBusqueda.ForçaBruta;

                return Program.storage.insertRainbow(r);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        private static bool editRainbow(string msg)
        {
            Dictionary<string, dynamic> editionData;
            try
            {
                editionData = (Dictionary<string, dynamic>)JsonConvert.DeserializeObject(msg, typeof(Dictionary<string, dynamic>));
            }
            catch (Exception)
            {
                return false;
            }

            dynamic uuid;
            if (editionData == null || !editionData.TryGetValue("uuid", out uuid) || !(uuid is string) || !Rainbow.isValid(editionData))
            {
                return false;
            }

            // search for the rainbow in the database
            Rainbow r = Program.storage.findRainbow(uuid);

            if (r != null && !r.hash.estaResolt && r.fromDictionary(editionData))
            {
                // APPLY CHANGES TO THE DATABASE
                Program.storage.updateRainbow(r);
                return true;
            }
            else
            {
                return false;
            }
        }

        private static string findPwdInRainbow(string message)
        {
            RainbowSearch rs;
            try
            {
                rs = (RainbowSearch)JsonConvert.DeserializeObject(message, typeof(RainbowSearch));
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(new RainbowSearchResult(RainbowSearchResult.RainbowSearchResultCode.err));
            }
            if (rs == null) return JsonConvert.SerializeObject(new RainbowSearchResult(RainbowSearchResult.RainbowSearchResultCode.err));

            // get the rainbow obect from the database
            Rainbow r = Program.storage.findRainbow(rs.uuid);
            if (r == null || !Hash.validateHashLength(r.hash.tipus, rs.hash))
                return JsonConvert.SerializeObject(new RainbowSearchResult(RainbowSearchResult.RainbowSearchResultCode.err));

            string clearTextPWD = Program.storage.findPwdInRainbow(rs.hash, rs.uuid);
            if (clearTextPWD == null)
                return JsonConvert.SerializeObject(new RainbowSearchResult());
            else
                return JsonConvert.SerializeObject(new RainbowSearchResult(clearTextPWD));
        }

        private static bool toggleRainbow(string data)
        {
            Rainbow r = Program.storage.findRainbow(data);
            if (r == null) return false;

            r.enabled = !r.enabled;

            return Program.storage.updateRainbow(r);
        }

        private static bool removeRainbow(string data)
        {
            ICollection<NetworkClient> clients = Program.llistaClients.Values;
            Rainbow r = Program.storage.findRainbow(data);
            if (r == null || r.enabled) return false;

            List<NetworkClient> clientsProcessingRainbow = new List<NetworkClient>();

            foreach (var client in clients)
            {
                if (client != null && client.isProcessingRainbow && client.processingUUID.Equals(data))
                {
                    clientsProcessingRainbow.Add(client);
                }
            }

            if (clientsProcessingRainbow.Count > 0) return false;

            return Program.storage.removeRainbow(data);
        }

    } //class WebSocketRainbow
} //namespace
