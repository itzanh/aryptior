﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading;

namespace serverHashes
{
    public static class WebSocketHashes
    {
        public static string processHashCommand(string cmd, string msg)
        {
            switch (cmd)
            {
                case "insert":
                    {
                        if (insertNewHash(msg))
                            return "OK";
                        else
                            return "ERR";
                    }
                case "get":
                    {
                        return listHashes(msg);
                    }
                case "surrender":
                    {
                        return surrender(msg);
                    }
                case "finished":
                    {
                        return listFinished(msg);
                    }
                case "edit":
                    {
                        if (editHash(msg))
                            return "OK";
                        else
                            return "ERR";
                    }
                case "remove":
                    {
                        return removeHash(msg);
                    }
                case "export":
                    {
                        return exportHash(msg);
                    }
            }
            return "ERR";
        }

        private static bool insertNewHash(string element)
        {
            if (element.Length == 0)
            {
                return false;
            }
            if (element[0] == '{')
            {
                try
                {
                    Hash newHash = JsonConvert.DeserializeObject<Hash>(element);
                    if (!validateAndPrepareHash(ref newHash))
                        return false;
                    return Program.storage.insertHash(newHash);
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else if (element[0] == '[')
            {
                try
                {
                    Hash[] newHash = JsonConvert.DeserializeObject<Hash[]>(element);
                    for (int i = 0; i < newHash.Length; i++)
                    {
                        if (!validateAndPrepareHash(ref newHash[i], false))
                            return false;
                    }
                    return Program.storage.insertHash(newHash);
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
                return false;
        }

        private static bool validateAndPrepareHash(ref Hash newHash, bool resetPendingCombinations = true)
        {
            if (newHash == null || !newHash.isValid()) return false;
            // recuperar el diccionari si es necessari
            if (newHash.tipusBusqueda == TipusBusqueda.Diccionari)
            {
                if (newHash.diccionari == null || newHash.diccionari.uuid == null || !Guid.TryParse(newHash.diccionari.uuid, out _))
                {
                    return false;
                }
                Dictionary d = Program.storage.getDictionary(newHash.diccionari.uuid);
                if (d == null)
                {
                    return false;
                }
            }
            else if (newHash.tipusBusqueda == TipusBusqueda.ForçaBruta)
            {
                newHash.diccionari = null;
            }

            newHash.uuid = Guid.NewGuid().ToString();
            newHash.surrender = false;
            newHash.estaResolt = false;
            newHash.solucio = null;
            newHash.dateCreated = DateTime.UtcNow;
            newHash.dateFinished = DateTime.MinValue;
            newHash.numLinea = 0;
            if (resetPendingCombinations)
            {
                newHash.combinacionsPendents = new List<BruteForceChunk>();
                newHash.lineesPendents = new List<DictionaryProcessingChunk>();
            }

            return true;
        }

        public static string listHashes(string msg)
        {
            List<Hash> list;
            int totalLength;
            if (msg.Equals(String.Empty))
            {
                list = Program.storage.getAllQueuedHashes(out totalLength);
            }
            else
            {
                WebSocketQuery query;
                try
                {
                    query = (WebSocketQuery)JsonConvert.DeserializeObject(msg, typeof(WebSocketQuery));
                }
                catch (Exception)
                {
                    return "ERR";
                }
                if (query == null) return "ERR";
                list = Program.storage.getAllQueuedHashes(out totalLength, query.offset, query.limit);
            }
            return JsonConvert.SerializeObject(new WebSocketQueryResponse<Hash>(totalLength, list));
        }

        public static string surrender(string msg)
        {
            Hash h = Program.storage.getHash(msg);
            if (h == null || h.estaResolt) return "ERR";

            h.surrender = !h.surrender;
            Program.storage.updateSurrenderHash(h);

            if (!h.surrender)
            {
                int pos = Program.storage.addToQueue(h);
                Program.websocketPubSub.onPush("hash", SubscriptionChangeType.insert, pos, JsonConvert.SerializeObject(h));
            }

            if (Program.hashActual == null || Program.hashActual.uuid.Equals(h.uuid))
            {
                Hash newHash;
                Program.estaIdle = Program.storage.getNextHash(out newHash);
                Program.hashActual = newHash;
            }

            if (!h.surrender)
            {
                Program.websocketPubSub.onPush("hash", SubscriptionChangeType.delete, -1, JsonConvert.SerializeObject(h));
                Program.clientPubSub.onPush("hash", SubscriptionChangeType.insert, -1, JsonConvert.SerializeObject(h));
            }
            else
            {
                Program.websocketPubSub.onPush("hash", SubscriptionChangeType.insert, -1, JsonConvert.SerializeObject(h));
            }

            Program.storage.onLog(LogType.SurrenderHash, h);

            return "OK";
        }

        /// <summary>
        /// Este metode permet editar un objecte de tipus hash per altre utilitzant el atribut del UUID.
        /// No permet modificar el hash, salt, iteracions, o feches de creacio o finalitzacio, ni el estat de finalitzacio.
        /// Te que validar el hash abans de actualitzarlo.
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static bool editHash(string msg)
        {
            Dictionary<string, dynamic> editionData;
            try
            {
                editionData = (Dictionary<string, dynamic>)JsonConvert.DeserializeObject(msg, typeof(Dictionary<string, dynamic>));
            }
            catch (Exception)
            {
                return false;
            }

            dynamic uuid = null;
            if (editionData == null || !editionData.TryGetValue("uuid", out uuid) || uuid == null || !(uuid is string) || !Hash.isValid(editionData))
            {
                return false;
            }

            // search for the hash by uuid
            Hash h = Program.storage.getHash(uuid);

            // return the result
            if (h != null && !h.estaResolt && h.fromDictionary(editionData))
            {
                // APPLY CHANGES TO THE DATABASE
                Program.storage.updateHash(h, true);
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string listFinished(string msg)
        {
            List<Hash> list;
            int totalLength;
            if (msg.Equals(String.Empty))
            {
                list = Program.storage.getAllFinishedHashes(out totalLength);
            }
            else
            {
                WebSocketQuery query;
                try
                {
                    query = (WebSocketQuery)JsonConvert.DeserializeObject(msg, typeof(WebSocketQuery));
                }
                catch (Exception)
                {
                    return "ERR";
                }
                if (query == null) return "ERR";
                list = Program.storage.getAllFinishedHashes(out totalLength, query.offset, query.limit);
            }
            if (list == null) list = new List<Hash>();

            return JsonConvert.SerializeObject(new WebSocketQueryResponse<Hash>(totalLength, list));
        }

        public static string removeHash(string msg)
        {
            Hash h = Program.storage.getHash(msg);
            // check that the hash exists (is not null when attempting to search for it, because, if not found by id, null will be returnred)
            // and checks that the hash is either finished or the user has surrendered
            if (h == null || (!h.surrender && !h.estaResolt)) return "ERR";

            if (Program.storage.removeHash(h.uuid))
            {
                // only the websocket clients are notified of a hash deletion. for this reason, this line is not on the Storage class
                Program.websocketPubSub.onPush("hash", SubscriptionChangeType.delete, -1, JsonConvert.SerializeObject(h));
                return "OK";
            }
            else
            {
                return "ERR";
            }
        }

        private static string exportHash(string msg)
        {
            if (msg.Length == 0)
                return "ERR";
            if (msg.Equals("queue") || msg.Equals("finished"))
            {
                List<Hash> list;
                if (msg.Equals("queue"))
                {
                    list = Program.storage.getAllQueuedHashes(out _);
                }
                else if (msg.Equals("finished"))
                {
                    list = Program.storage.getAllFinishedHashes(out _);
                }
                else
                {
                    return "ERR";
                }
                List<ExportHash> exportHashes = new List<ExportHash>();
                foreach (var item in list)
                {
                    exportHashes.Add(new ExportHash(item));
                }
                return JsonConvert.SerializeObject(exportHashes);
            }
            else if (msg[0] == '[')
            {
                string[] ids;
                try
                {
                    ids = (string[])JsonConvert.DeserializeObject(msg, typeof(string[]));
                }
                catch (Exception)
                {
                    return "ERR";
                }

                List<Hash> list = new List<Hash>();
                foreach (var id in ids)
                {
                    Hash h = Program.storage.getHash(id);
                    if (h == null)
                        continue;
                    if (!h.estaResolt && !h.surrender)
                        h = Program.storage.getQueuedHash(msg);
                    if (h == null)
                        continue;
                    list.Add(h);
                }

                List<ExportHash> exportHashes = new List<ExportHash>();
                foreach (var item in list)
                {
                    exportHashes.Add(new ExportHash(item));
                }
                return JsonConvert.SerializeObject(exportHashes);
            }
            else if (Guid.TryParse(msg, out _))
            {
                Hash h = Program.storage.getQueuedHash(msg);
                if (h == null)
                    h = Program.storage.getHash(msg);
                if (h == null)
                    return "ERR";
                return JsonConvert.SerializeObject(new ExportHash(h));
            }
            else
            {
                return "ERR";
            }
        }

        public static void runClientPerformanceTest(string msg, NetEventIO.Callback callback)
        {
            new Thread(new ThreadStart(() =>
            {
                testClientsPerformance(msg, callback);
            })).Start();
        }

        private static void testClientsPerformance(string msg, NetEventIO.Callback callback)
        {
            try
            {
                BruteForceTest bruteForceTest = (BruteForceTest)JsonConvert.DeserializeObject(msg, typeof(BruteForceTest));
                if (bruteForceTest == null || !bruteForceTest.isValid())
                {
                    callback("ERR");
                    return;
                }

                if (Program.llistaClients.Count == 0)
                {
                    callback(JsonConvert.SerializeObject(new BruteForceTestResults()));
                    return;
                }

                ManualResetEvent[] manualEvents = new ManualResetEvent[Program.llistaClients.Count];
                for (int i = 0; i < manualEvents.Length; i++)
                {
                    manualEvents[i] = new ManualResetEvent(false);
                }

                BruteForceTestResults testResults = new BruteForceTestResults();
                int j = 0;
                foreach (NetworkClient client in Program.llistaClients.Values)
                {
                    int k = (int)j;
                    client.connection.emit("test", "", msg, (NetEventIO _, string response) =>
                    {
                        if (response == null || response.Length == 0)
                        {
                            manualEvents[k].Set();
                            return;
                        }

                        if (response.Equals("ERR"))
                        {
                            testResults.addResult(new BruteForceTestClientResults(client.connection.id));
                        }
                        else
                        {
                            BruteForceTestResponse testResponse;
                            try
                            {
                                testResponse = (BruteForceTestResponse)JsonConvert.DeserializeObject(response, typeof(BruteForceTestResponse));
                                if (testResponse.isValid())
                                {
                                    testResults.incrementHashCount(testResponse.hashCount);
                                    testResults.addResult(
                                        new BruteForceTestClientResults(
                                            client.connection.id, client.connection.client.addr.Split(':')[0], testResponse.hashCount, testResponse.threads));
                                }
                                else
                                {
                                    testResults.addResult(new BruteForceTestClientResults(client.connection.id));
                                }
                            }
                            catch (Exception)
                            {
                                testResults.addResult(new BruteForceTestClientResults(client.connection.id));
                            }
                        }

                        manualEvents[k].Set();
                    });
                    j++;
                }

                WaitHandle.WaitAll(manualEvents, new TimeSpan(0, 0, bruteForceTest.timeout + 15), false);

                // search for clients that are not on the list, and therefore have timed out
                foreach (NetworkClient client in Program.llistaClients.Values)
                {
                    bool found = false;
                    foreach (var item in testResults.results)
                    {
                        if (item.uuid.Equals(client.connection.id))
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        testResults.addResult(new BruteForceTestClientResults(client.connection.id, client.connection.client.addr.Split(':')[0]));
                    }
                }

                callback(JsonConvert.SerializeObject(testResults));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                callback("ERR");
            }
        }
    }
}
