﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serverHashes
{
    public static class WebSocketLogs
    {
        public static string processCommand(string cmd, string msg)
        {
            switch (cmd)
            {
                case "list":
                    {
                        return getLogs(msg);
                    }
                case "clear":
                    {
                        return clearLogs();
                    }
            }

            return "ERR";
        }

        private static string getLogs(string msg)
        {
            List<Log> list;
            if (msg.Equals(String.Empty))
            {
                list = Program.storage.getLogs(DateTime.MinValue, 0, 0);
            }
            else
            {
                WebSocketLogQuery query;
                try
                {
                    query = (WebSocketLogQuery)JsonConvert.DeserializeObject(msg, typeof(WebSocketLogQuery));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return "ERR";
                }
                if (query == null) return "ERR";
                list = Program.storage.getLogs(query.start, query.offset, query.limit);
            }

            return JsonConvert.SerializeObject(list);

        }

        private static string clearLogs()
        {
            if (Program.storage.clearLogs())
            {
                Program.websocketPubSub.onPush("logs", SubscriptionChangeType.delete, -1, String.Empty);
                return "OK";
            }
            else
            {
                return "ERR";
            }
        }
    }
}
