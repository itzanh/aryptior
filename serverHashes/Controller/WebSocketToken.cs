﻿using Newtonsoft.Json;
using System;

namespace serverHashes
{
    public static class WebSocketToken
    {
        public static string processarToken(string cmd, string msg, string addr)
        {
            switch (cmd)
            {
                case "get":
                    {
                        return getNewToken(addr);
                    }
                case "revoke":
                    {
                        if (revokeToken(msg))
                            return "OK";
                        else
                            return "ERR";
                    }
                case "revokeall":
                    {
                        if (revokeAllTokens())
                            return "OK";
                        else
                            return "ERR";
                    }
                case "list":
                    {
                        return listAllTokens();
                    }
            }
            return "";
        }

        private static string listAllTokens()
        {
            return JsonConvert.SerializeObject(Program.storage.getAllTokens());
        }

        private static bool revokeToken(string msg)
        {
            return Program.storage.revokeToken(msg);
        }

        private static string getNewToken(string addr)
        {
            string token = Program.storage.createToken(addr.Split(':')[0]);
            if (token != null)
            {
                return "OK " + token;
            }
            else
            {
                return "ERR";
            }
        }

        private static bool revokeAllTokens()
        {
            return Program.storage.revokeAllTokens();
        }
    }
}
