﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace serverHashes
{
    public class AryptiorDB<T>
    {
        private const int INITIAL_DICTSIZE = 2048;
        public delegate bool ObjectQuery(T o);

        private readonly string fileName;
        private readonly int regenerationCount;
        private int regenerationOperationsPerformed;
        private readonly object MUTEX;

        public AryptiorDB(string fileName, int regenerationCount)
        {
            this.fileName = fileName;
            this.regenerationCount = regenerationCount;
            this.regenerationOperationsPerformed = 0;
            this.MUTEX = new object();
        }



        /***************/
        /* ARYPTIOR-DB */
        /***************/

        public bool initializeFile(bool dropFile = false)
        {
            lock (this.MUTEX)
                try
                {
                    if (dropFile)
                    {
                        File.Delete(this.fileName);
                    }
                    else if (File.Exists(this.fileName))
                    {
                        return regenerateData();
                    }

                    Stream stream = new FileStream(this.fileName, FileMode.Create, FileAccess.Write);

                    stream.Write(BitConverter.GetBytes(INITIAL_DICTSIZE), 0, 4);
                    BinaryFormatter formatter = new BinaryFormatter();

                    byte[] dict = serializeObjectDictionary(new Dictionary<string, int>(), INITIAL_DICTSIZE);

                    stream.Write(dict, 0, INITIAL_DICTSIZE);
                    stream.Flush();
                    stream.Close();
                    stream.Dispose();

                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return false;
                }
        }

        private bool regenerateData()
        {
            try
            {
                Stream stream = new FileStream(this.fileName, FileMode.Open, FileAccess.ReadWrite);
                BinaryFormatter formatter = new BinaryFormatter();

                byte[] readSize = new byte[4];
                stream.Read(readSize, 0, 4);
                int size = BitConverter.ToInt32(readSize, 0);

                byte[] dict = new byte[size];
                stream.Read(dict, 0, size);
                Dictionary<string, int> objDict = unserializeObjectDictionary(dict);

                Dictionary<string, T> results = new Dictionary<string, T>();

                foreach (string id in objDict.Keys)
                {
                    stream.Seek(objDict[id] + size + 4, SeekOrigin.Begin);
                    results.Add(id, (T)formatter.Deserialize(stream));
                }

                stream.Flush();
                stream.Close();
                stream.Dispose();

                stream = new FileStream(this.fileName, FileMode.Truncate, FileAccess.Write);
                stream.Position = 0;
                stream.Write(readSize, 0, 4);
                stream.Position = 4 + size;
                Dictionary<string, int> insertedObjects = new Dictionary<string, int>();
                foreach (string id in results.Keys)
                {
                    insertedObjects.Add(id, ((int)stream.Position) - size - 4);
                    formatter.Serialize(stream, results[id]);
                }
                stream.Position = 4;
                dict = serializeObjectDictionary(insertedObjects, size);
                stream.Write(dict, 0, dict.Length);

                stream.Flush();
                stream.Close();
                stream.Dispose();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        private static byte[] serializeObjectDictionary(Dictionary<string, int> objects, int size)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream dictms = new MemoryStream();
            formatter.Serialize(dictms, objects);
            byte[] dict;
            if (dictms.Length > size)
            {
                dict = new byte[(size * 2)];
            }
            else if (size > INITIAL_DICTSIZE && dictms.Length < (size / 2))
            {
                dict = new byte[(size / 2)];
            }
            else
            {
                dict = new byte[size];
            }
            Array.Copy(dictms.ToArray(), 0, dict, 0, dictms.Length);

            return dict;
        }

        private static Dictionary<string, int> unserializeObjectDictionary(byte[] dict)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream dictms = new MemoryStream(dict);
            Dictionary<string, int> objects = (Dictionary<string, int>)formatter.Deserialize(dictms);

            return objects;
        }

        public bool insertObject(string id, T objectToInsert)
        {
            lock (this.MUTEX)
                try
                {
                    Stream stream = new FileStream(this.fileName, FileMode.Open, FileAccess.ReadWrite);
                    BinaryFormatter formatter = new BinaryFormatter();

                    byte[] readSize = new byte[4];
                    stream.Read(readSize, 0, 4);
                    int size = BitConverter.ToInt32(readSize, 0);

                    byte[] dict = new byte[size];
                    stream.Read(dict, 0, size);
                    Dictionary<string, int> objects = unserializeObjectDictionary(dict);
                    objects.Add(id, ((int)stream.Length) - dict.Length - 4);
                    dict = serializeObjectDictionary(objects, size);

                    if (dict.Length > size || dict.Length < size)
                    {
                        stream.Position = 4 + size;
                        MemoryStream serializedObjects = new MemoryStream();
                        stream.CopyTo(serializedObjects);
                        stream.Flush();
                        stream.Close();
                        stream.Dispose();

                        stream = new FileStream(this.fileName, FileMode.Truncate, FileAccess.Write);
                        stream.Position = 0;
                        stream.Write(BitConverter.GetBytes(dict.Length), 0, 4);
                        stream.Write(dict, 0, dict.Length);
                        stream.Write(serializedObjects.ToArray(), 0, (int)serializedObjects.Length);
                    }
                    else
                    {
                        stream.Position = 4;
                        stream.Write(dict, 0, dict.Length);
                    }

                    stream.Flush();
                    stream.Close();
                    stream.Dispose();

                    stream = new FileStream(this.fileName, FileMode.Append, FileAccess.Write);

                    formatter.Serialize(stream, objectToInsert);
                    stream.Flush();
                    stream.Close();
                    stream.Dispose();

                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return false;
                }
        }

        public T getObjectById(string id)
        {
            lock (this.MUTEX)
                try
                {
                    Stream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    BinaryFormatter formatter = new BinaryFormatter();

                    byte[] readSize = new byte[4];
                    stream.Read(readSize, 0, 4);
                    int size = BitConverter.ToInt32(readSize, 0);

                    byte[] dict = new byte[size];
                    stream.Read(dict, 0, size);
                    Dictionary<string, int> objects = unserializeObjectDictionary(dict);
                    int objectPosition;
                    if (!objects.TryGetValue(id, out objectPosition))
                    {
                        stream.Flush();
                        stream.Close();
                        stream.Dispose();
                        return default;
                    }

                    stream.Seek(objectPosition, SeekOrigin.Current);
                    T result = (T)formatter.Deserialize(stream);

                    stream.Flush();
                    stream.Close();
                    stream.Dispose();
                    return result;
                }
                catch (Exception)
                {
                    return default;
                }
        }

        public List<T> getObjects(ObjectQuery query)
        {
            lock (this.MUTEX)
                try
                {
                    Stream stream = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);
                    BinaryFormatter formatter = new BinaryFormatter();

                    byte[] readSize = new byte[4];
                    stream.Read(readSize, 0, 4);
                    int size = BitConverter.ToInt32(readSize, 0);

                    byte[] dict = new byte[size];
                    stream.Read(dict, 0, size);
                    Dictionary<string, int> objects = unserializeObjectDictionary(dict);

                    List<int> positions = objects.Values.ToList();
                    positions.Sort();
                    List<T> results = new List<T>();

                    foreach (int position in positions)
                    {
                        stream.Seek(position + size + 4, SeekOrigin.Begin);
                        T obj = (T)formatter.Deserialize(stream);
                        if (query(obj))
                            results.Add(obj);
                    }

                    stream.Flush();
                    stream.Close();
                    stream.Dispose();

                    return results;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return null;
                }
        }

        public List<T> getObjects()
        {
            lock (this.MUTEX)
                try
                {
                    Stream stream = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);
                    BinaryFormatter formatter = new BinaryFormatter();

                    byte[] readSize = new byte[4];
                    stream.Read(readSize, 0, 4);
                    int size = BitConverter.ToInt32(readSize, 0);

                    byte[] dict = new byte[size];
                    stream.Read(dict, 0, size);
                    Dictionary<string, int> objects = unserializeObjectDictionary(dict);

                    List<int> positions = objects.Values.ToList();
                    positions.Sort();
                    List<T> results = new List<T>();

                    foreach (int position in positions)
                    {
                        stream.Seek(position + size + 4, SeekOrigin.Begin);
                        results.Add((T)formatter.Deserialize(stream));
                    }

                    stream.Flush();
                    stream.Close();
                    stream.Dispose();

                    return results;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return null;
                }
        }

        public bool deleteObject(string id)
        {
            lock (this.MUTEX)
                try
                {
                    Stream stream = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);
                    BinaryFormatter formatter = new BinaryFormatter();

                    byte[] readSize = new byte[4];
                    stream.Read(readSize, 0, 4);
                    int size = BitConverter.ToInt32(readSize, 0);

                    byte[] dict = new byte[size];
                    stream.Read(dict, 0, size);
                    Dictionary<string, int> objects = unserializeObjectDictionary(dict);
                    bool result = objects.Remove(id);
                    dict = serializeObjectDictionary(objects, size);

                    if (dict.Length > size || dict.Length < size)
                    {
                        stream.Position = 4 + size;
                        MemoryStream serializedObjects = new MemoryStream();
                        stream.CopyTo(serializedObjects);
                        stream.Flush();
                        stream.Close();
                        stream.Dispose();

                        stream = new FileStream(this.fileName, FileMode.Truncate, FileAccess.Write);
                        stream.Position = 0;
                        stream.Write(BitConverter.GetBytes(dict.Length), 0, 4);
                        stream.Write(dict, 0, dict.Length);
                        stream.Write(serializedObjects.ToArray(), 0, (int)serializedObjects.Length);
                    }
                    else
                    {
                        stream.Position = 4;
                        stream.Write(dict, 0, dict.Length);
                    }

                    stream.Flush();
                    stream.Close();
                    stream.Dispose();

                    regenerationOperationsPerformed++;
                    if (regenerationOperationsPerformed >= regenerationCount)
                    {
                        regenerationOperationsPerformed = 0;
                        regenerateData();
                    }

                    return result;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return default;
                }
        }

        public bool updateObject(string id, T updatedObject)
        {
            lock (this.MUTEX)
                try
                {
                    Stream stream = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);
                    BinaryFormatter formatter = new BinaryFormatter();

                    byte[] readSize = new byte[4];
                    stream.Read(readSize, 0, 4);
                    int size = BitConverter.ToInt32(readSize, 0);

                    byte[] dict = new byte[size];
                    stream.Read(dict, 0, size);
                    Dictionary<string, int> objects = unserializeObjectDictionary(dict);
                    int position;
                    if (!objects.TryGetValue(id, out position))
                    {
                        stream.Flush();
                        stream.Close();
                        stream.Dispose();
                        return false;
                    }

                    List<int> positions = objects.Values.ToList();
                    positions.Sort();

                    MemoryStream serializedObject = new MemoryStream();
                    formatter.Serialize(serializedObject, updatedObject);

                    if ((positions.IndexOf(position) + 1) < positions.Count)
                    {
                        int holeSizeInDisk = positions[(positions.IndexOf(position) + 1)] - position;

                        if (serializedObject.Length <= holeSizeInDisk)
                        {
                            stream.Seek(position + dict.Length + 4, SeekOrigin.Begin);
                            stream.Write(serializedObject.ToArray(), 0, (int)serializedObject.Length);
                        }
                        else
                        {
                            stream.Seek(0, SeekOrigin.End);
                            objects[id] = (int)stream.Position - size - 4;
                            stream.Write(serializedObject.ToArray(), 0, (int)serializedObject.Length);
                            stream.Seek(4, SeekOrigin.Begin);
                            dict = serializeObjectDictionary(objects, size);
                            stream.Write(dict, 0, dict.Length);

                            regenerationOperationsPerformed++;
                            if (regenerationOperationsPerformed >= regenerationCount)
                            {
                                regenerationOperationsPerformed = 0;
                                regenerateData();
                            }
                        }
                    }
                    else // the item is in the last position of the file, grow the file
                    {
                        stream.Seek(position + dict.Length + 4, SeekOrigin.Begin);
                        stream.Write(serializedObject.ToArray(), 0, (int)serializedObject.Length);
                    }

                    stream.Flush();
                    stream.Close();
                    stream.Dispose();

                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return false;
                }
        }
    }
}
