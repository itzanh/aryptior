﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace serverHashes
{
    public class MongoDBStorage
    {
        private readonly MongoDBSettings mongoDBSettings;
        private IMongoDatabase db;

        public MongoDBStorage(MongoDBSettings conf)
        {
            this.mongoDBSettings = conf;
        }

        public bool connect()
        {
            try
            {
                MongoClient client;
                if (this.mongoDBSettings.url.Equals(String.Empty))
                    client = new MongoClient(this.generateURL());
                else
                    client = new MongoClient(addAppNameToUri(this.mongoDBSettings.url));

                this.db = client.GetDatabase(this.mongoDBSettings.databaseName);
                initializeDatabase();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot connect to database server!!! Check your settings file. " + e.ToString());
                return false;
            }
        }

        private MongoClientSettings generateURL()
        {
            MongoClientSettings settings = new MongoClientSettings();
            MongoServerAddress address = new MongoServerAddress(this.mongoDBSettings.host, this.mongoDBSettings.port);
            settings.Server = address;

            if (!this.mongoDBSettings.mongoDbAuthMechanism.Equals("NONE"))
            {
                MongoInternalIdentity internalIdentity = new MongoInternalIdentity(this.mongoDBSettings.databaseName, this.mongoDBSettings.userName);
                PasswordEvidence passwordEvidence = new PasswordEvidence(this.mongoDBSettings.password);
                MongoCredential mongoCredential = new MongoCredential(this.mongoDBSettings.mongoDbAuthMechanism, internalIdentity, passwordEvidence);
                settings.Credential = mongoCredential;
            }
            settings.ApplicationName = "aryptiOR";

            return settings;
        }

        private static string addAppNameToUri(string url)
        {
            var uriBuilder = new UriBuilder(url);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["appName"] = "aryptiOR";
            uriBuilder.Query = query.ToString();
            return uriBuilder.ToString();
        }

        private void initializeDatabase()
        {
            if (this.db == null) return;
            BsonClassMap.RegisterClassMap<Settings>();
            BsonClassMap.RegisterClassMap<Hash>();
            BsonClassMap.RegisterClassMap<Dictionary>();
            BsonClassMap.RegisterClassMap<Rainbow>();
            BsonClassMap.RegisterClassMap<LogClient>();
            // create collections
            Console.WriteLine("Initializing database, please wait...");
            List<string> collection = db.ListCollectionNames().ToList<string>();
            if (collection.IndexOf("hash") == -1)
            {
                this.db.CreateCollection("hash");
                this.db.GetCollection<Hash>("hash").Indexes.CreateOneAsync(new CreateIndexModel<Hash>(
                    Builders<Hash>.IndexKeys.Combine(
                        Builders<Hash>.IndexKeys.Ascending(index => index.hash),
                        Builders<Hash>.IndexKeys.Ascending(index => index.iteracions),
                        Builders<Hash>.IndexKeys.Ascending(index => index.salt)
                    )
                , new CreateIndexOptions() { Unique = true }));
                this.db.GetCollection<Hash>("hash").Indexes.CreateOneAsync(new CreateIndexModel<Hash>(
                    Builders<Hash>.IndexKeys.Ascending(index => index.uuid)
                , new CreateIndexOptions() { Unique = true }));
                this.db.GetCollection<Hash>("hash").Indexes.CreateOneAsync(new CreateIndexModel<Hash>(
                    Builders<Hash>.IndexKeys.Combine(
                        Builders<Hash>.IndexKeys.Ascending(index => index.surrender),
                        Builders<Hash>.IndexKeys.Ascending(index => index.estaResolt)
                    )
                , new CreateIndexOptions() { Unique = false }));
                this.db.GetCollection<Hash>("hash").Indexes.CreateOneAsync(new CreateIndexModel<Hash>(
                    Builders<Hash>.IndexKeys.Ascending(index => index.solucio)
                , new CreateIndexOptions<Hash>()
                {
                    PartialFilterExpression = Builders<Hash>.Filter.Exists(item => item.solucio),
                    Unique = false
                }));
                this.db.GetCollection<Hash>("hash").Indexes.CreateOneAsync(new CreateIndexModel<Hash>(
                    Builders<Hash>.IndexKeys.Ascending(index => index.diccionari.uuid)
                , new CreateIndexOptions<Hash>()
                {
                    PartialFilterExpression = Builders<Hash>.Filter.Exists(item => item.diccionari.uuid),
                    Unique = false
                }));
            }
            if (collection.IndexOf("dictionary") == -1)
            {
                this.db.CreateCollection("dictionary");
                this.db.GetCollection<Dictionary>("dictionary").Indexes.CreateOneAsync(new CreateIndexModel<Dictionary>(
                    Builders<Dictionary>.IndexKeys.Ascending(index => index.fileName)
                , new CreateIndexOptions() { Unique = true }));
                this.db.GetCollection<Dictionary>("dictionary").Indexes.CreateOneAsync(new CreateIndexModel<Dictionary>(
                    Builders<Dictionary>.IndexKeys.Ascending(index => index.uuid)
                , new CreateIndexOptions() { Unique = true }));
            }
            if (collection.IndexOf("token") == -1)
            {
                this.db.CreateCollection("token");
                this.db.GetCollection<Token>("token").Indexes.CreateOneAsync(new CreateIndexModel<Token>(
                        Builders<Token>.IndexKeys.Ascending(index => index.token)
                , new CreateIndexOptions() { Unique = true }));
            }
            if (collection.IndexOf("rainbow") == -1)
            {
                this.db.CreateCollection("rainbow");
                this.db.GetCollection<Rainbow>("rainbow").Indexes.CreateOneAsync(new CreateIndexModel<Rainbow>(
                    Builders<Rainbow>.IndexKeys.Ascending(index => index.uuid)
                , new CreateIndexOptions() { Unique = true }));
                this.db.GetCollection<Rainbow>("rainbow").Indexes.CreateOneAsync(new CreateIndexModel<Rainbow>(
                    Builders<Rainbow>.IndexKeys.Ascending(index => index.name)
                , new CreateIndexOptions() { Unique = true }));
                this.db.GetCollection<Rainbow>("rainbow").Indexes.CreateOneAsync(new CreateIndexModel<Rainbow>(
                    Builders<Rainbow>.IndexKeys.Ascending(index => index.enabled)
                , new CreateIndexOptions() { Unique = false }));
                this.db.GetCollection<Rainbow>("rainbow").Indexes.CreateOneAsync(new CreateIndexModel<Rainbow>(
                    Builders<Rainbow>.IndexKeys.Ascending(index => index.hash.uuid)
                , new CreateIndexOptions() { Unique = true }));
            }
            if (collection.IndexOf("backup") == -1)
            {
                this.db.CreateCollection("backup");
                this.db.GetCollection<Backup>("backup").Indexes.CreateOneAsync(new CreateIndexModel<Backup>(
                    Builders<Backup>.IndexKeys.Ascending(index => index.uuid)
                , new CreateIndexOptions() { Unique = true }));
            }
            if (collection.IndexOf("certs") == -1)
            {
                this.db.CreateCollection("certs");
                this.db.GetCollection<CertificateInfo>("certs").Indexes.CreateOneAsync(new CreateIndexModel<CertificateInfo>(
                    Builders<CertificateInfo>.IndexKeys.Ascending(index => index.uuid)
                , new CreateIndexOptions() { Unique = true }));
            }
            if (collection.IndexOf("logs") == -1)
            {
                this.db.CreateCollection("logs");
                this.db.GetCollection<Log>("logs").Indexes.CreateOneAsync(new CreateIndexModel<Log>(
                    Builders<Log>.IndexKeys.Ascending(index => index.dateTime)
                , new CreateIndexOptions() { Unique = false, ExpireAfter = new TimeSpan(0, 0, Program.CONFIG.loggingSettings.logDuration) }));
            }
            else
            {
                this.updateTTLlogsCollection(Program.CONFIG.loggingSettings.logDuration);
            }
        }

        public void updateTTLlogsCollection(int logDuration)
        {
            IAsyncCursor<BsonDocument> indexes = this.db.GetCollection<Log>("logs").Indexes.List();
            indexes.MoveNext();
            if (indexes.Current.ElementAt(1).GetValue("expireAfterSeconds", -1) != logDuration)
            {
                this.db.GetCollection<Log>("logs").Indexes.DropOne(indexes.Current.ElementAt(1).GetValue("name", String.Empty).ToString());
                this.db.GetCollection<Log>("logs").Indexes.CreateOneAsync(new CreateIndexModel<Log>(
                    Builders<Log>.IndexKeys.Ascending(index => index.dateTime)
                , new CreateIndexOptions() { Unique = false, ExpireAfter = new TimeSpan(0, 0, Program.CONFIG.loggingSettings.logDuration) }));
            }
        }

        /********/
        /* HASH */
        /********/

        public bool insertHash(Hash h)
        {
            try
            {
                this.db.GetCollection<Hash>("hash").InsertOne(h);
                return true;
            }
            catch (Exception) { return false; }
        }

        public bool insertHash(Hash[] h)
        {
            try
            {
                this.db.GetCollection<Hash>("hash").InsertMany(h);
                return true;
            }
            catch (Exception) { return false; }
        }

        public bool updateHash(Hash nou)
        {
            try
            {
                return this.db.GetCollection<Hash>("hash").ReplaceOne(Builders<Hash>.Filter.Eq(item => item.uuid, nou.uuid), nou)
                    .MatchedCount > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Hash> findQueuedHashes()
        {
            try
            {
                return this.db.GetCollection<Hash>("hash").Find<Hash>(Builders<Hash>.Filter.And(
                    Builders<Hash>.Filter.Eq(item => item.estaResolt, false),
                    Builders<Hash>.Filter.Eq(item => item.surrender, false)
                    )).ToList();
            }
            catch (Exception) { return null; }
        }

        public List<Hash> findFinishedHashes(out int length, int offset = 0, int limit = 0)
        {
            try
            {
                IFindFluent<Hash, Hash> cursor = this.db.GetCollection<Hash>("hash").Find<Hash>(Builders<Hash>.Filter.Or(
                    Builders<Hash>.Filter.Eq(item => item.estaResolt, true),
                    Builders<Hash>.Filter.Eq(item => item.surrender, true)
                    )).Sort(Builders<Hash>.Sort.Descending("dateCreated"));
                length = (int)cursor.CountDocuments();
                if (limit > 0) cursor.Limit(limit);
                if (offset > 0) cursor.Skip(offset);
                return cursor.ToList();
            }
            catch (Exception)
            {
                length = -1;
                return null;
            }
        }

        public int countFinishedHashes()
        {
            try
            {
                return (int)this.db.GetCollection<Hash>("hash").CountDocuments(Builders<Hash>.Filter.Or(
                    Builders<Hash>.Filter.Eq(item => item.estaResolt, true),
                    Builders<Hash>.Filter.Eq(item => item.surrender, true)
                    ));
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public long countSolvedHashes()
        {
            try
            {
                return this.db.GetCollection<Hash>("hash").CountDocuments(Builders<Hash>.Filter.And(
                    Builders<Hash>.Filter.Eq(item => item.estaResolt, true),
                    Builders<Hash>.Filter.Not(
                        Builders<Hash>.Filter.Eq(item => item.solucio, null)
                        ),
                    Builders<Hash>.Filter.Eq(item => item.surrender, false)
                    ));
            }
            catch (Exception) { return 0; }
        }

        public Hash getHash(string uuid)
        {
            try
            {
                List<Hash> hashes = this.db.GetCollection<Hash>("hash").Find<Hash>(Builders<Hash>.Filter.Eq(item => item.uuid, uuid)).ToList();
                if (hashes.Count == 0)
                    return null;
                else
                    return hashes[0];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool removeHash(string uuid)
        {
            try
            {
                return this.db.GetCollection<Hash>("hash").DeleteOne(Builders<Hash>.Filter.Eq(item => item.uuid, uuid)).DeletedCount > 0;
            }
            catch (Exception) { return false; }
        }

        /**************/
        /* DICTIONARY */
        /**************/

        public List<Dictionary> findDictionaries()
        {
            try
            {
                return this.db.GetCollection<Dictionary>("dictionary").Find<Dictionary>(_ => true).ToList<Dictionary>();
            }
            catch (Exception) { return null; }
        }

        public List<Hash> findDictionaryInHashQueue(string uuid)
        {
            try
            {
                return this.db.GetCollection<Hash>("hash").Find<Hash>(Builders<Hash>.Filter.And(
                    Builders<Hash>.Filter.Eq(hash => hash.diccionari.uuid, uuid),
                    Builders<Hash>.Filter.Eq(hash => hash.estaResolt, false)
                    )).ToList();
            }
            catch (Exception) { return null; }
        }

        public bool insertDictionary(Dictionary d)
        {
            try
            {
                this.db.GetCollection<Dictionary>("dictionary").InsertOne(d);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        public bool updateDictionary(Dictionary nou, out List<Hash> hashes)
        {
            hashes = null;
            try
            {
                if ((this.db.GetCollection<Dictionary>("dictionary").ReplaceOne(
                    Builders<Dictionary>.Filter.Eq(item => item.fileName, nou.fileName), nou).ModifiedCount > 0)
                    && nou.isReady)
                {
                    this.db.GetCollection<Hash>("hash").UpdateMany(
                        Builders<Hash>.Filter.And(
                            Builders<Hash>.Filter.Eq(item => item.tipusBusqueda, TipusBusqueda.Diccionari),
                            Builders<Hash>.Filter.Not(
                                Builders<Hash>.Filter.Eq(item => item.diccionari, null)
                                ),
                            Builders<Hash>.Filter.Eq(item => item.diccionari.uuid, nou.uuid)
                            )
                    , Builders<Hash>.Update.Set("diccionari", nou));

                    hashes = this.db.GetCollection<Hash>("hash").Find(
                        Builders<Hash>.Filter.And(
                            Builders<Hash>.Filter.Eq(item => item.tipusBusqueda, TipusBusqueda.Diccionari),
                            Builders<Hash>.Filter.Not(
                                Builders<Hash>.Filter.Eq(item => item.diccionari, null)
                                ),
                            Builders<Hash>.Filter.Eq(item => item.diccionari.uuid, nou.uuid)
                            )
                        ).ToList();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception) { return false; }
        }

        public bool deleteDictionary(Dictionary d)
        {
            try
            {
                return this.db.GetCollection<Dictionary>("dictionary").DeleteOne(
                    Builders<Dictionary>.Filter.Eq(item => item.fileName, d.fileName)).DeletedCount > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /*********/
        /* TOKEN */
        /*********/

        public List<Token> findToken(string adreca, string token)
        {
            try
            {
                return this.db.GetCollection<Token>("token").Find<Token>(
                    Builders<Token>.Filter.And(
                        Builders<Token>.Filter.Eq(item => item.token, token),
                        Builders<Token>.Filter.Eq(item => item.addr, adreca)
                        )
                    ).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Token> findToken()
        {
            try
            {
                return this.db.GetCollection<Token>("token").Find(_ => true).Sort(Builders<Token>.Sort.Descending(t => t.lastUsed)).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool insertToken(Token t)
        {
            try
            {
                this.db.GetCollection<Token>("token").InsertOne(t);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool deleteToken(Token t)
        {
            try
            {
                this.db.GetCollection<Token>("token").DeleteOne(Builders<Token>.Filter.Eq(item => item.token, t.token));
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool deleteToken(string t)
        {
            try
            {
                return this.db.GetCollection<Token>("token").DeleteOne(Builders<Token>.Filter.Eq(item => item.token, t)).DeletedCount > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool deleteAllTokens()
        {
            try
            {
                return this.db.GetCollection<Token>("token").DeleteMany(Builders<Token>.Filter.Empty).DeletedCount > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool updateToken(Token t)
        {
            try
            {
                this.db.GetCollection<Token>("token").ReplaceOne(Builders<Token>.Filter.Eq(item => item.token, t.token), t);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public void clearExpiredTokens()
        {
            try
            {
                foreach (Token t in this.db.GetCollection<Token>("token").Find(Builders<Token>.Filter.Empty).ToListAsync().Result)
                {
                    if (t.isExpired())
                    {
                        this.deleteToken(t);
                    }
                }
            }
            catch (Exception) { }
        }

        /***********/
        /* RAINBOW */
        /***********/

        public bool insertRainbow(Rainbow r)
        {
            try
            {
                this.db.GetCollection<Rainbow>("rainbow").InsertOne(r);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Rainbow> getAllRainbows(out int length, int offset = 0, int limit = 0)
        {
            try
            {
                IFindFluent<Rainbow, Rainbow> query = this.db.GetCollection<Rainbow>("rainbow")
                    .Find(Builders<Rainbow>.Filter.Empty)
                    .Sort(Builders<Rainbow>.Sort.Descending("hash.dateCreated"));
                length = (int)query.CountDocuments();
                if (limit > 0) query.Limit(limit);
                if (offset > 0) query.Skip(offset);
                return (offset >= 0) ? query.ToList() : null;
            }
            catch (Exception)
            {
                length = -1;
                return null;
            }
        }

        public List<Rainbow> getEnabledRainbows()
        {
            try
            {
                return this.db.GetCollection<Rainbow>("rainbow").Find(
                    Builders<Rainbow>.Filter.And(
                    Builders<Rainbow>.Filter.Eq(item => item.enabled, true),
                    Builders<Rainbow>.Filter.Eq(item => item.hash.estaResolt, false)
                    )).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool updateRainbow(string uuid, Rainbow r)
        {
            try
            {
                return this.db.GetCollection<Rainbow>("rainbow").ReplaceOne(
                    Builders<Rainbow>.Filter.Eq(item => item.uuid, uuid), r).ModifiedCount > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Rainbow findRainbow(string uuid)
        {
            try
            {
                List<Rainbow> results = this.db.GetCollection<Rainbow>("rainbow").Find(
                    Builders<Rainbow>.Filter.Eq(item => item.uuid, uuid)).ToList();
                if (results.Count > 0)
                {
                    return results[0];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /*****************/
        /* COMPUTED HASH */
        /*****************/

        public bool deleteRainbow(Rainbow r)
        {
            try
            {
                return this.db.GetCollection<Rainbow>("rainbow").DeleteOne(Builders<Rainbow>.Filter.Eq(item => item.uuid, r.uuid)).DeletedCount > 0;
            }
            catch (Exception) { return false; }
        }

        /*************/
        /* DASHBOARD */
        /*************/

        public long getFinishedHashesCount()
        {
            try
            {
                return this.db.GetCollection<Hash>("hash").CountDocuments(Builders<Hash>.Filter.Eq(item => item.estaResolt, true));
            }
            catch (Exception) { return 0; }
        }

        public long getRainbowsCount()
        {
            try
            {
                return this.db.GetCollection<Rainbow>("rainbow").CountDocuments(Builders<Rainbow>.Filter.Empty);
            }
            catch (Exception) { return 0; }
        }

        public long getDictionaryCount()
        {
            try
            {
                return this.db.GetCollection<Dictionary>("dictionary").CountDocuments(Builders<Dictionary>.Filter.Empty);
            }
            catch (Exception) { return 0; }
        }

        public long getTokensCount()
        {
            try
            {
                return this.db.GetCollection<Token>("token").CountDocuments(Builders<Token>.Filter.Empty);
            }
            catch (Exception) { return 0; }
        }

        /**********/
        /* BACKUP */
        /**********/

        public List<Backup> getBackups()
        {
            try
            {
                return this.db.GetCollection<Backup>("backup").Find(Builders<Backup>.Filter.Empty).ToList();
            }
            catch (Exception) { return new List<Backup>(); }
        }

        public Backup getBackup(string uuid)
        {
            try
            {
                List<Backup> list = this.db.GetCollection<Backup>("backup").Find(Builders<Backup>.Filter.Eq("uuid", uuid)).ToList();
                if (list.Count != 1)
                {
                    return null;
                }
                return list[0];
            }
            catch (Exception e) { Console.WriteLine(e.ToString()); return null; }
        }

        public bool insertBackup(Backup b)
        {
            try
            {
                this.db.GetCollection<Backup>("backup").InsertOne(b);
                return true;
            }
            catch (Exception) { return false; }
        }

        public bool deleteBackup(string uuid)
        {
            try
            {
                return this.db.GetCollection<Backup>("backup").DeleteOne(Builders<Backup>.Filter.Eq(item => item.uuid, uuid)).DeletedCount > 0;
            }
            catch (Exception) { return false; }
        }

        /*******************/
        /* BACKUP DATABASE */
        /*******************/

        public async Task WriteCollectionToFile(string collectionName, string fileName)
        {
            IMongoCollection<RawBsonDocument> collection = this.db.GetCollection<RawBsonDocument>(collectionName);
            // Make sure the file is empty before we start writing to it
            File.WriteAllText(fileName, string.Empty);

            using (IAsyncCursor<RawBsonDocument> cursor = await collection.FindAsync(new BsonDocument()))
            {
                while (await cursor.MoveNextAsync())
                {
                    IEnumerable<RawBsonDocument> batch = cursor.Current;
                    foreach (RawBsonDocument document in batch)
                    {
                        File.AppendAllLines(fileName, new[] { document.ToString() });
                    }
                }
            }
        }

        public async Task LoadCollectionFromFile(string collectionName, string fileName)
        {
            using (FileStream fs = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (BufferedStream bs = new BufferedStream(fs))
            using (StreamReader sr = new StreamReader(bs))
            {
                IMongoCollection<BsonDocument> collection = this.db.GetCollection<BsonDocument>(collectionName);

                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    await collection.InsertOneAsync(BsonDocument.Parse(line));
                }
            }
        }

        /****************/
        /* CERTIFICATES */
        /****************/

        public bool insertCertificate(CertificateInfo certificateInfo)
        {
            try
            {
                this.db.GetCollection<CertificateInfo>("certs").InsertOne(certificateInfo);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<CertificateInfo> listCertificates()
        {
            try
            {
                return this.db.GetCollection<CertificateInfo>("certs").Find(Builders<CertificateInfo>.Filter.Empty).ToList();
            }
            catch (Exception)
            {
                return new List<CertificateInfo>();
            }
        }

        public CertificateInfo getCertificate(string uuid)
        {
            try
            {
                List<CertificateInfo> certificateInfos =
                    this.db.GetCollection<CertificateInfo>("certs").Find(Builders<CertificateInfo>.Filter.Eq(item => item.uuid, uuid)).ToList();
                if (certificateInfos.Count == 0)
                    return null;
                else
                    return certificateInfos[0];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool removeCertificate(string uuid)
        {
            try
            {
                return this.db.GetCollection<CertificateInfo>("certs").DeleteOne(
                    Builders<CertificateInfo>.Filter.Eq(item => item.uuid, uuid)
                    ).DeletedCount > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /********/
        /* LOGS */
        /********/

        public List<Log> getLogs(DateTime start, int offset, int limit)
        {
            try
            {
                IFindFluent<Log, Log> cursor;
                if (start == DateTime.MinValue)
                {
                    cursor = this.db.GetCollection<Log>("logs").Find(Builders<Log>.Filter.Empty);
                }
                else
                {
                    cursor = this.db.GetCollection<Log>("logs").Find(Builders<Log>.Filter.Gte(item => item.dateTime, start));
                }
                if (offset > 0) cursor.Skip(offset);
                if (limit > 0) cursor.Limit(limit);
                return cursor.SortByDescending(item => item.dateTime).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public bool insertLog(Log log)
        {
            try
            {
                this.db.GetCollection<Log>("logs").InsertOne(log);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool clearLogs()
        {
            try
            {
                this.db.GetCollection<Log>("logs").DeleteMany(Builders<Log>.Filter.Empty);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    } //class
} //namespace
