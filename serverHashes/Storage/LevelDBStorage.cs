﻿using LevelDB;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace serverHashes
{
    public class LevelDBStorage
    {
        private readonly string uuid;

        private static readonly ConcurrentDictionary<string, LevelDBStorage> instances = new ConcurrentDictionary<string, LevelDBStorage>();
        private readonly Semaphore MUTEX = new Semaphore(1, 1);

        private LevelDBStorage(string uuid)
        {
            this.uuid = uuid;
        }

        public static LevelDBStorage getInstance(string uuid)
        {
            if (!createDirStruture(uuid)) return null;
            try
            {
                LevelDBStorage s;
                instances.TryGetValue(uuid, out s);

                if (s != null)
                {
                    return s;
                }
                else
                {
                    s = new LevelDBStorage(uuid);
                    instances.TryAdd(uuid, s);
                    return s;
                }
            }
            catch (Exception)
            { return null; }
        }

        public static bool deleteInstance(string uuid)
        {
            try
            {
                return instances.TryRemove(uuid, out _);
            }
            catch (Exception) { return false; }
        }

        private static bool createDirStruture(string uuid)
        {
            try
            {
                if (!Directory.Exists("./computedHash")) Directory.CreateDirectory("./computedHash");
                if (!Directory.Exists("./computedHash/" + uuid + "/")) Directory.CreateDirectory("./computedHash/" + uuid + "/");
                return true;
            }
            catch (Exception) { return false; }
        }

        /*****************/
        /* COMPUTED HASH */
        /*****************/

        public string getComputedHash(string hash)
        {
            lock (MUTEX)
            {
                Options options = new Options { CreateIfMissing = true };
                DB db = new DB(options, @"./computedHash/" + uuid + "/");

                try
                {
                    string clearTextPWD = db.Get(hash);
                    db.Dispose();
                    return clearTextPWD;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public bool insertComputedHashes(Dictionary<string, string> computedHashes)
        {
            lock (MUTEX)
            {
                Options options = new Options { CreateIfMissing = true };
                DB db = new DB(options, @"./computedHash/" + uuid + "/");

                try
                {
                    foreach (string key in computedHashes.Keys)
                    {
                        db.Put(key, computedHashes[key]);
                    }
                    db.Dispose();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return false;
                }
            }
        }

        public bool removeComputedHashesStorage()
        {
            lock (MUTEX)
            {
                try
                {
                    Directory.Delete("./computedHash/" + this.uuid + "/");
                    return true;
                }
                catch (Exception) { return false; }
            }
        }

        public static long databaseSize()
        {
            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo(@"./computedHash/");

                long sizeCount = 0;
                foreach (DirectoryInfo dir in dirInfo.EnumerateDirectories())
                {
                    foreach (FileInfo file in dir.EnumerateFiles())
                    {
                        sizeCount += file.Length;
                    }
                }

                return sizeCount;
            }
            catch (DirectoryNotFoundException)
            {
                return 0;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}
