﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using YamlDotNet.Serialization;

namespace serverHashes
{
    public class LocalStorage
    {

        /// <summary>
        /// Name of the XML file where the server's settings are stored.
        /// </summary>
        public const string CONFIGURATION_FILENAME = "config.yaml";
        /// <summary>
        /// Name of the file where the hashes queue is going to be stored.
        /// </summary>
        private readonly string hashQueueFileName;
        /// <summary>
        /// Name of the file where the dictionaries objects are going to be stored (does not contain the dictionary files itself)
        /// </summary>
        private readonly string savedDictionariesFileName;
        /// <summary>
        /// Name of the file where the rainbow objects are going to be stored (does not contain the rainbow table itself)
        /// </summary>
        private readonly string rainbowsFileName;
        /// <summary>
        /// Name of the file where the token objects are going to be stored.
        /// </summary>
        private readonly string tokensFileName;
        /// <summary>
        /// Name of the file where a list of uploades secitiry certificates is going to be stored.
        /// </summary>
        private readonly string certificatesFileName;
        /// <summary>
        /// Name of the binary file where the server logs, if enabled, are going to be stored.
        /// </summary>
        private readonly string logsFileName;
        /// <summary>
        /// Path to the configuration file
        /// </summary>
        private readonly string configPath;

        public static bool justInitializedSettings;

        private static readonly Semaphore mutexSettings = new Semaphore(1, 1);
        private static readonly Semaphore mutexLogs = new Semaphore(1, 1);

        private readonly AryptiorDB<Hash> hashdb;
        private readonly AryptiorDB<Dictionary> dictionarydb;
        private readonly AryptiorDB<Token> tokendb;
        private readonly AryptiorDB<Rainbow> rainbowdb;
        private readonly AryptiorDB<Log> logsdb;
        private readonly AryptiorDB<CertificateInfo> certsdb;

        public LocalStorage(LocalStorageSettings conf)
        {
            this.hashQueueFileName = Path.Combine(conf.folderName, conf.hashesFileName);
            this.savedDictionariesFileName = Path.Combine(conf.folderName, conf.savedDictionariesFileName);
            this.rainbowsFileName = Path.Combine(conf.folderName, conf.rainbowsFileName);
            this.tokensFileName = Path.Combine(conf.folderName, conf.tokensFileName);
            this.certificatesFileName = Path.Combine(conf.folderName, conf.certificatesFileName);
            this.logsFileName = Path.Combine(conf.folderName, conf.logsFileName);
            this.configPath = conf.configPath;

            this.hashdb = new AryptiorDB<Hash>(Path.Combine(conf.folderName, conf.hashesFileName), conf.operationsToRegenerate);
            this.dictionarydb = new AryptiorDB<Dictionary>(Path.Combine(conf.folderName, conf.savedDictionariesFileName), conf.operationsToRegenerate);
            this.tokendb = new AryptiorDB<Token>(Path.Combine(conf.folderName, conf.tokensFileName), conf.operationsToRegenerate);
            this.rainbowdb = new AryptiorDB<Rainbow>(Path.Combine(conf.folderName, conf.rainbowsFileName), conf.operationsToRegenerate);
            this.logsdb = new AryptiorDB<Log>(Path.Combine(conf.folderName, conf.logsFileName), conf.operationsToRegenerate);
            this.certsdb = new AryptiorDB<CertificateInfo>(Path.Combine(conf.folderName, conf.certificatesFileName), conf.operationsToRegenerate);

            Directory.CreateDirectory(conf.folderName);

            this.initializeHashQueue();
            this.initializeSavedDictionaries();
            this.initializeSavedTokens();
            this.initializeRainbows();
            this.initializeCertificates();
            this.initializeLogs();
        }

        /************/
        /* SETTINGS */
        /************/

        /// <summary>
        /// Static function called at the beggining of the program in order to inicialize the server settings from the
        /// xml settings file. First, checks if the previously specified file exists, if not, a new one is inicializated
        /// with the default parameters and is saved to disk. If the file exists, initialization is continued.
        /// Then, this method attempts to reads the configuration file from disk in XML format.
        /// </summary>
        /// <returns>
        /// The configuration object readed, or null, if the file could not be readed.
        /// </returns>
        public static Settings readSettings(string settingsFilename)
        {
            if (!initializeSettings(settingsFilename))
            {
                return new Settings();
            }
            FileStream fs = null;
            try
            {
                mutexSettings.WaitOne();
                fs = new FileStream(settingsFilename, FileMode.Open);
                MemoryStream ms = new MemoryStream();
                fs.CopyTo(ms);
                string content = Encoding.UTF8.GetString(ms.ToArray());
                ms.Dispose();
                Settings c = null;
                if (settingsFilename.EndsWith(".json"))
                    c = (Settings)JsonConvert.DeserializeObject(content, typeof(Settings));
                else if (settingsFilename.EndsWith(".yaml") || settingsFilename.EndsWith(".yml"))
                {
                    Deserializer deserializer = new Deserializer();
                    c = deserializer.Deserialize<Settings>(content);
                }
                return c;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
                try
                {
                    mutexSettings.Release();
                }
                catch (Exception) { }
            }
        }

        public Settings readSettings()
        {
            return readSettings(this.configPath);
        }

        /// <summary>
        /// Replaces the current settings saved to disk with the given configuration object.
        /// </summary>
        /// <param name="c"></param>
        public static bool writeSettings(Settings c, string settingsFilename)
        {
            try
            {
                string content;
                if (settingsFilename.EndsWith(".json"))
                    content = JsonConvert.SerializeObject(c, Formatting.Indented);
                else if (settingsFilename.EndsWith(".yaml") || settingsFilename.EndsWith(".yml"))
                {
                    Serializer serializer = new Serializer();
                    content = serializer.Serialize(c);
                }
                else
                    return false;
                byte[] serializedSettings = Encoding.UTF8.GetBytes(content);
                FileStream fs = new FileStream(settingsFilename, FileMode.OpenOrCreate, FileAccess.Write);
                fs.SetLength(0);
                fs.Write(serializedSettings, 0, serializedSettings.Length);
                fs.Flush();
                fs.Close();
                fs.Dispose();
                return true;
            }
            catch (Exception) { }
            finally
            {
                try
                {
                    mutexSettings.Release();
                }
                catch (Exception) { }
            }
            return false;
        }

        public bool writeSettings(Settings c)
        {
            return writeSettings(c, this.configPath);
        }

        /// <summary>
        /// Checks if the previously specified settings file exists, if not, a new one is inicializated
        /// with the default parameters and is saved to disk.
        /// </summary>
        /// <returns>
        ///     true if the file already exists or it was initialized successfully
        ///     false if the file could not be written to disk
        /// </returns>
        private static bool initializeSettings(string settingsFilename)
        {
            try
            {
                mutexSettings.WaitOne();
                // check if the file already exists
                if (!File.Exists(settingsFilename))
                {
                    justInitializedSettings = true;
                    Console.WriteLine("Initializing...");
                    // if the file doesn't exists yet, a new one must be created with the default parameters
                    Settings s = new Settings();
                    s.socketSettings.pwd = SocketPassword.GetSocketPassword();
                    s.webSocketSettings.pwd = SocketPassword.GetSocketPassword();
                    string content;
                    if (settingsFilename.EndsWith(".json"))
                        content = JsonConvert.SerializeObject(s, Formatting.Indented);
                    else if (settingsFilename.EndsWith(".yaml") || settingsFilename.EndsWith(".yml"))
                    {
                        Serializer serializer = new Serializer();
                        content = serializer.Serialize(s);
                    }
                    else
                        return false;
                    byte[] serializedSettings = Encoding.UTF8.GetBytes(content);
                    FileStream fs = new FileStream(settingsFilename, FileMode.OpenOrCreate, FileAccess.Write);
                    fs.SetLength(0);
                    fs.Write(serializedSettings, 0, serializedSettings.Length);
                    fs.Flush();
                    fs.Close();
                    fs.Dispose();
                }
                else
                {
                    justInitializedSettings = false;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                try
                {
                    mutexSettings.Release();
                }
                catch (Exception) { }
            }
        }

        /********/
        /* HASH */
        /********/

        /// <summary>
        /// Llig la llista de tasques del fitxer de tasques pendents especificat per el atribut de la classe.
        /// </summary>
        /// <returns></returns>
        public List<Hash> readHashQueue()
        {
            return this.hashdb.getObjects((Hash hash) =>
            {
                return (!hash.surrender && !hash.estaResolt);
            });
        }

        /// <summary>
        /// Llig la coleccio de hashes finalitzats desde el nom de fitxer especiifcat en el atribut de la clase.
        /// </summary>
        /// <returns></returns>
        public List<Hash> readFinishedHashes(out int length, int offset = 0, int limit = 0)
        {
            List<Hash> hashes = this.hashdb.getObjects((Hash hash) =>
            {
                return hash.surrender || hash.estaResolt;
            });
            length = hashes.Count;
            for (int i = 0; i < offset; i++)
                hashes.RemoveAt(0);
            if (limit > 0)
            {
                hashes.Capacity = limit;
                hashes.TrimExcess();
            }
            return hashes;
        }

        public int countFinishedHashes()
        {
            try
            {
                return this.hashdb.getObjects((Hash hash) =>
                {
                    return hash.surrender || hash.estaResolt;
                }).Count;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public long countSolvedHashes()
        {
            try
            {
                return this.hashdb.getObjects((Hash hash) =>
                {
                    return hash.estaResolt && hash.solucio != null && !hash.surrender;
                }).Count;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public Hash getHash(string uuid)
        {
            return this.hashdb.getObjectById(uuid);
        }

        public bool insertHash(Hash h)
        {
            List<Hash> hashes = this.hashdb.getObjects();
            hashes.Add(h);
            // check for duplicates
            if (!checkHashList(hashes))
                return false;

            return this.hashdb.insertObject(h.uuid, h);
        }

        public bool insertHash(Hash[] h)
        {
            List<Hash> hashes = this.hashdb.getObjects();
            hashes.AddRange(h);
            // check for duplicates
            if (!checkHashList(hashes))
                return false;

            foreach (Hash hash in h)
                if (!this.hashdb.insertObject(hash.uuid, hash))
                    return false;
            return true;
        }

        private bool checkHashList(List<Hash> h)
        {
            for (int i = 0; i < h.Count; i++)
            {
                for (int j = 0; j < h.Count; j++)
                {
                    if (i != j &&
                        (h[i].hash.Equals(h[j].hash) && h[i].salt.Equals(h[j].salt) && h[i].iteracions == h[j].iteracions)
                        && (h[i].uuid.Equals(h[j].uuid)))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool updateHash(Hash h)
        {
            return this.hashdb.updateObject(h.uuid, h);
        }

        public bool removeHash(string uuid)
        {
            return this.hashdb.deleteObject(uuid);
        }

        /**************/
        /* DICTIONARY */
        /**************/

        /// <summary>
        /// Returns a List that contains all the dictionaries currently saved to disk, or null reference, 
        /// if the file could not be readed.
        /// </summary>
        /// <returns></returns>
        public List<Dictionary> readSavedDictionaries()
        {
            return this.dictionarydb.getObjects();
        }

        public bool insertDictionary(Dictionary d)
        {
            List<Dictionary> dictionaries = this.dictionarydb.getObjects((Dictionary dictSel) =>
            {
                return (dictSel.fileName.Equals(d.fileName));
            });
            if (dictionaries.Count > 0)
                return false;

            return this.dictionarydb.insertObject(d.uuid, d);
        }

        public bool updateDictionary(Dictionary nou, out List<Hash> updatedHashes)
        {
            updatedHashes = null;

            if (!this.dictionarydb.updateObject(nou.uuid, nou))
                return false;

            // cascade update to hashes
            updatedHashes = new List<Hash>();
            if (nou.isReady)
            {
                List<Hash> hashes = this.hashdb.getObjects((Hash hash) =>
                {
                    return (hash.tipusBusqueda == TipusBusqueda.Diccionari && hash.diccionari != null
                        && hash.diccionari.uuid != null && hash.diccionari.uuid.Equals(nou.uuid));
                });
                foreach (Hash hash in hashes)
                {
                    hash.diccionari = nou;
                    updatedHashes.Add(hash);
                    this.hashdb.updateObject(hash.uuid, hash);
                }
            }

            return true;
        }

        public List<Hash> findDictionaryInHashQueue(string uuid)
        {
            return this.hashdb.getObjects((Hash h) =>
            {
                return (h.tipusBusqueda == TipusBusqueda.Diccionari
                    && !h.estaResolt
                    && h.diccionari != null
                    && h.diccionari.uuid.Equals(uuid));
            });
        }

        public bool deleteDictionary(Dictionary d)
        {
            return this.dictionarydb.deleteObject(d.uuid);
        }

        /**********/
        /* TOKENS */
        /**********/

        public List<Token> getAllTokens()
        {
            return this.tokendb.getObjects();
        }

        public long getTokensCount()
        {
            return this.tokendb.getObjects().Count;
        }

        public void clearExpiredTokens()
        {
            List<Token> expiredTokens = this.tokendb.getObjects((Token t) =>
            {
                return t.isExpired();
            });
            foreach (Token t in expiredTokens)
            {
                this.hashdb.deleteObject(t.token);
            }
        }

        public bool insertToken(Token t)
        {
            return this.tokendb.insertObject(t.token, t);
        }

        public bool deleteToken(string t)
        {
            return this.tokendb.deleteObject(t);
        }

        public List<Token> findToken(string addr, string token)
        {
            return this.tokendb.getObjects((Token t) =>
            {
                return (t.addr.Equals(addr) && t.token.Equals(token));
            });
        }

        public bool updateToken(Token t)
        {
            return this.tokendb.updateObject(t.token, t);
        }

        public bool clearTokens()
        {
            return this.tokendb.initializeFile(true);
        }

        /************/
        /* RAINBOWS */
        /************/

        public List<Rainbow> getAllRainbows(out int length, int offset = 0, int limit = 0)
        {
            List<Rainbow> rainbowsList = this.rainbowdb.getObjects();

            length = rainbowsList.Count;
            if (offset < 0) return null;

            List<Rainbow> list = new List<Rainbow>();
            if (limit <= 0) limit = rainbowsList.Count - offset;
            for (int i = 0; (i + offset) < rainbowsList.Count && i < limit; i++)
                list.Add(rainbowsList[(i + offset)]);

            return list;
        }

        public List<Rainbow> getEnabledRainbows()
        {
            return this.rainbowdb.getObjects((Rainbow r) =>
            {
                return (r.enabled);
            });
        }

        public bool insertRainbow(Rainbow r)
        {
            if (r == null) return false;
            List<Rainbow> rainbows = this.rainbowdb.getObjects();

            foreach (var rainbow in rainbows)
            {
                if (rainbow.name.Equals(r.name))
                    return false;
            }

            return this.rainbowdb.insertObject(r.uuid, r);
        }

        public Rainbow findRainbow(string uuid)
        {
            return this.rainbowdb.getObjectById(uuid);
        }

        public bool updateRainbow(string uuid, Rainbow r)
        {
            return this.rainbowdb.updateObject(uuid, r);
        }

        public bool deleteRainbow(Rainbow r)
        {
            return this.rainbowdb.deleteObject(r.uuid);
        }

        /*******************/
        /* INITIALIZATIONS */
        /*******************/

        /// <summary>
        /// Checks if the previously specified queue file exists, if not, a new one is created empty is saved to disk.
        /// </summary>
        /// <returns>
        ///     true if the file already exists or it was initialized successfully
        ///     false if the file could not be written to disk
        /// </returns>
        private bool initializeHashQueue()
        {
            if (!File.Exists(this.hashQueueFileName))
            {
                Console.WriteLine("Beep boop boop beep. I'm runned by the first time and I create the save data file."
                    + this.hashQueueFileName);
            }
            return this.hashdb.initializeFile();
        }

        /// <summary>
        /// Checks if the previously specified dictionaries file exists, if not, a new one is created empty is saved to disk.
        /// </summary>
        /// <returns>
        ///     true if the file already exists or it was initialized successfully
        ///     false if the file could not be written to disk
        /// </returns>
        private bool initializeSavedDictionaries()
        {
            if (!File.Exists(this.savedDictionariesFileName))
            {
                Console.WriteLine("Beep boop boop beep. Me inicialitze per primera vegada i cree el fitxer de diccionaris guardats. " + this.savedDictionariesFileName);
            }
            return this.dictionarydb.initializeFile();
        }

        private bool initializeSavedTokens()
        {
            if (!File.Exists(this.tokensFileName))
            {
                Console.WriteLine("Beep boop boop beep. Creating the saved tokens file by the first time. " + this.tokensFileName);
            }
            return this.tokendb.initializeFile();
        }

        private bool initializeRainbows()
        {
            if (!File.Exists(this.rainbowsFileName))
            {
                Console.WriteLine("Beep boop boop beep. Creating the rainbows definitions file by the first time. " + this.rainbowsFileName);
            }
            return this.rainbowdb.initializeFile();
        }

        private bool initializeCertificates()
        {
            if (!File.Exists(this.certificatesFileName))
            {
                Console.WriteLine("Beep boop boop beep. Creating the certificates list file by the first time. " + this.certificatesFileName);
            }
            return this.certsdb.initializeFile();
        }

        private bool initializeLogs()
        {
            if (!File.Exists(this.logsFileName))
            {
                Console.WriteLine("Beep boop boop beep. Creating the logs file by the first time. " + this.logsFileName);
            }
            return this.logsdb.initializeFile();
        }

        /****************/
        /* CERTIFICATES */
        /****************/

        public bool insertCertificate(CertificateInfo certificateInfo)
        {
            return this.certsdb.insertObject(certificateInfo.uuid, certificateInfo);
        }

        public List<CertificateInfo> listCertificates()
        {
            return this.certsdb.getObjects();
        }

        public CertificateInfo getCertificate(string uuid)
        {
            return this.certsdb.getObjectById(uuid);
        }

        public bool removeCertificate(string uuid)
        {
            return this.certsdb.deleteObject(uuid);
        }

        /// <summary>
        /// Writes a certificate to the disk.
        /// </summary>
        /// <param name="certificate"></param>
        /// <param name="uuid"></param>
        /// <returns></returns>
        public bool writeCertificate(byte[] certificate, string uuid, bool truncate = true)
        {
            FileStream fitxer = null;
            try
            {
                fitxer = new FileStream("./cert/" + uuid, truncate ? FileMode.Truncate : FileMode.Create, FileAccess.Write);
                fitxer.Write(certificate, 0, certificate.Length);
                fitxer.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                if (fitxer != null) fitxer.Close();
                return false;
            }
        }

        /// <summary>
        /// Deletes a certificate from the disk.
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        public bool deleteCertificate(string uuid)
        {
            try
            {
                File.Delete("./cert/" + uuid);
                return true;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public byte[] readCertificate(string uuid)
        {
            FileStream fitxer = null;
            try
            {
                fitxer = new FileStream(Path.Combine("./cert/", uuid), FileMode.Open, FileAccess.Read);
                byte[] certificate = new byte[new FileInfo("./cert/" + uuid).Length];
                fitxer.Read(certificate, 0, certificate.Length);
                fitxer.Close();
                return certificate;
            }
            catch (Exception)
            {
                if (fitxer != null) fitxer.Close();
                return null;
            }
        }

        /********/
        /* LOGS */
        /********/

        public List<Log> getLogs(DateTime start, int offset, int limit)
        {
            // read the logs list by the olders and clear the log list
            int logDuration = Program.CONFIG.loggingSettings.logDuration;

            List<Log> logs = this.logsdb.getObjects((Log log) =>
            {
                return (DateTime.Now - log.dateTime).TotalSeconds > logDuration;
            });
            foreach (Log log in logs)
            {
                this.logsdb.deleteObject(log.uuid);
            }

            // load list with a limit by date if necessary
            if (start != DateTime.MinValue)
            {
                logs = this.logsdb.getObjects((Log item) =>
                {
                    return (item.dateTime >= start);
                });
            }
            else
            {
                logs = this.logsdb.getObjects();
            }

            // sort by date
            logs.Sort((Log a, Log b) =>
            {
                if (a.dateTime > b.dateTime)
                {
                    return -1;
                }
                else if (a.dateTime < b.dateTime)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            });

            // limit by number of records of necessary
            if (offset > 0)
            {
                logs = logs.Skip(offset).ToList();
            }
            if (limit > 0)
            {
                logs = logs.Take(limit).ToList();
            }

            return logs;
        }

        public bool insertLog(Log log)
        {
            return this.logsdb.insertObject(log.uuid, log);
        }

        public bool clearLogs()
        {
            try
            {
                lock (mutexLogs)
                {
                    return this.logsdb.initializeFile(true);
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
