﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace serverHashes
{
    public class Storage
    {
        /// <summary>
        /// De estar activat el guardat a una base de dades MongoDB, este es l'objecte que s'encarrega de gestionarho
        /// </summary>
        public MongoDBStorage mongo;
        /// <summary>
        /// Objecte que administra els fitxers locals de dades guardades
        /// </summary>
        public LocalStorage ls;
        /// <summary>
        /// Queue of hashes to be processed.
        /// The position 0 of this array represents the hash being currently processed.
        /// If this array if empty, it means that this server should be IDLE.
        /// </summary>
        private List<Hash> queue;
        /// <summary>
        /// Llista de tots els diccionaris guardats al servidor.
        /// </summary>
        private List<Dictionary> dictionaries;
        /// <summary>
        /// List of the queue that contains all the enabled rainbow objects on this server.
        /// </summary>
        private List<Rainbow> rainbows;
        /// <summary>
        /// Callback used in order to update all the clients subscribed to changes when there is an update.
        /// </summary>
        public OnChange onChange;

        private Semaphore configMutex = new Semaphore(1, 1);
        public LogConfig loggingSettings
        {
            get { lock (configMutex) return _loggingSettings; }
            set { lock (configMutex) _loggingSettings = value; }
        }
        private LogConfig _loggingSettings;

        private static readonly Semaphore semaphoreQueue = new Semaphore(1, 1);
        private static readonly Semaphore semaphoreDictionaries = new Semaphore(1, 1);
        private static readonly Semaphore semaphoreRainbows = new Semaphore(1, 1);

        public delegate void OnChange(string topicName, SubscriptionChangeType changeType, int pos, string newValue);

        private Storage()
        {
            this.mongo = null;
            this.ls = null;
            this.queue = new List<Hash>();
            this.dictionaries = new List<Dictionary>();
            this.rainbows = new List<Rainbow>();
        }

        public Storage(Settings c) : this()
        {
            // initialize storage
            if (c.mongoStorage.useDatabase)
            {
                this.initializeMongo(c.mongoStorage);
            }
            if (c.localStorage.useLocalStorage)
            {
                if (!this.initializeLocalStorage(c.localStorage))
                {
                    Console.WriteLine("LOCAL STORAGE COULD NOT BE INITIALIZED. CHECK THE FILE SISTEM SPACE AND PERMISSIONS.");
                    return;
                }
            }
            // clear garbage
            this.clearGarbage();
            // load data
            this.loadSavedData(c);
            // run the background crons
            this.runCrons();
            // if saved data could not be loaded, prevent a null pointer exception by initializing empty data
            if (this.queue == null)
            {
                this.queue = new List<Hash>();
            }
            if (this.dictionaries == null)
            {
                this.dictionaries = new List<Dictionary>();
            }
            // save logging settings
            this._loggingSettings = c.loggingSettings;
        }

        private bool initializeMongo(MongoDBSettings config)
        {
            mongo = new MongoDBStorage(config);
            // in case of connection error, database connection is disabled to avoid possible loss of data
            return mongo.connect();
        }

        private bool initializeLocalStorage(LocalStorageSettings config)
        {
            try
            {
                this.ls = new LocalStorage(config);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void loadSavedData(Settings c)
        {
            if (c.mongoStorage.useDatabase)
            {
                this.queue = this.mongo.findQueuedHashes();
                this.dictionaries = this.mongo.findDictionaries();
                this.rainbows = this.mongo.getEnabledRainbows();
            }
            else if (c.localStorage.useLocalStorage)
            {
                this.queue = this.ls.readHashQueue();
                this.dictionaries = this.ls.readSavedDictionaries();
                this.rainbows = this.ls.getEnabledRainbows();
            }
        }

        private void runCrons()
        {
            runAutoSave();
            runFastUpdateDashboard();

            watchSettingsFile();
        }

        private void runAutoSave()
        {
            // obrir un nou fil que guarde automaticament cada 60 segons
            Thread autoSave = new Thread(new ThreadStart(() =>
            {
                while (true)
                {
                    Thread.Sleep(60000);
                    this.saveAllData();
                }
            }));
            autoSave.Start();
        }

        private void runFastUpdateDashboard()
        {
            Thread updateNumberOfHashesPerSecond = new Thread(new ThreadStart(() =>
            {
                const int waitTime = 10000;
                while (true)
                {
                    int interval = 60000 / waitTime;

                    Controller.nHashesS = ((Controller.nHashesProcessed / 60) * interval);
                    Controller.nHashesProcessed = 0;
                    Thread.Sleep(waitTime);
                }
            }));
            updateNumberOfHashesPerSecond.Start();
        }

        public Dashboard getDashboard()
        {
            Dashboard d = new Dashboard();
            if (Utilities.IsRunningOnMono())
                d.getMonoSysInfo();
            else
                d.getWinSysInfo();

            // fast update
            d.clientsCount = Program.llistaClients.Count;
            lock (semaphoreQueue)
                d.hashQueueCount = this.queue.Count;

            d.nHashesS = Controller.nHashesS;

            // slow update
            if (this.mongo != null)
            {
                d.finishedHashesCount = this.mongo.getFinishedHashesCount();
                d.tokensCount = this.mongo.getTokensCount();
                d.dictionaryCount = this.mongo.getDictionaryCount();
                d.rainbowsCount = this.mongo.getRainbowsCount();
                d.enabledRainbows = this.mongo.getEnabledRainbows().Count;
                d.solvedHashes = this.mongo.countSolvedHashes();
            }
            else if (this.ls != null)
            {
                d.finishedHashesCount = this.ls.readFinishedHashes(out _).Count;
                d.tokensCount = this.ls.getTokensCount();
                d.dictionaryCount = this.ls.readSavedDictionaries().Count;
                d.rainbowsCount = this.ls.getAllRainbows(out _).Count;
                d.enabledRainbows = this.ls.getEnabledRainbows().Count;
                d.solvedHashes = this.ls.countSolvedHashes();
            }
            d.computedHashesSize = LevelDBStorage.databaseSize();

            return d;
        }

        private bool watchSettingsFile()
        {
            try
            {
                FileSystemWatcher fsw = new FileSystemWatcher();
                fsw.Path = Directory.GetCurrentDirectory();
                fsw.Filter = Path.GetFileName(LocalStorage.CONFIGURATION_FILENAME);
                fsw.Changed += fswConfigChanged;
                fsw.EnableRaisingEvents = true;
            }
            catch (Exception)
            {
                Console.WriteLine("Changes in the settings file could not be under watch. " +
                "Updating the settings will require a manual restart.");
                return false;
            }
            return true;
        }

        private void fswConfigChanged(object sender, FileSystemEventArgs e)
        {
            Settings s = Program.storage.ls.readSettings();
            if (s != null)
            {
                Console.WriteLine("Changes detected! Reloading settings... ");
                if (!s.isValid())
                {
                    Console.WriteLine("THE NEW CONFIGURATION IS INVALID! THE LAST VALID SETTINGS PREVIOUSLY LOADED TO MEMORY WILL NOT BE UPDATED.");
                }
                else
                {
                    this.performHotConfigChanges(s);

                    Program.CONFIG = s;

                }
            }
        }

        private void performHotConfigChanges(Settings s)
        {
            Settings currentSettings = Program.CONFIG;
            // stop or start the discovery daemon
            if (currentSettings.isDiscoverable != s.isDiscoverable)
            {
                if (s.isDiscoverable)
                {
                    Program.initializeLocalDiscovery(s);
                }
                else
                {
                    Program.networkDiscovery.Stop();
                }
            }

            // update the discovery demo with the newest server settings if they have changed, the deamon is set to run and was already running
            if (currentSettings.isDiscoverable
                && s.isDiscoverable
                && ((!s.discoverableName.Equals(currentSettings.discoverableName))
                || s.socketSettings.port != currentSettings.socketSettings.port
                || s.webSocketSettings.port != currentSettings.webSocketSettings.port))
            {
                Program.networkDiscovery.setNetworkDiscoveryData(
                    new NetworkDiscoveryData(s.socketSettings.port, s.webSocketSettings.port, s.discoverableName));
            }

            // stop or start the backup cron
            if (currentSettings.backupSettings.doBackups != s.backupSettings.doBackups)
            {
                if (s.backupSettings.doBackups)
                {
                    Program.initializeBackupService(s);
                }
                else
                {
                    Program.backupCron.Stop();
                }
            }
            if (s.backupSettings.doBackups && currentSettings.backupSettings.sizeLimit != s.backupSettings.sizeLimit)
            {
                Program.backupCron.sizeLimit = s.backupSettings.sizeLimit;
            }

            // update the logging settings
            if (!loggingSettings.Equals(s.loggingSettings))
            {
                this.loggingSettings = s.loggingSettings;

                // update the database index
                if (this.mongo != null)
                {
                    this.mongo.updateTTLlogsCollection(s.loggingSettings.logDuration);
                }
            }

            this.onLog(LogType.SettingsUpdated, currentSettings);
        }

        /// <summary>
        /// Collexts all the garbage from the tokens that has already expired from both the database and local storage.
        /// </summary>
        private void clearGarbage()
        {
            if (this.mongo != null)
            {
                this.mongo.clearExpiredTokens();
            }
            if (this.ls != null)
            {
                this.ls.clearExpiredTokens();
            }

            this.clearCertificatesGarbage();
        }

        public bool saveSettings(Settings c)
        {
            if (c == null || (!c.isValid())) return false;
            this.performHotConfigChanges(c);
            Program.CONFIG = c;
            Program.storage.ls.writeSettings(c);
            return true;
        }

        public void saveAllData()
        {
            try
            {
                lock (semaphoreQueue)
                {
                    int queueCount = this.queue.Count;
                    if (this.queue.Count > 0)
                        foreach (Hash hash in this.queue)
                            this.saveHash(hash);
                }
            }
            catch (Exception e) { Console.WriteLine(e.ToString()); }

            try
            {
                lock (semaphoreRainbows)
                    if (this.rainbows.Count > 0)
                        foreach (Rainbow rainbow in this.rainbows)
                            this.saveRainbow(this.rainbows[0]);
            }
            catch (Exception e) { Console.WriteLine(e.ToString()); }
        }

        /**********/
        /* HASHES */
        /**********/

        public List<Hash> getAllQueuedHashes(out int length, int offset = 0, int limit = 0)
        {
            List<Hash> lh = new List<Hash>();
            lock (semaphoreQueue)
            {
                length = this.queue.Count;
                if (limit < 0)
                {
                    return null;
                }
                if (offset == 0 && limit == 0)
                    for (int i = 0; i < queue.Count; i++)
                        lh.Add(queue[i]);
                else
                    for (int i = offset; i < (offset + limit) && i < queue.Count; i++)
                        lh.Add(queue[i]);
            }
            return lh;
        }

        /// <summary>
        /// Searches a hash by UUID in the queue (only unfinished hashes).
        /// This function was crated to simplify on-memory searches, 
        /// so it's not longer needed to iterate over the result of getting all the queued hashes from outside this class.
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        public Hash getQueuedHash(string uuid)
        {
            lock (semaphoreQueue)
            {
                IEnumerable<Hash> h = from hashSel in this.queue where (hashSel.uuid == uuid) select hashSel;
                // check if the hash is not on the queue, because on that case, saving a pending combination will not be necessary
                if (h.Count() == 1 && !h.ElementAt(0).estaResolt)
                {
                    return new Hash(h.ElementAt(0));
                }
            }
            return null;
        }

        public List<Hash> getAllFinishedHashes(out int length, int offset = 0, int limit = 0)
        {
            List<Hash> list = null;
            if (this.mongo != null)
            {
                if (limit < 0)
                {
                    length = this.mongo.countFinishedHashes();
                    return null;
                }
                list = this.mongo.findFinishedHashes(out length, offset, limit);
            }
            else if (this.ls != null)
            {
                if (limit < 0)
                {
                    length = this.ls.countFinishedHashes();
                    return null;
                }
                list = ls.readFinishedHashes(out length, offset, limit);
            }
            else
            {
                length = -1;
            }

            return list;
        }

        public Hash getHash(string uuid)
        {
            if (this.mongo != null)
                return this.mongo.getHash(uuid);
            else if (this.ls != null)
                return this.ls.getHash(uuid);
            return null;
        }

        public int addToQueue(Hash h)
        {
            lock (semaphoreQueue)
            {
                this.queue.Add(h);
                return this.queue.Count - 1;
            }
        }

        /// <summary>
        /// inserts a hash object to the queue and saves
        /// </summary>
        /// <param name="h"></param>
        public bool insertHash(Hash h)
        {
            bool result;
            lock (semaphoreQueue)
            {
                // and save
                if (this.mongo != null)
                {
                    result = this.mongo.insertHash(h);
                }
                else if (this.ls != null)
                {
                    result = this.ls.insertHash(h);
                }
                else
                {
                    result = false;
                }

                if (result && Program.estaIdle)
                {
                    // add to queue
                    this.queue.Add(h);

                    lock (Program.clientInitializationMutex)
                    {
                        Hash hash;
                        Program.estaIdle = Program.storage.getNextHash(out hash);
                        Program.hashActual = hash;
                    }
                }
            }

            this.onChange("hash", SubscriptionChangeType.insert, this.queue.Count - 1, JsonConvert.SerializeObject(h));
            this.onLog(LogType.InsertHash, h);

            return result;
        }

        public bool insertHash(Hash[] h)
        {
            bool result;
            lock (semaphoreQueue)
            {
                // and save
                if (this.mongo != null)
                {
                    result = this.mongo.insertHash(h);
                }
                else if (this.ls != null)
                {
                    result = this.ls.insertHash(h);
                }
                else
                {
                    result = false;
                }


                // add to queue
                this.queue.AddRange(h);

                if (result && Program.estaIdle)
                {
                    lock (Program.clientInitializationMutex)
                    {
                        Hash hash;
                        Program.estaIdle = Program.storage.getNextHash(out hash);
                        Program.hashActual = hash;
                    }
                }

                for (int i = 0; i < h.Length; i++)
                {
                    this.onChange("hash", SubscriptionChangeType.insert, this.queue.Count - i - 1, JsonConvert.SerializeObject(h[i]));
                    this.onLog(LogType.InsertHash, h[i]);
                }
            }

            return result;
        }

        /* HASH PROCESSING */

        public bool getNextHash(out Hash hashActual)
        {
            lock (semaphoreQueue)
            {
                bool estaIdle = false;
                hashActual = null;

                // si el servidor esta actualment inactiu
                // determinar si hi ha mes feina que fer i passar a ferla
                if (this.queue.Count > 0)
                {
                    Console.WriteLine("Queden " + this.queue.Count + " hashes en la cua.");
                    // comprovar el tipus de hash a buscar
                    if (this.queue[0].tipusBusqueda == TipusBusqueda.ForçaBruta)
                    {
                        hashActual = this.queue[0];
                        estaIdle = false;
                    }
                    else if (this.queue[0].tipusBusqueda == TipusBusqueda.Diccionari)
                    {
                        // si es per diccionari, obrir el fitxer i pasar al seguent
                        try
                        {
                            hashActual = this.queue[0];
                        }
                        catch (Exception)
                        {
                            this.queue.RemoveAt(0);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Informant de que no queda feina per fer.");
                    hashActual = null;
                    estaIdle = true;
                }
                return estaIdle;
            }
        }

        private void saveHash(Hash h)
        {
            if (this.mongo != null)
            {
                this.mongo.updateHash(h);
            }
            else if (this.ls != null)
            {
                this.ls.updateHash(h);
            }
        }

        /// <summary>
        /// gets the hash string and updates to the new object
        /// </summary>
        /// <param name="h"></param>
        /// <returns></returns>
        public void updateHash(Hash h, bool updateOnly = false)
        {
            if (!updateOnly)
            {
                if (h.estaResolt)
                {
                    this.hashProcessingFinished(h);
                }
                else if (h.surrender)
                {
                    this.dequeueHash(h);
                }
                else
                {
                    this.hashProcessingUpdate(h);
                }
            }
            else
            {
                this.hashProcessingUpdate(h);
            }

            // update from the database
            if (this.mongo != null)
            {
                this.mongo.updateHash(h);
            }
            else if (this.ls != null)
            {
                this.ls.updateHash(h);
            }
        }

        public void updateSurrenderHash(Hash h)
        {
            // update from the database
            if (this.mongo != null)
            {
                this.mongo.updateHash(h);
            }
            else if (this.ls != null)
            {
                this.ls.updateHash(h);
            }

            if (h.surrender)
            {
                this.dequeueHash(h);
            }
            else
            {
                this.hashProcessingUpdate(h, false);
            }
        }

        private void hashProcessingFinished(Hash h)
        {
            this.dequeueHash(h);
            this.onLog(LogType.FinishHash, h);
        }

        private void dequeueHash(Hash h)
        {
            int updatedPos = -1;

            lock (semaphoreQueue)
            {
                // delete from the queue in memory
                try
                {
                    for (int i = 0; i < this.queue.Count; i++)
                    {
                        Hash hashSel = this.queue[i];
                        if (hashSel.hash.Equals(h.hash))
                        {
                            this.queue.RemoveAt(i);
                            updatedPos = i;
                            break;
                        }
                    }

                    this.onChange("hash", SubscriptionChangeType.delete, updatedPos, JsonConvert.SerializeObject(h));
                }
                catch (Exception) { }
            }
        }

        private void hashProcessingUpdate(Hash h, bool log = true)
        {
            lock (semaphoreQueue)
                try
                {
                    // update in memory
                    for (int i = 0; i < this.queue.Count; i++)
                    {
                        Hash hashSel = this.queue[i];
                        if (hashSel.uuid.Equals(h.uuid))
                        {
                            this.queue[i] = h;

                            this.onChange("hash", SubscriptionChangeType.update, i, JsonConvert.SerializeObject(h));
                            if (log)
                                this.onLog(LogType.EditHash, hashSel);

                            return;
                        }
                    }
                    this.onChange("hash", SubscriptionChangeType.update, -1, JsonConvert.SerializeObject(h));

                    if (log)
                    {
                        Hash oldHash = getHash(h.uuid);
                        if (oldHash != null)
                            this.onLog(LogType.EditHash, oldHash);
                    }
                }
                catch (Exception) { }
        }

        public bool removeHash(string uuid)
        {
            // log
            Hash oldHash = getHash(uuid);
            if (oldHash != null)
                this.onLog(LogType.DeleteHash, oldHash);
            else
                return false;

            // delete if the hash were found
            if (this.mongo != null)
                return this.mongo.removeHash(uuid);
            else if (this.ls != null)
                return this.ls.removeHash(uuid);
            return false;
        }

        /****************/
        /* DICTIONARIES */
        /****************/

        public List<Dictionary> getAllDictionaries()
        {
            lock (semaphoreDictionaries)
                return this.dictionaries;
        }

        public Dictionary getDictionary(string uuid)
        {
            lock (semaphoreDictionaries)
                foreach (Dictionary d in this.dictionaries)
                {
                    if (d.uuid.Equals(uuid))
                    {
                        return d;
                    }
                }
            return null;
        }

        public Dictionary getDictionaryByFileName(string fileName)
        {
            lock (semaphoreDictionaries)
                foreach (Dictionary d in this.dictionaries)
                {
                    if (d.fileName.Equals(fileName))
                    {
                        return d;
                    }
                }
            return null;
        }

        public bool insertDictionary(Dictionary d)
        {
            lock (semaphoreDictionaries)
            {
                foreach (var item in this.dictionaries)
                {
                    if (item.uuid.Equals(d.uuid) || item.fileName.Equals(d.fileName))
                        return false;
                }

                this.dictionaries.Add(d);


                bool result = false;
                if (this.mongo != null)
                {
                    result = this.mongo.insertDictionary(d);
                }
                else if (this.ls != null)
                {
                    result = this.ls.insertDictionary(d);
                }

                if (result)
                {
                    this.onChange("dictionary", SubscriptionChangeType.insert, this.dictionaries.Count - 1, JsonConvert.SerializeObject(d));
                    this.onLog(LogType.DictionaryCreation, d);
                }

                return result;
            }
        }

        public bool removeDictionary(Dictionary d, out List<Hash> hashesInQueue)
        {
            hashesInQueue = new List<Hash>();
            lock (semaphoreDictionaries)
            {
                // search for unfinished hashes that use this dictionary
                if (this.mongo != null)
                {
                    hashesInQueue = this.mongo.findDictionaryInHashQueue(d.uuid);
                }
                else if (this.ls != null)
                {
                    hashesInQueue = this.ls.findDictionaryInHashQueue(d.uuid);
                }

                if (hashesInQueue.Count > 0) return false;
                int index = this.dictionaries.IndexOf(d);
                if (index >= 0)
                {
                    if (!this.dictionaries.Remove(d)) return false;

                    bool result = false;
                    if (this.mongo != null)
                    {
                        result = this.mongo.deleteDictionary(d);
                    }
                    else if (this.ls != null)
                    {
                        result = this.ls.deleteDictionary(d);
                    }

                    if (result)
                    {
                        this.onChange("dictionary", SubscriptionChangeType.delete, index, String.Empty);
                        this.onLog(LogType.DictionaryDeletion, d);
                    }

                    return result;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool updateDicionary(Dictionary d, bool log = true)
        {
            bool updated = false;
            // update in memory
            lock (semaphoreDictionaries)
                for (int i = 0; i < dictionaries.Count; i++)
                {
                    if (d.uuid.Equals(dictionaries[i].uuid))
                    {
                        Dictionary<string, dynamic> changes = Dictionary.compareTo(dictionaries[i], d);
                        changes.Add("uuid", d.uuid);
                        this.onChange("dictionary", SubscriptionChangeType.update, i, JsonConvert.SerializeObject(changes));
                        if (log)
                            this.onLog(LogType.DictionaryEdition, dictionaries[i]);
                        dictionaries[i] = d;
                        updated = true;
                        break;
                    }
                }
            if (updated)
            {
                // cascade update in memory
                lock (semaphoreQueue)
                    for (int i = 0; i < queue.Count; i++)
                    {
                        if (queue[i].tipusBusqueda == TipusBusqueda.Diccionari && queue[i].diccionari != null
                            && queue[i].diccionari.uuid != null && queue[i].diccionari.uuid.Equals(d.uuid))
                        {
                            queue[i].diccionari = d;
                            this.onChange("hash", SubscriptionChangeType.update, i, JsonConvert.SerializeObject(queue[i]));
                        }
                    }

                List<Hash> updatedHashes = null;
                if (this.mongo != null)
                {
                    if (!this.mongo.updateDictionary(d, out updatedHashes))
                        return false;
                }
                else if (this.ls != null)
                {
                    // save to disk
                    if (!this.ls.updateDictionary(d, out updatedHashes))
                        return false;
                }
                if (updatedHashes != null)
                {
                    foreach (Hash h in updatedHashes)
                    {
                        this.onChange("hash", SubscriptionChangeType.update, -1, JsonConvert.SerializeObject(h));
                    }
                }
                return (updatedHashes != null);
            }
            return updated;
        }

        public void resumeDictionaryUploadToServer(ref Dictionary d, ref long posInicial, ref bool continuarPutjada)
        {
            lock (semaphoreQueue)
                foreach (Dictionary diccioSel in this.dictionaries)
                {
                    // existeix un fitxer amb el mateix nom
                    if (diccioSel.fileName.Equals(d.fileName))
                    {
                        if (!diccioSel.isReady && diccioSel.sizeInBytes == d.sizeInBytes)
                        {
                            // si es igual pero sense haber sigut processat completament, reanudar la putjada
                            posInicial = diccioSel.uploadProgress;
                            continuarPutjada = true;
                            d = diccioSel;
                            return;
                        }
                        else
                        {
                            // si no son iguals, no es pot continuar perque existeix un fitxer amb el mateix nom
                            return;
                        }
                    }
                }
        }

        /**********/
        /* TOKENS */
        /**********/

        public List<Token> getAllTokens()
        {
            if (this.mongo != null)
            {
                return this.mongo.findToken();
            }
            if (this.ls != null)
            {
                return this.ls.getAllTokens();
            }
            return new List<Token>();
        }

        /// <summary>
        /// creates random token and saves to storage
        /// </summary>
        /// <param name="addr"></param>
        /// <returns></returns>
        public string createToken(string addr)
        {
            bool updated;
            string token = this.generateToken();
            Token newToken = new Token(addr, token);

            if (this.mongo != null)
            {
                updated = this.mongo.insertToken(newToken);
            }
            else if (this.ls != null)
            {
                updated = this.ls.insertToken(newToken);
            }
            else
                return null;

            this.onChange("token", SubscriptionChangeType.insert, -1, JsonConvert.SerializeObject(newToken));
            return updated ? token : null;
        }

        private string generateToken()
        {
            return generateToken(Program.CONFIG.tokenLength);
        }

        private string generateToken(int size)
        {
            const string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            StringBuilder str = new StringBuilder();
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                str.Append(characters[(rand.Next(0, characters.Length))]);
            }
            return str.ToString();
        }

        /// <summary>
        /// revoke token by token object
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public bool revokeToken(Token t)
        {
            return this.revokeToken(t.token);
        }

        /// <summary>
        /// revoke token by token string
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public bool revokeToken(string t)
        {
            bool deleted = false;
            if (this.mongo != null)
            {
                deleted = this.mongo.deleteToken(t);
            }
            else if (this.ls != null)
            {
                deleted = this.ls.deleteToken(t);
            }

            if (deleted)
            {
                this.onChange("token", SubscriptionChangeType.delete, -1, t);
            }

            return deleted;
        }

        public bool revokeAllTokens()
        {
            bool deleted;
            List<Token> tokens;
            if (this.mongo != null)
            {
                tokens = this.mongo.findToken();
                deleted = this.mongo.deleteAllTokens();
            }
            else if (this.ls != null)
            {
                tokens = this.ls.getAllTokens();
                deleted = this.ls.clearTokens();
            }
            else
            {
                return false;
            }

            if (deleted)
            {
                foreach (Token t in tokens)
                {
                    this.onChange("token", SubscriptionChangeType.delete, -1, t.token);
                }
            }
            return deleted;
        }

        /// <summary>
        /// searches the token into the database/file, renews or not the expiry date and return if exists
        /// </summary>
        /// <param name="token"></param>
        /// <param name="addr"></param>
        /// <returns></returns>
        public bool authToken(string addr, string token)
        {
            Token authenticated = null;
            if (this.mongo != null)
            {
                List<Token> results = this.mongo.findToken(addr, token);
                if (results == null || results.Count == 0)
                {
                    return false;
                }
                if (results.Count > 0)
                {
                    authenticated = results[0];
                }
            }
            else if (this.ls != null)
            {
                List<Token> results = this.ls.findToken(addr, token);
                if (results == null || results.Count == 0)
                {
                    return false;
                }
                if (results.Count > 0)
                {
                    authenticated = results[0];
                }
            }
            if (authenticated != null)
            {
                if (!authenticated.isExpired())
                {
                    this.renewToken(authenticated);
                    this.onChange("token", SubscriptionChangeType.update, -1, JsonConvert.SerializeObject(new Token(authenticated)));
                }
                else if (!authenticated.isExpired())
                {
                    this.revokeToken(authenticated);
                    authenticated = null;
                }
            }
            return authenticated != null;
        }

        private bool renewToken(Token t)
        {
            if (this.mongo != null)
            {
                return this.mongo.updateToken(new Token(t));
            }
            else if (this.ls != null)
            {
                return this.ls.updateToken(new Token(t));
            }
            else
            {
                return false;
            }
        }

        /************/
        /* RAINBOWS */
        /************/

        public bool insertRainbow(Rainbow r)
        {
            bool result;
            if (this.mongo != null)
                result = this.mongo.insertRainbow(r);
            else if (this.ls != null)
                result = this.ls.insertRainbow(r);
            else
                return false;

            if (result)
            {
                if (r.enabled)
                    lock (semaphoreRainbows)
                        this.rainbows.Add(r);

                this.onChange("rainbow", SubscriptionChangeType.insert, this.rainbows.Count, JsonConvert.SerializeObject(r));

                this.onLog(LogType.RainbowCreation, r);
            }

            return result;
        }

        public List<Rainbow> getAllRainbows(out int length, int offset = 0, int limit = 0)
        {
            if (this.mongo != null)
                return this.mongo.getAllRainbows(out length, offset, limit);
            else if (this.ls != null)
                return this.ls.getAllRainbows(out length);
            else
                length = -1;

            return null;
        }

        public Rainbow getNextRainbow()
        {
            lock (semaphoreRainbows)
            {
                if (rainbows == null || rainbows.Count == 0)
                {
                    return null;
                }
                return new Rainbow(rainbows[0]);
            }
        }

        public Rainbow findRainbow(string uuid)
        {
            if (this.mongo != null)
                return this.mongo.findRainbow(uuid);
            else if (this.ls != null)
                return this.ls.findRainbow(uuid);
            else
                return null;
        }

        public bool updateRainbow(Rainbow r, bool updateOnly = false)
        {
            int index = -1;

            Dictionary<string, dynamic> update = null;
            lock (semaphoreRainbows)
            {
                //update in memory
                for (int i = 0; i < this.rainbows.Count; i++)
                {
                    if (r.uuid.Equals(this.rainbows[i].uuid))
                    {
                        if (!updateOnly)
                        {
                            update = Rainbow.compareTo(this.rainbows[i], r);
                        }
                        index = i;
                        break;
                    }
                }

                if (index != -1 && !r.enabled) // add or remove from the queue
                {
                    this.rainbows.RemoveAt(index);
                }
                else if (index == -1 && r.enabled)
                {
                    this.rainbows.Add(r);
                }
                else if (index != -1 && r.enabled)
                {
                    this.rainbows[index] = r;
                }
            }

            if (!updateOnly && update == null)
            {
                update = Rainbow.compareTo(this.findRainbow(r.uuid), r);
            }
            if (!updateOnly && update != null)
            {
                update.Add("uuid", r.uuid);
                this.onChange("rainbow", SubscriptionChangeType.update, -1, JsonConvert.SerializeObject(update));
            }


            // update persistently on the database
            bool result;
            if (this.mongo != null)
                result = this.mongo.updateRainbow(r.uuid, r);
            else if (this.ls != null)
                result = this.ls.updateRainbow(r.uuid, r);
            else
                return false;

            if (result)
                this.onLog(LogType.RainbowEdition, r);
            return result;
        }

        private void saveRainbow(Rainbow r)
        {
            if (this.mongo != null)
            {
                this.mongo.updateRainbow(r.uuid, r);
            }
            else if (this.ls != null)
            {
                this.ls.updateRainbow(r.uuid, r);
            }
        }

        public string findPwdInRainbow(string hash, string rainbowUUID)
        {
            try
            {
                LevelDBStorage storage = LevelDBStorage.getInstance(rainbowUUID);
                return storage.getComputedHash(hash);
            }
            catch (Exception)
            {
                return "ERR";
            }
        }

        public bool removeRainbow(string uuid)
        {
            // search the rainbow to remote in the database, exit if it doensn't exists
            Rainbow r = this.findRainbow(uuid);
            if (r == null) return false;
            // remove all the computed hashes into the rainbow object
            LevelDBStorage storage = LevelDBStorage.getInstance(uuid);
            storage.removeComputedHashesStorage();
            LevelDBStorage.deleteInstance(uuid);
            storage = null;
            // remove the rainbow object from the database
            if (this.mongo != null)
            {
                if (!this.mongo.deleteRainbow(r))
                    return false;
            }
            else
            {
                if (!this.ls.deleteRainbow(r))
                    return false;
            }
            // delete from memory
            lock (semaphoreRainbows)
            {
                this.rainbows.Remove(r);
            }
            // emit deletion event
            this.onChange("rainbow", SubscriptionChangeType.delete, -1, r.uuid);
            this.onLog(LogType.RainbowDeletion, r);
            // return the result from deleting from the database
            return true;
        }

        public void rainbowPendingCombination(DictionaryProcessingChunk combination)
        {
            lock (semaphoreRainbows)
                if (this.rainbows != null && this.rainbows.Count > 0)
                    this.rainbows[0].hash.addPendingCombination(combination);
        }

        public void rainbowPendingCombination(BruteForceChunk combination)
        {
            lock (semaphoreRainbows)
                if (this.rainbows != null && this.rainbows.Count > 0)
                    this.rainbows[0].hash.addPendingCombination(combination);
        }

        /****************/
        /* CERTIFICATES */
        /****************/

        public bool insertCertificate(CertificateInfo certificateInfo)
        {
            certificateInfo.creationDate = DateTime.UtcNow;
            if (this.mongo != null)
            {
                return this.mongo.insertCertificate(certificateInfo);
            }
            else if (this.ls != null)
            {
                return this.ls.insertCertificate(certificateInfo);
            }

            return false;
        }

        public List<CertificateInfo> listCertificates()
        {
            if (this.mongo != null)
            {
                return this.mongo.listCertificates();
            }
            else if (this.ls != null)
            {
                return this.ls.listCertificates();
            }

            return new List<CertificateInfo>();
        }

        public CertificateInfo getCertificate(string uuid)
        {
            if (this.mongo != null)
            {
                return this.mongo.getCertificate(uuid);
            }
            else if (this.ls != null)
            {
                return this.ls.getCertificate(uuid);
            }

            return null;
        }

        public bool removeCertificate(string uuid)
        {
            if (this.mongo != null)
            {
                return this.mongo.removeCertificate(uuid);
            }
            else if (this.ls != null)
            {
                return this.ls.removeCertificate(uuid);
            }

            return false;
        }

        /// <summary>
        /// this function is called when the storage object is initialized in order 
        /// to delete from the database all the certificates that are no longer present in the file system,
        /// and to delete from the file system the certificates not registered in the database.
        /// </summary>
        private void clearCertificatesGarbage()
        {
            try
            {
                // create the certificates folder if it doesn't exist
                if (!Directory.Exists("./cert/")) Directory.CreateDirectory("./cert/");

                // list the database certificates
                List<CertificateInfo> certificates = this.listCertificates();

                // delete from the database if the file does not exist
                foreach (CertificateInfo cert in certificates)
                {
                    if (!File.Exists(Path.Combine("./cert", cert.uuid)))
                    {
                        this.removeCertificate(cert.uuid);
                    }
                }

                // list the database certificates
                certificates = this.listCertificates();

                // list the directory
                FileInfo[] files = new DirectoryInfo("./cert/").GetFiles();

                // delete from the file system if it's not registered in the database
                foreach (FileInfo file in files)
                {
                    bool found = false;
                    foreach (CertificateInfo cert in certificates)
                    {
                        if (cert.uuid.Equals(file.Name))
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        File.Delete(file.FullName);
                    }
                }
            }
            catch (Exception) { }
        }

        /********/
        /* LOGS */
        /********/

        public void onLog(LogType type, dynamic data)
        {
            if (!this.loggingSettings.log)
                return;

            // check if the logging is enabled
            switch (type)
            {
                case LogType.ClientConnection:
                case LogType.ClientDisconnection:
                case LogType.ClientKick:
                case LogType.ClientBan:
                    {
                        if (!this.loggingSettings.logClients)
                            return;
                        break;
                    }
                case LogType.InsertHash:
                case LogType.FinishHash:
                case LogType.EditHash:
                case LogType.SurrenderHash:
                case LogType.DeleteHash:
                    {
                        if (!this.loggingSettings.logHashes)
                            return;
                        break;
                    }
                case LogType.WebClientConnection:
                case LogType.WebClientDisconnection:
                    {
                        if (!this.loggingSettings.logWeb)
                            return;
                        break;
                    }
                case LogType.RainbowCreation:
                case LogType.RainbowEdition:
                case LogType.RainbowDeletion:
                    {
                        if (!this.loggingSettings.logRainbows)
                            return;
                        break;
                    }
                case LogType.DictionaryCreation:
                case LogType.DictionaryEdition:
                case LogType.DictionaryDeletion:
                    {
                        if (!this.loggingSettings.logDictionaries)
                            return;
                        break;
                    }
                case LogType.SettingsUpdated:
                    {
                        if (!this.loggingSettings.logSettings)
                            return;
                        break;
                    }
            }

            Log newLog = new Log(type, data);

            if (this.mongo != null)
            {
                this.mongo.insertLog(newLog);
            }
            else if (this.ls != null)
            {
                this.ls.insertLog(newLog);
            }

            this.onChange("logs", SubscriptionChangeType.insert, -1, JsonConvert.SerializeObject(newLog));
        }

        public List<Log> getLogs(DateTime start, int offset, int limit)
        {
            if (this.mongo != null)
            {
                return this.mongo.getLogs(start, offset, limit);
            }
            else if (this.ls != null)
            {
                return this.ls.getLogs(start, offset, limit);
            }
            return null;
        }

        public bool clearLogs()
        {
            if (this.mongo != null)
            {
                return this.mongo.clearLogs();
            }
            else if (this.ls != null)
            {
                return this.ls.clearLogs();
            }
            return false;
        }

    } // class Storage
} // namespace
