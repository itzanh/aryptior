﻿using Newtonsoft.Json;
using SharpCompress.Archives;
using SharpCompress.Archives.GZip;
using SharpCompress.Archives.Tar;
using SharpCompress.Common;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace serverHashes
{
    public class BackupCron
    {
        /// <summary>
        /// State of initialization of the Backup Cron
        /// </summary>
        public enum BackupCronState
        {
            stopped,
            starting,
            idle,
            backingUp,
            restoring,
            error
        }

        // delegates
        public delegate void OnBackupCompleted(Backup b);
        public delegate void OnBackupDeleted(string uuid);
        public delegate void OnBackupCronStateChanged(BackupCronState cronState);

        // attributes
        private MongoDBStorage mongo;
        private int timeout;
        private string[] collections;
        public BackupCronState cronState;
        private Thread t;
        private byte[] AESKey;
        public long sizeLimit;
        public DateTime nextBackup;
        public OnBackupCompleted onBackupCompleted;
        public OnBackupDeleted onBackupDeleted;
        public OnBackupCronStateChanged onBackupCronStateChanged;

        // constants
        [JsonIgnore]
        public const string BACKUP_TMP_DIR = "./backup/temp/";
        [JsonIgnore]
        public const string BACKUP_STORAGE_DIR = "./backup/storage/";
        [JsonIgnore]
        public const string BACKUP_UPLOAD_DIR = "./backup/upload/";

        // constructors
        private BackupCron()
        {
            this.cronState = BackupCronState.stopped;
            this.t = null;
            this.onBackupCompleted = (Backup b) => { };
            this.onBackupDeleted = (string uuid) => { };
        }

        public BackupCron(MongoDBStorage mongo, int timeout, long sizeLimit, string[] collections, string AESKey) : this()
        {
            this.mongo = mongo;
            this.timeout = timeout;
            this.sizeLimit = sizeLimit;
            this.collections = collections;
            this.AESKey = stringToByteArray(AESKey);
        }

        public static byte[] stringToByteArray(string hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public bool Run()
        {
            try
            {
                this.cronState = BackupCronState.starting;
                this.onBackupCronStateChanged(this.cronState);
                if (!initializeBackupStorage())
                {
                    this.onBackupCronStateChanged(this.cronState);
                    this.cronState = BackupCronState.error;
                    return false;
                }
                this.onBackupCronStateChanged(this.cronState);
                this.cronState = BackupCronState.idle;
                t = new Thread(async () =>
                {
                    await runCronAsync();
                });
                t.Start();
                return true;
            }
            catch (Exception) { return false; }
        }

        public bool Stop()
        {
            try
            {
                if (this.t != null && t.IsAlive)
                {
                    t.Abort();
                    this.cronState = BackupCronState.stopped;
                    this.onBackupCronStateChanged(this.cronState);
                }
                return true;
            }
            catch (Exception) { return false; }
        }

        public bool triggerBackup()
        {
            try
            {
                if (this.cronState != BackupCronState.idle) return false;
                if (t.ThreadState == ThreadState.WaitSleepJoin)
                {
                    new Thread(new ThreadStart(() =>
                    {
                        t.Abort();
                        Thread triggered = new Thread(new ThreadStart(async () =>
                        {
                            await backupAsync();
                        }));
                        triggered.Start();
                        triggered.Join();
                        this.cronState = BackupCronState.idle;
                        this.onBackupCronStateChanged(this.cronState);
                        t = new Thread(async () =>
                        {
                            await runCronAsync();
                        });
                        t.Start();
                    })).Start();
                    return true;
                }
            }
            catch (Exception e) { Console.WriteLine(e.ToString()); }
            return false;
        }

        private bool initializeBackupStorage()
        {
            try
            {
                if (!Directory.Exists("./backup")) Directory.CreateDirectory("./backup");
                if (!Directory.Exists(BACKUP_STORAGE_DIR)) Directory.CreateDirectory(BACKUP_STORAGE_DIR);
                if (!Directory.Exists(BACKUP_TMP_DIR)) Directory.CreateDirectory(BACKUP_TMP_DIR);
                if (!Directory.Exists(BACKUP_UPLOAD_DIR)) Directory.CreateDirectory(BACKUP_UPLOAD_DIR);
                else foreach (FileInfo file in new DirectoryInfo(BACKUP_TMP_DIR).GetFiles())
                    {
                        file.Delete();
                    }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task runCronAsync()
        {
            this.cronState = BackupCronState.idle;
            this.onBackupCronStateChanged(this.cronState);
            while (true)
            {
                this.nextBackup = DateTime.UtcNow.AddMilliseconds(this.timeout);
                this.onBackupCronStateChanged(this.cronState);
                Thread.Sleep(this.timeout);
                await backupAsync();
                this.cronState = BackupCronState.idle;
                this.onBackupCronStateChanged(this.cronState);
                //Thread.Sleep(this.timeout);

            }
        }

        private async Task backupAsync()
        {
            if (this.mongo == null) return;

            const string TAR_FILENAME = "backup.tar";
            const string GZIP_FILENAME = "backup.tar.gzip";
            const string ENCRYPT_FILENAME = "backup.tar.gzip.encrypt";

            // check the cron state
            if (this.cronState != BackupCronState.idle) return;
            this.cronState = BackupCronState.backingUp;
            this.onBackupCronStateChanged(this.cronState);

            // create a new backup object
            Backup b = new Backup();

            // backup every collection to a file in the temporary folder
            foreach (string collectionName in this.collections)
            {
                if (this.mongo != null)
                    await this.mongo.WriteCollectionToFile(collectionName, BACKUP_TMP_DIR + collectionName + ".collection");
            }

            // tar the temporary directory
            TarArchive tarArchive = TarArchive.Create();
            tarArchive.AddAllFromDirectory(BACKUP_TMP_DIR);
            tarArchive.SaveTo(BACKUP_TMP_DIR + TAR_FILENAME, CompressionType.None);
            tarArchive.Dispose();

            // gzip compress the tar backup
            this.gZipCompress(BACKUP_TMP_DIR, TAR_FILENAME, GZIP_FILENAME);

            // ecrypt with AES the current temporary file
            this.AESEncrypt(BACKUP_TMP_DIR + GZIP_FILENAME, BACKUP_TMP_DIR + ENCRYPT_FILENAME, this.AESKey);

            // set the hash and size of the backup
            b.size = new FileInfo(BACKUP_TMP_DIR + ENCRYPT_FILENAME).Length;
            b.hash = SHA512CheckSum(BACKUP_TMP_DIR + ENCRYPT_FILENAME);

            // move to the correct folder
            File.Move(BACKUP_TMP_DIR + ENCRYPT_FILENAME, BACKUP_STORAGE_DIR + b.toFileName());

            // save the backup to the database
            this.mongo.insertBackup(b);

            // emit backup event
            this.onBackupCompleted(b);

            // clear temporary folder
            if (Directory.Exists(BACKUP_TMP_DIR)) foreach (FileInfo file in new DirectoryInfo(BACKUP_TMP_DIR).GetFiles())
                {
                    file.Delete();
                }

            this.clearOlderBackups();
        }

        private void gZipCompress(string path, string input, string output)
        {
            GZipArchive gZipArchive = GZipArchive.Create();
            gZipArchive.AddEntry(input, path + input);
            gZipArchive.SaveTo(path + output, CompressionType.Deflate);
            gZipArchive.Dispose();
        }

        private void AESEncrypt(string input, string output, byte[] key)
        {
            FileStream fsOut = new FileStream(output, FileMode.Create);

            Aes aesAlg = Aes.Create();
            aesAlg.Key = key;

            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
            CryptoStream csEncrypt = new CryptoStream(fsOut, encryptor, CryptoStreamMode.Write);

            FileStream fsIn = new FileStream(input, FileMode.Open);

            byte[] iv = new byte[aesAlg.IV.Length];
            Array.Copy(aesAlg.IV, 0, iv, 0, aesAlg.IV.Length);
            fsOut.Write(iv, 0, iv.Length);

            int data;
            while ((data = fsIn.ReadByte()) != -1)
                csEncrypt.WriteByte((byte)data);

            fsIn.Close();
            csEncrypt.Close();
            fsOut.Close();
        }

        private string SHA512CheckSum(string filePath)
        {
            using (FileStream fileStream = File.OpenRead(filePath))
            {
                SHA512 sha512 = SHA512.Create();
                return hashToHex(sha512.ComputeHash(fileStream));
            }
        }

        private static string hashToHex(byte[] hash)
        {
            StringBuilder str = new StringBuilder();
            foreach (byte bSel in hash)
            {
                str.Append(bSel.ToString("x2"));
            }
            return str.ToString();
        }

        private void clearOlderBackups()
        {
            if (this.mongo == null) return;

            // if the cron was set to elete the backups in a size bound, compute the current size of the backup directory
            if (this.sizeLimit > 0)
            {
                // get the total size of the directory
                long dirSize = 0;
                foreach (FileInfo file in new DirectoryInfo(BACKUP_STORAGE_DIR).GetFiles())
                {
                    dirSize += file.Length;
                }

                // clear older backups if the file size has been exceeded
                if (dirSize > this.sizeLimit)
                {
                    FileInfo[] fileInfo = new DirectoryInfo(BACKUP_STORAGE_DIR).GetFiles();

                    // sort file names by date
                    this.sortBackupNamesByDate(ref fileInfo);

                    // delete until the enough space is freed
                    for (int i = 0; (i < fileInfo.Length) && (dirSize > this.sizeLimit); i++)
                    {
                        FileInfo file = fileInfo[i];

                        dirSize -= file.Length;
                        file.Delete();
                        this.mongo.deleteBackup(file.Name.Split('_')[0]);
                        this.onBackupDeleted(file.Name.Split('_')[0]);
                    }
                }
            }
        }

        private bool sortBackupNamesByDate(ref FileInfo[] fileInfo)
        {
            for (int i = 0; i < fileInfo.Length; i++)
            {
                for (int j = (i % 2); j < fileInfo.Length; j += 2)
                {
                    if ((j + 1) >= fileInfo.Length) continue;

                    // get the dates
                    DateTime dateTime1;
                    if (!this.dateFromBackupFileName(fileInfo[j].Name, out dateTime1)) return false;
                    DateTime dateTime2;
                    if (!this.dateFromBackupFileName(fileInfo[(j + 1)].Name, out dateTime2)) return false;


                    if (DateTime.Compare(dateTime1, dateTime2) > 0)
                    {
                        // flip the order
                        FileInfo aux = fileInfo[j];
                        fileInfo[j] = fileInfo[(j + 1)];
                        fileInfo[(j + 1)] = aux;
                    }
                }
            }
            return true;
        }

        private bool dateFromBackupFileName(string fileName, out DateTime dateTimeOutput)
        {
            try
            {
                string formattedDate = fileName.Split('_')[1];
                string[] date = formattedDate.Split(',')[0].Split('-');
                string[] time = formattedDate.Split(',')[1].Split('-');

                dateTimeOutput = new DateTime(Int32.Parse(date[0]), Int32.Parse(date[1]), Int32.Parse(date[2]),
                    Int32.Parse(time[0]), Int32.Parse(time[1]), Int32.Parse(time[2]));
                return true;
            }
            catch (Exception)
            {
                dateTimeOutput = new DateTime();
                return false;
            }
        }

        public bool deleteBackup(string uuid)
        {
            if (this.mongo == null) return false;
            try
            {
                Backup b = this.mongo.getBackup(uuid);
                if (b == null) return false;
                new FileInfo(BACKUP_STORAGE_DIR + b.toFileName()).Delete();
                this.mongo.deleteBackup(uuid);
                this.onBackupDeleted(uuid);
                return true;
            }
            catch (Exception) { return false; }
        }
    }
}
