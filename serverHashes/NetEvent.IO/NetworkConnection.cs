﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;

namespace serverHashes
{
    public class NetworkConnection : NetworkConnectionAbstract
    {
        /// <summary>
        /// TCP connection with a worker client.
        /// </summary>
        public TcpClient client;
        /// <summary>
        /// NetworkStream with the client.
        /// </summary>
        public NetworkStream ns;
        /// <summary>
        /// Secure sockets layer connection with the client, if it's set to use it.
        /// </summary>
        public SslStream ssl;
        /// <summary>
        /// The NetworkStream or SslStream to send the information through, as the connection may be secure or insecure and may not change.
        /// </summary>
        public Stream s;
        /// <summary>
        /// IP address of the remote endpoint
        /// </summary>
        public override string addr
        {
            get { return _addr; }
        }
        private string _addr;
        /// <summary>
        /// Get whether the current connection is secure or not.
        /// </summary>
        public override bool isSecure => this.ssl != null;
        /// <summary>
        /// Date and time when this client connected.
        /// </summary>
        public readonly DateTime timeConnected;
        /// <summary>
        /// Sets whether if the client is currently connected
        /// </summary>
        public bool connected;
        /// <summary>
        /// Sets the socket options to use, encryption, compression, etc.
        /// </summary>
        private SocketOptions options;
        /// <summary>
        /// object used to lock the receiveData method and use the socket thread-safely
        /// </summary>
        private readonly object lockReceiveData = new object();
        /// <summary>
        /// object used to lock the sendData method and use the socket thread-safely
        /// </summary>
        private readonly object lockSendData = new object();



        public NetworkConnection()
        {
            this.timeConnected = DateTime.UtcNow;
            this.connected = false;
        }

        public NetworkConnection(TcpClient client, NetworkStream ns) : this()
        {
            this.client = client;
            this.ns = ns;
            this.ssl = null;
            this.s = null;

            this.client.NoDelay = true;
            this._addr = client.Client.RemoteEndPoint.ToString();
        }

        public override bool isConnected()
        {
            return this.client.Connected && this.connected;
        }

        /// <summary>
        /// the packet can be received fragmented, so the program keeps reading and appending to the current position
        /// till the entire message is read
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private byte[] readNetworkData(int length)
        {
            try
            {
                byte[] savedData = new byte[length];
                int read = 0;
                while (read < length)
                {
                    read += this.s.Read(savedData, read, (length - read));
                    if (read == 0)
                        return null;
                }
                return savedData;
            }
            catch (Exception e) { Console.WriteLine(e.ToString()); }
            return null;
        }

        /// <summary>
        /// Reads binary data from the network connection
        /// </summary>
        /// <returns>
        /// received binary data or null if it's not possible to read data
        /// </returns>
        public override bool receiveData(out byte[] resultReceivedData, out bool isBinary)
        {
            resultReceivedData = null;
            isBinary = false;
            lock (lockReceiveData)
                try
                {
                    // integer to receive which contains el size of the information it's about to be sent
                    byte[] sizeToReceive = this.readNetworkData(4);
                    if (sizeToReceive == null)
                        return false;

                    // the information that is about to be received through the socket is read with the previously specified size
                    uint dataSize = BitConverter.ToUInt32(sizeToReceive, 0);

                    // check the maximum message size bound
                    if (Program.CONFIG.socketSettings.maxMessageSize > 0 && dataSize > Program.CONFIG.socketSettings.maxMessageSize)
                        this.disconnect();

                    byte[] parameters = this.readNetworkData(1);
                    if (parameters == null)
                        return false;
                    if (parameters[0] == 1)
                        isBinary = true;

                    byte[] receivedData = this.readNetworkData((int)dataSize);

                    // optionally dencrypt or uncompress
                    MemoryStream ms = new MemoryStream(receivedData);
                    if (this.options.compression)
                        ms = uncompressData(ms);

                    resultReceivedData = ms.ToArray();
                    return true;
                }
                catch (Exception)
                {
                    resultReceivedData = null;
                    isBinary = false;
                    return false;
                }
        }

        private string receiveData()
        {
            byte[] receivedData;
            bool isBinary;
            this.receiveData(out receivedData, out isBinary);
            if (isBinary) return null;
            return Encoding.UTF8.GetString(receivedData);
        }

        /// <summary>
        /// Send the data contained by the string in the parameter to the server.
        /// </summary>
        /// <param name="dataToSend"></param>
        /// <returns>
        ///     true if the data was sent successfully to the remote endpoint
        ///     false if there was a problem while sending
        /// </returns>
        public override bool sendData(byte[] dataToSend, bool isBinary)
        {
            lock (lockSendData)
                try
                {
                    MemoryStream dataToBeSent = new MemoryStream(dataToSend);
                    if (this.options.compression)
                        dataToBeSent = compressData(dataToBeSent);

                    dataToBeSent.Position = 0;

                    // calculate the size of what is going to be sent
                    byte[] sizeInfo = BitConverter.GetBytes((uint)dataToBeSent.Length);

                    // make know how many information it's about to be sent
                    this.s.Write(sizeInfo, 0, sizeInfo.Length);
                    this.s.Flush();

                    // append the binary parameter
                    if (isBinary)
                        this.s.Write(new byte[] { 1 }, 0, 1);
                    else
                        this.s.Write(new byte[] { 0 }, 0, 1);

                    // send the information through the socket
                    dataToBeSent.WriteTo(this.s);
                    this.ns.Flush();

                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
        }

        private bool sendData(string dataToSend)
        {
            return this.sendData(Encoding.UTF8.GetBytes(dataToSend), false);
        }

        /*********************/
        /* COMPRESSION LAYER */
        /*********************/

        private MemoryStream compressData(MemoryStream input)
        {
            try
            {
                return new MemoryStream(IronSnappy.Snappy.Encode(input.ToArray()));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        private MemoryStream uncompressData(MemoryStream input)
        {
            input.Position = 0;

            try
            {
                return new MemoryStream(IronSnappy.Snappy.Decode(input.ToArray()));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        /************************/
        /* SECURE SOCKETS LAYER */
        /************************/

        private SslStream initializeSSL(TcpClient client, X509Certificate2 certificate, int timeout)
        {
            SslStream sslStream = null;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                // A client has connected. Create the 
                // SslStream using the client's network stream.
                sslStream = new SslStream(
                    client.GetStream(), true);

                // Authenticate the server but don't require the client to authenticate.
                sslStream.AuthenticateAsServer(certificate, clientCertificateRequired: false, checkCertificateRevocation: true);

                // Set timeouts for the read and write to 50 seconds.
                sslStream.ReadTimeout = timeout == 0 ? Timeout.Infinite : timeout;
                sslStream.WriteTimeout = timeout == 0 ? Timeout.Infinite : timeout;
                // Read a message from the client.   
                return sslStream;
            }
            catch (AuthenticationException e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
                if (e.InnerException != null)
                {
                    Console.WriteLine("Inner exception: {0}", e.InnerException.Message);
                }
                Console.WriteLine("Authentication failed - closing the connection.");
                if (sslStream != null)
                    sslStream.Close();
                return null;
            }
        }

        public override void disconnect()
        {
            this.connected = false;
            this.ns.Close();
            this.client.Close();
        }

        public bool handshake(bool mustUseServerSettings, SocketOptions options)
        {
            try
            {
                this.options = new SocketOptions(false, false);
                this.s = this.ns;

                string handshake = this.receiveData();
                if (handshake == null)
                {
                    this.connected = false;
                    return false;
                }

                if (handshake.Equals("DEFAULT")) // if the client doesn't have a setting, send a default one from this server
                {
                    this.sendData("OK$" + JsonConvert.SerializeObject(new ClientSocketOptions(options)));
                    this.options = options;
                    this.connected = true;
                }
                else
                {
                    ClientSocketOptions handshakeOptions = (ClientSocketOptions)JsonConvert.DeserializeObject(handshake, typeof(ClientSocketOptions));

                    if (mustUseServerSettings) // check if client's settings matches the server's settings
                    {
                        if (handshakeOptions.encryption != options.encryption
                            || handshakeOptions.compression != options.compression)
                        {
                            this.sendData("ERR$" + JsonConvert.SerializeObject(new ClientSocketOptions(options)));
                            this.connected = false;
                        }
                        else
                        {
                            this.sendData("OK");
                            this.options = options;
                            this.connected = true;
                        }
                    }
                    else // allow client's settings
                    {
                        this.sendData("OK");
                        this.options = options;
                        this.options.addOptions(handshakeOptions);
                        this.connected = true;
                    }
                }

                if (!this.connected) return false;

                if (this.options.encryption)
                {
                    this.ssl = this.initializeSSL(client,
                        new X509Certificate2(Program.storage.ls.readCertificate(this.options.certUUID), this.options.password),
                        this.options.networkTimeout);
                    this.connected = (this.ssl != null);

                    this.s = this.ssl;
                }
                else
                {
                    int timeout = this.options.networkTimeout;
                    this.ns.ReadTimeout = timeout == 0 ? Timeout.Infinite : timeout;
                    this.ns.WriteTimeout = timeout == 0 ? Timeout.Infinite : timeout;

                    this.s = this.ns;
                }

                return this.connected;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

    }
}
