﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace serverHashes
{
    [Serializable]
    public class NetworkClient
    {
        /// <summary>
        /// IP address of the remote endpoint
        /// </summary>
        public readonly string addr;
        /// <summary>
        /// Date and time when this client connected.
        /// </summary>
        public readonly DateTime timeConnected;
        /// <summary>
        /// Tells whatever what this client is currently processing is a rainbow or not.
        /// </summary>
        public bool isProcessingRainbow;
        /// <summary>
        /// Indicates if the client is currently working on a hash
        /// </summary>
        public bool processing;
        /// <summary>
        /// In case of being processing a hash (the above 'processing' property is set to true) this variable contains the UUID of the hash being processed.
        /// If the client is IDLE (the 'processing' property is set to false), this string object will be set to null;
        /// </summary>
        public string processingUUID;
        /// <summary>
        /// If the clients is processing, and the hash is processed via brute-force, contains what's the combination 
        /// that's currently being processed by this client.
        /// Used in the event of a disconnection to queue this client's unfinished work, so other ones can finish it.
        /// </summary>
        public BruteForceChunk bruteForceChunk;
        /// <summary>
        /// If the clients is processing, and the hash is processed via dictionary search, contains what's the list of words
        /// that are currently being processed by this client.
        /// Used in the event of a disconnection to queue this client's unfinished work, so other ones can finish it.
        /// </summary>
        public DictionaryProcessingChunk dictionaryProcessingChunk;
        /// <summary>
        /// Rate of hashes processed by ms in the last cycle.
        /// </summary>
        public double hashRatePerMs = -1;
        /// <summary>
        /// Storage to use when reporting the result of processing keys in a rainbow.
        /// </summary>
        [JsonIgnore]
        public LevelDBStorage rainbowStorage = null;
        /// <summary>
        /// Time of the last new part requested by brute force or dictionary.
        /// </summary>
        public DateTime lastNewPartRequested;
        /// <summary>
        /// Time of the last report of the last newpart by brute force or dictionary.
        /// </summary>
        public DateTime lastReportedPart;
        /// <summary>
        /// NetEvent.IO object to send events to the client.
        /// </summary>
        [JsonIgnore]
        public NetEventIO connection;
        /// <summary>
        /// Used when processing hashes by dictionary, so it's not needed to reopen the file.
        /// </summary>
        [JsonIgnore]
        public StreamReader dictionaryReader;

        public NetworkClient()
        {
            this.isProcessingRainbow = false;
            this.processing = false;
            this.processingUUID = null;
            this.timeConnected = DateTime.UtcNow;

            this.dictionaryReader = null;
        }

        public NetworkClient(string addr) : this()
        {
            this.addr = addr;
        }
    }

    public class NetworkClientUpdate
    {
        public string uuid;
        public NetworkClient client;

        public NetworkClientUpdate(string uuid, NetworkClient client)
        {
            this.uuid = uuid;
            this.client = client;
        }
    }
}
