﻿using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System;
using Newtonsoft.Json;
using System.Threading;

namespace serverHashes
{
    public class NetworkDiscovery
    {
        public const int discoveryPort = 22217;
        public const int discovertResponsePort = 22218;
        public const string discoveryMessage = "$ARYPTIOR$9d09fba2-eff2-400a-9e84-04e7660b9348$";
        private readonly string discoveryAddress;
        private NetworkDiscoveryData data;
        private Semaphore networkDiscoveryDataMUTEX = new Semaphore(1, 1);
        private UdpClient udpClient = null;

        public NetworkDiscovery(string discoveryAddress, NetworkDiscoveryData data)
        {
            this.discoveryAddress = discoveryAddress;
            this.data = data;
        }

        public void Run()
        {
            this.udpClient = new UdpClient();
            udpClient.Client.Bind(new IPEndPoint(IPAddress.Parse(this.discoveryAddress), discoveryPort));

            IPEndPoint from = new IPEndPoint(0, 0);

            Task.Run(() =>
            {
                while (true)
                {
                    try
                    {
                        byte[] recvBuffer = udpClient.Receive(ref from);
                        string message = Encoding.UTF8.GetString(recvBuffer);

                        if (message.Equals(discoveryMessage))
                        {
                            networkDiscoveryDataMUTEX.WaitOne();
                            byte[] data = Encoding.UTF8.GetBytes("$ARYPTIOR$1$" + JsonConvert.SerializeObject(this.data) + "$");
                            networkDiscoveryDataMUTEX.Release();
                            udpClient.Send(data, data.Length, from.ToString().Split(':')[0], discovertResponsePort);
                        }
                    }
                    catch (Exception) { }
                }
            });

        }

        public void setNetworkDiscoveryData(NetworkDiscoveryData ndd)
        {
            networkDiscoveryDataMUTEX.WaitOne();
            this.data = ndd;
            networkDiscoveryDataMUTEX.Release();
        }

        public void Stop()
        {
            this.udpClient.Close();
            this.udpClient.Dispose();
        }
    }

    [Serializable]
    public class NetworkDiscoveryData
    {
        public int portSocket;
        public int portWS;
        public string name;

        public NetworkDiscoveryData()
        {
            this.portSocket = 22215;
            this.portWS = 22216;
            this.name = "";
        }

        public NetworkDiscoveryData(int portSocket, int portWS, string name)
        {
            this.portSocket = portSocket;
            this.portWS = portWS;
            this.name = name;
        }
    }
}
