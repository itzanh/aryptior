﻿using HashLib;
using SauceControl.Blake2Fast;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace serverHashes
{
    public static class Utilities
    {
        public static string computeSHA512Hash(string cleartextPwd, int iterations = 1)
        {
            SHA512 sha512 = SHA512.Create();
            byte[] hashBinari = Encoding.UTF8.GetBytes(cleartextPwd);
            for (int i = 0; i < iterations; i++)
                hashBinari = sha512.ComputeHash(hashBinari);

            sha512.Dispose();
            return hashToHex(hashBinari);
        }

        public static bool verifyHash(string hash, string cleartextPwd, Tipus type, int iterations = 1)
        {
            switch (type)
            {
                case Tipus.BCRYPT:
                    {
                        return BCrypt.Net.BCrypt.Verify(cleartextPwd, hash);
                    }
                case Tipus.BLAKE:
                    {
                        IHash ihash = HashFactory.Crypto.SHA3.CreateBlake((HashSize)(hash.Length / 2));
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.BLAKE2b:
                    {
                        return hashToHex(Blake2b.ComputeHash(new Span<byte>(Encoding.UTF8.GetBytes(cleartextPwd))))
                    .Equals(hash);
                    }
                case Tipus.BLAKE2s:
                    {
                        return hashToHex(Blake2s.ComputeHash(new Span<byte>(Encoding.UTF8.GetBytes(cleartextPwd))))
                    .Equals(hash);
                    }
                case Tipus.WHIRLPOOL:
                    {
                        IHash ihash = HashFactory.Crypto.CreateWhirlpool();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.RIPEMD128:
                    {
                        IHash ihash = HashFactory.Crypto.CreateRIPEMD128();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.RIPEMD256:
                    {
                        IHash ihash = HashFactory.Crypto.CreateRIPEMD256();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.RIPEMD320:
                    {
                        IHash ihash = HashFactory.Crypto.CreateRIPEMD320();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.TIGER:
                    {
                        IHash ihash = HashFactory.Crypto.CreateTiger((HashRounds)iterations);
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.TIGER2:
                    {
                        IHash ihash = HashFactory.Crypto.CreateTiger2();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.TIGER3:
                    {
                        IHash ihash = HashFactory.Crypto.CreateTiger_3_192();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.TIGER4:
                    {
                        IHash ihash = HashFactory.Crypto.CreateTiger_4_192();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.MODULAR:
                    {
                        return CryptSharp.Crypter.CheckPassword(cleartextPwd, hash);
                    }
                case Tipus.MYSQL:
                    {
                        return (GenerateMySQL5PasswordHash(cleartextPwd)).Equals(hash);
                    }
                case Tipus.WORDPRESS:
                    {
                        return (WordPressHashEncode(cleartextPwd, hash)).Equals(hash);
                    }
            }


            if (type == Tipus.NTLM || type == Tipus.MD4)
            {
                string currentHash = cleartextPwd;
                if (type == Tipus.NTLM)
                {
                    // generar NTLM
                    string hashInNTLM = "";
                    char[] combinationCharacters = currentHash.ToCharArray();
                    for (int j = 0; j < combinationCharacters.Length; j++)
                    {
                        hashInNTLM += combinationCharacters[j];
                        hashInNTLM += '\x00';
                    }
                    currentHash = hashInNTLM;
                }
                for (int i = 0; i < iterations; i++)
                    switch (type)
                    {
                        case Tipus.NTLM:
                            {

                                currentHash = calcularMD4(currentHash).ToUpper();
                                break;
                            }
                        case Tipus.MD4:
                            {
                                currentHash = calcularMD4(currentHash);
                                break;
                            }
                    }
                return currentHash.Equals(hash);
            }
            else
            {
                byte[] currentHash = Encoding.UTF8.GetBytes(cleartextPwd);
                for (int i = 0; i < iterations; i++)
                {
                    switch (type)
                    {
                        case Tipus.MD5:
                            {
                                MD5 md5 = MD5.Create();
                                currentHash = md5.ComputeHash(currentHash);
                                md5.Dispose();
                                break;
                            }
                        case Tipus.SHA1:
                            {
                                SHA1 sha1 = SHA1.Create();
                                currentHash = sha1.ComputeHash(currentHash);
                                sha1.Dispose();
                                break;
                            }
                        case Tipus.SHA256:
                            {
                                SHA256 sha256 = SHA256.Create();
                                currentHash = sha256.ComputeHash(currentHash);
                                sha256.Dispose();
                                break;
                            }
                        case Tipus.SHA384:
                            {
                                SHA384 sha384 = SHA384.Create();
                                currentHash = sha384.ComputeHash(currentHash);
                                break;
                            }
                        case Tipus.SHA512:
                            {
                                SHA512 sha512 = SHA512.Create();
                                currentHash = sha512.ComputeHash(currentHash);
                                sha512.Dispose();
                                break;
                            }
                        case Tipus.RIPEMD160:
                            {
                                RIPEMD160 rIPEMD160 = RIPEMD160.Create();
                                currentHash = rIPEMD160.ComputeHash(currentHash);
                                rIPEMD160.Dispose();
                                break;
                            }
                    }
                }
                return hashToHex(currentHash).Equals(hash);
            }
        }

        public static string hashToHex(byte[] hash)
        {
            StringBuilder str = new StringBuilder();
            foreach (byte bSel in hash)
            {
                str.Append(bSel.ToString("x2"));
            }
            return str.ToString();
        }

        private static string GenerateMySQL5PasswordHash(string password)
        {
            return GenerateMySQL5PasswordHash(password, Encoding.UTF8);
        }

        private static string GenerateMySQL5PasswordHash(string password, Encoding textEncoding)
        {
            byte[] passBytes = textEncoding.GetBytes(password);
            byte[] hash;
            using (var sha1 = SHA1.Create())
            {
                hash = sha1.ComputeHash(passBytes);
                hash = sha1.ComputeHash(hash);
            }

            var sb = new StringBuilder();
            sb.Append("*");
            for (int i = 0; i < hash.Length; i++)
                sb.AppendFormat("{0:X2}", hash[i]);

            return sb.ToString();
        }

        private static string WordPressHashEncode(string password, string hash)
        {
            const string itoa64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            string output = "*0";
            if (hash == null)
            {
                return output;
            }

            if (hash.StartsWith(output))
                output = "*1";

            string id = hash.Substring(0, 3);
            // We use "$P$", phpBB3 uses "$H$" for the same thing
            if (id != "$P$" && id != "$H$")
                return output;

            // get who many times will generate the hash
            int count_log2 = itoa64.IndexOf(hash[3]);
            if (count_log2 < 7 || count_log2 > 30)
                return output;

            int count = 1 << count_log2;

            string salt = hash.Substring(4, 8);
            if (salt.Length != 8)
                return output;

            byte[] hashBytes = { };
            using (MD5 md5Hash = MD5.Create())
            {
                hashBytes = md5Hash.ComputeHash(Encoding.ASCII.GetBytes(salt + password));
                byte[] passBytes = Encoding.ASCII.GetBytes(password);
                do
                {
                    hashBytes = md5Hash.ComputeHash(hashBytes.Concat(passBytes).ToArray());
                } while (--count > 0);
            }

            output = hash.Substring(0, 12);
            string newHash = Encode64(hashBytes, 16);

            return output + newHash;
        }

        private static string Encode64(byte[] input, int count)
        {
            const string itoa64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            StringBuilder sb = new StringBuilder();
            int i = 0;
            do
            {
                int value = (int)input[i++];
                sb.Append(itoa64[value & 0x3f]); // to uppercase
                if (i < count)
                    value = value | ((int)input[i] << 8);
                sb.Append(itoa64[(value >> 6) & 0x3f]);
                if (i++ >= count)
                    break;
                if (i < count)
                    value = value | ((int)input[i] << 16);
                sb.Append(itoa64[(value >> 12) & 0x3f]);
                if (i++ >= count)
                    break;
                sb.Append(itoa64[(value >> 18) & 0x3f]);
            } while (i < count);

            return sb.ToString();
        }

        /// <summary>
        /// Retorna una cadena de text amb el hash MD4 de la cadena de text especificada per parametre. 
        /// El hash esta representat a hexadecimal en minuscules.
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        private static string calcularMD4(string entrada)
        {
            // get padded uints from bytes
            List<byte> bytes = Encoding.ASCII.GetBytes(entrada).ToList();
            uint bitCount = (uint)(bytes.Count) * 8;
            bytes.Add(128);
            while (bytes.Count % 64 != 56) bytes.Add(0);
            var uints = new List<uint>();
            for (int i = 0; i + 3 < bytes.Count; i += 4)
                uints.Add(bytes[i] | (uint)bytes[i + 1] << 8 | (uint)bytes[i + 2] << 16 | (uint)bytes[i + 3] << 24);
            uints.Add(bitCount);
            uints.Add(0);

            // run rounds
            uint a = 0x67452301, b = 0xefcdab89, c = 0x98badcfe, d = 0x10325476;
            Func<uint, uint, uint> rol = (x, y) => x << (int)y | x >> 32 - (int)y;
            for (int q = 0; q + 15 < uints.Count; q += 16)
            {
                var chunk = uints.GetRange(q, 16);
                uint aa = a, bb = b, cc = c, dd = d;
                Action<Func<uint, uint, uint, uint>, uint[]> round = (f, y) =>
                {
                    foreach (uint i in new[] { y[0], y[1], y[2], y[3] })
                    {
                        a = rol(a + f(b, c, d) + chunk[(int)(i + y[4])] + y[12], y[8]);
                        d = rol(d + f(a, b, c) + chunk[(int)(i + y[5])] + y[12], y[9]);
                        c = rol(c + f(d, a, b) + chunk[(int)(i + y[6])] + y[12], y[10]);
                        b = rol(b + f(c, d, a) + chunk[(int)(i + y[7])] + y[12], y[11]);
                    }
                };
                round((x, y, z) => (x & y) | (~x & z), new uint[] { 0, 4, 8, 12, 0, 1, 2, 3, 3, 7, 11, 19, 0 });
                round((x, y, z) => (x & y) | (x & z) | (y & z), new uint[] { 0, 1, 2, 3, 0, 4, 8, 12, 3, 5, 9, 13, 0x5a827999 });
                round((x, y, z) => x ^ y ^ z, new uint[] { 0, 2, 1, 3, 0, 8, 4, 12, 3, 9, 11, 15, 0x6ed9eba1 });
                a += aa; b += bb; c += cc; d += dd;
            }

            // return hex encoded string
            byte[] outBytes = new[] { a, b, c, d }.SelectMany(BitConverter.GetBytes).ToArray();
            return BitConverter.ToString(outBytes).Replace("-", "").ToLower();
        }

        public static bool IsRunningOnMono()
        {
            return Type.GetType("Mono.Runtime") != null;
        }

        public static int getMaxPathLength()
        {
            try
            {
                FieldInfo maxPathField = typeof(Path).GetField("MaxPath",
                BindingFlags.Static |
                BindingFlags.GetField |
                BindingFlags.NonPublic);

                return (int)maxPathField.GetValue(null);
            }
            catch (Exception)
            {
                return 260;
            }
        }

        public static void printHelp()
        {
            Console.WriteLine(">>>> AYUDA\t\t[http://izcloud.tk/index.php/server-args/]\n===\n"
                + "\n"
                + "Los parámetros se introducen como si fuera un diccionario, pasado por los argumentos, utilizando el siguiente formato:\n"
                + "\n"
                + "clave = valor clave2 = valor2\n"
                + "\n"
                + "El servidor intentará cargar el fichero de configuración config.yaml, y sobrescribir los ajustes leídos o creados en el fichero con los parámetros pasados por la línea de comandos, pero no modificarán el contenido de ese fichero por el nuevo valor, por lo que, cuando el servidor se reinicie sin ese parámetro sobrescrito, el ajuste volverá a la normalidad usando el valor definido en el fichero.\n"
                + "\n"
                + "EJEMPLOS:");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("$ aryptiOR_server.exe configPath=config.yaml host=192.168.1.3 port=22214 hostws=192.168.1.3 portws=22222 requireClientPassword=true useDatabase=true mongohost=127.0.0.1 mongoport=27017 userName=app password=password databaseName=aryptior mongoDbAuthMechanism=SCRAM-SHA-256 isDiscoverable=false discoverableName=PEPESERVER\n"
                + "$ aryptiOR_server.exe useDatabase=true dburl=mongodb+srv://app:password@blahblah.blah.mongodb.net/test?authSource=admin\"&\"replicaSet=blah-shard-0\"&\"readPreference=primary\"&\"ssl=true\n");
            Console.ResetColor();
            Console.WriteLine("\t>> PARÁMETROS\n"
            + "\t==\n\n"
            + "configPath: (string) nombre del fichero de configuración que se usará para guardar los ajustes del servidor. Si se especifica un fichero con extensión .yaml o .yml, se creará o leerá el fichero de configuración (dependiendo de si exista o no) con serialización YAML. En caso de que la extensión sea .json, se creará o leerá con serialización JSON formateada. En caso de que la extensión no sea ninguna de esas tres, el servidor fallará al arrancar. Por defecto: config.yaml.\n"
            + "host: (string) especifica la dirección IP a la que escuchará la interfaz principal que atiende a los clientes de procesamiento.\n"
            + "port: (int) puerto a escuchar de la interfaz principal.\n"
            + "hostws: (string) especifica la dirección IP a la que escuchará la interfaz administrativa que atiende a la página web.\n"
            + "portws: (int) puerto de la interfaz de administración.\n"
            + "requireClientPassword: (bool) indica si los clientes de procesamiento deben utilizar una contraseña para acceder al servidor de procesamiento.\n"
            + "useDatabase: (bool) indica si usar o no una conexión a una base de datos MongoDB.\n"
            + "dburl: (string) usado para especificar la dirección URL de la base de datos MongoBD. Muy útil para cuando se tienen bases de datos en servicios en la nube, como MongoDB Atlas, y el esquema es «mongodb+srv» en lugar de simplemente «mongodb». De especificar este ajuste, no será necesario especificar los campos manualmente, así que el resto de configuraciones de base de datos que se explican abajo serán ignoradas por el servidor si se le da valor a este parámetro.\n"
            + "mongohost: (string) indica la dirección IP o nombre del servidor de base de datos.\n"
            + "mongoport: (int) puerto por el que escucha el servidor de base de datos.\n"
            + "userName: (string) nombre de usuario de MongoDB (normalmente opcional).\n"
            + "dbpassword: (string) contraseña del usuario de la base de datos.\n"
            + "databaseName: (string) nombre de la base de datos en el servidor MongoDB.\n"
            + "mongoDbAuthMechanism: (string) mecanismo de autentificación de MongoDB (más información en la configuración del servidor).\n"
            + "isDiscoverable: (bool) indica si se puede o no encontrar el servidor por descubrimiento local.\n"
            + "discoverableName: (string) nombre por el que el servidor será mostrado por pantalla si es descubierto.");
        }
    }
}
