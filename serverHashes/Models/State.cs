﻿using serverHashes.Models.Client;
using System;

namespace serverHashes
{
    [Serializable]
    public class State
    {
        public bool estaIdle;
        public ClientHash hashActual;

        public State()
        {
        }

        public State(bool estaIdle, ClientHash hashActual)
        {
            this.estaIdle = estaIdle;
            this.hashActual = hashActual;
        }
    }
}
