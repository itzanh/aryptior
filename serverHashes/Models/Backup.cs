﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace serverHashes
{
    [BsonIgnoreExtraElements]
    [Serializable]
    public class Backup
    {
        public string uuid;
        public DateTime date;
        public string hash;
        public long size;

        public Backup()
        {
            this.uuid = Guid.NewGuid().ToString();
            this.date = DateTime.UtcNow;
            this.hash = "";
            this.size = 0;
        }

        public Backup(string uuid, DateTime date, string hash, long size)
        {
            this.uuid = uuid;
            this.date = date;
            this.hash = hash;
            this.size = size;
        }

        public string toFileName()
        {
            return this.uuid + "_" + this.formatDate(this.date) + "_" + this.hash + ".backup";
        }

        private string formatDate(DateTime date)
        {
            return date.Year + "-" + date.Month + "-" + date.Day + "," + date.Hour + "-" + date.Minute + "-" + date.Second;
        }
    }

    public class BackupClient : Backup
    {
        public long downloaded;

        public BackupClient()
        {
            this.uuid = null;
            this.date = DateTime.MinValue;
            this.hash = "";
            this.size = 0;
            this.downloaded = 0;
        }

        public bool isValid()
        {
            return (this.uuid != null && Guid.TryParse(this.uuid, out _) && this.date > DateTime.MinValue
                && this.hash != null && !this.hash.Equals(String.Empty) && this.size > 0
                && this.downloaded > 0 && this.downloaded <= this.size);
        }
    }

    class BackupChunk
    {
        public string uuid;
        public long i;
        public long tamanyPart;

        public BackupChunk(string uuid, long i, long tamanyPart)
        {
            this.uuid = uuid;
            this.i = i;
            this.tamanyPart = tamanyPart;
        }
    }


    [Serializable]
    public class BackupCronCurrentState
    {
        public BackupCron.BackupCronState cronState;
        public DateTime nextBackup;

        public BackupCronCurrentState(BackupCron.BackupCronState cronState, DateTime nextBackup)
        {
            this.cronState = cronState;
            this.nextBackup = nextBackup;
        }
    }
}
