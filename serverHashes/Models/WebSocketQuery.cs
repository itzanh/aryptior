﻿using System;
using System.Collections.Generic;

namespace serverHashes
{
    [Serializable]
    public class WebSocketQuery
    {
        public int offset;
        public int limit;

        public WebSocketQuery()
        {
            this.offset = 0;
            this.limit = 0;
        }

        public WebSocketQuery(int offset, int limit)
        {
            this.offset = offset;
            this.limit = limit;
        }

    }

    [Serializable]
    public class WebSocketQueryResponse<T>
    {
        public int totalLength;
        public List<T> response;

        public WebSocketQueryResponse(int totalLength, List<T> response)
        {
            this.totalLength = totalLength;
            this.response = response;
        }
    }
}
