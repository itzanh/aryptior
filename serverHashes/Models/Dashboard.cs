﻿using Microsoft.VisualBasic.Devices;
using System.Diagnostics;
using System;
using System.IO;

namespace serverHashes
{
    [Serializable]
    public class Dashboard
    {
        private static readonly PerformanceCounter cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        /// <summary>
        /// Total memory on the system (in bytes)
        /// </summary>
        public ulong memTotal;
        /// <summary>
        /// Available memory on the system (in bytes)
        /// </summary>
        public ulong memAvailable;
        /// <summary>
        /// Count of the processing clients connected
        /// </summary>
        public int clientsCount;
        /// <summary>
        /// Count of hashes in the queue (including the one being processed)
        /// </summary>
        public long hashQueueCount;
        /// <summary>
        /// Cound of hashes in the finshed hashes list (solved, failed and surrendered)
        /// </summary>
        public long finishedHashesCount;
        /// <summary>
        /// Numer of hahses per second. Recalculated every 10 seconds.
        /// </summary>
        public long nHashesS;
        /// <summary>
        /// Count of tokens registered
        /// </summary>
        public long tokensCount;
        /// <summary>
        /// Count of dictionaries saved
        /// </summary>
        public long dictionaryCount;
        /// <summary>
        /// Count of all the rainbows (enabled, diabled and finished)
        /// </summary>
        public long rainbowsCount;
        /// <summary>
        /// Size in bytes of the storage folder where the dictionaries, result of the rainbow processing, are stored
        /// </summary>
        public long computedHashesSize;
        /// <summary>
        /// Size in bytes of the sistem's drive available space
        /// </summary>
        public long availableDriveSpace;
        /// <summary>
        /// Size in bytes of the sistem's drive total space
        /// </summary>
        public long totalDriveSpace;
        /// <summary>
        /// Count of enabled rainbows (without diabled or finished rainbows)
        /// </summary>
        public long enabledRainbows;
        /// <summary>
        /// Count of hashes that were solved witha password and not failed or set to surrender state
        /// </summary>
        public long solvedHashes;
        /// <summary>
        /// Percentage of usage of the system's CPU
        /// </summary>
        public float cpuUsage;

        public Dashboard()
        {
        }

        public void getWinSysInfo()
        {
            try
            {
                ComputerInfo CI = new ComputerInfo();
                this.memTotal = CI.TotalPhysicalMemory;
                this.memAvailable = CI.AvailablePhysicalMemory;

                this.getCommonSysInfo();
            }
            catch (Exception) { }
        }

        public void getMonoSysInfo()
        {
            try
            {
                PerformanceCounter pcRAMAv = new PerformanceCounter("Mono Memory", "Available Physical Memory");
                this.memAvailable = (ulong)pcRAMAv.RawValue;
                PerformanceCounter pcRAMtot = new PerformanceCounter("Mono Memory", "Total Physical Memory");
                this.memTotal = (ulong)pcRAMtot.RawValue;

                this.getCommonSysInfo();
            }
            catch (Exception) { }
        }

        public void getCommonSysInfo()
        {
            try
            {
                this.availableDriveSpace = DriveInfo.GetDrives()[0].AvailableFreeSpace;
                this.totalDriveSpace = DriveInfo.GetDrives()[0].TotalSize;
                this.cpuUsage = cpuCounter.NextValue();
            }
            catch (Exception) { }
        }
    }
}
