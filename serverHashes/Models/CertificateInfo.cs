﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace serverHashes
{
    [Serializable]
    [BsonIgnoreExtraElements]
    public class CertificateInfo
    {
        public string uuid;
        public string fileName;
        public long size;
        public DateTime creationDate;

        public CertificateInfo()
        {
            this.uuid = Guid.NewGuid().ToString();
            this.fileName = "";
            this.size = 0;
            this.creationDate = DateTime.UtcNow;
        }

        public CertificateInfo(string fileName, long size) : this()
        {
            this.fileName = fileName;
            this.size = size;
        }
    }
}
