﻿using System;

namespace serverHashes
{
    /// <summary>
    /// Class used in order to censore some parts of the SocketOptions class in the moment of serialization,
    /// so client don't see the certificate information.
    /// </summary>
    [Serializable]
    public class ClientSocketOptions
    {
        public bool encryption;
        public bool compression;

        public ClientSocketOptions()
        {
            this.encryption = true;
            this.compression = true;
        }

        /// <summary>
        /// Copies the encryption and compressio settings and hides the certificate options from a SocketOptions file.
        /// </summary>
        /// <param name="options"></param>
        public ClientSocketOptions(SocketOptions options)
        {
            this.encryption = options.encryption;
            this.compression = options.compression;
        }
    }
}
