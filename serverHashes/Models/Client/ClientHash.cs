﻿using System;

namespace serverHashes.Models.Client
{
    [Serializable]
    public class ClientHash
    {
        public string uuid;
        public string hash;
        public TipusBusqueda tipusBusqueda;
        public Tipus tipus;
        public int primerCaracterUnicode;
        public int ultimCaracterUnicode;
        public int caractersFinals;
        public int[] caracters;
        public int iteracions;
        public string salt;

        public ClientHash()
        {
            this.tipusBusqueda = TipusBusqueda.ForçaBruta;
            this.caracters = new int[1] { 65 };
            this.caractersFinals = 12;
            this.iteracions = 0;
            this.salt = "";
        }

        public ClientHash(Hash h)
        {
            this.uuid = h.uuid;
            this.hash = h.hash;
            this.tipusBusqueda = h.tipusBusqueda;
            this.tipus = h.tipus;
            this.primerCaracterUnicode = h.primerCaracterUnicode;
            this.ultimCaracterUnicode = h.ultimCaracterUnicode;
            this.caractersFinals = h.caractersFinals;
            this.caracters = h.caracters;
            this.iteracions = h.iteracions;
            this.salt = h.salt;
        }

    }
}
