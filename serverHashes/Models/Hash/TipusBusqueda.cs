﻿using System;

namespace serverHashes
{
    [Serializable]
    public enum TipusBusqueda
    {
        ForçaBruta,
        Diccionari
    }
}
