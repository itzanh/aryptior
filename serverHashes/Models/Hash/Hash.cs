﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;

namespace serverHashes
{
    [BsonIgnoreExtraElements]
    [Serializable]
    public class Hash
    {
        public string uuid;
        public string hash;
        public string descripcio;
        public TipusBusqueda tipusBusqueda;
        public Tipus tipus;
        public int initialTestCount;
        public int msProcessingCount;
        public bool surrender;
        public int primerCaracterUnicode;
        public int ultimCaracterUnicode;
        public int caractersFinals;
        public int[] caracters;
        public int iteracions;
        public string salt;
        [JsonIgnore]
        public List<BruteForceChunk> combinacionsPendents;
        [JsonIgnore]
        public List<DictionaryProcessingChunk> lineesPendents;
        public bool estaResolt;
        public string solucio;
        public Dictionary diccionari;
        public long numLinea;
        public DateTime dateCreated;
        public DateTime dateFinished;

        [NonSerialized]
        [JsonIgnore]
        [BsonIgnore]
        private Semaphore MUTEXpendingCombinations;
        [NonSerialized]
        [JsonIgnore]
        [BsonIgnore]
        private Semaphore MUTEXpendingLines;

        public Hash()
        {
            this.uuid = Guid.NewGuid().ToString();
            this.tipusBusqueda = TipusBusqueda.ForçaBruta;
            this.combinacionsPendents = new List<BruteForceChunk>();
            this.initialTestCount = 40000;
            this.msProcessingCount = 60000;
            this.surrender = false;
            this.caracters = new int[1] { this.primerCaracterUnicode };
            this.estaResolt = false;
            this.diccionari = null;
            this.numLinea = 0;
            this.lineesPendents = new List<DictionaryProcessingChunk>();
            this.descripcio = "";
            this.caractersFinals = 12;
            this.iteracions = 0;
            this.salt = "";
            this.dateCreated = DateTime.UtcNow;

            this.MUTEXpendingCombinations = new Semaphore(1, 1);
            this.MUTEXpendingLines = new Semaphore(1, 1);
        }

        public Hash(Hash h)
        {
            this.uuid = h.uuid;
            this.hash = h.hash;
            this.descripcio = h.descripcio;
            this.tipusBusqueda = h.tipusBusqueda;
            this.tipus = h.tipus;
            this.initialTestCount = h.initialTestCount;
            this.msProcessingCount = h.msProcessingCount;
            this.surrender = h.surrender;
            this.primerCaracterUnicode = h.primerCaracterUnicode;
            this.ultimCaracterUnicode = h.ultimCaracterUnicode;
            this.caractersFinals = h.caractersFinals;
            this.caracters = h.caracters;
            this.iteracions = h.iteracions;
            this.salt = h.salt;
            this.combinacionsPendents = h.combinacionsPendents;
            this.lineesPendents = h.lineesPendents;
            this.estaResolt = h.estaResolt;
            this.solucio = h.solucio;
            this.diccionari = h.diccionari;
            this.numLinea = h.numLinea;
            this.dateCreated = h.dateCreated;
            this.dateFinished = h.dateFinished;

            this.MUTEXpendingCombinations = new Semaphore(1, 1);
            this.MUTEXpendingLines = new Semaphore(1, 1);
        }

        public bool isValid()
        {
            // check attributes for null values, zeroes and negative numbers
            if (this.hash == null || this.descripcio == null || this.initialTestCount <= 100
                || this.msProcessingCount <= 5000 || this.combinacionsPendents == null || this.numLinea < 0
                || this.lineesPendents == null || (this.tipusBusqueda != TipusBusqueda.Diccionari
                && (this.primerCaracterUnicode < 32 || this.ultimCaracterUnicode <= this.primerCaracterUnicode)) || this.caractersFinals <= 1
                || this.iteracions <= 0 || this.caracters == null || this.salt == null
                || this.salt.Length > 128 || this.descripcio.Length > 3000 || this.caracters.Length > 32
                || this.hash.Length > 256 || this.caractersFinals > 32)
                return false;

            if (!validateHashLength(this.tipus, this.hash)) return false;

            // check regexp
            if (tipus == Tipus.BCRYPT)
            {
                string[] data = this.hash.Split('$');
                if (data.Length != 4) return false;

                if (!data[0].Equals("") || !this.findSheme(data[1])) return false;

                int workFactor;
                if (!Int32.TryParse(data[2], out workFactor) || workFactor < 4 || workFactor > 31
                    || data[3].Length != 53) return false;
            }
            else if (tipus == Tipus.NTLM)
            {
                if (!Regex.IsMatch(this.hash, @"\A[A-F0-9]*\z")) return false;
            }
            else if (tipus == Tipus.MODULAR)
            {
                if (!validateModularHash(hash)) return false;
            }
            else if (tipus != Tipus.MYSQL && tipus != Tipus.WORDPRESS)
            {
                // check it's a hexadecimal string
                if (!Regex.IsMatch(this.hash, @"\A[a-f0-9]*\z")) return false;
            }


            return true;
        }

        public static bool validateHashLength(Tipus tipus, string hash)
        {
            // check for the length in consideration with the hash type
            switch (tipus)
            {
                case Tipus.MD4:
                case Tipus.MD5:
                case Tipus.RIPEMD128:
                case Tipus.NTLM:
                    {
                        if (hash.Length != 32) return false;
                        break;
                    }
                case Tipus.RIPEMD160:
                case Tipus.SHA1:
                    {
                        if (hash.Length != 40) return false;
                        break;
                    }
                case Tipus.TIGER:
                case Tipus.TIGER2:
                case Tipus.TIGER3:
                case Tipus.TIGER4:
                    {
                        if (hash.Length != 48) return false;
                        break;
                    }
                case Tipus.BLAKE:
                    {
                        if (hash.Length != 56) return false;
                        break;
                    }
                case Tipus.BCRYPT:
                    {
                        if (hash.Length != 60) return false;
                        break;
                    }
                case Tipus.RIPEMD256:
                case Tipus.BLAKE2s:
                case Tipus.SHA256:
                    {
                        if (hash.Length != 64) return false;
                        break;
                    }
                case Tipus.RIPEMD320:
                    {
                        if (hash.Length != 80) return false;
                        break;
                    }
                case Tipus.SHA384:
                    {
                        if (hash.Length != 96) return false;
                        break;
                    }
                case Tipus.BLAKE2b:
                case Tipus.WHIRLPOOL:
                case Tipus.SHA512:
                    {
                        if (hash.Length != 128) return false;
                        break;
                    }
                case Tipus.MODULAR:
                    {
                        break;
                    }
                case Tipus.MYSQL:
                    {
                        if (!mySqlHashIsValid(hash)) return false;
                        break;
                    }
                case Tipus.WORDPRESS:
                    {
                        if (!wordpressHashIsValid(hash)) return false;
                        break;
                    }
                default:
                    {
                        return false;
                    }
            }
            return true;
        }

        private static bool validateModularHash(string hash)
        {
            if (hash[0] != '_' && hash[0] != '$')
            {
                // DES hash detected
                if (hash.Length != 13) return false;
            }
            else if (hash[0] == '_')
            {
                // BSDi hash detected
                if (hash.Length != 20) return false;
            }
            else if (hash[0] == '$')
            {
                string[] hashData = hash.Split('$');
                if (hashData.Length != 4) return false;
                switch (hashData[1])
                {
                    case "1":
                    case "md5":
                        {
                            if (hashData[3].Length != 22) return false;
                            break;
                        }
                    case "2":
                    case "2a":
                    case "2x":
                    case "2y":
                        {
                            if (hashData[3].Length != 53) return false;
                            break;
                        }
                    case "3":
                        {
                            if (hashData[3].Length != 32) return false;
                            break;
                        }
                    case "5":
                        {
                            if (hashData[3].Length != 43) return false;
                            break;
                        }
                    case "6":
                        {
                            if (hashData[3].Length != 86) return false;
                            break;
                        }
                    case "sha1":
                        {
                            if (hashData[3].Length != 37) return false;
                            break;
                        }
                    default:
                        {
                            return false;
                        }
                }
            }

            return true;
        }

        private bool findSheme(string scheme)
        {
            string[] schemes = new string[] { "2", "2a", "2b", "2x", "2y" };
            foreach (string s in schemes)
            {
                if (scheme.Equals(s)) return true;
            }

            return false;
        }

        private static bool mySqlHashIsValid(string hash)
        {
            return !(hash.Length != 41 || hash[0] != '*');
        }

        private static bool wordpressHashIsValid(string hash)
        {
            return !(hash.Length != 34 || (hash.Substring(0, 3) != "$P$" && hash.Substring(0, 3) != "$H$"));
        }

        public static bool isValid(Dictionary<string, dynamic> data)
        {
            foreach (string key in data.Keys)
            {
                dynamic value = data[key];
                switch (key)
                {
                    case "uuid":
                        {
                            if (!(value is string) || !Guid.TryParse(value, out Guid _)) return false;
                            break;
                        }
                    case "descripcio":
                        {
                            if (!(value is string) || value == null || value.Length > 3000) return false;
                            break;
                        }
                    case "initialTestCount":
                        {
                            if (!(value is long) || value <= 0) return false;
                            break;
                        }
                    case "msProcessingCount":
                        {
                            if (!(value is long) || value <= 0) return false;
                            break;
                        }
                    case "caractersFinals":
                        {
                            if (!(value is long) || value <= 1) return false;
                            break;
                        }
                    default:
                        {
                            return false;
                        }
                }
            }
            return true;
        }

        public bool fromDictionary(Dictionary<string, dynamic> data)
        {
            foreach (string key in data.Keys)
            {
                dynamic value = data[key];
                switch (key)
                {
                    case "uuid":
                        {
                            if (!value.Equals(this.uuid)) return false;
                            break;
                        }
                    case "descripcio":
                        {
                            this.descripcio = value;
                            break;
                        }
                    case "initialTestCount":
                        {
                            this.initialTestCount = (int)value;
                            break;
                        }
                    case "msProcessingCount":
                        {
                            this.msProcessingCount = (int)value;
                            break;
                        }
                    case "caractersFinals":
                        {
                            this.caractersFinals = (int)value;
                            break;
                        }
                    default:
                        {
                            return false;
                        }
                }
            }
            return true;
        }

        public static Dictionary<string, dynamic> compareTo(Hash old, Hash newh)
        {
            Dictionary<string, dynamic> differences = new Dictionary<string, dynamic>();

            foreach (FieldInfo oldProp in typeof(Hash).GetFields())
            {
                switch (oldProp.Name)
                {
                    case "caracters":
                        {
                            int[] oldArray = (int[])oldProp.GetValue(old);
                            int[] newArray = (int[])oldProp.GetValue(old);

                            if (oldArray.Length != newArray.Length)
                            {
                                differences.Add(oldProp.Name, oldProp.GetValue(newh));
                            }
                            for (int i = 0; i < oldArray.Length; i++)
                            {
                                if (oldArray[i] != newArray[i])
                                {
                                    differences.Add(oldProp.Name, oldProp.GetValue(newh));
                                    break;
                                }
                            }

                            break;
                        }
                    case "combinacionsPendents":
                        {
                            List<BruteForceChunk> oldArray = (List<BruteForceChunk>)oldProp.GetValue(old);
                            List<BruteForceChunk> newArray = (List<BruteForceChunk>)oldProp.GetValue(old);

                            if (oldArray.Count != newArray.Count)
                            {
                                differences.Add(oldProp.Name, oldProp.GetValue(newh));
                            }
                            for (int i = 0; i < oldArray.Count; i++)
                            {
                                if (oldArray[i] != newArray[i])
                                {
                                    differences.Add(oldProp.Name, oldProp.GetValue(newh));
                                    break;
                                }
                            }

                            break;
                        }
                    case "lineesPendents":
                        {
                            List<DictionaryProcessingChunk> oldArray = (List<DictionaryProcessingChunk>)oldProp.GetValue(old);
                            List<DictionaryProcessingChunk> newArray = (List<DictionaryProcessingChunk>)oldProp.GetValue(old);

                            if (oldArray.Count != newArray.Count)
                            {
                                differences.Add(oldProp.Name, oldProp.GetValue(newh));
                            }
                            for (int i = 0; i < oldArray.Count; i++)
                            {
                                if (oldArray[i] != newArray[i])
                                {
                                    differences.Add(oldProp.Name, oldProp.GetValue(newh));
                                    break;
                                }
                            }

                            break;
                        }
                    case "diccionari":
                        {
                            break;
                        }
                    default:
                        {
                            if (!object.Equals(oldProp.GetValue(old), oldProp.GetValue(newh)))
                            {
                                differences.Add(oldProp.Name, oldProp.GetValue(newh));
                            }

                            break;
                        }
                }

            }

            return differences;
        }

        public int pendingCombinationCount()
        {
            if (MUTEXpendingCombinations == null)
                this.MUTEXpendingCombinations = new Semaphore(1, 1);

            lock (MUTEXpendingCombinations)
                return this.combinacionsPendents.Count;
        }

        public int pendingLinesCount()
        {
            if (MUTEXpendingLines == null)
                this.MUTEXpendingLines = new Semaphore(1, 1);

            lock (MUTEXpendingLines)
                return this.lineesPendents.Count;
        }

        public void addPendingCombination(BruteForceChunk combination)
        {
            if (MUTEXpendingCombinations == null)
                this.MUTEXpendingCombinations = new Semaphore(1, 1);

            lock (MUTEXpendingCombinations)
                this.combinacionsPendents.Add(combination);
        }

        public void addPendingCombination(DictionaryProcessingChunk combination)
        {
            if (MUTEXpendingLines == null)
                this.MUTEXpendingLines = new Semaphore(1, 1);

            lock (MUTEXpendingLines)
                this.lineesPendents.Add(combination);
        }

        public BruteForceChunk getPendingCombination()
        {
            if (MUTEXpendingCombinations == null)
                this.MUTEXpendingCombinations = new Semaphore(1, 1);

            lock (MUTEXpendingCombinations)
            {
                if (this.combinacionsPendents.Count > 0)
                {
                    BruteForceChunk bfc = this.combinacionsPendents[0];
                    this.combinacionsPendents.RemoveAt(0);
                    return bfc;
                }
                else
                {
                    return null;
                }
            }
        }

        public DictionaryProcessingChunk getPendingLine()
        {
            if (MUTEXpendingLines == null)
                this.MUTEXpendingLines = new Semaphore(1, 1);

            lock (MUTEXpendingLines)
            {
                if (this.lineesPendents.Count > 0)
                {
                    DictionaryProcessingChunk dpc = this.lineesPendents[0];
                    this.lineesPendents.RemoveAt(0);
                    return dpc;
                }
                else
                {
                    return null;
                }
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Hash hash &&
                   this.uuid.Equals(hash.uuid);
        }

        public override int GetHashCode()
        {
            var hashCode = 797299145;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(uuid);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(hash);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(descripcio);
            hashCode = hashCode * -1521134295 + tipusBusqueda.GetHashCode();
            hashCode = hashCode * -1521134295 + tipus.GetHashCode();
            hashCode = hashCode * -1521134295 + initialTestCount.GetHashCode();
            hashCode = hashCode * -1521134295 + msProcessingCount.GetHashCode();
            hashCode = hashCode * -1521134295 + surrender.GetHashCode();
            hashCode = hashCode * -1521134295 + primerCaracterUnicode.GetHashCode();
            hashCode = hashCode * -1521134295 + ultimCaracterUnicode.GetHashCode();
            hashCode = hashCode * -1521134295 + caractersFinals.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<int[]>.Default.GetHashCode(caracters);
            hashCode = hashCode * -1521134295 + iteracions.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(salt);
            hashCode = hashCode * -1521134295 + estaResolt.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(solucio);
            hashCode = hashCode * -1521134295 + EqualityComparer<Dictionary>.Default.GetHashCode(diccionari);
            hashCode = hashCode * -1521134295 + numLinea.GetHashCode();
            hashCode = hashCode * -1521134295 + dateCreated.GetHashCode();
            hashCode = hashCode * -1521134295 + dateFinished.GetHashCode();
            return hashCode;
        }
    }

    /*[Serializable]
    public class ExportHash : Hash
    {
        public new List<BruteForceChunk> combinacionsPendents;
        public new List<DictionaryProcessingChunk> lineesPendents;


    }*/

    [Serializable]
    public class ExportHash
    {
        public string uuid;
        public string hash;
        public string descripcio;
        public TipusBusqueda tipusBusqueda;
        public Tipus tipus;
        public int initialTestCount;
        public int msProcessingCount;
        public bool surrender;
        public int primerCaracterUnicode;
        public int ultimCaracterUnicode;
        public int caractersFinals;
        public int[] caracters;
        public int iteracions;
        public string salt;
        public List<BruteForceChunk> combinacionsPendents;
        public List<DictionaryProcessingChunk> lineesPendents;
        public bool estaResolt;
        public string solucio;
        public Dictionary diccionari;
        public long numLinea;
        public DateTime dateCreated;
        public DateTime dateFinished;

        public ExportHash(Hash h)
        {
            this.uuid = h.uuid;
            this.hash = h.hash;
            this.descripcio = h.descripcio;
            this.tipusBusqueda = h.tipusBusqueda;
            this.tipus = h.tipus;
            this.initialTestCount = h.initialTestCount;
            this.msProcessingCount = h.msProcessingCount;
            this.surrender = h.surrender;
            this.primerCaracterUnicode = h.primerCaracterUnicode;
            this.ultimCaracterUnicode = h.ultimCaracterUnicode;
            this.caractersFinals = h.caractersFinals;
            this.caracters = h.caracters;
            this.iteracions = h.iteracions;
            this.salt = h.salt;
            this.combinacionsPendents = h.combinacionsPendents;
            this.lineesPendents = h.lineesPendents;
            this.estaResolt = h.estaResolt;
            this.solucio = h.solucio;
            this.diccionari = h.diccionari;
            this.numLinea = h.numLinea;
            this.dateCreated = h.dateCreated;
            this.dateFinished = h.dateFinished;
        }
    }
}
