﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;

namespace serverHashes
{
    [BsonIgnoreExtraElements]
    [Serializable]
    public class BruteForceChunk
    {
        public int charactersCount;
        public int totalCharactersCount;
        public int[] characters;

        public BruteForceChunk()
        {
            this.charactersCount = 0;
            this.characters = new int[0];
        }

        public BruteForceChunk(int charactersCount, int[] characters, int totalCharactersCount)
        {
            this.charactersCount = charactersCount;
            this.characters = characters;
            this.totalCharactersCount = totalCharactersCount;
        }
    }

    [BsonIgnoreExtraElements]
    [Serializable]
    public class DictionaryProcessingChunk
    {
        public long numLinea;
        public long endNumLine;
        public long linesCount;

        public DictionaryProcessingChunk()
        {
            this.numLinea = 0;
            this.endNumLine = 0;
            this.linesCount = 0;
        }

        public DictionaryProcessingChunk(long numLinea, long endNumLine, long linesCount)
        {
            this.numLinea = numLinea;
            this.endNumLine = endNumLine;
            this.linesCount = linesCount;
        }
    }

    [Serializable]
    public class BruteForceTest
    {
        public int timeout;
        public Tipus algorithm;
        public int iterations;
        public string salt;
        public int firstUnicodeCharacter;
        public int lastUnicodeCharacter;
        public int initialLength;
        public int finalLength;

        public BruteForceTest()
        {

        }

        public bool isValid()
        {
            return (this.timeout >= 15000 && this.timeout <= 60000 && this.iterations >= 0
                && this.salt != null && this.firstUnicodeCharacter >= 32 && this.lastUnicodeCharacter >= 0
                && (this.lastUnicodeCharacter > this.firstUnicodeCharacter) && this.initialLength >= 1 && this.finalLength > 1
                && (this.finalLength > this.initialLength));
        }
    }

    [Serializable]
    public class BruteForceTestResults
    {
        public long hashCount;
        public List<BruteForceTestClientResults> results;

        [JsonIgnore]
        private object hashCountMutex = new object();
        [JsonIgnore]
        private object resultsMutex = new object();

        public BruteForceTestResults()
        {
            this.hashCount = 0;
            this.results = new List<BruteForceTestClientResults>();
        }

        public void addResult(BruteForceTestClientResults testResults)
        {
            lock (resultsMutex)
                this.results.Add(testResults);
        }

        public void incrementHashCount(long count)
        {
            lock (hashCountMutex)
                this.hashCount += count;
        }
    }

    [Serializable]
    public class BruteForceTestClientResults
    {
        public string uuid;
        public string address;
        public int threads;
        public long hashCount;

        public BruteForceTestClientResults(string uuid)
        {
            this.uuid = uuid;
            this.hashCount = -1;
            this.threads = -1;
        }

        public BruteForceTestClientResults(string uuid, string address)
        {
            this.uuid = uuid;
            this.address = address;
            this.threads = -1;
            this.hashCount = -2;
        }

        public BruteForceTestClientResults(string uuid, string address, long hashCount, int threads)
        {
            this.uuid = uuid;
            this.address = address;
            this.hashCount = hashCount;
            this.threads = threads;
        }
    }

    [Serializable]
    public class BruteForceTestResponse
    {
        public long hashCount;
        public int threads;

        public BruteForceTestResponse(long hashCount, int threads)
        {
            this.hashCount = hashCount;
            this.threads = threads;
        }

        public bool isValid()
        {
            return (this.hashCount > 0 && this.threads > 0 && this.threads <= 64);
        }
    }
}
