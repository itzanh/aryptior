﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace serverHashes
{
    [BsonIgnoreExtraElements]
    [Serializable]
    public class Dictionary
    {
        public string uuid;
        public string fileName;
        public string description;
        public long sizeInBytes;
        public long uploadProgress;
        public bool isReady;

        public Dictionary()
        {
            this.fileName = "";
            this.description = "";
            this.sizeInBytes = 0;
            this.uploadProgress = 0;
            this.isReady = false;
        }

        public Dictionary(Dictionary d)
        {
            this.uuid = d.uuid;
            this.fileName = d.fileName;
            this.description = d.description;
            this.sizeInBytes = d.sizeInBytes;
            this.uploadProgress = d.uploadProgress;
            this.isReady = d.isReady;
        }

        public bool isValid()
        {
            // check filename for invalid characters
            if (this.fileName == null || this.fileName.Length == 0 || this.fileName.Length > Utilities.getMaxPathLength()
                || this.fileName.IndexOfAny(Path.GetInvalidFileNameChars()) > -1) return false;

            // check numbers
            return !(this.sizeInBytes <= 0 || this.uploadProgress < 0 || this.uploadProgress > this.sizeInBytes
                || this.description == null || this.description.Length > 3000);
        }

        public static Dictionary<string, dynamic> compareTo(Dictionary old, Dictionary newd)
        {
            Dictionary<string, dynamic> differences = new Dictionary<string, dynamic>();

            foreach (FieldInfo oldProp in typeof(Dictionary).GetFields())
            {
                if (!object.Equals(oldProp.GetValue(old), oldProp.GetValue(newd)))
                {
                    differences.Add(oldProp.Name, oldProp.GetValue(newd));
                }
            }

            return differences;
        }

        public override bool Equals(object obj)
        {
            return obj is Dictionary dictionary &&
                   this.uuid.Equals(dictionary.uuid);
        }

        public override int GetHashCode()
        {
            var hashCode = 777167684;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(uuid);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(fileName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(description);
            hashCode = hashCode * -1521134295 + sizeInBytes.GetHashCode();
            hashCode = hashCode * -1521134295 + uploadProgress.GetHashCode();
            hashCode = hashCode * -1521134295 + isReady.GetHashCode();
            return hashCode;
        }
    }

    public class DictionaryUploadChunk
    {
        public string fileName;
        public long i;
        public long chunkSize;

        public DictionaryUploadChunk()
        {
        }

        public DictionaryUploadChunk(string fileName, long i, long chunkSize)
        {
            this.fileName = fileName;
            this.i = i;
            this.chunkSize = chunkSize;
        }
    }

    public enum DictionaryDeletionOperationResult
    {
        OK,
        ERR
    }

    public class DictionaryDeletionHash
    {
        public string hash;
        public string uuid;

        public DictionaryDeletionHash(Hash h)
        {
            this.hash = h.hash;
            this.uuid = h.uuid;
        }

        public DictionaryDeletionHash(string hash, string uuid)
        {
            this.hash = hash;
            this.uuid = uuid;
        }
    }

    public class DictionaryDeletionResult
    {
        public DictionaryDeletionOperationResult operationResult;
        public List<DictionaryDeletionHash> hashesUsingDictionary;

        public DictionaryDeletionResult(DictionaryDeletionOperationResult operationResult)
        {
            this.operationResult = operationResult;
            this.hashesUsingDictionary = null;
        }

        public DictionaryDeletionResult(DictionaryDeletionOperationResult operationResult, List<DictionaryDeletionHash> hashesUsingDictionary)
        {
            this.operationResult = operationResult;
            this.hashesUsingDictionary = hashesUsingDictionary;
        }
    }
}
