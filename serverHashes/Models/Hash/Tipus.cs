﻿using System;

namespace serverHashes
{
    [Serializable]
    public enum Tipus
    {
        NTLM,
        MD4,
        MD5,
        SHA1,
        SHA256,
        SHA384,
        SHA512,
        BCRYPT,
        RIPEMD128,
        RIPEMD160,
        RIPEMD256,
        RIPEMD320,
        BLAKE,
        BLAKE2b,
        BLAKE2s,
        WHIRLPOOL,
        TIGER,
        TIGER2,
        TIGER3,
        TIGER4,
        MODULAR,
        MYSQL,
        WORDPRESS
    }
}
