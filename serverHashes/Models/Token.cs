﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace serverHashes
{
    [BsonIgnoreExtraElements]
    [Serializable]
    public class Token
    {
        public string addr;
        public DateTime lastUsed;
        public string token;

        public Token()
        {
            this.addr = "";
            this.lastUsed = DateTime.UtcNow;
            this.token = "";
        }

        public Token(string adreca, string token)
        {
            this.addr = adreca;
            this.lastUsed = DateTime.UtcNow;
            this.token = token;
        }

        public Token(string adreca, DateTime dataCreacio, string token)
        {
            this.addr = adreca;
            this.lastUsed = dataCreacio;
            this.token = token;
        }

        public Token(Token t)
        {
            this.addr = t.addr;
            this.token = t.token;
            this.lastUsed = DateTime.UtcNow;
        }

        public bool isExpired()
        {
            return (DateTime.UtcNow.Subtract(this.lastUsed).TotalHours > 24);
        }
    }
}