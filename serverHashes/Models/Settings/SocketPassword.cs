﻿using System;
using System.Text;

namespace serverHashes
{
    [Serializable]
    public class SocketPassword
    {
        public int iterations;
        public string salt;
        public string hashSHA512;

        public SocketPassword()
        {
            
        }

        public SocketPassword(int iterations, string salt, string hashSHA512)
        {
            this.iterations = iterations;
            this.salt = salt;
            this.hashSHA512 = hashSHA512;
        }

        public static SocketPassword GetSocketPassword()
        {
            SocketPassword password = new SocketPassword();
            password.iterations = 256000;
            password.salt = generateSalt();
            password.hashSHA512 = Utilities.computeSHA512Hash(password.salt + "1234", password.iterations);
            return password;
        }

        public static string generateSalt(int length = 32)
        {
            StringBuilder str = new StringBuilder();
            const string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            Random r = new Random();
            for (int i = 0; i < length; i++)
            {
                str.Append(characters[r.Next(0, characters.Length)]);
            }
            return str.ToString();
        }

        public bool isValid()
        {
            return !((this.iterations <= 0)
                || (this.iterations > 512000)
                || (this.salt == null)
                || (this.salt.Length < 10)
                || (this.salt.Length > 512)
                || (this.hashSHA512 == null)
                || (this.hashSHA512.Length != 128));
        }
    }

    [Serializable]
    public class SocketNewPassword
    {
        public int iterations;
        public string salt;
        public string pwd;

        public SocketNewPassword()
        {
            this.iterations = 256000;
            this.salt = SocketPassword.generateSalt();
            this.pwd = null;
        }

        public SocketNewPassword(int iterations, string salt, string pwd)
        {
            this.iterations = iterations;
            this.salt = salt;
            this.pwd = pwd;
        }

        public bool isValid()
        {
            return !(this.iterations <= 0 || this.iterations > 512000 || this.salt == null
                || this.pwd == null || this.salt.Length <= 3 || this.pwd.Length <= 3);
        }
    }
}
