﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;
using YamlDotNet.Serialization;

namespace serverHashes
{
    [Serializable]
    public class WebSocketSettings
    {
        /// <summary>
        /// server's IP or domain to listen to
        /// </summary>
        public string host;
        /// <summary>
        /// server's port to listen to
        /// </summary>
        public int port;
        /// <summary>
        /// server's password to request to the processing clients before putting them to work
        /// </summary>
        public SocketPassword pwd;
        /// <summary>
        /// Contains a list of IP adresses which will be blocked from connecting to this server.
        /// </summary>
        public List<string> blackList;
        /// <summary>
        /// If this property is set to true, only the IPs listed below 'whiteList' will be able to connect to this server.
        /// </summary>
        public bool useWhiteList;
        /// <summary>
        /// If 'useWhiteList' is set to true, the list of IP addresses that are able to connect to this server.
        /// </summary>
        public List<string> whiteList;
        /// <summary>
        /// If set to true, an SSL WebSocket Secure connection must be done between the client and the server. (WSS)
        /// If it's set to false, an insecure connection will be done and the data will be transferred straight through TCP.
        /// </summary>
        public bool isSecure;
        /// <summary>
        /// If SSL is set to be used in the above property 'isSecure', this setting has to contain the path 
        /// on the file system to the certificate file (.pfx).
        /// </summary>
        public string certUUID;
        /// <summary>
        /// Password of the certificate set on the above tag 'certFileName'.
        /// </summary>
        public string password;
        /// <summary>
        /// Maximum of failed loggin attemps that a WebSocket client can perform before it is addedd automatically to the list of banned addresses from the server.
        /// Set to 0 to don't apply an automatic address ban.
        /// </summary>
        public int autoBanMaxAttemps;

        [NonSerialized]
        [JsonIgnore]
        [BsonIgnore]
        [YamlIgnore]
        private Semaphore MUTEXblackList = new Semaphore(1, 1);

        public WebSocketSettings()
        {
            this.host = "0.0.0.0";
            this.port = 22216;
            this.useWhiteList = false;
            this.pwd = new SocketPassword();
            this.blackList = new List<string>();
            this.whiteList = new List<string>();
            this.isSecure = false;
            this.certUUID = "";
            this.password = "";
            this.autoBanMaxAttemps = 3;
        }

        public bool isValid()
        {
            // check ip addresses
            if (!this.isValidAddr(this.host)) return false;

            // check port numbers
            if (this.port < 0 || this.port > 65535) return false;

            // check socket passwords
            if (this.pwd == null || !this.pwd.isValid())
            {
                return false;
            }

            if (this.blackList == null || this.whiteList == null)
                return false;

            // verify blacklist and whitelist's addresses
            foreach (string addrSel in this.blackList)
            {
                if (!this.isValidAddr(addrSel)) return false;
            }
            foreach (string addrSel in this.whiteList)
            {
                if (!this.isValidAddr(addrSel)) return false;
            }

            if (this.isSecure && (this.certUUID == null || this.password == null || !Guid.TryParse(this.certUUID, out _))) return false;

            if (this.autoBanMaxAttemps < 0)
                return false;

            return true;
        }

        private bool isValidAddr(string addr)
        {
            System.Net.IPAddress ip;
            return System.Net.IPAddress.TryParse(addr, out ip);
        }

        public void addToBlacklist(string addr)
        {
            lock (MUTEXblackList)
                this.blackList.Add(addr);
        }

        public bool connectionIsValid(string addr)
        {
            lock (MUTEXblackList)
                if (!this.useWhiteList)
                {
                    return !(this.blackList.IndexOf(addr) > -1);
                }
                else
                {
                    return (this.whiteList.IndexOf(addr) > -1);
                }
        }
    }
}
