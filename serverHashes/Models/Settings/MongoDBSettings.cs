﻿using MongoDB.Driver;
using System;

namespace serverHashes
{
    [Serializable]
    public class MongoDBSettings
    {
        /// <summary>
        /// Indica si hi ha que connectarse a un servidor de base de dades
        /// </summary>
        public bool useDatabase;
        /// <summary>
        /// Connections string to connect to the database server. Eg. mongodb://
        /// If this property is an emprty string, this settings will be ingored and the connection will be done throught the individual fields specified below
        /// </summary>
        public string url;
        public string host;
        public int port;
        public string userName;
        public string password;
        public string databaseName;
        public string mongoDbAuthMechanism;

        public MongoDBSettings()
        {
            this.useDatabase = false;
            this.url = String.Empty;
            this.host = "127.0.0.1";
            this.port = 27017;
            this.userName = String.Empty;
            this.password = String.Empty;
            this.databaseName = "aryptior";
            this.mongoDbAuthMechanism = "NONE";
        }

        public MongoDBSettings(string host, int port, string nomUsuari, string contrasenya, string nomBaseDeDades)
        {
            this.useDatabase = true;
            this.host = host;
            this.port = port;
            this.userName = nomUsuari;
            this.password = contrasenya;
            this.databaseName = nomBaseDeDades;
            this.mongoDbAuthMechanism = "SCRAM-SHA-256";
        }

        public MongoDBSettings(bool connexioABaseDeDades, string host, int port, string nomUsuari, string contrasenya, string nomBaseDeDades)
        {
            this.useDatabase = connexioABaseDeDades;
            this.host = host;
            this.port = port;
            this.userName = nomUsuari;
            this.password = contrasenya;
            this.databaseName = nomBaseDeDades;
            this.mongoDbAuthMechanism = "SCRAM-SHA-256";
        }

        public bool isValid()
        {
            if (this.url == null || this.host == null || this.userName == null
                || this.password == null || this.databaseName == null || this.mongoDbAuthMechanism == null)
                return false;

            if (!this.url.Equals(String.Empty))
            {
                try
                {
                    new MongoUrl(this.url);
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }

            if (this.port < 0 || this.port > 65535) return false;
            if (!this.isDatabaseNameValid(this.databaseName)) return false;

            // check mongodb authenciation mechanism
            string[] mongoAuthMechanisms = new string[] { "NONE", "SCRAM-SHA-1", "SCRAM-SHA-256", "MONGODB-X509", "GSSAPI", "PLAIN" };
            if (Array.IndexOf(mongoAuthMechanisms, this.mongoDbAuthMechanism) == -1)
            {
                return false;
            }

            return true;
        }

        private bool isDatabaseNameValid(string collectionName)
        {
            return !collectionName.Equals("") && !collectionName.StartsWith("system.") && !collectionName.Contains("$") && !collectionName.Contains("\x00");
        }
    }
}
