﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using YamlDotNet.Serialization;

namespace serverHashes
{
    [Serializable]
    public class LocalStorageSettings
    {
        /// <summary>
        /// Default name of the file where the hashes queue is going to be stored.
        /// </summary>
        private const string FOLDER_NAME = "local_storage";
        /// <summary>
        /// Default name of the file where the hashes queue is going to be stored.
        /// </summary>
        private const string HASHES_FILENAME = "hashes.arydb";
        /// <summary>
        /// Default name of the file where the dictionaries objects are going to be stored.
        /// </summary>
        private const string DICTIONARIES_FILENAME = "dictionaries.arydb";
        /// <summary>
        /// Default name of the file where the rainbows definitions are going to be stored.
        /// </summary>
        private const string RAINBOWS_FILENAME = "rainbows.arydb";
        /// <summary>
        /// Default name of the file where a list of authentication tokens are stored.
        /// </summary>
        private const string TOKENS_FILENAME = "tokens.arydb";
        /// <summary>
        /// Default name of the file where a list of uploaded security certificates are stored.
        /// </summary>
        private const string CERTIFICATES_FILENAME = "certs.arydb";
        /// <summary>
        /// Default ame of the binary file where the server logs, if enabled, are going to be stored.
        /// </summary>
        private const string LOGS_FILENAME = "logs.arydb";

        /// <summary>
        /// Sets if we should use local storage (server's file system) to store persistent saved data.
        /// </summary>
        public bool useLocalStorage;
        public string folderName;
        public string hashesFileName;
        public string savedDictionariesFileName;
        public string rainbowsFileName;
        public string tokensFileName;
        public string certificatesFileName;
        public string logsFileName;
        public int operationsToRegenerate;
        [NonSerialized]
        [JsonIgnore]
        [BsonIgnore]
        [YamlIgnore]
        public string configPath;

        public LocalStorageSettings()
        {
            this.useLocalStorage = true;
            this.folderName = FOLDER_NAME;
            this.hashesFileName = HASHES_FILENAME;
            this.savedDictionariesFileName = DICTIONARIES_FILENAME;
            this.rainbowsFileName = RAINBOWS_FILENAME;
            this.tokensFileName = TOKENS_FILENAME;
            this.certificatesFileName = CERTIFICATES_FILENAME;
            this.logsFileName = LOGS_FILENAME;
            this.operationsToRegenerate = 50;
            this.configPath = LocalStorage.CONFIGURATION_FILENAME;
        }

        public bool isValid()
        {
            char[] v = System.IO.Path.GetInvalidFileNameChars();
            int maxNameLength = Utilities.getMaxPathLength();
            if ((this.folderName.IndexOfAny(v) != -1)
                || (this.hashesFileName.IndexOfAny(v) != -1)
                || (this.savedDictionariesFileName.IndexOfAny(v) != -1)
                || (this.tokensFileName.IndexOfAny(v) != -1)
                || (this.certificatesFileName.IndexOfAny(v) != -1)
                || (this.rainbowsFileName.IndexOfAny(v) != -1)
                || (this.logsFileName.IndexOfAny(v) != -1)

                || (this.folderName.Length > maxNameLength)
                || (this.hashesFileName.Length > maxNameLength)
                || (this.savedDictionariesFileName.Length > maxNameLength)
                || (this.tokensFileName.Length > maxNameLength)
                || (this.certificatesFileName.Length > maxNameLength)
                || (this.operationsToRegenerate < 10)
                || (this.operationsToRegenerate > 512)
                || !this.fileNameIsUnique())
            {
                return false;
            }

            return true;
        }

        private bool fileNameIsUnique()
        {
            List<string> fileNames = new List<string>();
            fileNames.Add(this.hashesFileName);
            fileNames.Add(this.savedDictionariesFileName);
            fileNames.Add(this.tokensFileName);
            fileNames.Add(this.certificatesFileName);
            fileNames.Add(this.rainbowsFileName);
            fileNames.Add(this.logsFileName);
            for (int i = 0; i < fileNames.Count; i++)
                for (int j = 0; j < fileNames.Count; j++)
                    if ((i != j) && fileNames[i].Equals(fileNames[j]))
                        return false;
            return true;
        }

    }
}
