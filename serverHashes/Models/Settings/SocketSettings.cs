﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;
using YamlDotNet.Serialization;

namespace serverHashes
{
    [Serializable]
    public class SocketSettings
    {
        /// <summary>
        /// server's IP or domain to listen to
        /// </summary>
        public string host;
        /// <summary>
        /// server's port to listen to
        /// </summary>
        public int port;
        /// <summary>
        /// server's password to request to the processing clients before putting them to work
        /// </summary>
        public SocketPassword pwd;
        /// <summary>
        /// Contains a list of IP adresses which will be blocked from connecting to this server.
        /// </summary>
        public List<string> blackList;
        /// <summary>
        /// If this property is set to true, only the IPs listed below 'whiteList' will be able to connect to this server.
        /// </summary>
        public bool useWhiteList;
        /// <summary>
        /// If 'useWhiteList' is set to true, the list of IP addresses that are able to connect to this server.
        /// </summary>
        public List<string> whiteList;
        /// <summary>
        /// If set to true, the client will be unable to connect to the server 
        /// unless they're configuration matches this server's settings under the attribute 'options'.
        /// On the other hand, if this property is set to false, all the client's specific settings that are not by default will be accepted.
        /// </summary>
        public bool mustUseServerSetings;
        /// <summary>
        /// This settings specifies the options used in the socket connection with the processing client. In case that the client does not have specific 
        /// options, this settings will be applied.
        /// </summary>
        public SocketOptions socketOptions;
        /// <summary>
        /// Sets the maximum size per message sent to this server through the main socket. If a client exceeds this limit, it will be disconnected from the socket.
        /// If this propiety is set to 0, no maximum bound will be applied.
        /// </summary>
        public uint maxMessageSize;
        /// <summary>
        /// Sets the maximum number of client connected simultaneously to this server through the main socket. 
        /// Once the number of clients currently connected reaches this bound, the server will refuse more client to connect to this server
        /// by closing theyr TCP connection.
        /// If this propiety is set to 0, no maximum bound will be applied.
        /// </summary>
        public uint maxClients;
        /// <summary>
        /// If set to true, processing clients require a password set on the field 'clientPassword' to connect to this server and do work.
        /// </summary>
        public bool requireClientPassword;
        /// <summary>
        /// Maximum of failed loggin attemps that a WebSocket client can perform before it is addedd automatically to the list of banned addresses from the server.
        /// Set to 0 to don't apply an automatic address ban.
        /// </summary>
        public int autoBanMaxAttemps;

        [NonSerialized]
        [JsonIgnore]
        [BsonIgnore]
        [YamlIgnore]
        private Semaphore MUTEXblackList = new Semaphore(1, 1);

        public SocketSettings()
        {
            this.host = "0.0.0.0";
            this.port = 22215;
            this.blackList = new List<string>();
            this.useWhiteList = false;
            this.whiteList = new List<string>();
            this.pwd = new SocketPassword();
            this.socketOptions = new SocketOptions();
            this.mustUseServerSetings = false;
            this.maxMessageSize = 256000000; // 256 Mb
            this.maxClients = 3000;
            this.requireClientPassword = false;
            this.autoBanMaxAttemps = 10;
        }

        public bool isValid()
        {
            // check ip addresses
            if (!this.isValidAddr(this.host)) return false;

            // check port numbers
            if (this.port < 0 || this.port > 65535) return false;

            // check socket passwords
            if (this.pwd == null || !this.pwd.isValid())
            {
                return false;
            }

            if (this.blackList == null || this.whiteList == null)
                return false;

            // verify blacklist and whitelist's addresses
            foreach (string addrSel in this.blackList)
            {
                if (!this.isValidAddr(addrSel)) return false;
            }
            foreach (string addrSel in this.whiteList)
            {
                if (!this.isValidAddr(addrSel)) return false;
            }

            if (this.socketOptions == null) return false;

            // socket maximum clients and maximum message size
            if (this.maxMessageSize < 0 || (this.maxMessageSize != 0 && this.maxMessageSize < 24000000) || this.maxClients < 0)
            {
                return false;
            }

            if (this.autoBanMaxAttemps < 0)
                return false;

            if (!this.socketOptions.isValid()) return false;

            return true;
        }

        private bool isValidAddr(string addr)
        {
            System.Net.IPAddress ip;
            return System.Net.IPAddress.TryParse(addr, out ip);
        }

        public void addToBlacklist(string addr)
        {
            lock (MUTEXblackList)
                this.blackList.Add(addr);
        }

        public bool connectionIsValid(string addr)
        {
            lock (MUTEXblackList)
                if (!this.useWhiteList)
                {
                    return !(this.blackList.IndexOf(addr) > -1);
                }
                else
                {
                    return (this.whiteList.IndexOf(addr) > -1);
                }
        }
    }
}
