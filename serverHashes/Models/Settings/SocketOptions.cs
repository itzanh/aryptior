﻿using System;

namespace serverHashes
{
    [Serializable]
    public class SocketOptions
    {
        /// <summary>
        /// If set to true, an SSL secure connection must be done between the client and the server.
        /// If it's set to false, an insecure connection will be done and the data will be transferred straight through TCP.
        /// </summary>
        public bool encryption;
        /// <summary>
        /// Sets whether to use or not Snappy compression to transfer data between client and server over the network.
        /// Snappy is a fast data compression library that allows compression speed up to 250Mb/s and decompression speed up 
        /// to 500Mb/s using a single core 2.26GHz i7. Set it to 'true' for faster network transfers on slow networks 
        /// or to 'false' to save up some CPU usage.
        /// </summary>
        public bool compression;
        /// <summary>
        /// If SSL is set to be used in the above property 'encryption', this setting has to contain the path 
        /// on the file system to the certificate file (.pfx).
        /// </summary>
        public string certUUID;
        /// <summary>
        /// Password of the certificate set on the above tag 'certFileName'.
        /// </summary>
        public string password;
        /// <summary>
        /// Read and write timeout to be used when communicating with the clients. Set this attribute to 0 to disable the timeout.
        /// </summary>
        public int networkTimeout;
        /// <summary>
        /// Sets the interval that the clients are going to wait between ping and ping to the server.
        /// This time has to be shorter than the network timeout, and will be used to reset that timeout in runtime.
        /// </summary>
        public int pingInterval;

        public SocketOptions()
        {
            this.encryption = false;
            this.compression = false;
            this.certUUID = "";
            this.password = "";
            this.networkTimeout = 60;
            this.pingInterval = 50;
        }

        public SocketOptions(bool encryption, bool compression)
        {
            this.encryption = encryption;
            this.compression = compression;
        }

        public SocketOptions(bool encryption, bool compression, string certUUID, string password) : this(encryption, compression)
        {
            this.certUUID = certUUID;
            this.password = password;
        }

        public void addOptions(ClientSocketOptions options)
        {
            this.encryption = options.encryption;
            this.compression = options.compression;
        }

        public bool isValid()
        {
            if (this.pingInterval != 0 || this.networkTimeout != 0)
            {
                if (this.networkTimeout < 0 || this.pingInterval < 0 || this.networkTimeout < 10
                    || (this.networkTimeout - this.pingInterval < 10))
                {
                    return false;
                }
            }

            if (!this.encryption) return true;
            return this.encryption && !(this.certUUID == null || this.password == null || !Guid.TryParse(this.certUUID, out _)
                || (this.pingInterval == 0 || this.networkTimeout == 0));
        }

    }
}
