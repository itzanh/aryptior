﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace serverHashes
{
    [Serializable]
    public class BackupSettings
    {
        /// <summary>
        /// Sets whether this server will backup automatically or not.
        /// </summary>
        public bool doBackups;
        /// <summary>
        /// Indicates the interval of time between one backup is finished and the next one is started. Expressed in miliseconds.
        /// </summary>
        public int timeoutInterval;
        /// <summary>
        /// This settings specifies the maximum size on disk that the server will use for storing backups locally. If this bound is exceeded,
        /// this server will automatically delete the older backups till the enought space for the newer backups is freed.
        /// If this property is set to zero, no bounds will be applied.
        /// </summary>
        public long sizeLimit;
        /// <summary>
        /// List of strings of collection names on the database to back up.
        /// </summary>
        public string[] collections;
        /// <summary>
        /// AES encryption key to secure the stored backups.
        /// </summary>
        public string AESKey;

        public BackupSettings()
        {
            this.doBackups = false;
            this.timeoutInterval = 3600000;
            this.sizeLimit = 0;
            this.collections = new string[] { "hash", "rainbow", "token", "dictionary", "backup" };

            using (Aes aesAlg = Aes.Create())
            {
                this.AESKey = ByteArrayToHexString(aesAlg.Key);

            }
        }

        public BackupSettings(bool doBackups, int timeoutInterval, long sizeLimit)
        {
            this.doBackups = doBackups;
            this.timeoutInterval = timeoutInterval;
            this.sizeLimit = sizeLimit;
        }

        public static string ByteArrayToHexString(byte[] Bytes)
        {
            StringBuilder Result = new StringBuilder(Bytes.Length * 2);
            string HexAlphabet = "0123456789ABCDEF";

            foreach (byte B in Bytes)
            {
                Result.Append(HexAlphabet[(int)(B >> 4)]);
                Result.Append(HexAlphabet[(int)(B & 0xF)]);
            }

            return Result.ToString();
        }

        public bool isValid()
        {
            if (this.timeoutInterval < 60000 || this.sizeLimit < 0) return false;
            // validate the collection names and check for unique names
            foreach (string collectionName in this.collections)
            {
                if (!this.isCollectionNameValid(collectionName)) return false;
            }
            for (int i = 0; i < this.collections.Length; i++)
                for (int j = 0; j < this.collections.Length; j++)
                    if ((i != j) && this.collections[i].Equals(this.collections[j]))
                        return false;
            if (this.AESKey.Length != 64) return false;
            const string charset = "0123456789ABCDEF";
            for (int i = 0; i < AESKey.Length; i++)
            {
                if (charset.IndexOf(AESKey[i]) == -1)
                    return false;
            }

            return true;
        }

        private bool isCollectionNameValid(string collectionName)
        {
            if (collectionName == null || collectionName.Equals("") || collectionName.StartsWith("system.")) return false;
            return collectionName.IndexOfAny(new char[] { '/', '\\', '.', ' ', '"', '$', '*', '<', '>', ':', '|', '?', '\x00' }) == -1;
        }
    }
}
