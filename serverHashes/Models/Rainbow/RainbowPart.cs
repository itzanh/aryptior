﻿using serverHashes.Models.Client;
using System;

namespace serverHashes
{
    [Serializable]
    public class RainbowPart
    {
        public string uuid;
        public ClientHash hash;
        public BruteForceChunk bruteForceChunk;

        public RainbowPart()
        {
        }

        public RainbowPart(string uuid, Hash hash, BruteForceChunk bruteForceChunk)
        {
            this.uuid = uuid;
            this.hash = hash != null ? new ClientHash(hash) : null;
            this.bruteForceChunk = bruteForceChunk;
        }
    }
}
