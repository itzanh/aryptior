﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace serverHashes
{
    [Serializable]
    [BsonIgnoreExtraElements]
    public class Rainbow
    {
        public string uuid;
        public string name;
        public bool enabled;
        public Hash hash;

        public Rainbow()
        {
            this.name = "";
            this.enabled = true;
            this.hash = new Hash();
        }

        public Rainbow(Rainbow r)
        {
            this.uuid = r.uuid;
            this.name = r.name;
            this.enabled = r.enabled;
            this.hash = new Hash(r.hash);
        }

        public bool isValid()
        {
            return !(this.name == null || this.name.Length < 1 || this.name.Length > 128
                || this.hash == null || !this.hashIsValid(this.hash));
        }

        public bool hashIsValid(Hash h)
        {
            // check attributes for null values, zeroes and negative numbers
            return !(h.descripcio == null || h.initialTestCount <= 0 || h.msProcessingCount <= 0
                || h.primerCaracterUnicode < 32 || h.ultimCaracterUnicode <= h.primerCaracterUnicode || h.caractersFinals <= 1
                || h.iteracions <= 0 || h.salt == null || h.combinacionsPendents == null
                || h.lineesPendents == null || h.caracters == null || h.descripcio == null
                || h.descripcio.Length > 3000 || h.caracters.Length > 32 || h.salt.Length > 128
                || h.caractersFinals > 32);
        }

        public static bool isValid(Dictionary<string, dynamic> data)
        {
            foreach (string key in data.Keys)
            {
                dynamic value = data[key];
                switch (key)
                {
                    case "uuid":
                        {
                            if (!(value is string) || !Guid.TryParse(value, out Guid _)) return false;
                            break;
                        }
                    case "name":
                        {
                            if (!(value is string) || value == null || value.Length > 128) return false;
                            break;
                        }
                    case "descripcio":
                        {
                            if (!(value is string) || value == null || value.Length > 3000) return false;
                            break;
                        }
                    case "initialTestCount":
                        {
                            if (!(value is long) || value <= 0) return false;
                            break;
                        }
                    case "msProcessingCount":
                        {
                            if (!(value is long) || value <= 0) return false;
                            break;
                        }
                    case "caractersFinals":
                        {
                            if (!(value is long) || value <= 1) return false;
                            break;
                        }
                    default:
                        {
                            return false;
                        }
                }
            }
            return true;
        }

        public bool fromDictionary(Dictionary<string, dynamic> data)
        {
            foreach (string key in data.Keys)
            {
                dynamic value = data[key];
                switch (key)
                {
                    case "uuid":
                        {
                            if (!value.Equals(this.uuid)) return false;
                            break;
                        }
                    case "name":
                        {
                            this.name = value;
                            break;
                        }
                    case "descripcio":
                        {
                            this.hash.descripcio = value;
                            break;
                        }
                    case "initialTestCount":
                        {
                            this.hash.initialTestCount = (int)value;
                            break;
                        }
                    case "msProcessingCount":
                        {
                            this.hash.msProcessingCount = (int)value;
                            break;
                        }
                    case "caractersFinals":
                        {
                            this.hash.caractersFinals = (int)value;
                            break;
                        }
                    default:
                        {
                            return false;
                        }
                }
            }
            return true;
        }

        public static Dictionary<string, dynamic> compareTo(Rainbow old, Rainbow newr)
        {
            Dictionary<string, dynamic> differences = new Dictionary<string, dynamic>();
            Dictionary<string, dynamic> hashDifferences = Hash.compareTo(old.hash, newr.hash);
            if (hashDifferences.Keys.Count > 0)
                differences.Add("hash", hashDifferences);

            foreach (FieldInfo oldProp in typeof(Rainbow).GetFields())
            {
                if (!oldProp.Name.Equals("hash") && !object.Equals(oldProp.GetValue(old), oldProp.GetValue(newr)))
                {
                    differences.Add(oldProp.Name, oldProp.GetValue(newr));
                }
            }

            return differences;
        }

        public override bool Equals(object obj)
        {
            return obj is Rainbow rainbow &&
                   this.uuid.Equals(rainbow.uuid);
        }

        public override int GetHashCode()
        {
            var hashCode = 1556130758;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(uuid);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(name);
            hashCode = hashCode * -1521134295 + enabled.GetHashCode();
            return hashCode;
        }
    }

    [Serializable]
    public class RainbowSearch
    {
        public string uuid;
        public string hash;

        public RainbowSearch()
        {
            this.uuid = String.Empty;
            this.hash = String.Empty;
        }
    }

    public class RainbowSearchResult
    {
        public enum RainbowSearchResultCode
        {
            ok,
            err,
            notFound
        }

        public RainbowSearchResultCode resultCode;
        public string result;

        public RainbowSearchResult()
        {
            this.resultCode = RainbowSearchResultCode.notFound;
            this.result = String.Empty;
        }

        public RainbowSearchResult(RainbowSearchResultCode resultCode)
        {
            this.resultCode = resultCode;
            this.result = String.Empty;
        }

        public RainbowSearchResult(string result)
        {
            this.resultCode = RainbowSearchResultCode.ok;
            this.result = result;
        }
    }
}
