﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading;

namespace serverHashes
{
    class Program
    {
        /// <summary>
        /// Configuracio actual del programa
        /// </summary>
        public static Settings CONFIG
        {
            get
            {
                lock (configMUTEX)
                {
                    return _CONFIG;
                }
            }
            set
            {
                lock (configMUTEX)
                {
                    _CONFIG = value;
                }
            }
        }
        private static Settings _CONFIG;
        private static readonly Semaphore configMUTEX = new Semaphore(1, 1);
        /// <summary>
        /// Indica si el servidor no te mes feina que fer
        /// </summary>
        public static bool estaIdle;
        /// <summary>
        /// Objecte que representa la tasca de processar un hash actualment en curs.
        /// </summary>
        public static Hash hashActual
        {
            get { lock (hashActualMUTEX) { return _hashActual; } }
            set { lock (hashActualMUTEX) { _hashActual = value; } }
        }
        private static readonly Semaphore hashActualMUTEX = new Semaphore(1, 1);
        private static Hash _hashActual;
        /// <summary>
        /// Llista de tots els client que processen hashes connectats al servidor
        /// </summary>
        public static ConcurrentDictionary<string, NetworkClient> llistaClients;
        /// <summary>
        /// Llista de les connexions de WebSocket al servidor
        /// </summary>
        public static ConcurrentDictionary<string, string> listWebSocketClients;
        /// <summary>
        /// Indica si ja hi ha un fil buscant en el diccionari el hash actual abans de començar
        /// </summary>
        public static bool estaProcessantLlista;
        /// <summary>
        /// Semaphore that only allows one processing client to be initialized at the same time.
        /// </summary>
        public static readonly Semaphore clientInitializationMutex = new Semaphore(1, 1);
        /// <summary>
        /// Allows the access to the program's main storage functions and settings.
        /// </summary>
        public static Storage storage;
        /// <summary>
        /// Contains the number of processing clients currently connected to this server.
        /// </summary>
        private static int clientsConnectedCount;
        /// <summary>
        /// MUTEX semaphore that allows the access to the above property of the client connection count.
        /// </summary>
        private static readonly Semaphore MUTEXclientsConnectedCount = new Semaphore(1, 1);
        /// <summary>
        /// If the network discovery service is running, the instance is referenced here.
        /// </summary>
        public static NetworkDiscovery networkDiscovery;
        /// <summary>
        /// If the backup cron service is running, the instance is referenced here.
        /// </summary>
        public static BackupCron backupCron;
        /// <summary>
        /// Publication/Subscriptions system for the websocket client to be aware of new changes on the server without 
        /// needing to periodically or manually refresh.
        /// This object emits permits to send events to a topis by its topis name, and allows to NetEventIO clients to subscribe and unsubscribe to the topics,
        /// so they can be notified about the changes the need to be aware of.
        /// </summary>
        public static PubSub websocketPubSub;
        /// <summary>
        /// This object does the same as 'websocketPubSub', but it's intended to be used by the processing clients and not by the frontend, 
        /// so the processing clients does not have full access to the news on the server side.
        /// </summary>
        public static PubSub clientPubSub;
        /// <summary>
        /// Counts the number of failed login attempts from the processing client, 
        /// where the key is te IP address and the value is the count of failed attemps.
        /// </summary>
        private static ConcurrentDictionary<string, int> failedAttemps;

        static void Main(string[] args)
        {
            if (!inicialitzarServidor(args))
            {
                Console.ReadKey(true);
                return;
            }

            // esperar connexions per al listener dels clients
            TcpListener listener = new TcpListener(System.Net.IPAddress.Parse(CONFIG.socketSettings.host), CONFIG.socketSettings.port);
            listener.Start();

            // preparar sistema publicació/subscripció
            websocketPubSub = new PubSub(new string[] { "hash", "dictionary", "client", "token", "rainbow", "backup", "backupCron", "logs" });
            clientPubSub = new PubSub(new string[] { "hash", "rainbow", "backup" });

            // gestio administrativa remota. crear el objecte que controal la gestio remota, 
            // conectar el listener, i rebre les connexions el altre fil
            WaitForWebSocket ws = new WaitForWebSocket(CONFIG.webSocketSettings);
            Thread threadWS = new Thread(new ThreadStart(ws.Run));
            threadWS.Start();

            // prepare options
            NetEventIOServer_Options options = prepareNetEventIOOptions();
            initializeLocalDiscovery(CONFIG);
            initializeBackupService(CONFIG);

            // sempre esperar noves connexions, acceptarles, crear un objecte de la clase
            while (true)
            {
                Console.WriteLine("Esperat connexio... " + CONFIG.socketSettings.port);

                // accept TCP connections
                TcpClient client = listener.AcceptTcpClient();
                incrementClientCount();

                // checks that the connection is valid and that the bound of maximum simultaneous connections has not been reached
                if (!CONFIG.socketSettings.connectionIsValid(client.Client.RemoteEndPoint.ToString().Split(':')[0])
                    || getClientCount() > CONFIG.socketSettings.maxClients)
                {
                    client.Close();
                    decrementClientCount();
                }
                else
                {
                    // get network stream
                    Console.WriteLine("Connexio acceptada. " + client.Client.RemoteEndPoint.ToString().Split(':')[0]);
                    NetworkStream ns = client.GetStream();

                    // start the network communication
                    NetworkConnection nc = new NetworkConnection(client, ns);

                    // handshake
                    if (!nc.handshake(CONFIG.socketSettings.mustUseServerSetings,
                        new SocketOptions(CONFIG.socketSettings.socketOptions.encryption, CONFIG.socketSettings.socketOptions.compression,
                        CONFIG.socketSettings.socketOptions.certUUID, CONFIG.socketSettings.socketOptions.password)))
                    {
                        Console.WriteLine("ERROR DURING HANDSHAKE!!! " + client.Client.RemoteEndPoint.ToString().Split(':')[0]);
                        client.Close();
                    }
                    else
                    {
                        // start the network communication through NetEventIO
                        NetEventIO clientIO = new NetEventIO(nc, options);
                        clientIO.onDisconnect = onDiconnect;

                        // create the object that represents the processing client and add to the list of clients
                        NetworkClient nouClientPerXarxa = new NetworkClient(client.Client.RemoteEndPoint.ToString());
                        nouClientPerXarxa.connection = clientIO;

                        // start a new thread to attend the new client in the background
                        new Thread(new ThreadStart(() =>
                        {
                            clientIO.Run(false);
                        })).Start();

                        Controller.processarMissatge(clientIO);
                        if (nc.isConnected())
                            llistaClients.TryAdd(clientIO.id, nouClientPerXarxa);

                        // emit connection event
                        websocketPubSub.onPush("client", SubscriptionChangeType.insert, -1,
                            JsonConvert.SerializeObject(new NetworkClientUpdate(clientIO.id, nouClientPerXarxa)));
                        // log connection
                        storage.onLog(LogType.ClientConnection, new LogClient(client.Client.RemoteEndPoint.ToString().Split(':')[0], clientIO.id));
                    }
                }
            }
        }

        private static void resetFailedAttemps(string addr)
        {
            int autoBanMaxAttemps = CONFIG.socketSettings.autoBanMaxAttemps;
            if (autoBanMaxAttemps > 0 && failedAttemps.TryGetValue(addr, out _))
                failedAttemps.TryRemove(addr, out _);
        }

        private static void addFailedAttemp(NetEventIO client)
        {
            int autoBanMaxAttemps = CONFIG.socketSettings.autoBanMaxAttemps;
            string addr = client.client.addr.Split(':')[0];
            if (autoBanMaxAttemps > 0)
            {
                int times;
                if (failedAttemps.TryGetValue(addr, out times))
                    times++;
                else
                    times = 1;

                if (times > autoBanMaxAttemps)
                {
                    CONFIG.socketSettings.addToBlacklist(addr);
                    storage.saveSettings(CONFIG);
                    client.client.disconnect();
                    resetFailedAttemps(addr);
                }
                else
                {
                    failedAttemps[addr] = times;
                }
            }
        }

        public static void initializeBackupService(Settings s)
        {
            // initialize backup services
            if (s.backupSettings.doBackups)
            {
                backupCron = new BackupCron(storage.mongo, s.backupSettings.timeoutInterval, s.backupSettings.sizeLimit,
                    s.backupSettings.collections, s.backupSettings.AESKey);
                backupCron.onBackupCompleted = (Backup b) =>
                {
                    websocketPubSub.onPush("backup", SubscriptionChangeType.insert, -1, JsonConvert.SerializeObject(b));
                    clientPubSub.onPush("backup", SubscriptionChangeType.insert, -1, JsonConvert.SerializeObject(b));
                };
                backupCron.onBackupDeleted = (string uuid) =>
                {
                    websocketPubSub.onPush("backup", SubscriptionChangeType.delete, -1, uuid);
                    clientPubSub.onPush("backup", SubscriptionChangeType.delete, -1, uuid);
                };
                backupCron.onBackupCronStateChanged = (BackupCron.BackupCronState cronState) =>
                {
                    websocketPubSub.onPush("backupCron", SubscriptionChangeType.update, -1, WebSocketBackup.cronStatus());
                };
                backupCron.Run();
            }
        }

        public static void initializeLocalDiscovery(Settings s)
        {
            // repond to local discovery signals
            if (s.isDiscoverable)
            {
                networkDiscovery = new NetworkDiscovery(s.socketSettings.host,
                    new NetworkDiscoveryData(s.socketSettings.port, s.webSocketSettings.port, s.discoverableName));
                networkDiscovery.Run();
            }
        }

        private static NetEventIOServer_Options prepareNetEventIOOptions()
        {

            // preparar opcions per al escoltador de events
            return new NetEventIOServer_Options((NetEventIO client, string pwd) =>
            {
                if (!CONFIG.socketSettings.requireClientPassword) return true;
                if (Utilities.verifyHash(
                    CONFIG.socketSettings.pwd.hashSHA512,
                    CONFIG.socketSettings.pwd.salt + pwd, Tipus.SHA512, CONFIG.socketSettings.pwd.iterations
                    ))
                {
                    resetFailedAttemps(client.client.addr.Split(':')[0]);
                    return true;
                }
                else
                {
                    addFailedAttemp(client);
                    return false;
                }
            }, (NetEventIO client, string token, string addr) =>
            {
                return false;
            }, (NetEventIO sender, string topicName) =>
            {
                if (topicName.Equals("backup") || topicName.Equals("hash") || topicName.Equals("rainbow"))
                    return clientPubSub.onSubscribe(sender, topicName);
                return false;
            }, clientPubSub.onUnsubscribe, clientPubSub.onUnsubscribeAll);
        }

        public static void incrementClientCount()
        {
            try
            {
                MUTEXclientsConnectedCount.WaitOne();
                clientsConnectedCount++;
                MUTEXclientsConnectedCount.Release();
            }
            catch (Exception) { }
        }

        public static void decrementClientCount()
        {
            try
            {
                MUTEXclientsConnectedCount.WaitOne();
                clientsConnectedCount--;
                MUTEXclientsConnectedCount.Release();
            }
            catch (Exception) { }
        }

        public static int getClientCount()
        {
            try
            {
                MUTEXclientsConnectedCount.WaitOne();
                int count = clientsConnectedCount;
                MUTEXclientsConnectedCount.Release();
                return count;
            }
            catch (Exception) { }
            return -1;
        }

        private static void onDiconnect(NetEventIO sender)
        {
            decrementClientCount();
            clientPubSub.onUnsubscribeAll(sender);
            string clientid = sender.id;
            NetworkClient c = llistaClients[clientid];
            if (c.processing)
            {
                if (!c.isProcessingRainbow)
                {
                    if (c.bruteForceChunk != null)
                    {
                        combinacioPendent(c.bruteForceChunk, c.processingUUID, c.processing);
                        Console.WriteLine("Queueing unprocessed data by disconnection: "
                            + combinacioAString(c.bruteForceChunk.characters));
                    }
                    else if (c.dictionaryProcessingChunk != null)
                    {
                        combinacioPendent(c.dictionaryProcessingChunk, c.processingUUID, c.processing);
                    }
                }
                else
                {
                    if (c.bruteForceChunk != null)
                        storage.rainbowPendingCombination(c.bruteForceChunk);
                    else if (c.dictionaryProcessingChunk != null)
                        storage.rainbowPendingCombination(c.dictionaryProcessingChunk);
                }
            }
            if (c.rainbowStorage != null)
            {
                c.rainbowStorage = null;
            }
            eliminarClient(clientid);
            storage.onLog(LogType.ClientDisconnection, new LogClient(sender.client.addr.Split(':')[0], sender.id));
        }

        /// <summary>
        /// Inicialitza les dades de este servidor a les dades guardades en fitxer.
        /// Primer inicialitza el EngamatzenamentLocal i despres llig les dades serialitzades en binari que hi ha en els fitxers.
        /// Per ultim, crea un thread que serialitza i guarda automaticament la cua de processament cada 60 segons.
        /// </summary>
        private static bool inicialitzarServidor(string[] args)
        {
            /* SUPER COOL LOADING ASCII ART
                                         _   _  ____  _____  
                                        | | (_)/ __ \|  __ \ 
                   __ _ _ __ _   _ _ __ | |_ _| |  | | |__) |
                  / _` | '__| | | | '_ \| __| | |  | |  _  / 
                 | (_| | |  | |_| | |_) | |_| | |__| | | \ \ 
                  \__,_|_|   \__, | .__/ \__|_|\____/|_|  \_\
                              __/ | |                        
                             |___/|_|                        
             */
            Console.WriteLine("                         _   _  ____  _____  \n"
                + "                        | | (_)/ __ \\|  __ \\ \n"
                + "   __ _ _ __ _   _ _ __ | |_ _| |  | | |__) |\n"
                + "  / _` | '__| | | | '_ \\| __| | |  | |  _  / \n"
                + " | (_| | |  | |_| | |_) | |_| | |__| | | \\ \\ \n"
                + "  \\__,_|_|   \\__, | .__/ \\__|_|\\____/|_|  \\_\\\n"
                + "              __/ | |                        \n"
                + "             |___/|_|                        \n");

            estaIdle = true;
            estaProcessantLlista = false;

            // llegir la configuracio del programa
            Dictionary<string, string> parameters = parseArgs(args);
            if (parameters != null && (parameters.ContainsKey("configPath")))
            {
                string settingsFilename = parameters["configPath"];
                if (!settingsFilename.EndsWith(".yaml") && !settingsFilename.EndsWith(".yml") && !settingsFilename.EndsWith(".json"))
                {
                    Console.WriteLine("ERROR! THE SERVER ONLY SUPPORTS JSON AND YAML SERIALIZATION FOR THE SETTINGS FILE.\n" +
                        "THE ONY ALLOWED EXTENSIONS FOR THE CONFIG FILE ARE .yaml, .yml and .json");
                    return false;
                }
                CONFIG = LocalStorage.readSettings(settingsFilename);
                CONFIG.localStorage.configPath = settingsFilename;
            }
            else
            {
                CONFIG = LocalStorage.readSettings(LocalStorage.CONFIGURATION_FILENAME);
            }

            if (CONFIG == null)
            {
                Console.WriteLine("ERROR! COULD NOT READ THE SERVER'S SETTINGS FILE.");
                return false;
            }
            if (!CONFIG.isValid())
            {
                Console.WriteLine("ERROR! INVALID SETTINGS DETECTED. CHECK THE CONFIGURATION FILE." +
                    " [ http://izcloud.tk/index.php/documentacion-servidor-configuracion/ ]");
                return false;
            }

            parseParametersToSettings(parameters);
            if (parameters != null && (parameters.ContainsKey("--help") || parameters.ContainsKey("-help") || parameters.ContainsKey("help")
                    || parameters.ContainsKey("-h") || parameters.ContainsKey("h")))
            {
                Utilities.printHelp();
                return false;
            }
            if (parameters != null && parameters.ContainsKey("modifyPasswd"))
            {
                Console.WriteLine("CHANGING PASSWORD, PLEASE WAIT...");
                string modifyPasswd = parameters["modifyPasswd"];
                int type;
                if (modifyPasswd == null)
                    return false;
                else if (modifyPasswd.Equals("clients"))
                    type = 0;
                else if (modifyPasswd.Equals("admin"))
                    type = 1;
                else
                {
                    Console.WriteLine("COULD NOT CHANGE PASSWORD! CHECK THE PARAMETERS.");
                    return false;
                }
                if (changePasswordFromParameters(parameters, type))
                    Console.WriteLine("PASSWORD CHANGED!");
                else
                {
                    Console.WriteLine("THERE WAS AN ERROR CHANGING THE PASSWORD!");
                    return false;
                }
            }

            llistaClients = new ConcurrentDictionary<string, NetworkClient>();
            listWebSocketClients = new ConcurrentDictionary<string, string>();
            failedAttemps = new ConcurrentDictionary<string, int>();
            // inicialitzar el enmagatzenament local i llegir
            storage = new Storage(CONFIG);
            // check the certificates exist in case security is set to be used
            if ((CONFIG.socketSettings.socketOptions.encryption
                && storage.ls.readCertificate(CONFIG.socketSettings.socketOptions.certUUID) == null)
                || (CONFIG.webSocketSettings.isSecure
                && storage.ls.readCertificate(CONFIG.webSocketSettings.certUUID) == null))
            {
                Console.WriteLine("ERROR! COULD NOT READ THE SPECIFIED CERTIFICATE IN THE SETTINGS FILE." +
                    " SELECT ANOTHER ONE OR DIABLE THE SECURITY.");
                return false;
            }
            // initialize system metrics
            Dashboard _ = storage.getDashboard();
            // llegir, establir al seguent hash i calcular si el servidor esta o no IDLE
            Hash h;
            Program.estaIdle = Program.storage.getNextHash(out h);
            hashActual = h;

            // initialize the publish/subscription by listening the events in the storage class
            storage.onChange = (string topicName, SubscriptionChangeType changeType, int pos, string newValue) =>
            {
                websocketPubSub.onPush(topicName, changeType, pos, newValue);

                if (
                (topicName.Equals("backup") && (changeType == SubscriptionChangeType.insert
                || changeType == SubscriptionChangeType.delete))
                || (topicName.Equals("rainbow") && (changeType == SubscriptionChangeType.insert
                || changeType == SubscriptionChangeType.update))
                || (topicName.Equals("hash") && changeType == SubscriptionChangeType.insert))
                    clientPubSub.onPush(topicName, changeType, pos, newValue);
            };

            return true;
        }

        /// <summary>
        /// Changes both the clients and admin password throught parsed command line argument
        /// </summary>
        /// <param name="parameters">command line arguments</param>
        /// <param name="type">0 = clients, 1 = admin</param>
        private static bool changePasswordFromParameters(Dictionary<string, string> parameters, int type)
        {
            try
            {
                // get password
                string password = parameters["passwd"];
                if (password == null || password.Length < 4)
                    return false;

                // get or generate salt
                string salt;
                if (!parameters.TryGetValue("salt", out salt))
                    salt = SocketPassword.generateSalt();

                // get iterations, or use the already defined ones
                int iterations;
                if (parameters.TryGetValue("iterations", out string iter))
                    iterations = Int32.Parse(iter);
                else if (type == 0)
                    iterations = CONFIG.socketSettings.pwd.iterations;
                else if (type == 1)
                    iterations = CONFIG.webSocketSettings.pwd.iterations;
                else
                    return false;

                // change password
                SocketPassword newSocketPassword = new SocketPassword(iterations, salt,
                    Utilities.computeSHA512Hash(salt + password, iterations));
                if (!newSocketPassword.isValid())
                    return false;

                if (type == 0)
                    CONFIG.socketSettings.pwd = newSocketPassword;
                else if (type == 1)
                    CONFIG.webSocketSettings.pwd = newSocketPassword;
                else
                    return false;

                return LocalStorage.writeSettings(CONFIG, CONFIG.localStorage.configPath);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static Dictionary<string, string> parseArgs(string[] args)
        {
            if (args.Length == 0)
                return null;

            Dictionary<string, string> parameters = new Dictionary<string, string>();

            foreach (string arg in args)
            {
                int value = arg.IndexOf('=');
                parameters[(value == -1 ? arg : arg.Substring(0, value))] = (value > -1 ? arg.Substring(value + 1) : null);
            }

            return parameters;
        }

        private static void parseParametersToSettings(Dictionary<string, string> parameters)
        {
            if (parameters == null)
                return;

            Settings sts = CONFIG.Clone();
            try
            {
                string value;
                foreach (string key in parameters.Keys)
                {
                    value = parameters[key];
                    switch (key)
                    {
                        case "host":
                            {
                                sts.socketSettings.host = value;
                                break;
                            }
                        case "port":
                            {
                                sts.socketSettings.port = Int32.Parse(value);
                                break;
                            }
                        case "hostws":
                            {
                                sts.webSocketSettings.host = value;
                                break;
                            }
                        case "portws":
                            {
                                sts.webSocketSettings.port = Int32.Parse(value);
                                break;
                            }
                        case "requireClientPassword":
                            {
                                sts.socketSettings.requireClientPassword = Boolean.Parse(value);
                                break;
                            }
                        case "useDatabase":
                            {
                                sts.mongoStorage.useDatabase = Boolean.Parse(value);
                                break;
                            }
                        case "dburl":
                            {
                                sts.mongoStorage.url = value;
                                break;
                            }
                        case "mongohost":
                            {
                                sts.mongoStorage.host = value;
                                break;
                            }
                        case "mongoport":
                            {
                                sts.mongoStorage.port = Int32.Parse(value);
                                break;
                            }
                        case "userName":
                            {
                                sts.mongoStorage.userName = value;
                                break;
                            }
                        case "dbpassword":
                            {
                                sts.mongoStorage.password = value;
                                break;
                            }
                        case "databaseName":
                            {
                                sts.mongoStorage.databaseName = value;
                                break;
                            }
                        case "mongoDbAuthMechanism":
                            {
                                sts.mongoStorage.mongoDbAuthMechanism = value;
                                break;
                            }
                        case "isDiscoverable":
                            {
                                sts.isDiscoverable = Boolean.Parse(value);
                                break;
                            }
                        case "discoverableName":
                            {
                                sts.discoverableName = value;
                                break;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not parse input parameters. Settings in config.json will be used instead.");
                Console.WriteLine(e.ToString());
                Utilities.printHelp();
            }
            finally
            {
                if (sts.isValid())
                {
                    CONFIG = sts;
                }
                else
                {
                    Console.WriteLine("Could not parse input parameters. Settings in config.json will be used instead.");

                }
            }
        }

        public static bool passwordFound(string missatge)
        {
            // comprovar si es veritat que se ha trobat la contrasenya calculant el hash 
            // de la contrasenya que ens han donat i comparantla en el hash del treball actual
            string hashResultat = hashActual.salt + missatge;
            if (Utilities.verifyHash(hashActual.hash, hashResultat, hashActual.tipus, hashActual.iteracions))
            {
                // se ha trobat correctament la contrasenya i se ha resolt el hash actual
                Console.WriteLine("SE HA TROBAT LA CONTRASENYA " + missatge);
                // guardar el amb el resultat hash
                lock (clientInitializationMutex)
                {
                    hashActual.estaResolt = true;
                    hashActual.solucio = missatge;
                    hashActual.dateFinished = DateTime.UtcNow;
                    hashActual.combinacionsPendents = null;
                    hashActual.lineesPendents = null;
                    storage.updateHash(hashActual);
                    // set to IDLE till a new client requests the next queued hash
                    onHashFinished(hashActual);
                    Hash h;
                    estaIdle = storage.getNextHash(out h);
                    hashActual = h;
                }
                return true;
            }
            else // informar si la contrasenya que ens donen no genera el hash
            {
                Console.WriteLine("ENS ESTAN INTENTANT ENGANYAR... " + hashActual.iteracions + " "
                    + hashResultat + " " + hashActual.hash);
                return false;
            }
        }

        public static bool passwordNotFound()
        {
            // se ha trobat correctament la contrasenya i se ha resolt el hash actual
            // guardar el amb el resultat hash
            lock (clientInitializationMutex)
            {
                Console.WriteLine("GIVING UP BREAKING HASH " + hashActual.hash);

                hashActual.estaResolt = true;
                hashActual.dateFinished = DateTime.UtcNow;
                hashActual.combinacionsPendents = null;
                hashActual.lineesPendents = null;
                storage.updateHash(hashActual);

                onHashFinished(hashActual);
                Hash h;
                estaIdle = storage.getNextHash(out h);
                hashActual = h;

            }
            return true;
        }

        private static void onHashFinished(Hash h)
        {
            foreach (NetworkClient client in llistaClients.Values)
            {
                client.hashRatePerMs = -1;
            }
        }

        /// <summary>
        /// Afegeix una combinacio a la llista de combinacions pendents de processat (per desconnexio)
        /// </summary>
        /// <param name="combinacioPendent"></param>
        private static void combinacioPendent(BruteForceChunk combinacioPendent, string processingUUID, bool processing)
        {
            if (!processing) return;
            // if the hash is the same as the one that is being currenly processed, add to the current queue
            if (hashActual != null && processingUUID.Equals(hashActual.uuid))
                hashActual.addPendingCombination(combinacioPendent);
            else // if not, grab the hash by id from the datastore
            {
                // CHECK IF IT'S ON THE QUEUE
                Hash h = storage.getQueuedHash(processingUUID);
                // check if the hash is not on the queue, because on that case, saving a pending combination will not be necessary
                if (h != null)
                {
                    // if the hash is enqueued or surrendered, but not finished, grab it from the database, modify the queue of pending combinations, and update
                    h.addPendingCombination(combinacioPendent);
                    storage.updateHash(h, true);
                    return;
                }

                // CHECK IF IT'S IN THE DATABASE
                Hash hash = storage.getHash(processingUUID);
                // check if the hash is finished or it's not anymore in the database, because on that case, saving a pending combination will not be necessary
                if (hash != null && !hash.estaResolt)
                {
                    hash.addPendingCombination(combinacioPendent);
                    storage.updateHash(hash, true);
                };

            }
        }

        private static void combinacioPendent(DictionaryProcessingChunk combinacioPendent, string processingUUID, bool processing)
        {
            if (!processing) return;
            // if the hash is the same as the one that is being currenly processed, add to the current queue
            if (hashActual != null && processingUUID.Equals(hashActual.uuid))
                hashActual.addPendingCombination(combinacioPendent);
            else
            {
                // CHECK IF IT'S ON THE QUEUE
                IEnumerable<Hash> h = from hashSel in storage.getAllQueuedHashes(out _) where (hashSel.uuid == processingUUID) select hashSel;
                // check if the hash is not on the queue, because on that case, saving a pending combination will not be necessary
                if (h.Count() == 1 && !h.ElementAt(0).estaResolt)
                {
                    // if the hash is enqueued or surrendered, but not finished, grab it from the database, modify the queue of pending combinations, and update
                    h.ElementAt(0).addPendingCombination(combinacioPendent);
                    storage.updateHash(h.ElementAt(0), true);
                    return;
                };

                // CHECK IF IT'S IN THE DATABASE
                Hash hash = storage.getHash(processingUUID);
                // check if the hash is finished or it's not anymore in the database, because on that case, saving a pending combination will not be necessary
                if (hash != null && !hash.estaResolt)
                {
                    hash.addPendingCombination(combinacioPendent);
                    storage.updateHash(hash, true);
                };

            }
        }

        public static void incrementarBlocCombinacio(ref Hash h, int contarCaracters)
        {
            for (int i = 0; i < contarCaracters; i++)
            {
                augmentarCombinacio(ref h.caracters, h.primerCaracterUnicode, h.ultimCaracterUnicode);
            }
        }

        public static void incrementarBlocCombinacio(ref int[] caracters, int primerCaracterUnicode, int ultimCaracterUnicode, int contarCaracters)
        {
            for (int i = 0; i < contarCaracters; i++)
            {
                augmentarCombinacio(ref caracters, primerCaracterUnicode, ultimCaracterUnicode);
            }
        }

        /// <summary>
        /// Augmenta en u la combinacio de caracters que actualment se esta probant per a la contrasenya.
        /// Va del primer al ultim caracter unicode, i, en acabar, afegeix altre caracter a la longitud de la combinacio.
        /// P.Ex. si utilitzem un rang de caracters en majuscules: A - B - C ... Z - AA - AB - AC... ZZ - AAA - AAB...
        /// </summary>
        static void augmentarCombinacio(ref int[] caracters, int primerCaracterUnicode, int ultimCaracterUnicode)
        {
            // augmentar el prrimer caracter
            caracters[0]++;
            for (int i = 0; i < caracters.Length; i++)
            {
                // comprovar si el caracter actual es mes gran que el maxim
                if (caracters[i] > ultimCaracterUnicode)
                {
                    // reinicialitzar al primer caracter la posicio actual
                    caracters[i] = primerCaracterUnicode;
                    // si la seguent posicio esta incialitzada, augmentar, si no, inicialitzar al primer caracter
                    if ((i + 2) > caracters.Length)
                    {
                        int[] longerCombination = new int[(caracters.Length + 1)];
                        for (int j = 0; j < caracters.Length; j++)
                        {
                            longerCombination[j + 1] = caracters[j];
                        }
                        longerCombination[0] = primerCaracterUnicode;
                        caracters = longerCombination;
                    }
                    else
                    {
                        caracters[(i + 1)]++;
                    }
                }
            }
        }

        /// <summary>
        /// Retorna la combinacio que se esta probant actualment contra la contrasenya com a String.
        /// </summary>
        /// <returns></returns>
        public static string combinacioAString()
        {
            return combinacioAString(hashActual.caracters);
        }

        /// <summary>
        /// Retorna una combinacio contra la contrasenya com a String.
        /// </summary>
        /// <param name="combionacio">La combinacio deu ser un array de numeros enters que representen caracters Unicode en decimal.</param>
        /// <returns></returns>
        public static string combinacioAString(int[] combionacio)
        {
            string combinacioCaracters = "";
            for (int i = (combionacio.Length - 1); i >= 0; i--)
            {
                combinacioCaracters += (char)combionacio[i];
            }
            return combinacioCaracters;
        }

        /// <summary>
        /// Setter per a eliminar un client de la llista. Utilitzat en cas de desconnexio.
        /// </summary>
        /// <param name="client"></param>
        public static bool eliminarClient(string clientid)
        {
            // remove from the dictionary
            bool result;
            try
            {
                result = llistaClients.TryRemove(clientid, out _);
            }
            catch (Exception) { return false; }

            // push an event on the client deletion
            if (result)
                websocketPubSub.onPush("client", SubscriptionChangeType.delete, -1, clientid);

            return result;
        }


    }
}
