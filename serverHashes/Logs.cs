﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;

namespace serverHashes
{
    /// <summary>
    /// Model
    /// </summary>
    [Serializable]
    [BsonIgnoreExtraElements]
    public class Log
    {
        [BsonIgnore]
        [JsonIgnore]
        public string uuid;
        public LogType type;
        public dynamic data;
        public DateTime dateTime;

        public Log(LogType type, dynamic data)
        {
            this.uuid = Guid.NewGuid().ToString();
            this.type = type;
            this.data = data;
            this.dateTime = DateTime.UtcNow;
        }
    }

    [Serializable]
    public enum LogType
    {
        ClientConnection,
        ClientDisconnection,
        ClientKick,
        ClientBan,
        InsertHash,
        FinishHash,
        EditHash,
        SurrenderHash,
        DeleteHash,
        WebClientConnection,
        WebClientDisconnection,
        RainbowCreation,
        RainbowEdition,
        RainbowDeletion,
        DictionaryCreation,
        DictionaryEdition,
        DictionaryDeletion,
        SettingsUpdated
    }

    [Serializable]
    public class LogConfig
    {
        public bool log;
        public bool logClients;
        public bool logWeb;
        public bool logHashes;
        public bool logRainbows;
        public bool logDictionaries;
        public bool logSettings;
        public int logDuration;

        public LogConfig()
        {
            this.log = true;
            this.logClients = true;
            this.logWeb = true;
            this.logHashes = true;
            this.logRainbows = true;
            this.logDictionaries = true;
            this.logSettings = true;
            this.logDuration = 86400; // default duration of 24h
        }

        public override bool Equals(object obj)
        {
            return obj is LogConfig config &&
                   log == config.log &&
                   logClients == config.logClients &&
                   logWeb == config.logWeb &&
                   logHashes == config.logHashes &&
                   logRainbows == config.logRainbows &&
                   logDictionaries == config.logDictionaries &&
                   logSettings == config.logSettings &&
                   logDuration == config.logDuration;
        }

        public bool isValid()
        {
            return !(this.logDuration < 60);
        }

        public override int GetHashCode()
        {
            var hashCode = -226294162;
            hashCode = hashCode * -1521134295 + log.GetHashCode();
            hashCode = hashCode * -1521134295 + logClients.GetHashCode();
            hashCode = hashCode * -1521134295 + logWeb.GetHashCode();
            hashCode = hashCode * -1521134295 + logHashes.GetHashCode();
            hashCode = hashCode * -1521134295 + logRainbows.GetHashCode();
            hashCode = hashCode * -1521134295 + logDictionaries.GetHashCode();
            hashCode = hashCode * -1521134295 + logSettings.GetHashCode();
            hashCode = hashCode * -1521134295 + logDuration.GetHashCode();
            return hashCode;
        }
    }

    [Serializable]
    public class WebSocketLogQuery : WebSocketQuery
    {
        public DateTime start;

        public WebSocketLogQuery()
        {
            this.start = DateTime.MinValue;
        }

    }

    [Serializable]
    public class LogClient
    {
        public string addr;
        public string uuid;

        public LogClient(string addr, string uuid)
        {
            this.addr = addr;
            this.uuid = uuid;
        }
    }

}

