﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace serverHashes
{
    class WaitForWebSocket
    {
        private static ConcurrentDictionary<string, int> failedAttemps = new ConcurrentDictionary<string, int>();

        private readonly string host;
        private readonly int port;

        public WaitForWebSocket(WebSocketSettings conf)
        {
            this.host = conf.host;
            this.port = conf.port;
        }

        public void Run()
        {
            // start listening connections
            TcpListener server = new TcpListener(System.Net.IPAddress.Parse(host), port);
            server.Start();

            // generate options for the future socket connections
            NetEventIOServer_Options options = new NetEventIOServer_Options(onWebSocketPasswordLogin, onWebSocketTokenLogin,
                Program.websocketPubSub.onSubscribe, Program.websocketPubSub.onUnsubscribe, Program.websocketPubSub.onUnsubscribeAll);

            Console.WriteLine("WebSocket server listening on " + host + ":" + port + ".");

            // wait for more TCP clients
            while (true)
            {
                TcpClient client = server.AcceptTcpClient();
                if (!Program.CONFIG.webSocketSettings.connectionIsValid(client.Client.RemoteEndPoint.ToString().Split(':')[0]))
                {
                    client.Close();
                    continue;
                }
                NetworkStream stream = client.GetStream();

                Console.WriteLine("WebSocket client connected :D");

                new Thread(new ThreadStart(() =>
                {
                    WebSocket ws = new WebSocket();

                    WebSocketConnection conn = new WebSocketConnection(client, stream);
                    if (Program.CONFIG.webSocketSettings.isSecure)
                    {
                        if (!conn.connect(
                            new X509Certificate2(
                                Program.storage.ls.readCertificate(Program.CONFIG.webSocketSettings.certUUID),
                                Program.CONFIG.webSocketSettings.password)))
                        {
                            Console.WriteLine("SSL ERROR!");
                        }
                    }
                    else
                    {
                        if (!conn.connect())
                        {
                            return;
                        }
                    }
                    Program.storage.onLog(LogType.WebClientConnection, conn.addr.Split(':')[0]);

                    NetEventIO clientIO = new NetEventIO(conn, options);
                    new Thread(new ThreadStart(clientIO.Run)).Start();

                    clientIO.onDisconnect = onWebSocketDisconnect;
                    ws.processarMissatge(clientIO);
                })).Start();
            }
        }

        public static bool onWebSocketPasswordLogin(NetEventIO client, string passwd)
        {
            Console.WriteLine("processant login... ");

            // recuperar la informacio del hash enmagatzemat
            int iteracions = Program.CONFIG.webSocketSettings.pwd.iterations;
            string salt = Program.CONFIG.webSocketSettings.pwd.salt;
            string hash = Program.CONFIG.webSocketSettings.pwd.hashSHA512;

            // comprovar si les contrasenyes coincideixen
            bool loggedIn = Utilities.verifyHash(hash, (salt + passwd), Tipus.SHA512, iteracions);

            if (loggedIn)
            {
                Console.WriteLine("login fet");
                resetFailedAttemps(client.client.addr.Split(':')[0]);
            }
            else
            {
                Console.WriteLine("login no fet");
                addFailedAttemp(client);

            }

            return loggedIn;
        }

        public static bool onWebSocketTokenLogin(NetEventIO client, string token, string addr)
        {
            Console.WriteLine("processant token... " + token + " " + addr);
            bool result = Program.storage.authToken(addr.Split(':')[0], token);
            if (result)
            {
                foreach (string tokenSel in Program.listWebSocketClients.Values)
                {
                    if (tokenSel.Equals(token))
                    {
                        result = false;
                        break;
                    }
                }

                Program.listWebSocketClients.TryAdd(client.id, token);
            }

            if (result)
                resetFailedAttemps(client.client.addr.Split(':')[0]);
            else
                addFailedAttemp(client);

            return result;
        }

        private static void resetFailedAttemps(string addr)
        {
            int autoBanMaxAttemps = Program.CONFIG.webSocketSettings.autoBanMaxAttemps;
            if (autoBanMaxAttemps > 0 && failedAttemps.TryGetValue(addr, out _))
                failedAttemps.TryRemove(addr, out _);
        }

        private static void addFailedAttemp(NetEventIO client)
        {
            int autoBanMaxAttemps = Program.CONFIG.webSocketSettings.autoBanMaxAttemps;
            string addr = client.client.addr.Split(':')[0];
            if (autoBanMaxAttemps > 0)
            {
                int times;
                if (failedAttemps.TryGetValue(addr, out times))
                    times++;
                else
                    times = 1;

                if (times > autoBanMaxAttemps)
                {
                    Program.CONFIG.webSocketSettings.addToBlacklist(addr);
                    Program.storage.saveSettings(Program.CONFIG);
                    client.client.disconnect();
                    resetFailedAttemps(addr);
                }
                else
                {
                    failedAttemps[addr] = times;
                }
            }
        }

        public static void onWebSocketDisconnect(NetEventIO sender)
        {
            Program.storage.onLog(LogType.WebClientDisconnection, sender.client.addr.Split(':')[0]);
            Program.websocketPubSub.onUnsubscribeAll(sender);
            resetFailedAttemps(sender.client.addr.Split(':')[0]);
            if (Program.listWebSocketClients.TryGetValue(sender.id, out _))
            {
                Program.listWebSocketClients.TryRemove(sender.id, out _);
            }
            Thread.CurrentThread.Abort();
        }

    }
}
