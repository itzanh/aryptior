﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace serverHashes
{
    class WebSocket
    {
        /// <summary>
        /// If it is setted to search the password on a dictionary on the server side before the processing, this stream opens the file.
        /// </summary>
        private FileStream fitxer = null;

        public WebSocket()
        {
            this.fitxer = null;
        }

        public void processarMissatge(NetEventIO client)
        {
            client.on("ping", (NetEventIO sender, OnMessageEventArgs msg, NetEventIO.Callback callback) =>
            {
                Console.WriteLine("PING!");
                callback("pong");
            });

            client.on("dashboard", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                callback(dashboard());
            });

            client.on("hash", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                if (e.message.command.Equals("test"))
                    WebSocketHashes.runClientPerformanceTest(e.message.message, callback);
                else
                    callback(WebSocketHashes.processHashCommand(e.message.command, e.message.message));
            });

            client.on("clients", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                WebSocketClients.handleClients(e.message.command, e.message.message, callback, sender);
            });

            client.on("dictionaries", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                callback(WebSocketDictionary.processDictionaryCommand(sender, e.message.command, e.message.message, ref this.fitxer));
            });

            client.on("settings", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                callback(processSettingsCommand(e.message.command, e.message.message));
            });

            client.on("pwd", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                callback(this.changePasswd(e.message.command, e.message.message));
            });

            client.on("token", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                callback(WebSocketToken.processarToken(e.message.command, e.message.message, sender.client.addr));
            });

            client.on("rainbow", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                callback(WebSocketRainbow.processRainbow(e.message.command, e.message.message));
            });

            client.on("backup", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                callback(WebSocketBackup.processMessage(sender, e.message.command, e.message.message));
            });

            client.on("cert", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                WebSocketCertificates.manageCert(sender, callback, e.message.command, e.message.message);
            });

            client.on("logs", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                callback(WebSocketLogs.processCommand(e.message.command, e.message.message));
            });

            client.on("save", (NetEventIO sender, OnMessageEventArgs e, NetEventIO.Callback callback) =>
            {
                guardar();
                callback("OK");
            });

            client.on("exit", (NetEventIO sender, OnMessageEventArgs e) =>
            {
                guardar();
                eixir();
            });

        } //processarMissatge

        private string changePasswd(string cmd, string msg)
        {
            if (cmd.Equals("init"))
            {
                if (msg.Equals(String.Empty))
                {
                    return LocalStorage.justInitializedSettings.ToString();
                }
                else if (msg.Equals("False"))
                {
                    LocalStorage.justInitializedSettings = false;
                    return "OK";
                }
                else
                {
                    return "ERR";
                }
            }

            SocketNewPassword newPassword;
            try
            {
                newPassword = (SocketNewPassword)JsonConvert.DeserializeObject(msg, typeof(SocketNewPassword));
            }
            catch (Exception)
            {
                return "ERR";
            }
            if (newPassword == null || !newPassword.isValid()) return "ERR";
            // compute hash and generate password string
            string hash = Utilities.computeSHA512Hash(newPassword.salt + newPassword.pwd, newPassword.iterations);
            // change settings and save
            Settings s = Program.CONFIG;
            switch (cmd)
            {
                case "webSocketSettings":
                    {
                        s.webSocketSettings.pwd = new SocketPassword(newPassword.iterations, newPassword.salt, hash);
                        break;
                    }
                case "clientPassword":
                    {
                        s.socketSettings.pwd = new SocketPassword(newPassword.iterations, newPassword.salt, hash);

                        break;
                    }
                default:
                    {
                        return "ERR";

                    }
            }
            LocalStorage.justInitializedSettings = false;

            return Program.storage.saveSettings(s) ? "OK" : "ERR";
        }

        private static string processSettingsCommand(string cmd, string msg)
        {
            switch (cmd)
            {
                case "readConf":
                    {
                        return sendSettings();
                    }
                case "writeConf":
                    {
                        if (loadSettings(msg))
                            return "OK";
                        else
                            return "ERR";
                    }
                case "configPath":
                    {
                        return Program.CONFIG.localStorage.configPath;
                    }
            }
            return "ERR";
        }

        private static string sendSettings()
        {
            return JsonConvert.SerializeObject(Program.CONFIG);
        }

        private static bool loadSettings(string confParsejar)
        {
            try
            {
                Settings novaConf = (Settings)JsonConvert.DeserializeObject(confParsejar, typeof(Settings));
                if (novaConf == null || !novaConf.isValid() || ((novaConf.socketSettings.socketOptions.encryption
                && Program.storage.ls.readCertificate(novaConf.socketSettings.socketOptions.certUUID) == null)
                || (novaConf.webSocketSettings.isSecure
                && Program.storage.ls.readCertificate(novaConf.webSocketSettings.certUUID) == null)))
                    return false;
                return Program.storage.saveSettings(novaConf);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        private static string dashboard()
        {
            return JsonConvert.SerializeObject(Program.storage.getDashboard());
        }

        private static void eixir()
        {
            guardar();
            Environment.Exit(0);
        }

        private static void guardar()
        {
            Program.storage.saveAllData();
        }
    }
}
