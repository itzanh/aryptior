import React, { Component } from 'react';

class Alert extends Component {

    constructor({ title, body }) {
        super();
        this.title = title ? title : 'Error!';
        this.body = body ? body : 'An error has ocurred!';
    }

    componentDidMount() {
        window.$('#alertModalCenter').modal({ show: true });
    }

    closeModal() {
        window.$('#alertModalCenter').modal('hide');
    }

    render() {
        return (
            <div className="modal fade" id="alertModalCenter" tabIndex="-1" role="dialog" aria-labelledby="alertModalCenter" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">{this.title}</h5>
                            <button type="button" className="close" aria-label="Close" onClick={this.closeModal}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>{this.body}</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-danger" onClick={this.closeModal}>Close</button>
                        </div>
                    </div>
                </div>
            </div>)
    }
};

export default Alert;
