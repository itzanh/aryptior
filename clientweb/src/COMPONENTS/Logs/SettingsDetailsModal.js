import React, { Component } from 'react';
import ReactDOM from 'react-dom';


// IMG
import socketICO from './../../IMG/socket.svg';
import websocketICO from './../../IMG/websocket.svg';
import enmagatzenamentICO from './../../IMG/enmagatzenament.svg';
import mongodbICO from './../../IMG/mongodb_logo.png';
import backupICO from "./../../IMG/backup.svg";
import logsICO from "./../../IMG/logs.svg";

class SettingsDetailsModal extends Component {

    constructor({ settings, exportarAJSON }) {
        super();

        this.ConfigServer = settings;
        this.exportarAJSON = exportarAJSON;
    }

    componentDidMount() {
        window.$('#settingsDetails').modal({ show: true });

        this.formatSizeUnit();
        this.formatTimeUnit();

        this.renderBlackList();
        this.renderWhiteList();
    }

    componentWillUnmount() {
        window.$('#settingsDetails').modal('hide');
    }

    formatSizeUnit() {
        var maxMessageSizeUnit = parseInt(this.refs.maxMessageSizeUnit.value);
        var maxMessageSize = parseInt(this.refs.maxMessageSize.value);
        var sizeLimitUnit = parseInt(this.refs.sizeLimitUnit.value);
        var sizeLimit = parseInt(this.refs.sizeLimit.value);

        while (maxMessageSize >= 1000 && maxMessageSizeUnit < 3) {
            maxMessageSize /= 1000;
            maxMessageSizeUnit++;
        }
        while (sizeLimit >= 1000 && sizeLimitUnit < 3) {
            sizeLimit /= 1000;
            sizeLimitUnit++;
        }

        this.refs.maxMessageSizeUnit.value = maxMessageSizeUnit;
        this.refs.maxMessageSize.value = maxMessageSize;
        this.refs.sizeLimitUnit.value = sizeLimitUnit;
        this.refs.sizeLimit.value = sizeLimit;
    }

    formatTimeUnit() {
        // backup: interval
        var timeoutIntervalUnit = parseInt(this.refs.timeoutIntervalUnit.value);
        var timeoutInterval = parseInt(this.refs.timeoutInterval.value);

        timeoutInterval /= 1000;
        while (timeoutInterval >= 60 && timeoutIntervalUnit < 2) {
            timeoutInterval /= 60;
            timeoutIntervalUnit++;
        }

        this.refs.timeoutIntervalUnit.value = timeoutIntervalUnit;
        this.refs.timeoutInterval.value = timeoutInterval;

        // logs: log duration
        var logDurationUnit = parseInt(this.refs.logDurationUnit.value);
        var logDuration = parseInt(this.refs.logDuration.value);

        while (logDuration >= 60 && logDurationUnit < 2) {
            logDuration /= 60;
            logDurationUnit++;
        }

        if (logDuration >= 24) {
            logDuration /= 24;
            logDurationUnit++;
        }

        this.refs.logDurationUnit.value = logDurationUnit;
        this.refs.logDuration.value = logDuration;
    }

    renderBlackList() {
        ReactDOM.unmountComponentAtNode(document.getElementById('srenderBlackList'));
        ReactDOM.render(
            this.ConfigServer.socketSettings.blackList.map((element, i) => {
                return (<SocketAddr
                    key={i}

                    addr={element}
                />);
            }), document.getElementById('srenderBlackList'));

        ReactDOM.unmountComponentAtNode(document.getElementById('wsrenderBlackList'));
        ReactDOM.render(
            this.ConfigServer.webSocketSettings.blackList.map((element, i) => {
                return (<SocketAddr
                    key={i}

                    addr={element}
                />);
            }), document.getElementById('wsrenderBlackList'));
    }

    renderWhiteList() {
        ReactDOM.unmountComponentAtNode(document.getElementById('srenderWhiteList'));
        ReactDOM.render(
            this.ConfigServer.socketSettings.whiteList.map((element, i) => {
                return (<SocketAddr
                    key={i}

                    addr={element}
                />);
            }), document.getElementById('srenderWhiteList'));

        ReactDOM.unmountComponentAtNode(document.getElementById('wsrenderWhiteList'));
        ReactDOM.render(
            this.ConfigServer.webSocketSettings.whiteList.map((element, i) => {
                return (<SocketAddr
                    key={i}

                    addr={element}
                />);
            }), document.getElementById('wsrenderWhiteList'));
    }

    render() {
        return (
            <div className="modal fade" id="settingsDetails" tabIndex="-1" role="dialog" aria-labelledby="settingsDetails" aria-hidden="false">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">Settings</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div id="ajustosServer">
                                <form>{
                                    // MAIN SERVER'S SETTINGS
                                }
                                    <div id="confGeneral">
                                        <label className="switch">
                                            <input type="checkbox" disabled={true} id="isDiscoverable" ref="isDiscoverable" defaultChecked={this.ConfigServer.isDiscoverable} />
                                            <span className="slider"></span>
                                        </label>
                                        <label htmlFor="isDiscoverable">Is discoverable over the network?</label>
                                        <br />
                                        <label htmlFor="discoverableName">Discovery name:</label>
                                        <br />
                                        <input type="text" id="discoverableName" className="form-control" ref="discoverableName" defaultValue={this.ConfigServer.discoverableName} readOnly />
                                        <br />
                                        <br />
                                    </div>
                                    <div id="confSocket">
                                        <h4 className="settingsTitle"><img src={socketICO} alt="socketICO" />Socket</h4>
                                        <div className="form-row">
                                            <div className="col">
                                                <label htmlFor="host">HOST:</label>
                                                <input type="text" id="host" className="form-control" ref="host" defaultValue={this.ConfigServer.socketSettings.host} readOnly />
                                            </div>
                                            <div className="col">
                                                <label htmlFor="port">PORT:</label>
                                                <input type="number" className="form-control" id="port" ref="port" min="0" max="65535" defaultValue={this.ConfigServer.socketSettings.port} readOnly />
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="col">
                                                <label htmlFor="maxMessageSize">Maximum message size:</label>
                                                <div className="input-group">
                                                    <input type="number" id="maxMessageSize" ref="maxMessageSize" className="form-control" min="0" defaultValue={this.ConfigServer.socketSettings.maxMessageSize} readOnly />
                                                    <div className="input-group-append">
                                                        <select id="maxMessageSizeUnit" ref="maxMessageSizeUnit" className="form-control" disabled={true}>
                                                            <option value="0">Bytes</option>
                                                            <option value="1">Kb</option>
                                                            <option value="2">Mb</option>
                                                            <option value="3">Gb</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <label htmlFor="maxClients">Maximum number of clients:</label>
                                                <input type="number" id="maxClients" ref="maxClients" className="form-control" min="1" defaultValue={this.ConfigServer.socketSettings.maxClients} readOnly />
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <h6>SOCKET OPTIONS</h6>
                                        <div className="form-group">
                                            <label className="switch">
                                                <input type="checkbox" id="encryption" ref="encryption" defaultChecked={this.ConfigServer.socketSettings.socketOptions.encryption} disabled={true} />
                                                <span className="slider"></span>
                                            </label>
                                            <label htmlFor="buscarEnLLista">Encrypt?</label>
                                            <br />
                                            <label className="switch">
                                                <input type="checkbox" id="compression" ref="compression" defaultChecked={this.ConfigServer.socketSettings.socketOptions.compression} disabled={true} />
                                                <span className="slider"></span>
                                            </label>
                                            <label htmlFor="buscarEnLLista">Compress?</label>
                                            <div className="row">
                                                <div className="col">
                                                    <label htmlFor="sCertUUID">Select the certificate:</label>
                                                    <input type="text" disabled={true} id="sCertUUID" ref="sCertUUID" className="form-control" defaultValue={this.ConfigServer.socketSettings.socketOptions.certUUID} />
                                                </div>
                                                <div className="col">
                                                    <label htmlFor="sCertPassword">Password of the certificate:</label>
                                                    <input disabled={true} type="password" id="sCertPassword" className="form-control" ref="sCertPassword" defaultValue={this.ConfigServer.socketSettings.socketOptions.password} />
                                                </div>
                                            </div>
                                            <label className="switch">
                                                <input type="checkbox" disabled={true} id="mustUseServerSetings" ref="mustUseServerSetings" defaultChecked={this.ConfigServer.socketSettings.mustUseServerSetings} />
                                                <span className="slider"></span>
                                            </label>
                                            <label htmlFor="mustUseServerSetings">Client's socket settings must be the same?</label>
                                            <br />
                                            <div className="form-row">
                                                <div className="col">
                                                    <label htmlFor="host">Network timeout (seconds):</label>
                                                    <input type="number" disabled={true} id="networkTimeout" className="form-control" ref="networkTimeout" defaultValue={this.ConfigServer.socketSettings.socketOptions.networkTimeout / 1000} />
                                                </div>
                                                <div className="col">
                                                    <label htmlFor="port">Ping interval (seconds):</label>
                                                    <input type="number" disabled={true} className="form-control" id="pingInterval" ref="pingInterval" min="0" defaultValue={this.ConfigServer.socketSettings.socketOptions.pingInterval / 1000} />
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div className="form-group">
                                            <h6>CLIENTS PASSWORD:</h6>
                                            <label className="switch">
                                                <input type="checkbox" disabled={true} id="requireClientPassword" ref="requireClientPassword" defaultChecked={this.ConfigServer.socketSettings.requireClientPassword} />
                                                <span className="slider"></span>
                                            </label>
                                            <label htmlFor="buscarEnLLista">Client requires a password to log in?</label>
                                            <div className="form-row">
                                                <div className="col">
                                                    <label htmlFor="iterPasswdWS">Hash interations:</label>
                                                    <input type="number" disabled={true} id="iterPasswdClient" ref="iterPasswdClient" className="form-control" min="0" defaultValue={this.ConfigServer.socketSettings.pwd.iterations} />
                                                </div>
                                                <div className="col">
                                                    <label htmlFor="saltPasswdWS">Salt:</label>
                                                    <input type="text" disabled={true} id="saltPasswdClient" ref="saltPasswdClient" className="form-control" defaultValue={this.ConfigServer.socketSettings.pwd.salt} />
                                                </div>
                                            </div>
                                            <label htmlFor="passwdWS">Hashed password (SHA-512 only):</label>
                                            <br />
                                            <input type="text" disabled={true} id="passwdClient" className="form-control" ref="passwdClient" defaultValue={this.ConfigServer.socketSettings.pwd.hashSHA512} />
                                            <p id="preguntarCanviantPWD"></p>
                                            <label htmlFor="sautoBanMaxAttemps">Maximum login attempts before autoban:</label>
                                            <input type="number" disabled={true} id="sautoBanMaxAttemps" ref="sautoBanMaxAttemps" className="form-control" min="1" defaultValue={this.ConfigServer.socketSettings.autoBanMaxAttemps} />
                                        </div>
                                        <div>
                                            <h3>Black List</h3>
                                            <table className="table table-dark">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">IP Address</th>
                                                        <th scope="col">Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="srenderBlackList">
                                                </tbody>
                                            </table>
                                            <br />
                                            <br />
                                            <h3>White List</h3>
                                            <label className="switch">
                                                <input type="checkbox" disabled={true} id="useWhiteList" name="useWhiteList" defaultChecked={this.ConfigServer.socketSettings.useWhiteList} />
                                                <span className="slider"></span>
                                            </label>
                                            <label htmlFor="useWhiteList">Use whitelist?</label>
                                            <br />
                                            <table className="table table-dark">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">IP Address</th>
                                                        <th scope="col">Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="srenderWhiteList">
                                                </tbody>
                                            </table>
                                        </div>
                                        <br />
                                    </div>
                                    <div id="confWebSocket">
                                        <h4 className="settingsTitle"><img src={websocketICO} alt="websocketICO" />WebSocket</h4>
                                        <div className="form-row">
                                            <div className="col">
                                                <label htmlFor="hostWS">WEBSOCKET HOST:</label>
                                                <input type="text" disabled={true} id="hostWS" className="form-control" ref="hostWS" defaultValue={this.ConfigServer.webSocketSettings.host} />
                                            </div>
                                            <div className="col">
                                                <label htmlFor="portWS">WEBSOCKET PORT:</label>
                                                <input type="number" disabled={true} id="portWS" className="form-control" ref="portWS" min="0" max="65535" defaultValue={this.ConfigServer.webSocketSettings.port} />
                                            </div>
                                        </div>
                                        <br />
                                        <h6>WEBSOCKET PASSWORD:</h6>
                                        <div className="form-group">
                                            <div className="form-row">
                                                <div className="col">
                                                    <label htmlFor="iterPasswdWS">Hash interations:</label>
                                                    <input type="number" disabled={true} id="iterPasswdWS" className="form-control" ref="iterPasswdWS" min="0" defaultValue={this.ConfigServer.webSocketSettings.pwd.iterations} />
                                                </div>
                                                <div className="col">

                                                    <label htmlFor="saltPasswdWS">Salt:</label>
                                                    <input type="text" disabled={true} id="saltPasswdWS" className="form-control" ref="saltPasswdWS" defaultValue={this.ConfigServer.webSocketSettings.pwd.salt} />
                                                </div>
                                            </div>
                                            <br />
                                            <label htmlFor="passwdWS">Hashed password (SHA-512 only):</label>
                                            <input type="text" disabled={true} id="passwdWS" ref="passwdWS" defaultValue={this.ConfigServer.webSocketSettings.pwd.hashSHA512} className="form-control" />
                                        </div>
                                        <h6>SECURITY:</h6>
                                        <label className="switch">
                                            <input type="checkbox" disabled={true} id="wsIsSecure" ref="wsIsSecure" defaultChecked={this.ConfigServer.webSocketSettings.isSecure} />
                                            <span className="slider"></span>
                                        </label>
                                        <label htmlFor="wsIsSecure">Is secure (enable wss://)?</label>
                                        <div className="row">
                                            <div className="col">
                                                <div className="col">
                                                    <label htmlFor="wsCertUUID">Select the certificate:</label>
                                                    <input type="text" disabled={true} id="wsCertUUID" className="form-control" ref="wsCertUUID" defaultValue={this.ConfigServer.webSocketSettings.certUUID} />
                                                </div>
                                            </div>
                                            <div className="col">
                                                <label htmlFor="wsCertPassword">Password of the certificate:</label>
                                                <input type="password" disabled={true} id="wsCertPassword" className="form-control" ref="wsCertPassword" defaultValue={this.ConfigServer.webSocketSettings.password} />
                                            </div>
                                        </div>
                                        <label htmlFor="sautoBanMaxAttemps">Maximum login attempts before autoban:</label>
                                        <input type="number" disabled={true} id="wsautoBanMaxAttemps" ref="wsautoBanMaxAttemps" className="form-control" min="1" defaultValue={this.ConfigServer.webSocketSettings.autoBanMaxAttemps} />
                                        <br />
                                        <div>
                                            <h3>Black List</h3>
                                            <table className="table table-dark">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">IP Address</th>
                                                        <th scope="col">Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="wsrenderBlackList">
                                                </tbody>
                                            </table>
                                            <br />
                                            <br />
                                            <h3>White List</h3>
                                            <label className="switch">
                                                <input type="checkbox" disabled={true} id="wsuseWhiteList" name="wsuseWhiteList" defaultChecked={this.ConfigServer.webSocketSettings.useWhiteList} />
                                                <span className="slider"></span>
                                            </label>
                                            <label htmlFor="useWhiteList">Use whitelist?</label>
                                            <br />
                                            <table className="table table-dark">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">IP Address</th>
                                                        <th scope="col">Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="wsrenderWhiteList">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="confBackup">
                                        <h4 className="settingsTitle"><img src={backupICO} alt="backupICO" />Backup Settings</h4>
                                        <label className="switch">
                                            <input type="checkbox" disabled={true} id="doBackups" ref="doBackups" defaultChecked={this.ConfigServer.backupSettings.doBackups} />
                                            <span className="slider"></span>
                                        </label>
                                        <label htmlFor="doBackups">Do backups?</label>
                                        <div className="form-row">
                                            <div className="col">
                                                <label htmlFor="timeoutInterval">Timeout interval:</label>
                                                <div className="input-group">
                                                    <input type="number" disabled={true} id="timeoutInterval" className="form-control" ref="timeoutInterval" min="60000" defaultValue={this.ConfigServer.backupSettings.timeoutInterval} />
                                                    <div className="input-group-append">
                                                        <select id="timeoutIntervalUnit" ref="timeoutIntervalUnit" className="form-control" disabled={true}>
                                                            <option value="0">Seconds</option>
                                                            <option value="1">Minutes</option>
                                                            <option value="2">Hours</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <label htmlFor="sizeLimit">Size limit:</label>
                                                <div className="input-group">
                                                    <input type="number" disabled={true} id="sizeLimit" className="form-control" ref="sizeLimit" min="0" defaultValue={this.ConfigServer.backupSettings.sizeLimit} />
                                                    <div className="input-group-append">
                                                        <select id="sizeLimitUnit" ref="sizeLimitUnit" className="form-control" disabled={true}>
                                                            <option value="0">Bytes</option>
                                                            <option value="1">Kb</option>
                                                            <option value="2">Mb</option>
                                                            <option value="3">Gb</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <label htmlFor="backupSettingsAESKey">AES Key:</label>
                                        <input type="text" disabled={true} id="backupSettingsAESKey" ref="backupSettingsAESKey" defaultValue={this.ConfigServer.backupSettings.AESKey} className="form-control" />
                                        <br />
                                    </div>
                                    <div id="confLogs">
                                        <h4 className="settingsTitle"><img src={logsICO} alt="logsICO" />Logs</h4>
                                        <div className="form-row">
                                            <div className="col">
                                                <label className="switch">
                                                    <input type="checkbox" disabled={true} id="log" ref="log"
                                                        defaultChecked={this.ConfigServer.loggingSettings.log} />
                                                    <span className="slider"></span>
                                                </label>
                                                <label htmlFor="log">Enable logging?</label>
                                            </div>
                                            <div className="col">
                                                <label htmlFor="logDuration">Log duration:</label>
                                                <div id="input-group">
                                                    <input type="number" disabled={true} className="form-control" id="logDuration" ref="logDuration" min="60" defaultValue={this.ConfigServer.loggingSettings.logDuration} />
                                                    <div className="input-group-append">
                                                        <select disabled={true} id="logDurationUnit" ref="logDurationUnit" className="form-control">
                                                            <option value="0">Seconds</option>
                                                            <option value="1">Minutes</option>
                                                            <option value="2">Hours</option>
                                                            <option value="3">Days</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div className="form-row">
                                            <div className="col">
                                                <label className="switch">
                                                    <input type="checkbox" disabled={true} id="logClients" ref="logClients"
                                                        defaultChecked={this.ConfigServer.loggingSettings.logClients} />
                                                    <span className="slider"></span>
                                                </label>
                                                <label htmlFor="logClients">Log clients?</label>
                                            </div>
                                            <div className="col">
                                                <label className="switch">
                                                    <input type="checkbox" disabled={true} id="logWeb" ref="logWeb"
                                                        defaultChecked={this.ConfigServer.loggingSettings.logWeb} />
                                                    <span className="slider"></span>
                                                </label>
                                                <label htmlFor="logWeb">Log WebSocket connections?</label>
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="col">
                                                <label className="switch">
                                                    <input type="checkbox" disabled={true} id="logHashes" ref="logHashes"
                                                        defaultChecked={this.ConfigServer.loggingSettings.logHashes} />
                                                    <span className="slider"></span>
                                                </label>
                                                <label htmlFor="logHashes">Log hashes?</label>
                                            </div>
                                            <div className="col">
                                                <label className="switch">
                                                    <input type="checkbox" disabled={true} id="logRainbows" ref="logRainbows"
                                                        defaultChecked={this.ConfigServer.loggingSettings.logRainbows} />
                                                    <span className="slider"></span>
                                                </label>
                                                <label htmlFor="logRainbows">Log rainbows?</label>
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="col">
                                                <label className="switch">
                                                    <input type="checkbox" disabled={true} id="logDictionaries" ref="logDictionaries"
                                                        defaultChecked={this.ConfigServer.loggingSettings.logDictionaries} />
                                                    <span className="slider"></span>
                                                </label>
                                                <label htmlFor="logDictionaries">Log dictionaries?</label>
                                            </div>
                                            <div className="col">
                                                <label className="switch">
                                                    <input type="checkbox" disabled={true} id="logSettings" ref="logSettings"
                                                        defaultChecked={this.ConfigServer.loggingSettings.logSettings} />
                                                    <span className="slider"></span>
                                                </label>
                                                <label htmlFor="logSettings">Log settings?</label>
                                            </div>
                                        </div>
                                        <br />
                                    </div>
                                    <div id="confEnmagatzenamentLocal">
                                        <h4 className="settingsTitle"><img src={enmagatzenamentICO} alt="enmagatzenamentICO" />Local Storage</h4>
                                        <label className="switch">
                                            <input type="checkbox" disabled={true} id="useLocalStorage" ref="useLocalStorage"
                                                defaultChecked={this.ConfigServer.localStorage.useLocalStorage} />
                                            <span className="slider"></span>
                                        </label>
                                        <label htmlFor="habilitarBaseDeDades">Enable local storage?</label>
                                        <br />
                                        <div className="form-row">
                                            <div className="col">
                                                <label htmlFor="folderName">Folder name</label>
                                                <input type="text" disabled={true} id="folderName" className="form-control" ref="folderName" defaultValue={this.ConfigServer.localStorage.folderName} />
                                            </div>
                                            <div className="col">
                                                <label htmlFor="operationsToRegenerate">Count of operations to regenerate the file:</label>
                                                <input type="number" disabled={true} id="operationsToRegenerate" className="form-control" ref="operationsToRegenerate" defaultValue={this.ConfigServer.localStorage.operationsToRegenerate} />
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="col">
                                                <label htmlFor="fitxerCua">Hash storage file name:</label>
                                                <input type="text" disabled={true} id="fitxerCua" className="form-control" ref="fitxerCua" defaultValue={this.ConfigServer.localStorage.hashesFileName} />
                                            </div>
                                            <div className="col">
                                                <label htmlFor="rainbowFile">Rainbow definitions list filename:</label>
                                                <input type="text" disabled={true} id="rainbowFile" className="form-control" ref="rainbowFile"
                                                    defaultValue={this.ConfigServer.localStorage.rainbowsFileName} />
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="col">
                                                <label htmlFor="fitxerDiccionaris">Dictionaries definition list filename:</label>
                                                <input type="text" disabled={true} id="fitxerDiccionaris" className="form-control" ref="fitxerDiccionaris"
                                                    defaultValue={this.ConfigServer.localStorage.savedDictionariesFileName} />
                                            </div>
                                            <div className="col">
                                                <label htmlFor="fitxerDiccionaris">Authorisation tokens filename:</label>
                                                <input type="text" disabled={true} id="tokensFileName" className="form-control" ref="tokensFileName"
                                                    defaultValue={this.ConfigServer.localStorage.tokensFileName} />
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="col">
                                                <label htmlFor="certificatesFileName">Certificates definition list filename:</label>
                                                <input type="text" disabled={true} id="certificatesFileName" className="form-control" ref="certificatesFileName"
                                                    defaultValue={this.ConfigServer.localStorage.certificatesFileName} />
                                            </div>
                                            <div className="col">
                                                <label htmlFor="logsFileName">Logs filename:</label>
                                                <input type="text" disabled={true} id="logsFileName" className="form-control" ref="logsFileName"
                                                    defaultValue={this.ConfigServer.localStorage.logsFileName} />
                                            </div>
                                            <br />
                                            <br />
                                        </div>
                                    </div>
                                    <div id="confEnmagatzenamentMongo">
                                        <h4 className="settingsTitle"><img src={mongodbICO} alt="mongodbICO" />MongoDB Storage (optional)</h4>
                                        <label className="switch">
                                            <input type="checkbox" disabled={true} id="useDatabase" ref="useDatabase"
                                                defaultChecked={this.ConfigServer.mongoStorage.useDatabase} />
                                            <span className="slider"></span>
                                        </label>
                                        <label htmlFor="habilitarBaseDeDades">Connect to MongoDB database?</label>
                                        <br />
                                        <label htmlFor="mongoURL">URL:</label>
                                        <input type="text" disabled={true} id="mongoURL" ref="mongoURL"
                                            defaultValue={this.ConfigServer.mongoStorage.url} className="form-control" />
                                        <br />
                                        <div className="form-row">
                                            <div className="col">
                                                <label htmlFor="mongoHost">Database host:</label>
                                                <input type="text" disabled={true} id="mongoHost" ref="mongoHost"
                                                    defaultValue={this.ConfigServer.mongoStorage.host} className="form-control" />
                                            </div>
                                            <div className="col">
                                                <label htmlFor="portMongo">Database port:</label>
                                                <input type="number" disabled={true} id="portMongo" ref="portMongo" min="0" max="65535"
                                                    defaultValue={this.ConfigServer.mongoStorage.port} className="form-control" />
                                            </div>
                                            <div className="col">
                                                <label htmlFor="dbMongo">Database name:</label>
                                                <input type="text" disabled={true} id="dbMongo" ref="dbMongo"
                                                    defaultValue={this.ConfigServer.mongoStorage.databaseName} className="form-control" />
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="col">
                                                <label htmlFor="usuariMongo">User name (optional):</label>
                                                <input type="text" disabled={true} id="usuariMongo" ref="usuariMongo"
                                                    defaultValue={this.ConfigServer.mongoStorage.userName} className="form-control" />
                                            </div>
                                            <div className="col">
                                                <label htmlFor="pwdMongo">User password (optional):</label>
                                                <div className="input-group">
                                                    <input type="password" disabled={true} id="pwdMongo" ref="pwdMongo"
                                                        defaultValue={this.ConfigServer.mongoStorage.password} className="form-control" />
                                                    <div className="input-group-append">
                                                        <select id="authMongo" ref="authMongo" defaultValue={this.ConfigServer.mongoStorage.mongoDbAuthMechanism} className="form-control" disabled={true} >
                                                            <option value="NONE">No authentication</option>
                                                            <option value="SCRAM-SHA-1">SCRAM-SHA-1</option>
                                                            <option value="SCRAM-SHA-256">SCRAM-SHA-256</option>
                                                            <option value="MONGODB-X509">MONGODB-X509</option>
                                                            <option value="GSSAPI">GSSAPI (Kerberos)</option>
                                                            <option value="PLAIN">PLAIN (LDAP SASL)</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-outline-light" onClick={this.exportarAJSON}>Export to JSON</button>
                            <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



class SocketAddr extends Component {
    constructor({ addr, handleRemove }) {
        super();

        this.addr = addr;
    }

    render() {
        return (
            <tr>
                <td>{this.addr}</td>
                <td></td>
            </tr>
        )
    }
}



export default SettingsDetailsModal;
