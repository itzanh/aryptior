import React, { Component } from 'react';

import errorICO from './../../IMG/error.svg';

class LogsListError extends Component {
    constructor({ }) {
        super();
    }

    render() {
        return (<div id="logsListError">
            <img src={errorICO} alt="errorICO" />
            <h3>Logging is diabled on this server</h3>
            <p>To enable this feature, go to Settings->Logging, check "Enable logging?" and save changes to start logging.</p>
        </div>)
    }
}

export default LogsListError;


