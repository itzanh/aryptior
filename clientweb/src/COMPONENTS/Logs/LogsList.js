import React, { Component } from 'react';

import './../../CSS/Logs.css';

class LogsList extends Component {
    constructor({ handleFilter, handleChangeLimit }) {
        super();

        this.handleFilter = handleFilter;
        this.handleChangeLimit = handleChangeLimit;

        this.onFilter = this.onFilter.bind(this);
        this.onChangeLimit = this.onChangeLimit.bind(this);
    }

    onFilter() {
        this.handleFilter(parseInt(this.refs.eventName.value));
    }

    onChangeLimit() {
        this.handleChangeLimit(this.refs.start.value, parseInt(this.refs.rows.value));
    }

    render() {
        return (<div id="logsList">
            <div id="renderModal"></div>
            <div id="options">
                <div className="row">
                    <div className="col">
                        <div id="exportLogs">

                        </div>
                    </div>
                    <div className="col">
                        <div id="clearLogs">

                        </div>
                    </div>
                    <div className="col">
                        <label>Show since:</label>
                        <input type="datetime-local" id="start" className="form-control" ref="start" onChange={this.onChangeLimit} />
                    </div>
                    <div className="col">
                        <label>Event:</label>
                        <select id="eventName" ref="eventName" className="form-control" onChange={this.onFilter}>
                            <option value="-1">Unfiltered</option>
                            <option value="0">Client Connection</option>
                            <option value="1">Client Disconnection</option>
                            <option value="2">Client kicked</option>
                            <option value="3">Client banned</option>
                            <option value="4">Hash insertion</option>
                            <option value="5">Hash finished</option>
                            <option value="6">Hash edition</option>
                            <option value="7">Hash surrendered/resumed</option>
                            <option value="8">Hash deletion</option>
                            <option value="9">WebSocket client connection</option>
                            <option value="10">WebSocket client disconnection</option>
                            <option value="11">Rainbow insertion</option>
                            <option value="12">Rainbow edition</option>
                            <option value="13">Rainbow deletion</option>
                            <option value="14">Dictionary insertion</option>
                            <option value="15">Dictionary edition</option>
                            <option value="16">Dictionary deletion</option>
                            <option value="17">Settings update</option>
                        </select>
                    </div>
                    <div className="col">
                        <label>Limit of rows:</label>
                        <div id="input-group">
                            <div className="input-group mb-3">
                                <input type="number" id="rows" className="form-control" ref="rows" defaultValue="500" min="1" onChange={this.onChangeLimit} />
                                <div className="input-group-append">
                                    <button className="btn btn-outline-light" type="button"
                                        onClick={() => { document.getElementById("rows").value = "0"; }}>Unbounded</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <table className="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Date and time</th>
                            <th scope="col">Message</th>
                            <th scope="col">Details</th>
                            <th scope="col">More details</th>
                        </tr>
                    </thead>
                    <tbody id="renderLogsList">

                    </tbody>
                </table>
            </div>
        </div>)
    }
}

export default LogsList;


