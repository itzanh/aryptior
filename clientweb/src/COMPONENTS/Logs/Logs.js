import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// IMG
import hashICO from "./../../IMG/hash.svg";
import diccionarisICO from "./../../IMG/diccionaris.svg";
import clientsICO from "./../../IMG/clients.svg";
import ajustosICO from "./../../IMG/ajustos.svg";
import rainbowICO from "./../../IMG/rainbow.svg";
import logoutICO from "./../../IMG/logout.svg";
import dashboardICO from "./../../IMG/dashboard.svg";
import optionsICO from './../../IMG/options.svg';

// COMPONENTS
import HashDetailsModal from '../HashDetailsModal';
import RainbowDetailsModal from '../Rainbow/RainbowDetailsModal';
import DictionaryDetails from '../LlistarDiccionaris/DictionaryDetails';
import SettingsDetailsModal from './SettingsDetailsModal';


const LOGTYPE = ["Client Connection", "Client Disconnection", "Client kicked", "Client banned", "Hash insertion", "Hash finished", "Hash edition",
    "Hash surrendered/resumed", "Hash deletion", "WebSocket client connection", "WebSocket client disconnection", "Rainbow insertion", "Rainbow edition",
    "Rainbow deletion", "Dictionary insertion", "Dictionary edition", "Dictionary deletion", "Settings update"];

class Logs extends Component {
    constructor({ log }) {
        super();

        this.log = log;

        this.openDetailsHash = this.openDetailsHash.bind(this);
        this.openDetailsRainbow = this.openDetailsRainbow.bind(this);
        this.openDetailsSettings = this.openDetailsSettings.bind(this);
        this.openDetailsDictionary = this.openDetailsDictionary.bind(this);
    }

    formatDate(horaConnectat) {
        const fecha = new Date(horaConnectat);
        return fecha.getDate() + '/'
            + (fecha.getMonth() + 1) + '/'
            + fecha.getFullYear() + ' '
            + fecha.getHours() + ':'
            + fecha.getMinutes() + ':'
            + fecha.getSeconds();
    }

    getDetails() {
        if (this.log.type === 17) {
            return "- Settings object -";
        } else if (this.log.type === 9 || this.log.type === 10 || this.log.type === 3) {
            return this.log.data;
        } else if (this.log.type === 0 || this.log.type === 1 || this.log.type === 2) {
            return "ID: " + this.log.data.uuid + " IP " + this.log.data.addr;
        } else {
            return this.log.data.uuid;
        }
    }

    getMoreDetails() {
        switch (this.log.type) {
            case 4:
            case 5:
            case 6:
            case 7:
            case 8: {
                return <img src={optionsICO} alt="optionsICO" onClick={this.openDetailsHash} />;
            }
            case 11:
            case 12:
            case 13: {
                return <img src={optionsICO} alt="optionsICO" onClick={this.openDetailsRainbow} />;
            }
            case 14:
            case 15:
            case 16: {
                return <img src={optionsICO} alt="optionsICO" onClick={this.openDetailsDictionary} />;
            }
            case 17: {
                return <img src={optionsICO} alt="optionsICO" onClick={this.openDetailsSettings} />;
            }
        }
    }

    openDetailsHash() {
        ReactDOM.unmountComponentAtNode(document.getElementById('renderModal'));
        const window = <HashDetailsModal
            hash={this.log.data}

            readOnly={true}
        />;
        ReactDOM.render(window, document.getElementById('renderModal'));
    }

    openDetailsRainbow() {
        ReactDOM.unmountComponentAtNode(document.getElementById('renderModal'));
        ReactDOM.render(<RainbowDetailsModal
            r={this.log.data}

            readOnly={true}
        />, document.getElementById("renderModal"));
    }

    openDetailsDictionary() {
        ReactDOM.unmountComponentAtNode(document.getElementById('renderModal'));
        ReactDOM.render(<DictionaryDetails
            dictionary={this.log.data}

            readOnly={true}
        />, document.getElementById('renderModal'));
    }

    openDetailsSettings() {
        ReactDOM.unmountComponentAtNode(document.getElementById('renderModal'));
        ReactDOM.render(<SettingsDetailsModal
            settings={this.log.data}
            exportarAJSON={() => {
                this.exportarAJSON(this.log.data);
            }}
        />, document.getElementById('renderModal'));
    }

    exportarAJSON(objectOrArray = [], fileName = 'settings.json') {
        this.openExternalFile(JSON.stringify(objectOrArray), fileName);
    }

    openExternalFile(text, fileName = 'download') {
        const fitxerTemp = new Blob([text]);
        const url = URL.createObjectURL(fitxerTemp);
        //window.open(url, '_blank');
        var tempLink = document.createElement('a');
        tempLink.href = url;
        tempLink.setAttribute('download', fileName);
        tempLink.click();
    }

    getImage() {
        switch (this.log.type) {
            case 0:
            case 1:
            case 2:
            case 3: {
                return clientsICO;
            }
            case 4:
            case 5:
            case 6:
            case 7:
            case 8: {
                return hashICO;
            }
            case 9: {
                return dashboardICO;
            }
            case 10: {
                return logoutICO;
            }
            case 11:
            case 12:
            case 13: {
                return rainbowICO;
            }
            case 14:
            case 15:
            case 16: {
                return diccionarisICO;
            }
            case 17: {
                return ajustosICO;
            }
        }
    };

    render() {
        return (<tr>
            <td>
                <img src={this.getImage()} alt="img" />
            </td>
            <td>{this.formatDate(this.log.dateTime)}</td>
            <td>{LOGTYPE[this.log.type]}</td>
            <td>{this.getDetails()}</td>
            <td>{this.getMoreDetails()}</td>
        </tr>);
    }
}

export default Logs;


