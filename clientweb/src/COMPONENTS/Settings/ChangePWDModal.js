import React, { Component } from 'react';

// CSS
import './../../CSS/Settings.css';

class ChangePWDModal extends Component {
    constructor({ canviarPWD }) {
        super();

        this.canviarPWD = canviarPWD;
        this.salt = this.generateSaltn();

        this.handleSaveData = this.handleSaveData.bind(this);
    }

    componentDidMount() {
        window.$('#changePwdModalCenter').modal({ show: true });
    }

    generateSaltn(size = 32) {
        const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var str = '';
        for (var i = 0; i < size; i++) {
            str += (characters[Math.floor(Math.random() * characters.length)]);
        }
        return str;
    }

    handleSaveData() {
        var iterations = this.refs.chgPwdIterations.value;
        try {
            iterations = parseInt(iterations);
        } catch (e) {
            alert("You must enter a number on the iterations count!");
            return;
        }
        if (iterations <= 0) {
            alert("Iterations count must be greater than zero!");
            return;
        }
        const salt = this.refs.chgPwdSalt.value;
        if (salt === null) {
            return;
        }
        const newPWD = this.refs.chgPwdNewPwd.value;
        if (newPWD === null || newPWD.length <= 3) {
            alert("Password must be, at least, 4 characters long!");
            return;
        }

        this.canviarPWD(iterations, salt, newPWD);

        window.$('#changePwdModalCenter').modal('hide');
    }

    render() {
        return (
            <div id="changePwdModal">
                <div className="modal fade" id="changePwdModalCenter" tabIndex="-1" role="dialog" aria-labelledby="ChangePwdCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">Change password</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col">
                                        <label htmlFor="chgPwdIterations">Interations:</label>
                                        <input type="number" id="chgPwdIterations" ref="chgPwdIterations" className="form-control" min="1" defaultValue="256000" autoComplete="no" />
                                    </div>
                                    <div className="col">
                                        <label htmlFor="chgPwdSalt">Salt: (optional)</label>
                                        <input type="text" id="chgPwdSalt" ref="chgPwdSalt" className="form-control" defaultValue={this.salt} autoComplete="no" />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <label htmlFor="chgPwdNewPwd">Password:</label>
                                        <input type="password" id="chgPwdNewPwd" ref="chgPwdNewPwd" className="form-control" autoComplete="no" autoComplete="new-password" />
                                    </div>
                                </div>
                                <br />
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-primary" onClick={this.handleSaveData}>Change password</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ChangePWDModal;
