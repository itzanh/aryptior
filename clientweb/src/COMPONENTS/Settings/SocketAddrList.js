// REACT
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// IMG
import trashICO from './../../IMG/trash.svg';

class SocketAddrList extends Component {
    constructor({ blackList, useWhiteList, whiteList, handleSave }) {
        super();

        this.blackList = blackList;
        this.useWhiteList = useWhiteList;
        this.whiteList = whiteList;

        this.handleSave = handleSave;

        this.renderBlackList = this.renderBlackList.bind(this);
        this.renderWhiteList = this.renderWhiteList.bind(this);
        this.handleAddBlackList = this.handleAddBlackList.bind(this);
        this.handleAddWhiteList = this.handleAddWhiteList.bind(this);
        this.handleRemoveBlackList = this.handleRemoveBlackList.bind(this);
        this.handleRemoveWhiteList = this.handleRemoveWhiteList.bind(this);
        this.handleSaveData = this.handleSaveData.bind(this);
    }

    componentDidMount() {
        window.$('#SocketAddrModal').modal({ show: true });

        this.renderBlackList();
        this.renderWhiteList();
    }

    renderBlackList() {
        const componentsToRender = [];

        this.blackList.forEach((element, i) => {
            componentsToRender.push(<SocketAddr
                key={i}

                addr={element}
                handleRemove={this.handleRemoveBlackList}
            />);
        });

        ReactDOM.unmountComponentAtNode(document.getElementById('renderBlackList'));
        ReactDOM.render(
            componentsToRender, document.getElementById('renderBlackList'));
    }

    renderWhiteList() {
        const componentsToRender = [];

        this.whiteList.forEach((element, i) => {
            componentsToRender.push(<SocketAddr
                key={i}

                addr={element}
                handleRemove={this.handleRemoveWhiteList}
            />);
        });

        ReactDOM.unmountComponentAtNode(document.getElementById('renderWhiteList'));
        ReactDOM.render(
            componentsToRender, document.getElementById('renderWhiteList'));
    }

    handleAddBlackList() {
        const newAddr = document.getElementById("newIPBlackList").value;
        this.blackList.push(newAddr);
        document.getElementById("newIPBlackList").value = "";

        this.renderBlackList();
    }

    handleAddWhiteList() {
        const newAddr = document.getElementById("newIPWhiteList").value;
        this.whiteList.push(newAddr);
        document.getElementById("newIPWhiteList").value = "";

        this.renderWhiteList();
    }

    handleRemoveBlackList(addr) {
        const index = this.blackList.indexOf(addr);
        this.blackList.splice(index, 1);

        this.renderBlackList();
    }

    handleRemoveWhiteList(addr) {
        const index = this.whiteList.indexOf(addr);
        this.whiteList.splice(index, 1);

        this.renderWhiteList();
    }

    handleSaveData() {
        this.handleSave(this.blackList,
            document.getElementById("useWhiteList").checked,
            this.whiteList);

    }

    render() {
        return (
            <div id="socketAddrList">
                <div className="modal fade" id="SocketAddrModal" tabIndex="-1" role="dialog" aria-labelledby="SocketAddrModal" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">Address restrictions</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.handleSaveData}>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <h3>Black List</h3>
                                <table className="table table-dark">
                                    <thead>
                                        <tr>
                                            <th scope="col">IP Address</th>
                                            <th scope="col">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody id="renderBlackList">
                                    </tbody>
                                </table>
                                <div className="input-group mb-3">
                                    <input type="text" id="newIPBlackList" className="form-control" autoComplete="no" placeholder="IP Address" />
                                    <div className="input-group-append">
                                        <button type="button" className="btn btn-outline-light" onClick={this.handleAddBlackList}>Add to black list</button>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <h3>White List</h3>
                                <label className="switch">
                                    <input type="checkbox" id="useWhiteList" name="useWhiteList" defaultChecked={this.useWhiteList} />
                                    <span className="slider"></span>
                                </label>
                                <label htmlFor="useWhiteList">Use whitelist?</label>
                                <br />
                                <table className="table table-dark">
                                    <thead>
                                        <tr>
                                            <th scope="col">IP Address</th>
                                            <th scope="col">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody id="renderWhiteList">
                                    </tbody>
                                </table>
                                <div className="input-group mb-3">
                                    <input type="text" id="newIPWhiteList" className="form-control" autoComplete="no" placeholder="IP Address" />
                                    <div className="input-group-append">
                                        <button type="button" className="btn btn-outline-light" onClick={this.handleAddWhiteList}>Add to white list</button>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.handleSaveData}>Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class SocketAddr extends Component {
    constructor({ addr, handleRemove }) {
        super();

        this.addr = addr;
        this.handleRemove = handleRemove;

        this.handleRemoveAddr = this.handleRemoveAddr.bind(this);
    }

    handleRemoveAddr() {
        this.handleRemove(this.addr);
    }

    render() {
        return (
            <tr>
                <td>{this.addr}</td>
                <td><img src={trashICO} alt="trashICO" onClick={this.handleRemoveAddr} /></td>
            </tr>
        )
    }
}

export default SocketAddrList;
