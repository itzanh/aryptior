import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// IMG
import trashICO from './../../IMG/trash.svg';
import downloadICO from './../../IMG/download.svg';
import sortArrowICO from './../../IMG/sortArrow.svg';

class UploadSocketCertModal extends Component {
    constructor({ handleModalSelected, handleListCerts, handleRemoveCert, handleCertUpload, handleCertDownload }) {
        super();

        this.certs = null;

        this.handleListCerts = handleListCerts;
        this.handleRemoveCert = handleRemoveCert;
        this.handleCertUpload = handleCertUpload;
        this.handleCertDownload = handleCertDownload;
        this.handleModalSelected = handleModalSelected;

        this.uploadCert = this.uploadCert.bind(this);
        this.mainScreen = this.mainScreen.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
        this.showCertList = this.showCertList.bind(this);
        this.renderCertList = this.renderCertList.bind(this);
        this.showUploadScreen = this.showUploadScreen.bind(this);
        this.selectCertificate = this.selectCertificate.bind(this);
        this.mainScreenButtons = this.mainScreenButtons.bind(this);
        this.uploadScreenButtons = this.uploadScreenButtons.bind(this);

        this.sort = this.sort.bind(this);
        this.sortByDate = this.sortByDate.bind(this);
        this.sortBySize = this.sortBySize.bind(this);
        this.sortBySizeDescending = this.sortBySizeDescending.bind(this);

        this.orderBy = 2;
        this.orderDirectionAscending = true;
    }

    componentDidMount() {
        window.$('#uploadSocketCertModal').modal({ show: true });

        this.showCertList();
    }

    async showCertList() {
        ReactDOM.unmountComponentAtNode(document.getElementById("modal-body"));
        ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
        ReactDOM.render(<p>Loading certificates on the server, please wait...</p>, document.getElementById("modal-body"));

        const certs = await this.handleListCerts();
        this.certs = certs;
        this.sortByDate();
        console.log(certs);

        this.renderCertList();
    }

    renderCertList() {
        ReactDOM.unmountComponentAtNode(document.getElementById("modal-body"));
        ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
        ReactDOM.render(<this.mainScreen />, document.getElementById("modal-body"));
        ReactDOM.render(<this.mainScreenButtons />, document.getElementById("modal-footer"));

        const componentsToRender = [];
        this.certs.forEach((element, i) => {
            componentsToRender.push(<CertItem
                key={i}

                cert={element}
                handleRemove={this.handleRemove}
                handleDownload={this.handleCertDownload}
            />);
        });
        ReactDOM.render(componentsToRender, document.getElementById("renderCertList"));
    }

    async handleRemove(uuid) {
        this.handleRemoveCert(uuid).then(() => {
            this.showCertList();
        }, () => {
            alert('Error deleting! Check if you have enought permissions to delete the file.');
        });
    }

    mainScreen() {
        return (<div>
            <CertList
                sortColumn={this.orderBy}
                sortOrder={this.orderDirectionAscending}
                handleSort={this.sort}
            />
        </div>)
    }

    mainScreenButtons() {
        return (<div>
            <button type="button" className="btn btn-success" onClick={this.selectCertificate}>Select</button>
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" className="btn btn-primary" onClick={this.showUploadScreen}>Upload</button>
        </div>)
    }

    showUploadScreen() {
        ReactDOM.unmountComponentAtNode(document.getElementById("modal-body"));
        ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
        ReactDOM.render(<this.uploadScreen />, document.getElementById("modal-body"));
        ReactDOM.render(<this.uploadScreenButtons />, document.getElementById("modal-footer"));
    }

    uploadScreen() {
        return (<div>
            <p>Please, select a .pfx certificate from your local drive.</p>
            <input type="file" id="uploadCertFile" name="uploadCertFile" />
        </div>)
    }

    uploadScreenButtons() {
        return (<div>
            <button type="button" className="btn btn-outline-warning" onClick={this.showCertList}>Back</button>
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" className="btn btn-primary" onClick={this.uploadCert}>Submit!</button>
        </div>)
    }

    async uploadCert() {
        const files = document.getElementById("uploadCertFile").files;
        if (files.length === 0) {
            alert("You must select a file or cancel!");
            return;
        }
        const file = files[0];

        ReactDOM.unmountComponentAtNode(document.getElementById("modal-body"));
        ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
        ReactDOM.render(<p>Uploading file to the server, please wait...</p>, document.getElementById("modal-body"));
        await this.handleCertUpload(file).then(() => {
            this.showCertList();
        }, () => {
            alert("There was an error uploading the sertificate to the server!");
        });
    }

    selectCertificate() {
        if (document.querySelector('input[name="selectCert"]:checked') === null) {
            alert('You must select a certificate or cancel.');
            return;
        }
        this.handleModalSelected(document.querySelector('input[name="selectCert"]:checked').value);
        window.$('#uploadSocketCertModal').modal('hide');
    };

    /***********/
    /* SORTING */
    /***********/

    sort(column) {
        if (this.orderBy !== column) {
            this.orderDirectionAscending = true;

        }
        if (this.orderBy === column) {
            this.orderDirectionAscending = !this.orderDirectionAscending;
        }

        this.orderBy = column;

        switch (this.orderBy) {
            case 0: {
                if (this.orderDirectionAscending) {
                    this.sortByName();
                } else {
                    this.sortByNameDescending();
                }
                break;
            }
            case 1: {
                if (this.orderDirectionAscending) {
                    this.sortBySize();
                } else {
                    this.sortBySizeDescending();
                }
                break;
            }
            case 2: {
                if (this.orderDirectionAscending) {
                    this.sortByDate();
                } else {
                    this.sortByDateDescending();
                }
                break;
            }
            case 3: {
                if (this.orderDirectionAscending) {
                    this.sortByUUID();
                } else {
                    this.sortByUUIDDescending();
                }
                break;
            }
        }

        this.renderCertList();
    }

    sortByDate() {
        for (let i = 0; i < this.certs.length; i++) {
            for (let j = (i % 2); j < this.certs.length; j += 2) {
                if ((j + 1) < this.certs.length
                    && new Date(this.certs[j].creationDate) < new Date(this.certs[(j + 1)].creationDate)) {
                    var aux = this.certs[j];
                    this.certs[j] = this.certs[(j + 1)];
                    this.certs[(j + 1)] = aux;
                }
            }
        }
    };

    sortByDateDescending() {
        for (let i = 0; i < this.certs.length; i++) {
            for (let j = (i % 2); j < this.certs.length; j += 2) {
                if ((j + 1) < this.certs.length
                    && new Date(this.certs[j].creationDate) > new Date(this.certs[(j + 1)].creationDate)) {
                    var aux = this.certs[j];
                    this.certs[j] = this.certs[(j + 1)];
                    this.certs[(j + 1)] = aux;
                }
            }
        }
    };

    sortBySize() {
        for (let i = 0; i < this.certs.length; i++) {
            for (let j = (i % 2); j < this.certs.length; j += 2) {
                if ((j + 1) < this.certs.length
                    && this.certs[j].size < this.certs[(j + 1)].size) {
                    var aux = this.certs[j];
                    this.certs[j] = this.certs[(j + 1)];
                    this.certs[(j + 1)] = aux;
                }
            }
        }
    };

    sortBySizeDescending() {
        for (let i = 0; i < this.certs.length; i++) {
            for (let j = (i % 2); j < this.certs.length; j += 2) {
                if ((j + 1) < this.certs.length
                    && this.certs[j].size > this.certs[(j + 1)].size) {
                    var aux = this.certs[j];
                    this.certs[j] = this.certs[(j + 1)];
                    this.certs[(j + 1)] = aux;
                }
            }
        }
    };

    sortByName() {
        for (let i = 0; i < this.certs.length; i++) {
            for (let j = (i % 2); j < this.certs.length; j += 2) {
                if ((j + 1) < this.certs.length
                    && this.certs[j].fileName < this.certs[(j + 1)].fileName) {
                    var aux = this.certs[j];
                    this.certs[j] = this.certs[(j + 1)];
                    this.certs[(j + 1)] = aux;
                }
            }
        }
    }

    sortByNameDescending() {
        for (let i = 0; i < this.certs.length; i++) {
            for (let j = (i % 2); j < this.certs.length; j += 2) {
                if ((j + 1) < this.certs.length
                    && this.certs[j].fileName > this.certs[(j + 1)].fileName) {
                    var aux = this.certs[j];
                    this.certs[j] = this.certs[(j + 1)];
                    this.certs[(j + 1)] = aux;
                }
            }
        }
    }

    sortByUUID() {
        for (let i = 0; i < this.certs.length; i++) {
            for (let j = (i % 2); j < this.certs.length; j += 2) {
                if ((j + 1) < this.certs.length
                    && this.certs[j].uuid < this.certs[(j + 1)].uuid) {
                    var aux = this.certs[j];
                    this.certs[j] = this.certs[(j + 1)];
                    this.certs[(j + 1)] = aux;
                }
            }
        }
    }

    sortByUUIDDescending() {
        for (let i = 0; i < this.certs.length; i++) {
            for (let j = (i % 2); j < this.certs.length; j += 2) {
                if ((j + 1) < this.certs.length
                    && this.certs[j].uuid > this.certs[(j + 1)].uuid) {
                    var aux = this.certs[j];
                    this.certs[j] = this.certs[(j + 1)];
                    this.certs[(j + 1)] = aux;
                }
            }
        }
    }

    render() {
        return (
            <div>
                <div className="modal fade" id="uploadSocketCertModal" tabIndex="-1" role="dialog" aria-labelledby="uploadSocketCertModal" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">Manage certificates</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div id="renderDownloads" />
                            <div className="modal-body" id="modal-body">

                            </div>
                            <div className="modal-footer" id="modal-footer">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class CertList extends Component {
    constructor({ handleSort, sortColumn, sortOrder }) {
        super();

        this.sortColumn = sortColumn;
        this.sortOrder = sortOrder;

        this.handleSort = handleSort;
    }

    render() {
        return (
            <table className="table table-dark">
                <thead>
                    <tr>
                        <th scope="col">
                        </th>
                        <th scope="col" onClick={() => { this.handleSort(0); }}>
                            {this.sortColumn === 0 && <img src={sortArrowICO} alt="sortArrowICO" className={!this.sortOrder ? '' : 'descending'} />}
                            File name
                        </th>
                        <th scope="col" onClick={() => { this.handleSort(1); }}>
                            {this.sortColumn === 1 && <img src={sortArrowICO} alt="sortArrowICO" className={!this.sortOrder ? '' : 'descending'} />}
                            Size
                        </th>
                        <th scope="col" onClick={() => { this.handleSort(2); }}>
                            {this.sortColumn === 2 && <img src={sortArrowICO} alt="sortArrowICO" className={!this.sortOrder ? '' : 'descending'} />}
                            Upload date
                        </th>
                        <th scope="col" onClick={() => { this.handleSort(3); }}>
                            {this.sortColumn === 3 && <img src={sortArrowICO} alt="sortArrowICO" className={!this.sortOrder ? '' : 'descending'} />}
                            UUID
                        </th>
                        <th scope="col">
                            Download
                        </th>
                        <th scope="col">
                            Delete
                        </th>
                    </tr>
                </thead>
                <tbody id="renderCertList"></tbody>
            </table>
        )
    }
}

class CertItem extends Component {
    constructor({ cert, handleRemove, handleDownload }) {
        super();

        this.cert = cert;
        this.handleRemove = handleRemove;
        this.handleDownload = handleDownload;
    }

    formatSize(sizeInBytes) {
        const units = ['B', 'Kb', 'Mb', 'Gb', 'Tb'];
        var unit = 0;
        while (sizeInBytes >= 1024) {
            sizeInBytes /= 1024;
            unit++;
        }
        return Math.floor(sizeInBytes) + ' ' + units[unit];
    }

    formatDate(horaConnectat) {
        const fecha = new Date(horaConnectat);
        return fecha.getDate() + '/'
            + (fecha.getMonth() + 1) + '/'
            + fecha.getFullYear() + ' '
            + fecha.getHours() + ':'
            + fecha.getMinutes() + ':'
            + fecha.getSeconds();
    }

    render() {
        return (
            <tr>
                <td>
                    <input type="radio" name="selectCert" value={this.cert.uuid} />
                </td>
                <td>{this.cert.fileName}</td>
                <td>{this.formatSize(this.cert.size)}</td>
                <td>{this.formatDate(this.cert.creationDate)}</td>
                <td>{this.cert.uuid}</td>
                <td><img src={downloadICO} alt="downloadICO" onClick={() => { this.handleDownload(this.cert.uuid, this.cert.fileName); }} /></td>
                <td><img src={trashICO} alt="trashICO" onClick={() => { this.handleRemove(this.cert.uuid); }} /></td>
            </tr>
        )
    }
}

export default UploadSocketCertModal;
