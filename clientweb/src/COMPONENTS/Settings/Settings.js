import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// CSS
import './../../CSS/Settings.css';

// IMG
import serverICO from './../../IMG/server.svg';
import socketICO from './../../IMG/socket.svg';
import websocketICO from './../../IMG/websocket.svg';
import enmagatzenamentICO from './../../IMG/enmagatzenament.svg';
import mongodbICO from './../../IMG/mongodb_logo.png';
import backupICO from "./../../IMG/backup.svg";
import logsICO from "./../../IMG/logs.svg";
import gearICO from "./../../IMG/gear.svg";

// COMPONENTS
import ChangePWDModal from './ChangePWDModal';
import SelectCollectionsModal from './SelectCollectionsModal';
import SocketAddrList from './SocketAddrList';
import UploadSocketCertModal from './UploadSocketCertModal';

class Settings extends Component {
    constructor({ ConfigServer, handleRevokeToken, guardarServer, exportarAJSON, importarJSON, canviarPWD, handleListCerts,
        handleRemoveCert, handleCertUpload, handleCertDownload, modalAlert }) {
        super();

        this.ConfigServer = ConfigServer;
        this.enviarServer = guardarServer;

        this.handleRevokeToken = handleRevokeToken;

        this.mongoDbShowURL = this.mongoDbShowURL.bind(this);
        this.handleGuardarServer = this.handleGuardarServer.bind(this);
        this.handleOpenCertModal = this.handleOpenCertModal.bind(this);
        this.handleSaveCollections = this.handleSaveCollections.bind(this);
        this.seleccionarFitxerJSON = this.seleccionarFitxerJSON.bind(this);
        this.handleSelectCollections = this.handleSelectCollections.bind(this);
        this.changePWDClientPassword = this.changePWDClientPassword.bind(this);
        this.handleAddressRestrictions = this.handleAddressRestrictions.bind(this);
        this.changePWDwebSocketSettings = this.changePWDwebSocketSettings.bind(this);
        this.handleWSAddressRestrictions = this.handleWSAddressRestrictions.bind(this);
        this.mongoDbShowIndividualFields = this.mongoDbShowIndividualFields.bind(this);
        this.handleSaveAddressRestrictions = this.handleSaveAddressRestrictions.bind(this);
        this.handleModalCertificateSelected = this.handleModalCertificateSelected.bind(this);
        this.handleSaveWSAddressRestrictions = this.handleSaveWSAddressRestrictions.bind(this);
        this.handleModalWebSocketCertificateSelected = this.handleModalWebSocketCertificateSelected.bind(this);

        this.modalAlert = (body) => { modalAlert(body, "Validation error!"); };
        this.handleListCerts = handleListCerts;
        this.handleRemoveCert = handleRemoveCert;
        this.handleCertUpload = handleCertUpload;
        this.handleCertDownload = handleCertDownload;

        this.canviarPWD = canviarPWD;

        this.importarJSON = importarJSON;
        this.exportarAJSON = exportarAJSON;
    }

    componentDidMount() {
        this.formatSizeUnit();
        this.formatTimeUnit();
        if (this.ConfigServer.mongoStorage.url !== '') {
            this.mongoDbShowURL();
        } else {
            this.mongoDbShowIndividualFields();
        }
    }

    /**
     * Recull les dades del formulari dels ajustos de servidor al fer click i els envia a la funcio superior, 
     * organizant-les en un objecte amb la esctructura oportuna
     * */
    handleGuardarServer() {
        // main settings
        const isDiscoverable = this.refs.isDiscoverable.checked;
        const discoverableName = this.refs.discoverableName.value;
        const tokenLength = parseInt(this.refs.tokenLength.value);

        // socket settings
        const host = this.refs.host.value;
        const port = parseInt(this.refs.port.value);
        const requireClientPassword = this.refs.requireClientPassword.checked;

        const iterPasswdClient = this.refs.iterPasswdClient.value;
        const saltPasswdClient = this.refs.saltPasswdClient.value;
        const passwdClient = this.refs.passwdClient.value;

        const encryption = this.refs.encryption.checked;
        const compression = this.refs.compression.checked;
        const sCertUUID = this.refs.sCertUUID.value;
        const sCertPassword = this.refs.sCertPassword.value;
        const mustUseServerSetings = this.refs.mustUseServerSetings.checked;
        const maxMessageSizeUnit = parseInt(this.refs.maxMessageSizeUnit.value);
        const maxMessageSize = this.convertSizeUnit(parseInt(this.refs.maxMessageSize.value), maxMessageSizeUnit);
        const maxClients = parseInt(this.refs.maxClients.value);
        const sautoBanMaxAttemps = parseInt(this.refs.sautoBanMaxAttemps.value);

        const networkTimeout = parseInt(this.refs.networkTimeout.value);
        const pingInterval = parseInt(this.refs.pingInterval.value);

        // configuracio de websocket
        const hostWS = this.refs.hostWS.value;
        const portWS = parseInt(this.refs.portWS.value);
        const iterPasswdWS = this.refs.iterPasswdWS.value;
        const saltPasswdWS = this.refs.saltPasswdWS.value;
        const passwdWS = this.refs.passwdWS.value;
        const wsIsSecure = this.refs.wsIsSecure.checked;
        const wsCertUUID = this.refs.wsCertUUID.value;
        const wsCertPassword = this.refs.wsCertPassword.value;
        const wsautoBanMaxAttemps = parseInt(this.refs.wsautoBanMaxAttemps.value);

        // backup settings
        const doBackups = this.refs.doBackups.checked;
        const timeoutIntervalUnit = parseInt(this.refs.timeoutIntervalUnit.value);
        const timeoutInterval = this.convertTimeUnit(parseInt(this.refs.timeoutInterval.value), timeoutIntervalUnit);
        const sizeLimitUnit = parseInt(this.refs.sizeLimitUnit.value);
        const sizeLimit = this.convertSizeUnit(parseInt(this.refs.sizeLimit.value), sizeLimitUnit);
        const backupSettingsAESKey = this.refs.backupSettingsAESKey.value;

        // logs
        const log = this.refs.log.checked;
        const logClients = this.refs.logClients.checked;
        const logWeb = this.refs.logWeb.checked;
        const logHashes = this.refs.logHashes.checked;
        const logRainbows = this.refs.logRainbows.checked;
        const logDictionaries = this.refs.logDictionaries.checked;
        const logSettings = this.refs.logSettings.checked;
        const logDuration = this.convertLogDurationTime(parseInt(this.refs.logDuration.value), parseInt(this.refs.logDurationUnit.value));

        // configuracio del enmagatzenament per el costat del servidor
        // enmagatzenament local
        const useLocalStorage = this.refs.useLocalStorage.checked;
        const folderName = this.refs.folderName.value;
        const hashesFileName = this.refs.fitxerCua.value;
        const savedDictionariesFileName = this.refs.fitxerDiccionaris.value;
        const rainbowsFileName = this.refs.rainbowFile.value;
        const tokensFileName = this.refs.tokensFileName.value;
        const certificatesFileName = this.refs.certificatesFileName.value;
        const logsFileName = this.refs.logsFileName.value;
        const operationsToRegenerate = parseInt(this.refs.operationsToRegenerate.value);

        // enmagatzenament mongo
        const useDatabase = this.refs.useDatabase.checked;
        const mongoURL = this.refs.mongoURL.value;
        const mongoHost = this.refs.mongoHost.value;
        const portMongo = parseInt(this.refs.portMongo.value);
        const databaseName = this.refs.dbMongo.value;
        const userName = this.refs.usuariMongo.value;
        const pwdMongo = this.refs.pwdMongo.value;
        const authMongo = this.refs.authMongo.value;

        // CHECK GENERAL SETTINGS
        if (discoverableName.length == 0) {
            this.modalAlert("The discoverable name cannot be empty.");
            return;
        }
        if (discoverableName.length > 25) {
            this.modalAlert("The discoverable name cannot be longer than 25 characters.");
            return;
        }
        if (tokenLength < 128 || tokenLength > 512) {
            this.modalAlert("Incorrect token length.");
            return;
        }

        // CHECK SETTINGS
        // check socket password
        if (!/^[0-9A-F]{128}$/i.test(passwdWS)) {
            this.modalAlert("The hash of the WebSocket password is not a SHA-512 valid hash!");
            return;
        }
        if (!/^[0-9A-F]{128}$/i.test(passwdClient)) {
            this.modalAlert("The hash of the socket password is not a SHA-512 valid hash!");
            return;
        }
        if (iterPasswdWS < 0) {
            this.modalAlert("Iterations in the WebSocket password hash must be grater than 0.");
            return;
        }
        if (iterPasswdClient < 0) {
            this.modalAlert("Iterations in the socket password hash must be grater than 0.");
            return;
        }
        if (iterPasswdWS > 512000) {
            this.modalAlert("Iterations in the WebSocket password hash must be less than 512000.");
            return;
        }
        if (iterPasswdClient > 512000) {
            this.modalAlert("Iterations in the socket password hash must be less than 512000.");
            return;
        }

        // check port numbers
        if ((port < 0 || port > 65535)
            || (portWS < 0 || portWS > 65535)
            || (portMongo < 0 || portMongo > 65535)) {
            this.modalAlert("Los puertos deben ser numeros entre el 0 i el 65535.");
            return;
        }
        // check ip addresses
        const IpRegex =
            /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        if (!(IpRegex.test(host))
            || !(IpRegex.test(hostWS))) {
            this.modalAlert("On IP addres on the input is not valid.");
            return;
        }

        for (let i = 0; i < this.ConfigServer.socketSettings.blackList.length; i++) {
            const addr = this.ConfigServer.socketSettings.blackList[i];
            if (!IpRegex.test(addr)) {
                this.modalAlert("One IP address entered in the main socket's blacklist in not valid.");
                return;
            }
        }
        for (let i = 0; i < this.ConfigServer.socketSettings.whiteList.length; i++) {
            const addr = this.ConfigServer.socketSettings.whiteList[i];
            if (!IpRegex.test(addr)) {
                this.modalAlert("One IP address entered in the main socket's whitelist in not valid.");
                return;
            }
        }
        for (let i = 0; i < this.ConfigServer.webSocketSettings.blackList.length; i++) {
            const addr = this.ConfigServer.webSocketSettings.blackList[i];
            if (!IpRegex.test(addr)) {
                this.modalAlert("One IP address entered in the main socket's blacklist in not valid.");
                return;
            }
        }
        for (let i = 0; i < this.ConfigServer.webSocketSettings.whiteList.length; i++) {
            const addr = this.ConfigServer.webSocketSettings.whiteList[i];
            if (!IpRegex.test(addr)) {
                this.modalAlert("One IP address entered in the main socket's whitelist in not valid.");
                return;
            }
        }

        // socket options
        if (networkTimeout < 0) {
            this.modalAlert("Network timeout cannot be negative. Use 0 in order to diable network timeout.");
            return;
        }
        if (pingInterval < 0) {
            this.modalAlert("Ping interval cannot be negative. Use 0 in order to diable pings and pongs.");
            return;
        }
        if (networkTimeout > 0 && pingInterval >= networkTimeout) {
            this.modalAlert("Ping interval cannot be equals or greater than the network timeout, because that would always cause a timeout disconnection on the server.");
            return;
        }
        if ((networkTimeout > 0 && pingInterval <= 0)) {
            this.modalAlert("Network timeout is enabled (has a value greater than 0), but pinging is diabled (set to 0). That would always cause a timeout disconnection on the server.");
            return;
        }
        if (networkTimeout != 0 && networkTimeout > 1000) {
            this.modalAlert("Network timeout cannot be grater than 1000 seconds long.");
            return;
        }
        if (pingInterval != 0 && pingInterval > 10000) {
            this.modalAlert("Ping interval cannot be grater than 1000 seconds long.");
            return;
        }
        if (networkTimeout - pingInterval < 10) {
            this.modalAlert("The difference between the network timeout and the ping interval cannot be shorter than ten seconds.");
            return;
        }
        if (sautoBanMaxAttemps < 0) {
            this.modalAlert("The number of attempts to autoban cannot be negative.");
            return;
        }
        if (wsautoBanMaxAttemps < 0) {
            this.modalAlert("The number of attempts to autoban cannot be negative.");
            return;
        }

        // backup settings
        if (sizeLimit < 0 || timeoutInterval < 60000) {
            this.modalAlert("Invalid backup settings.");
            return;
        }
        for (let i = 0; i < this.ConfigServer.backupSettings.collections; i++) {
            const collection = this.ConfigServer.backupSettings.collections[i];
            if (collection.includes('$') || collection.includes('\x00')
                || collection.startsWith("system.") || collection === '') {
                this.modalAlert("A collection name in the backup settings is not valid.");
                return;
            }
        }

        // check AES keys
        if (backupSettingsAESKey.length != 64) {
            this.modalAlert("Invalid AES key!");
            return;
        }

        // make sure that at least one of the storage methods are enabled
        if (!useLocalStorage && !useDatabase) {
            this.modalAlert("You must select at least one storage system.");
            return;
        }
        // check mongodb authenciation mechanism
        if (["NONE", "SCRAM-SHA-1", "SCRAM-SHA-256", "MONGODB-X509", "GSSAPI", "PLAIN"].indexOf(authMongo) === -1) {
            this.modalAlert("El mecanismo de autentificación de MongoDB no es valido.");
            this.modalAlert("MongoDB Authentication mechanism is not valid.");
            return;
        }

        if (databaseName.includes('/') || databaseName.includes('\\') || databaseName.includes(' ')
            || databaseName.includes('"') || databaseName.includes('$') || databaseName.includes('*')
            || databaseName.includes('<') || databaseName.includes('>') || databaseName.includes(':')
            || databaseName.includes('|') || databaseName.includes('?')) {
            this.modalAlert("Database name in the MongoDB settings settings is not valid.");
            return;
        }

        if (operationsToRegenerate < 10) {
            this.modalAlert("The number of operations to renegerate the local files cannot be less than 10.");
            return;
        }
        if (operationsToRegenerate > 512) {
            this.modalAlert("The number of operations to renegerate the local files cannot be greater than 512.");
            return;
        }

        ReactDOM.render(
            <img
                id="gearICO"
                src={gearICO}
                alt="gearICO"
            />
            , document.getElementById('guardatConfSrv'));

        this.enviarServer({
            isDiscoverable,
            discoverableName,
            tokenLength,
            socketSettings: {
                host,
                port,
                pwd: {
                    iterations: iterPasswdClient,
                    salt: saltPasswdClient,
                    hashSHA512: passwdClient
                },
                blackList: this.ConfigServer.socketSettings.blackList,
                useWhiteList: this.ConfigServer.socketSettings.useWhiteList,
                whiteList: this.ConfigServer.socketSettings.whiteList,
                mustUseServerSetings,
                socketOptions: {
                    encryption,
                    compression,
                    certUUID: sCertUUID,
                    password: sCertPassword,
                    networkTimeout,
                    pingInterval
                },
                maxMessageSize,
                maxClients,
                requireClientPassword,
                autoBanMaxAttemps: sautoBanMaxAttemps
            },
            webSocketSettings: {
                host: hostWS,
                port: portWS,
                pwd: {
                    iterations: iterPasswdWS,
                    salt: saltPasswdWS,
                    hashSHA512: passwdWS
                },
                blackList: this.ConfigServer.webSocketSettings.blackList,
                useWhiteList: this.ConfigServer.webSocketSettings.useWhiteList,
                whiteList: this.ConfigServer.webSocketSettings.whiteList,
                isSecure: wsIsSecure,
                certUUID: wsCertUUID,
                password: wsCertPassword,
                autoBanMaxAttemps: wsautoBanMaxAttemps
            },
            backupSettings: {
                doBackups,
                timeoutInterval,
                sizeLimit,
                collections: this.ConfigServer.backupSettings.collections,
                AESKey: backupSettingsAESKey
            },
            loggingSettings: {
                log,
                logClients,
                logWeb,
                logHashes,
                logRainbows,
                logDictionaries,
                logSettings,
                logDuration
            },
            localStorage: {
                useLocalStorage,
                folderName,
                hashesFileName,
                savedDictionariesFileName,
                rainbowsFileName,
                tokensFileName,
                certificatesFileName,
                logsFileName,
                operationsToRegenerate
            },
            mongoStorage: {
                useDatabase,
                url: mongoURL,
                host: mongoHost,
                port: portMongo,
                userName,
                password: pwdMongo,
                databaseName,
                mongoDbAuthMechanism: authMongo
            }
        });
    }

    convertSizeUnit(size, unit) {
        for (let i = 0; i < unit; i++) {
            size *= 1000;
        }
        return size;
    }

    convertTimeUnit(timeInMs, unit) {
        timeInMs *= 1000;
        for (let i = 0; i < unit; i++) {
            timeInMs *= 60;
        }
        return timeInMs;
    }

    convertLogDurationTime(timeInSeconds, unit) {
        for (let i = 0; i < Math.min(unit, 2); i++) {
            timeInSeconds *= 60;
        }

        if (unit === 3) {
            timeInSeconds *= 24;
        }

        return timeInSeconds;
    }

    formatSizeUnit() {
        var maxMessageSizeUnit = parseInt(this.refs.maxMessageSizeUnit.value);
        var maxMessageSize = parseInt(this.refs.maxMessageSize.value);
        var sizeLimitUnit = parseInt(this.refs.sizeLimitUnit.value);
        var sizeLimit = parseInt(this.refs.sizeLimit.value);

        while (maxMessageSize >= 1000 && maxMessageSizeUnit < 3) {
            maxMessageSize /= 1000;
            maxMessageSizeUnit++;
        }
        while (sizeLimit >= 1000 && sizeLimitUnit < 3) {
            sizeLimit /= 1000;
            sizeLimitUnit++;
        }

        this.refs.maxMessageSizeUnit.value = maxMessageSizeUnit;
        this.refs.maxMessageSize.value = maxMessageSize;
        this.refs.sizeLimitUnit.value = sizeLimitUnit;
        this.refs.sizeLimit.value = sizeLimit;
    }

    formatTimeUnit() {
        // backup: interval
        var timeoutIntervalUnit = parseInt(this.refs.timeoutIntervalUnit.value);
        var timeoutInterval = parseInt(this.refs.timeoutInterval.value);

        timeoutInterval /= 1000;
        while (timeoutInterval >= 60 && timeoutIntervalUnit < 2) {
            timeoutInterval /= 60;
            timeoutIntervalUnit++;
        }

        this.refs.timeoutIntervalUnit.value = timeoutIntervalUnit;
        this.refs.timeoutInterval.value = timeoutInterval;

        // logs: log duration
        var logDurationUnit = parseInt(this.refs.logDurationUnit.value);
        var logDuration = parseInt(this.refs.logDuration.value);

        while (logDuration >= 60 && logDurationUnit < 2) {
            logDuration /= 60;
            logDurationUnit++;
        }

        if (logDuration >= 24) {
            logDuration /= 24;
            logDurationUnit++;
        }

        this.refs.logDurationUnit.value = logDurationUnit;
        this.refs.logDuration.value = logDuration;
    }

    /**
     * Obri el quadre de dialeg per a seleccionar un fitxer JSON
     * */
    seleccionarFitxerJSON() {
        this.refs.fitxerJSON.click();
    }

    changePWDClientPassword() {
        ReactDOM.unmountComponentAtNode(this.refs.renderitzarModal);
        ReactDOM.render(<ChangePWDModal
            canviarPWD={(iterations, salt, newPWD) => {
                this.canviarPWD("clientPassword", iterations, salt, newPWD);
            }}
        />, this.refs.renderitzarModal);
    }

    changePWDwebSocketSettings() {
        ReactDOM.unmountComponentAtNode(this.refs.renderitzarModal);
        ReactDOM.render(<ChangePWDModal
            canviarPWD={(iterations, salt, newPWD) => {
                this.canviarPWD("webSocketSettings", iterations, salt, newPWD);
            }}
        />, this.refs.renderitzarModal);
    }

    handleSelectCollections() {
        ReactDOM.unmountComponentAtNode(this.refs.renderitzarModal);
        ReactDOM.render(<SelectCollectionsModal
            collections={this.ConfigServer.backupSettings.collections}
            handleSave={this.handleSaveCollections}
        />, this.refs.renderitzarModal);
    };

    handleSaveCollections(collections) {
        this.ConfigServer.backupSettings.collections = collections;
    };

    handleAddressRestrictions() {
        ReactDOM.unmountComponentAtNode(this.refs.renderitzarModal);
        ReactDOM.render(<SocketAddrList
            blackList={this.ConfigServer.socketSettings.blackList}
            useWhiteList={this.ConfigServer.socketSettings.useWhiteList}
            whiteList={this.ConfigServer.socketSettings.whiteList}

            handleSave={this.handleSaveAddressRestrictions}
        />, this.refs.renderitzarModal);
    };

    handleSaveAddressRestrictions(blackList, useWhiteList, whiteList) {
        this.ConfigServer.socketSettings.blackList = blackList;
        this.ConfigServer.socketSettings.useWhiteList = useWhiteList;
        this.ConfigServer.socketSettings.whiteList = whiteList;
    };

    handleWSAddressRestrictions() {
        ReactDOM.unmountComponentAtNode(this.refs.renderitzarModal);
        ReactDOM.render(<SocketAddrList
            blackList={this.ConfigServer.webSocketSettings.blackList}
            useWhiteList={this.ConfigServer.webSocketSettings.useWhiteList}
            whiteList={this.ConfigServer.webSocketSettings.whiteList}

            handleSave={this.handleSaveWSAddressRestrictions}
        />, this.refs.renderitzarModal);
    };

    handleSaveWSAddressRestrictions(blackList, useWhiteList, whiteList) {
        this.ConfigServer.webSocketSettings.blackList = blackList;
        this.ConfigServer.webSocketSettings.useWhiteList = useWhiteList;
        this.ConfigServer.webSocketSettings.whiteList = whiteList;
    };

    handleOpenCertModal(websocket) {
        ReactDOM.unmountComponentAtNode(this.refs.renderitzarModal);
        ReactDOM.render(<UploadSocketCertModal
            handleListCerts={this.handleListCerts}
            handleRemoveCert={this.handleRemoveCert}
            handleCertUpload={this.handleCertUpload}
            handleCertDownload={this.handleCertDownload}

            handleModalSelected={
                websocket ? this.handleModalWebSocketCertificateSelected : this.handleModalCertificateSelected
            }
        />, this.refs.renderitzarModal);

    }

    handleModalCertificateSelected(uuid) {
        this.refs.sCertUUID.value = uuid;
    };

    handleModalWebSocketCertificateSelected(uuid) {
        this.refs.wsCertUUID.value = uuid;
    };

    mongoDbShowIndividualFields() {
        // change the tab content
        document.getElementById("mongoDbStorageIndividualFields").style.display = '';
        document.getElementById("mongoDbStorageURL").style.display = 'none';

        // change the active tab
        ReactDOM.unmountComponentAtNode(document.getElementById('mongoDbStorageTabs'));
        ReactDOM.render(
            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <a className="nav-link" onClick={this.mongoDbShowURL}>URL</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link active" onClick={this.mongoDbShowIndividualFields}>Individual fields</a>
                </li>
            </ul>, document.getElementById('mongoDbStorageTabs'));
    }

    mongoDbShowURL() {
        // change the tab content
        document.getElementById("mongoDbStorageURL").style.display = '';
        document.getElementById("mongoDbStorageIndividualFields").style.display = 'none';

        // change the active tab
        ReactDOM.unmountComponentAtNode(document.getElementById('mongoDbStorageTabs'));
        ReactDOM.render(
            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <a className="nav-link active" onClick={this.mongoDbShowURL}>URL</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" onClick={this.mongoDbShowIndividualFields}>Individual fields</a>
                </li>
            </ul>, document.getElementById('mongoDbStorageTabs'));

    }

    render() {
        return (
            <div>
                <nav className="navbar navbar-dark bg-dark" id="settingsTabMenu">
                    <a className="navbar-brand">
                        <img src={serverICO} alt="serverICO" />
                        <h3>
                            Server settings:
                        </h3>
                        <h5 id="settingsTabMenuFilename"></h5>
                    </a>
                    <form className="form-inline btn-group" role="group">
                        <button type="button" className="btn btn-outline-light" onClick={this.exportarAJSON}>
                            Export to JSON
                        </button>
                        <button type="button" className="btn btn-outline-info" onClick={this.seleccionarFitxerJSON}>Import JSON</button>
                        <button type="button" className="btn btn-outline-danger" onClick={this.handleGuardarServer}>
                            <div id="guardatConfSrv"></div>
                            Save
                        </button>
                        <input type="file" id="fitxerJSON" ref="fitxerJSON" onChange={this.importarJSON} />
                    </form>
                </nav>
                <div id="renderitzarModal" ref="renderitzarModal"></div>
                <div id="pantallaAjustos">
                    <div id="renderAlertModal"></div>
                    <div id="ajustosServer">

                        {!this.ConfigServer.mongoStorage.useDatabase &&
                            <div id="useMongoAlert">
                                <h4><img src={mongodbICO} alt="mongodbICO" />MongoDB Storage</h4>
                                <p>It is highly recommended to connect the server to a MongoDB database in order to improve the storage performance of the server.
                                Please install a MongoDB server, fill the connection fields below and enable the database connection.</p>
                            </div>
                        }

                        <form>{
                            // MAIN SERVER'S SETTINGS
                        }
                            <div id="confGeneral">
                                <label className="switch">
                                    <input type="checkbox" id="isDiscoverable" ref="isDiscoverable" defaultChecked={this.ConfigServer.isDiscoverable} />
                                    <span className="slider"></span>
                                </label>
                                <label htmlFor="isDiscoverable">Is discoverable over the network?</label>
                                <br />
                                <label htmlFor="discoverableName">Discovery name:</label>
                                <br />
                                <input type="text" id="discoverableName" className="form-control" ref="discoverableName" defaultValue={this.ConfigServer.discoverableName} />
                                <br />
                                <label htmlFor="tokenLength">Token length:</label>
                                <br />
                                <div className="input-group mb-3">
                                    <input type="number" id="tokenLength" className="form-control" ref="tokenLength" min="128" max="512" defaultValue={this.ConfigServer.tokenLength} />
                                    <div className="input-group-append">
                                        <button className="btn btn-outline-light" type="button"
                                            onClick={() => { document.getElementById("tokenLength").value = "256"; }}>Default</button>
                                    </div>
                                </div>
                                <br />
                                <br />
                            </div>
                            <div id="confSocket">
                                <h4 className="settingsTitle"><img src={socketICO} alt="socketICO" />Socket</h4>
                                <div className="collapse show " id="collapseSocketSettings">
                                    <div className="form-row">
                                        <div className="col">
                                            <label htmlFor="host">HOST:</label>
                                            <div className="input-group mb-3">
                                                <input type="text" id="host" className="form-control" ref="host" defaultValue={this.ConfigServer.socketSettings.host} />
                                                <div className="input-group-append">
                                                    <button className="btn btn-outline-light" type="button"
                                                        onClick={() => { document.getElementById("host").value = "0.0.0.0"; }}>Any</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <label htmlFor="port">PORT:</label>
                                            <div className="input-group mb-3">
                                                <input type="number" className="form-control" id="port" ref="port" min="0" max="65535" defaultValue={this.ConfigServer.socketSettings.port} />
                                                <div className="input-group-append">
                                                    <button className="btn btn-outline-light" type="button"
                                                        onClick={() => { document.getElementById("port").value = "22215"; }}>Default</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col">
                                            <label htmlFor="maxMessageSize">Maximum message size:</label>
                                            <div className="input-group">
                                                <input type="number" id="maxMessageSize" ref="maxMessageSize" className="form-control" min="0" defaultValue={this.ConfigServer.socketSettings.maxMessageSize} />
                                                <div className="input-group-append">
                                                    <select id="maxMessageSizeUnit" ref="maxMessageSizeUnit" className="form-control">
                                                        <option value="0">Bytes</option>
                                                        <option value="1">Kb</option>
                                                        <option value="2">Mb</option>
                                                        <option value="3">Gb</option>
                                                    </select>
                                                    <button className="btn btn-outline-light" type="button"
                                                        onClick={() => { document.getElementById("maxMessageSize").value = "0"; }}>Unbounded</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <label htmlFor="maxClients">Maximum number of clients:</label>
                                            <div className="input-group mb-3">
                                                <input type="number" id="maxClients" ref="maxClients" className="form-control" min="1" defaultValue={this.ConfigServer.socketSettings.maxClients} />
                                                <div className="input-group-append">
                                                    <button className="btn btn-outline-light" type="button"
                                                        onClick={() => { document.getElementById("maxClients").value = "0"; }}>Unbounded</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                    <h6>SOCKET OPTIONS</h6>
                                    <div className="form-group">
                                        <label className="switch">
                                            <input type="checkbox" id="encryption" ref="encryption" defaultChecked={this.ConfigServer.socketSettings.socketOptions.encryption} />
                                            <span className="slider"></span>
                                        </label>
                                        <label htmlFor="buscarEnLLista">Encrypt?</label>
                                        <br />
                                        <label className="switch">
                                            <input type="checkbox" id="compression" ref="compression" defaultChecked={this.ConfigServer.socketSettings.socketOptions.compression} />
                                            <span className="slider"></span>
                                        </label>
                                        <label htmlFor="buscarEnLLista">Compress?</label>
                                        <div className="row">
                                            <div className="col">
                                                <label htmlFor="sCertUUID">Select the certificate:</label>
                                                <div className="input-group mb-3">
                                                    <input type="text" id="sCertUUID" ref="sCertUUID" className="form-control" defaultValue={this.ConfigServer.socketSettings.socketOptions.certUUID} />
                                                    <div className="input-group-append">
                                                        <button type="button" className="btn btn-outline-danger" onClick={() => { this.handleOpenCertModal(false); }}>Select</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <label htmlFor="sCertPassword">Password of the certificate:</label>
                                                <input type="password" id="sCertPassword" className="form-control" ref="sCertPassword" defaultValue={this.ConfigServer.socketSettings.socketOptions.password} />
                                            </div>
                                        </div>
                                        <label className="switch">
                                            <input type="checkbox" id="mustUseServerSetings" ref="mustUseServerSetings" defaultChecked={this.ConfigServer.socketSettings.mustUseServerSetings} />
                                            <span className="slider"></span>
                                        </label>
                                        <label htmlFor="mustUseServerSetings">Client's socket settings must be the same?</label>
                                        <br />
                                        <div className="form-row">
                                            <div className="col">
                                                <label htmlFor="host">Network timeout (seconds):</label>
                                                <div className="input-group mb-3">
                                                    <input type="number" id="networkTimeout" className="form-control" ref="networkTimeout" defaultValue={this.ConfigServer.socketSettings.socketOptions.networkTimeout} />
                                                    <div className="input-group-append">
                                                        <button className="btn btn-outline-light" type="button"
                                                            onClick={() => { document.getElementById("networkTimeout").value = "60"; }}>Default</button>
                                                        <button className="btn btn-outline-warning" type="button"
                                                            onClick={() => { document.getElementById("networkTimeout").value = "0"; }}>Disabled</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col">
                                                <label htmlFor="port">Ping interval (seconds):</label>
                                                <div className="input-group mb-3">
                                                    <input type="number" className="form-control" id="pingInterval" ref="pingInterval" min="0" defaultValue={this.ConfigServer.socketSettings.socketOptions.pingInterval} />
                                                    <div className="input-group-append">
                                                        <button className="btn btn-outline-light" type="button"
                                                            onClick={() => { document.getElementById("pingInterval").value = "50"; }}>Default</button>
                                                        <button className="btn btn-outline-warning" type="button"
                                                            onClick={() => { document.getElementById("pingInterval").value = "0"; }}>Disabled</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <div className="form-group">
                                        <h6>CLIENTS PASSWORD:</h6>
                                        <label className="switch">
                                            <input type="checkbox" id="requireClientPassword" ref="requireClientPassword" defaultChecked={this.ConfigServer.socketSettings.requireClientPassword} />
                                            <span className="slider"></span>
                                        </label>
                                        <label htmlFor="buscarEnLLista">Client requires a password to log in?</label>
                                        <div className="form-row">
                                            <div className="col">
                                                <label htmlFor="iterPasswdWS">Hash interations:</label>
                                                <input type="number" id="iterPasswdClient" ref="iterPasswdClient" className="form-control" min="0" defaultValue={this.ConfigServer.socketSettings.pwd.iterations} />
                                            </div>
                                            <div className="col">
                                                <label htmlFor="saltPasswdWS">Salt:</label>
                                                <input type="text" id="saltPasswdClient" ref="saltPasswdClient" className="form-control" defaultValue={this.ConfigServer.socketSettings.pwd.salt} />
                                            </div>
                                        </div>
                                        <label htmlFor="passwdWS">Hashed password (SHA-512 only):</label>
                                        <br />
                                        <input type="text" id="passwdClient" className="form-control" ref="passwdClient" defaultValue={this.ConfigServer.socketSettings.pwd.hashSHA512} />
                                        <p id="preguntarCanviantPWD"></p>
                                        <label htmlFor="sautoBanMaxAttemps">Maximum login attempts before autoban:</label>
                                        <div className="input-group mb-3">
                                            <input type="number" id="sautoBanMaxAttemps" ref="sautoBanMaxAttemps" className="form-control" min="0" defaultValue={this.ConfigServer.socketSettings.autoBanMaxAttemps} />
                                            <div className="input-group-append">
                                                <button className="btn btn-outline-light" type="button"
                                                    onClick={() => { document.getElementById("sautoBanMaxAttemps").value = "10"; }}>Default</button>
                                                <button className="btn btn-outline-warning" type="button"
                                                    onClick={() => { document.getElementById("sautoBanMaxAttemps").value = "0"; }}>Disabled</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col">
                                            <button type="button" className="btn btn-outline-warning" onClick={this.changePWDClientPassword}>Change password</button>
                                        </div>
                                        <div className="col">
                                            <button type="button" className="btn btn-outline-light" onClick={this.handleAddressRestrictions}>Address restrictions</button>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <button className="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseSocketSettings" aria-expanded="false" aria-controls="collapseSocketSettings">

                                </button>
                            </div>
                            <div id="confWebSocket">
                                <h4 className="settingsTitle"><img src={websocketICO} alt="websocketICO" />WebSocket</h4>
                                <div className="collapse" id="collapseWebSocket">
                                    <div className="form-row">
                                        <div className="col">
                                            <label htmlFor="hostWS">WEBSOCKET HOST:</label>
                                            <div className="input-group mb-3">
                                                <input type="text" id="hostWS" className="form-control" ref="hostWS" defaultValue={this.ConfigServer.webSocketSettings.host} />
                                                <div className="input-group-append">
                                                    <button className="btn btn-outline-light" type="button"
                                                        onClick={() => { document.getElementById("hostWS").value = "0.0.0.0"; }}>Any</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <label htmlFor="portWS">WEBSOCKET PORT:</label>
                                            <div className="input-group mb-3">
                                                <input type="number" id="portWS" className="form-control" ref="portWS" min="0" max="65535" defaultValue={this.ConfigServer.webSocketSettings.port} />
                                                <div className="input-group-append">
                                                    <button className="btn btn-outline-light" type="button"
                                                        onClick={() => { document.getElementById("portWS").value = "22216"; }}>Default</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <h6>WEBSOCKET PASSWORD:</h6>
                                    <div className="form-group">
                                        <div className="form-row">
                                            <div className="col">
                                                <label htmlFor="iterPasswdWS">Hash interations:</label>
                                                <input type="number" id="iterPasswdWS" className="form-control" ref="iterPasswdWS" min="0" defaultValue={this.ConfigServer.webSocketSettings.pwd.iterations} />
                                            </div>
                                            <div className="col">

                                                <label htmlFor="saltPasswdWS">Salt:</label>
                                                <input type="text" id="saltPasswdWS" className="form-control" ref="saltPasswdWS" defaultValue={this.ConfigServer.webSocketSettings.pwd.salt} />
                                            </div>
                                        </div>
                                        <br />
                                        <label htmlFor="passwdWS">Hashed password (SHA-512 only):</label>
                                        <input type="text" id="passwdWS" ref="passwdWS" defaultValue={this.ConfigServer.webSocketSettings.pwd.hashSHA512} className="form-control" />
                                    </div>
                                    <h6>SECURITY:</h6>
                                    <label className="switch">
                                        <input type="checkbox" id="wsIsSecure" ref="wsIsSecure" defaultChecked={this.ConfigServer.webSocketSettings.isSecure} />
                                        <span className="slider"></span>
                                    </label>
                                    <label htmlFor="wsIsSecure">Is secure (enable wss://)?</label>
                                    <div className="row">
                                        <div className="col">
                                            <div className="col">
                                                <label htmlFor="wsCertUUID">Select the certificate:</label>
                                                <div className="input-group mb-3">
                                                    <input type="text" id="wsCertUUID" className="form-control" ref="wsCertUUID" defaultValue={this.ConfigServer.webSocketSettings.certUUID} />
                                                    <div className="input-group-append">
                                                        <button type="button" className="btn btn-outline-danger" onClick={() => { this.handleOpenCertModal(true); }}>Select</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <label htmlFor="wsCertPassword">Password of the certificate:</label>
                                            <input type="password" id="wsCertPassword" className="form-control" ref="wsCertPassword" defaultValue={this.ConfigServer.webSocketSettings.password} />
                                        </div>
                                    </div>
                                    <label htmlFor="sautoBanMaxAttemps">Maximum login attempts before autoban:</label>
                                    <div className="input-group mb-3">
                                        <input type="number" id="wsautoBanMaxAttemps" ref="wsautoBanMaxAttemps" className="form-control" min="0" defaultValue={this.ConfigServer.webSocketSettings.autoBanMaxAttemps} />
                                        <div className="input-group-append">
                                            <button className="btn btn-outline-light" type="button"
                                                onClick={() => { document.getElementById("wsautoBanMaxAttemps").value = "3"; }}>Default</button>
                                            <button className="btn btn-outline-warning" type="button"
                                                onClick={() => { document.getElementById("wsautoBanMaxAttemps").value = "0"; }}>Disabled</button>
                                        </div>
                                    </div>
                                    <br />
                                    <div className="form-row">
                                        <div className="col">
                                            <button type="button" className="btn btn-outline-warning" id="preguntarCanviarPWD" onClick={this.changePWDwebSocketSettings}>Change password</button>
                                            <p id="preguntarCanviantPWD"></p>
                                        </div>
                                        <div className="col">
                                            <button type="button" className="btn btn-outline-light" onClick={this.handleWSAddressRestrictions}>Address restrictions</button>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-primary collapsed" type="button" data-toggle="collapse" data-target="#collapseWebSocket" aria-expanded="false" aria-controls="collapseWebSocket">

                                </button>
                            </div>
                            <div id="confBackup">
                                <h4 className="settingsTitle"><img src={backupICO} alt="backupICO" />Backup Settings</h4>
                                <div className="collapse" id="collapseBackup">
                                    <label className="switch">
                                        <input type="checkbox" id="doBackups" ref="doBackups" defaultChecked={this.ConfigServer.backupSettings.doBackups} />
                                        <span className="slider"></span>
                                    </label>
                                    <label htmlFor="doBackups">Do backups?</label>
                                    <div className="form-row">
                                        <div className="col">
                                            <label htmlFor="timeoutInterval">Timeout interval:</label>
                                            <div className="input-group">
                                                <input type="number" id="timeoutInterval" className="form-control" ref="timeoutInterval" min="60000" defaultValue={this.ConfigServer.backupSettings.timeoutInterval} />
                                                <div className="input-group-append">
                                                    <select id="timeoutIntervalUnit" ref="timeoutIntervalUnit" className="form-control">
                                                        <option value="0">Seconds</option>
                                                        <option value="1">Minutes</option>
                                                        <option value="2">Hours</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <label htmlFor="sizeLimit">Size limit:</label>
                                            <div className="input-group">
                                                <input type="number" id="sizeLimit" className="form-control" ref="sizeLimit" min="0" defaultValue={this.ConfigServer.backupSettings.sizeLimit} />
                                                <div className="input-group-append">
                                                    <select id="sizeLimitUnit" ref="sizeLimitUnit" className="form-control">
                                                        <option value="0">Bytes</option>
                                                        <option value="1">Kb</option>
                                                        <option value="2">Mb</option>
                                                        <option value="3">Gb</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <label htmlFor="backupSettingsAESKey">AES Key:</label>
                                    <input type="text" id="backupSettingsAESKey" ref="backupSettingsAESKey" defaultValue={this.ConfigServer.backupSettings.AESKey} className="form-control" />
                                    <br />
                                    <button type="button" className="btn btn-outline-light" onClick={this.handleSelectCollections}>Customize collections</button>
                                </div>
                                <br />
                                <button className="btn btn-primary collapsed" type="button" data-toggle="collapse" data-target="#collapseBackup" aria-expanded="false" aria-controls="collapseBackup">

                                </button>
                            </div>
                            <div id="confLogs">
                                <h4 className="settingsTitle"><img src={logsICO} alt="logsICO" />Logs</h4>
                                <div className="collapse" id="collapseLogs">
                                    <div className="form-row">
                                        <div className="col">
                                            <label className="switch">
                                                <input type="checkbox" id="log" ref="log"
                                                    defaultChecked={this.ConfigServer.loggingSettings.log} />
                                                <span className="slider"></span>
                                            </label>
                                            <label htmlFor="log">Enable logging?</label>
                                        </div>
                                        <div className="col">
                                            <label htmlFor="logDuration">Log duration:</label>
                                            <div id="input-group">
                                                <div className="input-group mb-3">
                                                    <input type="number" className="form-control" id="logDuration" ref="logDuration" min="60" defaultValue={this.ConfigServer.loggingSettings.logDuration} />
                                                    <div className="input-group-append">
                                                        <select id="logDurationUnit" ref="logDurationUnit" className="form-control">
                                                            <option value="0">Seconds</option>
                                                            <option value="1">Minutes</option>
                                                            <option value="2">Hours</option>
                                                            <option value="3">Days</option>
                                                        </select>
                                                        <button className="btn btn-outline-light" type="button"
                                                            onClick={() => {
                                                                document.getElementById("logDuration").value = "1";
                                                                document.getElementById("logDurationUnit").value = "3";
                                                            }}>Default</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <div className="form-row">
                                        <div className="col">
                                            <label className="switch">
                                                <input type="checkbox" id="logClients" ref="logClients"
                                                    defaultChecked={this.ConfigServer.loggingSettings.logClients} />
                                                <span className="slider"></span>
                                            </label>
                                            <label htmlFor="logClients">Log clients?</label>
                                        </div>
                                        <div className="col">
                                            <label className="switch">
                                                <input type="checkbox" id="logWeb" ref="logWeb"
                                                    defaultChecked={this.ConfigServer.loggingSettings.logWeb} />
                                                <span className="slider"></span>
                                            </label>
                                            <label htmlFor="logWeb">Log WebSocket connections?</label>
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col">
                                            <label className="switch">
                                                <input type="checkbox" id="logHashes" ref="logHashes"
                                                    defaultChecked={this.ConfigServer.loggingSettings.logHashes} />
                                                <span className="slider"></span>
                                            </label>
                                            <label htmlFor="logHashes">Log hashes?</label>
                                        </div>
                                        <div className="col">
                                            <label className="switch">
                                                <input type="checkbox" id="logRainbows" ref="logRainbows"
                                                    defaultChecked={this.ConfigServer.loggingSettings.logRainbows} />
                                                <span className="slider"></span>
                                            </label>
                                            <label htmlFor="logRainbows">Log rainbows?</label>
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col">
                                            <label className="switch">
                                                <input type="checkbox" id="logDictionaries" ref="logDictionaries"
                                                    defaultChecked={this.ConfigServer.loggingSettings.logDictionaries} />
                                                <span className="slider"></span>
                                            </label>
                                            <label htmlFor="logDictionaries">Log dictionaries?</label>
                                        </div>
                                        <div className="col">
                                            <label className="switch">
                                                <input type="checkbox" id="logSettings" ref="logSettings"
                                                    defaultChecked={this.ConfigServer.loggingSettings.logSettings} />
                                                <span className="slider"></span>
                                            </label>
                                            <label htmlFor="logSettings">Log settings?</label>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <button className="btn btn-primary collapsed" type="button" data-toggle="collapse" data-target="#collapseLogs" aria-expanded="false" aria-controls="collapseLogs">

                                </button>
                            </div>
                            <div id="confEnmagatzenamentLocal">
                                <h4 className="settingsTitle"><img src={enmagatzenamentICO} alt="enmagatzenamentICO" />Local Storage</h4>
                                <div className="collapse" id="collapseLocalStorage">
                                    <label className="switch">
                                        <input type="checkbox" id="useLocalStorage" ref="useLocalStorage"
                                            defaultChecked={this.ConfigServer.localStorage.useLocalStorage} />
                                        <span className="slider"></span>
                                    </label>
                                    <label htmlFor="habilitarBaseDeDades">Enable local storage?</label>
                                    <br />
                                    <div className="form-row">
                                        <div className="col">
                                            <label htmlFor="folderName">Folder name:</label>
                                            <div className="input-group mb-3">
                                                <input type="text" id="folderName" className="form-control" ref="folderName" defaultValue={this.ConfigServer.localStorage.folderName} />
                                                <div className="input-group-append">
                                                    <button type="button" className="btn btn-outline-light"
                                                        onClick={() => { document.getElementById("folderName").value = "local_storage"; }}>Default</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <label htmlFor="operationsToRegenerate">Count of operations to regenerate the file:</label>
                                            <div className="input-group mb-3">
                                                <input type="number" id="operationsToRegenerate" min="10" max="512" className="form-control" ref="operationsToRegenerate" defaultValue={this.ConfigServer.localStorage.operationsToRegenerate} />
                                                <div className="input-group-append">
                                                    <button type="button" className="btn btn-outline-light"
                                                        onClick={() => { document.getElementById("operationsToRegenerate").value = "50"; }}>Default</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col">
                                            <label htmlFor="fitxerCua">Hash storage file name:</label>
                                            <div className="input-group mb-3">
                                                <input type="text" id="fitxerCua" className="form-control" ref="fitxerCua" defaultValue={this.ConfigServer.localStorage.hashesFileName} />
                                                <div className="input-group-append">
                                                    <button type="button" className="btn btn-outline-light"
                                                        onClick={() => { document.getElementById("fitxerCua").value = "hashes.arydb"; }}>Default</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <label htmlFor="rainbowFile">Rainbow definitions list filename:</label>
                                            <div className="input-group mb-3">
                                                <input type="text" id="rainbowFile" className="form-control" ref="rainbowFile"
                                                    defaultValue={this.ConfigServer.localStorage.rainbowsFileName} />
                                                <div className="input-group-append">
                                                    <button type="button" className="btn btn-outline-light"
                                                        onClick={() => { document.getElementById("rainbowFile").value = "rainbows.arydb"; }}>Default</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col">
                                            <label htmlFor="fitxerDiccionaris">Dictionaries definition list filename:</label>
                                            <div className="input-group mb-3">
                                                <input type="text" id="fitxerDiccionaris" className="form-control" ref="fitxerDiccionaris"
                                                    defaultValue={this.ConfigServer.localStorage.savedDictionariesFileName} />
                                                <div className="input-group-append">
                                                    <button type="button" className="btn btn-outline-light"
                                                        onClick={() => { document.getElementById("fitxerDiccionaris").value = "dictionaries.arydb"; }}>Default</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <label htmlFor="fitxerDiccionaris">Authorisation tokens filename:</label>
                                            <div className="input-group mb-3">
                                                <input type="text" id="tokensFileName" className="form-control" ref="tokensFileName"
                                                    defaultValue={this.ConfigServer.localStorage.tokensFileName} />
                                                <div className="input-group-append">
                                                    <button type="button" className="btn btn-outline-light"
                                                        onClick={() => { document.getElementById("tokensFileName").value = "tokens.arydb"; }}>Default</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col">
                                            <label htmlFor="certificatesFileName">Certificates definition list filename:</label>
                                            <div className="input-group mb-3">
                                                <input type="text" id="certificatesFileName" className="form-control" ref="certificatesFileName"
                                                    defaultValue={this.ConfigServer.localStorage.certificatesFileName} />
                                                <div className="input-group-append">
                                                    <button type="button" className="btn btn-outline-light"
                                                        onClick={() => { document.getElementById("certificatesFileName").value = "certs.arydb"; }}>Default</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <label htmlFor="logsFileName">Logs filename:</label>
                                            <div className="input-group mb-3">
                                                <input type="text" id="logsFileName" className="form-control" ref="logsFileName"
                                                    defaultValue={this.ConfigServer.localStorage.logsFileName} />
                                                <div className="input-group-append">
                                                    <button type="button" className="btn btn-outline-light"
                                                        onClick={() => { document.getElementById("logsFileName").value = "logs.arydb"; }}>Default</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <button className="btn btn-primary collapsed" type="button" data-toggle="collapse" data-target="#collapseLocalStorage" aria-expanded="false" aria-controls="collapseLocalStorage">

                                </button>
                                <br />
                            </div>
                            <div id="confEnmagatzenamentMongo">
                                <h4 className="settingsTitle"><img src={mongodbICO} alt="mongodbICO" />MongoDB Storage (optional)</h4>
                                <div className="collapse" id="collapseMongo">
                                    <label className="switch">
                                        <input type="checkbox" id="useDatabase" ref="useDatabase"
                                            defaultChecked={this.ConfigServer.mongoStorage.useDatabase} />
                                        <span className="slider"></span>
                                    </label>
                                    <label htmlFor="habilitarBaseDeDades">Connect to MongoDB database?</label>
                                    <br />
                                    <div id="mongoDbStorageTabs"></div>
                                    <br />
                                    <div id="mongoDbStorageConnection">
                                        <div id="mongoDbStorageIndividualFields">
                                            <div>
                                                <div className="form-row">
                                                    <div className="col">
                                                        <label htmlFor="mongoHost">Database host:</label>
                                                        <input type="text" id="mongoHost" ref="mongoHost"
                                                            defaultValue={this.ConfigServer.mongoStorage.host} className="form-control" />
                                                    </div>
                                                    <div className="col">
                                                        <label htmlFor="portMongo">Database port:</label>
                                                        <div className="input-group mb-3">
                                                            <input type="number" id="portMongo" ref="portMongo" min="0" max="65535"
                                                                defaultValue={this.ConfigServer.mongoStorage.port} className="form-control" />
                                                            <div className="input-group-append">
                                                                <button type="button" className="btn btn-outline-light" onClick={() => { document.getElementById("portMongo").value = "27017"; }}>Default</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col">
                                                        <label htmlFor="dbMongo">Database name:</label>
                                                        <div className="input-group mb-3">
                                                            <input type="text" id="dbMongo" ref="dbMongo"
                                                                defaultValue={this.ConfigServer.mongoStorage.databaseName} className="form-control" />
                                                            <div className="input-group-append">
                                                                <button type="button" className="btn btn-outline-light" onClick={() => { document.getElementById("dbMongo").value = "serverHashes"; }}>Default</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-row">
                                                    <div className="col">
                                                        <label htmlFor="usuariMongo">User name (optional):</label>
                                                        <input type="text" id="usuariMongo" ref="usuariMongo"
                                                            defaultValue={this.ConfigServer.mongoStorage.userName} className="form-control" />
                                                    </div>
                                                    <div className="col">
                                                        <label htmlFor="pwdMongo">User password (optional):</label>
                                                        <div className="input-group">
                                                            <input type="password" id="pwdMongo" ref="pwdMongo"
                                                                defaultValue={this.ConfigServer.mongoStorage.password} className="form-control" />
                                                            <div className="input-group-append">
                                                                <select id="authMongo" ref="authMongo" defaultValue={this.ConfigServer.mongoStorage.mongoDbAuthMechanism} className="form-control" >
                                                                    <option value="NONE">No authentication</option>
                                                                    <option value="SCRAM-SHA-1">SCRAM-SHA-1</option>
                                                                    <option value="SCRAM-SHA-256">SCRAM-SHA-256</option>
                                                                    <option value="MONGODB-X509">MONGODB-X509</option>
                                                                    <option value="GSSAPI">GSSAPI (Kerberos)</option>
                                                                    <option value="PLAIN">PLAIN (LDAP SASL)</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="mongoDbStorageURL" style={{ 'display': 'none' }}>
                                            <label htmlFor="mongoURL">Connection string:</label>
                                            <input type="text" id="mongoURL" ref="mongoURL"
                                                defaultValue={this.ConfigServer.mongoStorage.url} className="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <button className="btn btn-primary collapsed" type="button" data-toggle="collapse" data-target="#collapseMongo" aria-expanded="false" aria-controls="collapseMongo">

                                </button>
                                <br />
                            </div>
                            <br />
                        </form>
                        <br />
                    </div>
                </div>
            </div>
        )
    }
}

export default Settings;
