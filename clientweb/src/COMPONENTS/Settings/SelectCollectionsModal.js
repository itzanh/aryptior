// REACT
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// IMG
import trashICO from './../../IMG/trash.svg';

class SelectCollectionsModal extends Component {
    constructor({ collections, handleSave }) {
        super();

        this.collections = collections;
        this.handleSave = handleSave;

        this.renderCollections = this.renderCollections.bind(this);
        this.handleRemoveCollection = this.handleRemoveCollection.bind(this);
        this.handleAddCollection = this.handleAddCollection.bind(this);
        this.handleSaveData = this.handleSaveData.bind(this);
    }

    componentDidMount() {
        window.$('#selectCollections').modal({ show: true });

        this.renderCollections();
    };

    renderCollections() {
        const componentsToRender = [];

        this.collections.forEach((element, i) => {
            componentsToRender.push(<Collection
                key={i}

                name={element}
                handleRemove={this.handleRemoveCollection}
            />);
        });

        ReactDOM.unmountComponentAtNode(document.getElementById('collectionList'));
        ReactDOM.render(
            componentsToRender, document.getElementById('collectionList'));
    };

    handleRemoveCollection(name) {
        const index = this.collections.indexOf(name);
        this.collections.splice(index, 1);

        this.renderCollections();
    };

    handleAddCollection() {
        const newCollection = document.getElementById("newCollection").value;
        this.collections.push(newCollection);
        document.getElementById("newCollection").value = "";

        this.renderCollections();
    };

    handleSaveData() {
        this.handleSave(this.collections);
    };

    render() {
        return (
            <div id="selectCollectionsModal">
                <div className="modal fade show" id="selectCollections" tabIndex="-1" role="dialog" aria-labelledby="selectCollections" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Select collections to backup</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <table className="table table-dark">
                                    <thead>
                                        <tr>
                                            <th scope="col">Collection name</th>
                                            <th scope="col">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody id="collectionList">
                                    </tbody>
                                </table>
                                <div className="input-group">
                                    <input type="text" id="newCollection" className="form-control" autoComplete="no" placeholder="Collection name" />
                                    <div className="input-group-append">
                                        <button type="button" className="btn btn-outline-light" onClick={this.handleAddCollection}>Add</button>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.handleSaveData}>Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class Collection extends Component {
    constructor({ name, handleRemove }) {
        super();

        this.name = name;
        this.handleRemove = handleRemove;

        this.handleCollectionRemoved = this.handleCollectionRemoved.bind(this);
    }

    handleCollectionRemoved() {
        this.handleRemove(this.name);
    }

    render() {
        return (
            <tr>
                <td>{this.name}</td>
                <td><img src={trashICO} alt="trashICO" onClick={this.handleCollectionRemoved} /></td>
            </tr>
        )
    }
}

export default SelectCollectionsModal;
