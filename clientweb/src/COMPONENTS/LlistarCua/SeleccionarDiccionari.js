// REACT
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class SeleccionarDiccionari extends Component {
    constructor({ getDictionaries, handleSelect }) {
        super();

        this.getDictionaries = getDictionaries;
        this.handleSelect = handleSelect;

        this.dictionaries = null;

        this.saveResults = this.saveResults.bind(this);
    }

    componentDidMount() {
        window.$('#selectDictionaryModal').modal({ show: true });

        this.renderMainScreen();
    }

    async renderMainScreen() {
        ReactDOM.render(
            <small>Loading dictionaries list, please wait...</small>,
            document.getElementById("modal-body"));

        this.dictionaries = await this.getDictionaries();

        ReactDOM.unmountComponentAtNode(document.getElementById('modal-body'));
        ReactDOM.render(
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th>UUID</th>
                        <th>File name</th>
                        <th>Size</th>
                    </tr>
                </thead>
                <tbody id="renderDictionarySelection"></tbody>
            </table>, document.getElementById('modal-body'));

        const componentsToRender = [];

        this.dictionaries.forEach((element, i) => {
            componentsToRender.push(
                <SelectDictionary
                    key={i}

                    dictionary={element}
                />);
        });

        ReactDOM.unmountComponentAtNode(document.getElementById('renderDictionarySelection'));
        ReactDOM.render(
            componentsToRender, document.getElementById('renderDictionarySelection'));
    }

    saveResults() {
        const radioButtons = document.getElementsByName("selectDictionary");
        for (let i = 0; i < radioButtons.length; i++) {
            const radioButton = radioButtons[i];
            if (radioButton.checked) {
                const uuidSel = radioButton.value;

                // searh for the uuid on the list
                for (let j = 0; j < this.dictionaries.length; i++) {
                    const dictionarySel = this.dictionaries[j];

                    if (dictionarySel.uuid === uuidSel) {
                        this.handleSelect(dictionarySel);
                        window.$('#selectDictionaryModal').modal('hide');
                        return;
                    }
                }
            }
        }

        alert("You must select a dictioonary or cancel.");
        return;
    }

    render() {
        return (
            <div>
                <div className="modal fade" id="selectDictionaryModal" tabIndex="-1" role="dialog" aria-labelledby="selectDictionaryModal" aria-hidden="false">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">Select dictionary to continue</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body" id="modal-body">

                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" className="btn btn-primary" onClick={this.saveResults}>Select</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class SelectDictionary extends Component {

    constructor({ dictionary }) {
        super();

        this.dictionary = dictionary;
        this.sizeInBytes = this.formatSize(dictionary.sizeInBytes);
    }

    formatSize(sizeInBytes) {
        const units = ['B', 'Kb', 'Mb', 'Gb', 'Tb'];
        var unit = 0;
        while (sizeInBytes >= 1024) {
            sizeInBytes /= 1024;
            unit++;
        }
        return Math.floor(sizeInBytes) + ' ' + units[unit];
    }

    render() {
        return (
            <tr className="diccionari">
                <td>
                    <input type="radio" name="selectDictionary" value={this.dictionary.uuid}></input>
                </td>
                <td>{this.dictionary.uuid}</td>
                <td>{this.dictionary.fileName}</td>
                <td>{this.sizeInBytes}</td>
            </tr>
        )
    }
}

export default SeleccionarDiccionari;
