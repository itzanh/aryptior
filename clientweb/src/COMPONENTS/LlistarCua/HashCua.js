// REACT
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// COMPONENTS
import HashDetailsModal from '../HashDetailsModal';

// IMG
import optionsICO from './../../IMG/options.svg';

const tipusEnum = ['NTLM', 'MD4', 'MD5', 'SHA1', 'SHA-256', 'SHA-384', 'SHA-512', 'BCRYPT', 'RIPEMD128',
    'RIPEMD160', 'RIPEMD256', 'RIPEMD320', 'BLAKE', 'BLAKE2b (512)', 'BLAKE2s (256)', 'WHIRLPOOL', 'TIGER',
    'TIGER2', 'TIGER3', 'TIGER4', 'MODULAR', 'MYSQL', 'WORDPRESS'];

class HashCua extends Component {
    constructor({ objecte, handleShowDetails }) {
        super();

        this.hash = objecte;
        this.tipus = tipusEnum[objecte.tipus];
        this.caracters = this.combinacioAString(objecte.caracters);
        this.primerCaracterUnicode = this.numCaracterAString(objecte.primerCaracterUnicode);
        this.ultimCaracterUnicode = this.numCaracterAString(objecte.ultimCaracterUnicode);
        this.msProcessingCount = this.formatejarTemps(objecte.msProcessingCount);

        this.handleShowDetails = handleShowDetails;

        this.renderModal = this.renderModal.bind(this);
    }

    numCaracterAString(num) {
        return String.fromCharCode(num);
    }

    combinacioAString(combinacio) {
        var cadena = "";
        for (var i = (combinacio.length - 1); i >= 0; i--) {
            cadena += this.numCaracterAString(combinacio[i]);
        }
        return cadena;
    }

    formatejarTemps(ms) {
        const unitats = ['ms', 's', 'min', 'hrs'];
        var unitat = 0;
        if (ms > 1000) {
            unitat++;
            ms /= 1000;
        }
        while (ms > 60 && unitat < unitats.length - 1) {
            ms /= 60;
            unitat++;
        }
        return Math.floor(ms) + " " + unitats[unitat];
    }

    renderModal() {
        this.handleShowDetails(this.hash);
    }

    render() {
        return (
            <tr onClick={this.renderModal} >
                <td>
                    {this.hash.hash}
                </td>
                <td>
                    <span className="badge badge-pill badge-danger">
                        {this.tipus}
                    </span>
                </td>
                <td>
                    {this.primerCaracterUnicode + " - " + this.ultimCaracterUnicode}
                </td>
                <td>
                    {this.hash.initialTestCount}
                </td>
                <td>
                    {this.msProcessingCount}
                </td>
                <td>
                    {this.caracters}
                </td>
                <td>
                    <img src={optionsICO} alt="optionsICO" />
                </td>
            </tr>
        )
    }
}

export default HashCua;
