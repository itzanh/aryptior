// REACT
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// COMPONENTS
import { SeleccioForcaBruta } from './FormulariCua';

// IMG
import errorICO from './../../IMG/error.svg';
import clientsICO from './../../IMG/clients.svg';

const tipusEnum = ['NTLM', 'MD4', 'MD5', 'SHA1', 'SHA256', 'SHA384', 'SHA512', 'BCRYPT', 'RIPEMD128',
    'RIPEMD160', 'RIPEMD256', 'RIPEMD320', 'BLAKE', 'BLAKE2b', 'BLAKE2s', 'WHIRLPOOL', 'TIGER',
    'TIGER2', 'TIGER3', 'TIGER4', 'MODULAR', 'MYSQL', 'WORDPRESS'];


class HashPerformanceTest extends Component {
    constructor({ hash, handleTest }) {
        super();

        this.hash = hash;
        this.handleTest = handleTest;

        this.handleBeginTest = this.handleBeginTest.bind(this);
        this.MainFooter = this.MainFooter.bind(this);
    }

    componentDidMount() {
        window.$('#hashPerformanceTestModal').modal({ show: true });

        ReactDOM.render(<HashPerformanceTestForm
            computeNumberOfCombinations={this.computeNumberOfCombinations}

            ref={inst => {
                if (!inst) return;
                Object.keys(inst.refs).forEach((element) => {
                    this.refs[element] = inst.refs[element];
                });
            }}
        />, this.refs.renderModalContent);

        ReactDOM.render(<this.MainFooter />
            , this.refs.modalFooter);
    }

    computeNumberOfCombinations(firstUnicodeCharacter, lastUnicodeCharacter, initalLength, finalLength) {
        const range = lastUnicodeCharacter - firstUnicodeCharacter;
        var combinations = 0;
        for (let i = initalLength; i <= finalLength; i++) {
            combinations += Math.pow(range, i);
        }
        return combinations;
    }

    async handleBeginTest() {
        const algorithm = tipusEnum.indexOf(this.refs.tipoHash.value);
        const timeout = parseInt(this.refs.timeLapseProcessingCount.value);
        const iterations = parseInt(this.refs.iteracions.value);
        const salt = this.refs.salt.value;
        const firstUnicodeCharacter = parseInt(this.refs.primerCaracter.value);
        const lastUnicodeCharacter = parseInt(this.refs.ultimCaracter.value);
        const initialLength = parseInt(this.refs.initalLength.value);
        const finalLength = parseInt(this.refs.finalLength.value);

        if (timeout < 15 || timeout > 60) {
            this.refs.timeLapseProcessingCount.style.border = 'solid';
            this.refs.timeLapseProcessingCount.style.borderColor = 'red';
            return;
        } else if (iterations < 1) {
            this.refs.iteracions.style.border = 'solid';
            this.refs.iteracions.style.borderColor = 'red';
            return;
        } else if (initialLength < 1 || initialLength < 1) {
            this.refs.initalLength.style.border = 'solid';
            this.refs.initalLength.style.borderColor = 'red';
            return;
        } else if (finalLength <= initialLength) {
            this.refs.initalLength.style.border = 'solid';
            this.refs.initalLength.style.borderColor = 'red';
            this.refs.finalLength.style.border = 'solid';
            this.refs.finalLength.style.borderColor = 'red';
            alert('The final length cannot be less or equal the the initial length');
            return;
        } else if (firstUnicodeCharacter < 32) {
            this.refs.primerCaracter.style.border = 'solid';
            this.refs.primerCaracter.style.borderColor = 'red';
            alert('The first character cannot be less than 32.');
            return;
        } else if (lastUnicodeCharacter <= firstUnicodeCharacter) {
            this.refs.primerCaracter.style.border = 'solid';
            this.refs.primerCaracter.style.borderColor = 'red';
            this.refs.ultimCaracter.style.border = 'solid';
            this.refs.ultimCaracter.style.borderColor = 'red';
            alert('The last character cannot be less or equal than the first character.');
            return;
        } else if (isNaN(firstUnicodeCharacter) || isNaN(lastUnicodeCharacter)) {
            this.refs.primerCaracter.style.border = 'solid';
            this.refs.primerCaracter.style.borderColor = 'red';
            this.refs.ultimCaracter.style.border = 'solid';
            this.refs.ultimCaracter.style.borderColor = 'red';
            alert('You must specify a character set.');
            return;
        }

        ReactDOM.unmountComponentAtNode(this.refs.modalFooter);

        var progress = 0;
        const maxProgress = timeout * 1000;
        const interval = setInterval(() => {
            ReactDOM.unmountComponentAtNode(this.refs.renderModalContent);
            ReactDOM.render(<div>
                <progress value={(progress / maxProgress) * 100} max="100"></progress>
                <p className="progressing">{Math.floor((progress / maxProgress) * 100)}% {timeout - Math.floor(progress / 1000)}s / {timeout}s</p>
            </div>, this.refs.renderModalContent);

            progress += 100;

            if (progress >= maxProgress) {
                clearInterval(interval);

                ReactDOM.unmountComponentAtNode(this.refs.renderModalContent);
                ReactDOM.render(<div>
                    <progress></progress>
                    <p>Waiting for the processing to be finished...</p>
                </div>, this.refs.renderModalContent);
            }
        }, 100);

        this.handleTest({
            timeout: (timeout * 1000),
            algorithm,
            iterations,
            salt,
            firstUnicodeCharacter,
            lastUnicodeCharacter,
            initialLength,
            finalLength
        }).then((results) => {
            results.combinations = this.computeNumberOfCombinations
                (firstUnicodeCharacter, lastUnicodeCharacter, initialLength, finalLength);
            results.rate = (results.hashCount / timeout);
            results.timeout = timeout;

            clearInterval(interval);

            ReactDOM.unmountComponentAtNode(this.refs.renderModalContent);
            ReactDOM.render(<HashPerformanceTestResults
                results={results}
            />, this.refs.renderModalContent);

            ReactDOM.render(<this.CloseFooter />
                , this.refs.modalFooter);
        }, () => {
            clearInterval(interval);
            ReactDOM.unmountComponentAtNode(this.refs.renderModalContent);
            ReactDOM.render(<p>There was an error during testing. Check the server and try again.</p>, this.refs.renderModalContent);

            ReactDOM.render(<this.CloseFooter />
                , this.refs.modalFooter);
        });

    }

    MainFooter() {
        return (<div>
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="button" className="btn btn-primary" onClick={this.handleBeginTest}>Begin test</button>
        </div>);
    }

    CloseFooter() {
        return (<div>
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>);
    }

    render() {
        return (<div className="modal fade bd-example-modal-lg" tabIndex="-1" id="hashPerformanceTestModal"
            role="dialog" aria-labelledby="hashPerformanceTestModal" aria-hidden="true">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLongTitle">Test performance</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div ref="renderModalContent">
                        </div>
                    </div>
                    <div className="modal-footer" ref="modalFooter">
                    </div>
                </div>
            </div>
        </div>)
    }
}

class HashPerformanceTestForm extends Component {

    constructor({ computeNumberOfCombinations }) {
        super();

        this.computeNumberOfCombinations = computeNumberOfCombinations;

        this.renderCombinations = this.renderCombinations.bind(this);
    }

    componentDidMount() {
        this.refs.tipoBusqueda.addEventListener('change', () => {
            setTimeout(() => {
                this.renderCombinations();
            }, 10);
        });
        this.refs.primerCaracter.addEventListener('change', this.renderCombinations);
        this.refs.ultimCaracter.addEventListener('change', this.renderCombinations);

        this.renderCombinations();
    }


    renderCombinations() {
        const firstUnicodeCharacter = parseInt(this.refs.primerCaracter.value);
        const lastUnicodeCharacter = parseInt(this.refs.ultimCaracter.value);
        const initalLength = parseInt(this.refs.initalLength.value);
        const finalLength = parseInt(this.refs.finalLength.value);

        if (firstUnicodeCharacter >= 32 && lastUnicodeCharacter > 0 && lastUnicodeCharacter > firstUnicodeCharacter
            && initalLength > 0 && finalLength > initalLength) {
            ReactDOM.unmountComponentAtNode(this.refs.previewCombinations);
            ReactDOM.render(<div>
                <p>Number of combinations</p>
                <h4>{this.computeNumberOfCombinations(firstUnicodeCharacter, lastUnicodeCharacter, initalLength, finalLength).toLocaleString('eu')}</h4>
            </div>, this.refs.previewCombinations);
        } else {
            ReactDOM.unmountComponentAtNode(this.refs.previewCombinations);
            ReactDOM.render(<div>
                <p>Number of combinations</p>
                <h4>-</h4>
            </div>, this.refs.previewCombinations);
        }
    }

    render() {
        return (<div>
            <div ref="previewCombinations" id="previewCombinations">
            </div>

            <div className="row">
                <div className="col">
                    <label>Algorithm:</label>
                    <select ref="tipoHash" className="form-control">
                        <option value="NTLM">NTLM</option>
                        <option value="MD4">MD4</option>
                        <option value="MD5">MD5</option>
                        <option value="SHA1">SHA-1</option>
                        <option value="SHA256">SHA-256</option>
                        <option value="SHA384">SHA-384</option>
                        <option value="SHA512">SHA-512</option>
                        <option value="BCRYPT">BCRYPT</option>
                        <option value="RIPEMD128">RIPEMD-128</option>
                        <option value="RIPEMD160">RIPEMD-160</option>
                        <option value="RIPEMD256">RIPEMD-256</option>
                        <option value="RIPEMD320">RIPEMD-320</option>
                        <option value="BLAKE">BLAKE</option>
                        <option value="BLAKE2b">BLAKE2b (512)</option>
                        <option value="BLAKE2s">BLAKE2s (256)</option>
                        <option value="WHIRLPOOL">WHIRLPOOL</option>
                        <option value="TIGER">Tiger</option>
                        <option value="TIGER2">Tiger2</option>
                        <option value="TIGER3">Tiger3 192</option>
                        <option value="TIGER4">Tiger4 192</option>
                        <option value="MODULAR">Modular ($x$salt$hash)</option>
                        <option value="MYSQL">MySQL (mysql.user)</option>
                        <option value="WORDPRESS">Wordpress (wp_users)</option>
                    </select>
                </div>
                <div className="col">
                    <label>Test timeout:</label>
                    <div className="input-group">
                        <input type="number" className="form-control"
                            ref="timeLapseProcessingCount" defaultValue="15" min="15" max="60" />
                        <div className="input-group-append">
                            <select className="form-control" defaultValue="s" disabled>
                                <option value="s">Seconds</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <label>Interations:</label>
                    <input type="number" className="form-control" ref="iteracions" min="1" defaultValue="1" />
                </div>
                <div className="col">
                    <label>Salt:</label>
                    <input type="text" className="form-control" ref="salt" defaultValue="" />
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <label>Initial character length:</label>
                    <input type="number" className="form-control" ref="initalLength" defaultValue="1" min="1" onChange={this.renderCombinations} />
                </div>
                <div className="col">
                    <label>Final character length:</label>
                    <input type="number" ref="finalLength" className="form-control" defaultValue="8" min="2" onChange={this.renderCombinations} />
                </div>
            </div>

            <SeleccioForcaBruta
                ref={inst => {
                    if (!inst) return;
                    Object.keys(inst.refs).forEach((element) => {
                        this.refs[element] = inst.refs[element];
                    });
                }}
            />

            <div id="connectClientAlert">
                <h4>
                    <img src={clientsICO} alt="clientsICO" />
                    Connect clients and check settings</h4>
                <p>
                    This test will determinate how many time it will take to get to the lastest combination possible of this password,
                    using the current amount of power (hashes/s) of the clients. Make sure thay are configured and connected as they will
                    be during the processing to make this test as accurate as possible.
                </p>
            </div>

        </div>);
    }
}

class HashPerformanceTestResults extends Component {
    constructor({ results }) {
        super();

        this.results = results;

        this.ResultsClient = this.ResultsClient.bind(this);
    }

    componentDidMount() {
        const components = this.results.results.map((element, i) => {
            return (<this.ResultsClient
                key={i}

                client={element}
            />);
        });

        ReactDOM.render(components, this.refs.clientList);
    }

    ResultsClient({ client }) {
        return (<tr>
            <td>{client.uuid}</td>
            <td>{client.address}</td>
            <td>
                {client.threads != -1 &&
                    <span className={"badge badge-pill badge-" + this.getBadgeColor(client.threads)}>{client.threads}</span>}
            </td>
            <td>
                {client.hashCount > 0 &&
                    <p>{Math.floor(client.hashCount / this.results.timeout).toLocaleString('eu')}h/s</p>}
                {client.hashCount == -1 &&
                    <span className="badge badge-pill badge-danger">ERROR</span>}
                {client.hashCount == -2 &&
                    <span className="badge badge-pill badge-danger">TIMED OUT</span>}
            </td>
        </tr>)
    }

    getBadgeColor(threads) {
        if (threads <= 2) {
            return "danger";
        } else if (threads > 2 && threads <= 7) {
            return "warning";
        } else {
            return "success";
        }
    }

    formatTimeUnit(seconds) {
        const units = ['seconds', 'minutes', 'hours', 'days', 'months', 'years'];
        var unit = 0;

        if (seconds >= 60) {
            unit++;
            seconds /= 60;

            if (seconds >= 60) {
                unit++;
                seconds /= 60;

                if (seconds >= 24) {
                    unit++;
                    seconds /= 24;

                    if (seconds >= 30) {
                        unit++;
                        seconds /= 30;

                        if (seconds >= 12) {
                            unit++;
                            seconds /= 12;
                        }
                    }
                }
            }
        }

        return Math.floor(seconds) + ' ' + units[unit];
    }

    getFinalizationDate(rate) {
        const completedDate = new Date((new Date()).getTime() + (rate * 1000));

        return completedDate.getFullYear() + '-' + completedDate.getMonth() + '-' + completedDate.getDate()
            + ' ' + completedDate.getHours() + ':' + completedDate.getMinutes() + ':' + completedDate.getSeconds();
    }

    render() {
        return (<div id="hashPerformanceTestResults">
            <div className="card-group">
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">Combinations</h5>
                        <p className="card-text">{this.results.combinations.toLocaleString('eu')}</p>
                    </div>
                </div>
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">Hashing rate</h5>
                        <p className="card-text">{this.results.rate.toLocaleString('eu')}h/s</p>
                    </div>
                </div>
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">Processed hashes</h5>
                        <p className="card-text">{this.results.hashCount.toLocaleString('eu')}h</p>
                    </div>
                </div>
            </div>
            <div className="card-group">
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">Processing time until finishing</h5>
                        <p className="card-text">{this.results.rate === 0 ?
                            '-'
                            : this.formatTimeUnit((this.results.combinations / this.results.rate))}</p>
                    </div>
                </div>
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">Estimated reach of the lastest combination</h5>
                        <p className="card-text">{this.results.rate === 0 ?
                            '-'
                            : this.getFinalizationDate((this.results.combinations / this.results.rate))}</p>
                    </div>
                </div>
            </div>
            <table className="table table-dark">
                <thead>
                    <tr>
                        <th scope="col">UUID</th>
                        <th scope="col">Address</th>
                        <th scope="col">Threads</th>
                        <th scope="col">Hashes</th>
                    </tr>
                </thead>
                <tbody ref="clientList">

                </tbody>
            </table>
            {this.results.results.length === 0 && <div id="emtpyClientsError">
                <img src={errorICO} alt="errorICO" />
                <h5>There are no clients currently connected to test this hash.</h5>
                <small>Install and connect some processing clients to this server and try testing again.</small>
            </div>}
        </div>)
    }
}

export default HashPerformanceTest;
