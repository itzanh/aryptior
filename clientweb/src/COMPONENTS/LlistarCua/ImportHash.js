import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import trashICO from './../../IMG/trash.svg';


const tipusEnum = ['NTLM', 'MD4', 'MD5', 'SHA1', 'SHA-256', 'SHA-384', 'SHA-512', 'BCRYPT', 'RIPEMD128',
    'RIPEMD160', 'RIPEMD256', 'RIPEMD320', 'BLAKE', 'BLAKE2b (512)', 'BLAKE2s (256)', 'WHIRLPOOL', 'TIGER',
    'TIGER2', 'TIGER3', 'TIGER4', 'MODULAR', 'MYSQL', 'WORDPRESS'];

class ImportHash extends Component {

    constructor({ getHashQueue, getFinishedHashes, validateHash, sendHash }) {
        super();

        this.importMethod = 0;

        this.getHashQueue = getHashQueue;
        this.getFinishedHashes = getFinishedHashes;

        this.sendHash = sendHash;
        this.validateHash = validateHash;

        this.handleNext = this.handleNext.bind(this);
        this.sendToQueue = this.sendToQueue.bind(this);
        this.setImportMethod = this.setImportMethod.bind(this);
        this.renderMainScreen = this.renderMainScreen.bind(this);
        this.importHashQueueFromJSON = this.importHashQueueFromJSON.bind(this);
    }

    /****************/
    /* MODAL WINDOW */
    /****************/

    componentDidMount() {
        window.$('#importQueueModal').modal({ show: true });

        this.renderMainScreen();
    }

    removeDuplicates(hashList) {
        return new Promise(async (resolve) => {
            const hashQueue = (await this.getHashQueue()).response;
            const finishedHashes = (await this.getFinishedHashes()).response;

            // search duplicates in the hash queue first
            for (let i = (hashList.length - 1); i >= 0; i--) {
                for (let j = 0; j < hashQueue.length; j++) {
                    if (hashList[i].hash === hashQueue[j].hash) {
                        hashList.splice(i, 1);
                        break;
                    }
                }
            }

            // search duplicates in the finished hashes
            for (let i = (hashList.length - 1); i >= 0; i--) {
                for (let j = 0; j < finishedHashes.length; j++) {
                    if (hashList[i].hash === finishedHashes[j].hash) {
                        hashList.splice(i, 1);
                        break;
                    }
                }
            }
            resolve();
        });
    }

    async sendToQueue(hashList) {
        ReactDOM.unmountComponentAtNode(document.getElementById("renderModalDisplay"));
        ReactDOM.render(
            <div>
                <p id="importHashImported">Importing {hashList.length} hashes...</p>
            </div>, document.getElementById("renderModalDisplay"));
        ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));

        await this.sendHash(hashList);

        window.$('#importQueueModal').modal('hide');
    }

    llegirFitxerComAText(fitxer) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsText(fitxer);
            reader.onload = () => {
                resolve(reader.result);
            }
            reader.onerror = () => {
                reject();
            }
        })
    }


    /***************/
    /* MAIN SCREEN */
    /***************/

    renderMainScreen() {
        ReactDOM.unmountComponentAtNode(document.getElementById("renderModalDisplay"));
        ReactDOM.render(<TaskSelect
            setImportMethod={this.setImportMethod}
        />, document.getElementById("renderModalDisplay"));

        ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
        ReactDOM.render(<div className="btn-group">
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" className="btn btn-primary" onClick={this.handleNext}>Next</button>
        </div>, document.getElementById("modal-footer"));
    }

    setImportMethod(importMethod) {
        this.importMethod = importMethod;
    }

    async handleNext() {
        var textToParse;
        // get the file
        if (this.importMethod === 0) {
            const fitxers = document.getElementById("importQueueFile").files;
            if (fitxers.length === 0) {
                alert("You must select a file before continuing!");
                return;
            }
            const fitxer = document.getElementById("importQueueFile").files[0];
            textToParse = await this.llegirFitxerComAText(fitxer);

        } else if (this.importMethod === 1) {
            textToParse = document.getElementById("importQueueText").value;
        }

        if (textToParse === undefined) return;
        // get the selected operation
        const operation = document.getElementById("importOperation").value;
        switch (operation) {
            case "json": {
                this.importHashQueueFromJSON(textToParse);
                break;
            }
            case "sam": {
                this.importHashQueueFromSAM(textToParse);
                break;
            }
            case "shadow": {
                this.importHashQueueFromShadow(textToParse);
                break;
            }
            case "mysql": {
                this.importHashQueueFromMySQL(textToParse);
                break;
            }
            case "wordpress": {
                this.importHashQueueFromWordpress(textToParse);
                break;
            }
        }
    }

    /********************/
    /* IMPORT FROM JSON */
    /********************/

    async importHashQueueFromJSON(jsonToParse) {
        var llistaHashes;
        try {
            llistaHashes = JSON.parse(jsonToParse);
        } catch (e) {
            alert("Error loading JSON file. Check the selected operation!");
            return;
        }
        if (!(llistaHashes instanceof Array)) llistaHashes = [llistaHashes];

        // validate every hash and remove the objects that could not be validated
        for (let i = 0; i < llistaHashes.length; i++) {
            if (!this.validateHash(llistaHashes[i]) || llistaHashes[i].estaResolt) {
                llistaHashes.splice(i, 1);
            }

            delete llistaHashes[i].uuid;
            delete llistaHashes[i].surrender;
            delete llistaHashes[i].estaResolt;
            delete llistaHashes[i].solucio;
            delete llistaHashes[i].dateCreated;
            delete llistaHashes[i].dateFinished;
            delete llistaHashes[i].numLinea;
        }

        // remove the hashes that are already added to the hash queue or on the finished hashes list
        await this.removeDuplicates(llistaHashes);

        // render the list
        ReactDOM.unmountComponentAtNode(document.getElementById("renderModalDisplay"));
        ReactDOM.render(<BulkImport
            hashList={llistaHashes}
        />, document.getElementById("renderModalDisplay"));

        if (llistaHashes.length === 0) {
            ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
            ReactDOM.render(<div className="btn-group">
                <button type="button" className="btn btn-warning" onClick={this.renderMainScreen}>Back</button>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>, document.getElementById("modal-footer"));
            return;
        }

        ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
        ReactDOM.render(<div className="btn-group">
            <button type="button" className="btn btn-warning" onClick={this.renderMainScreen}>Back</button>
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" className="btn btn-primary" onClick={() => { this.sendToQueue(llistaHashes); }}>Save</button>
        </div>, document.getElementById("modal-footer"));
    }

    /**********************/
    /* IMPORT FROM SHADOW */
    /**********************/

    async importHashQueueFromShadow(shadow) {
        ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
        ReactDOM.unmountComponentAtNode(document.getElementById("renderModalDisplay"));
        ReactDOM.render(<ProcessingSettingsScreen
            defaultInitialTestCount={40000}
            defaultMsProcessingCount={15}
            defaultTimeUnitProcessingCount={'m'}
        />, document.getElementById("renderModalDisplay"));
        new Promise((resolve, reject) => {
            ReactDOM.render(<div className="btn-group">
                <button type="button" className="btn btn-warning" onClick={() => { reject(); }}>Back</button>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary" onClick={() => { resolve(); }}>Next</button>
            </div >, document.getElementById("modal-footer"));
        }).then(async () => {
            const hashes = await this.extractHashesFromShadow(shadow);
            const hashDetails = this.readHashParametersDetails();

            const hashesList = hashes.map((element, i) => {
                return {
                    hash: element.hash,
                    descripcio: element.descripcio,
                    tipus: 20,

                    primerCaracterUnicode: hashDetails.primerCaracterUnicode,
                    ultimCaracterUnicode: hashDetails.ultimCaracterUnicode,
                    caracters: hashDetails.caracters,
                    caractersFinals: hashDetails.caractersFinals,
                    iteracions: hashDetails.iteracions,
                    salt: hashDetails.salt,
                    initialTestCount: hashDetails.initialTestCount,
                    msProcessingCount: hashDetails.msProcessingCount
                };
            });

            // remove the hashes that are already added to the hash queue or on the finished hashes list
            await this.removeDuplicates(hashesList);

            // render the list
            ReactDOM.unmountComponentAtNode(document.getElementById("renderModalDisplay"));
            ReactDOM.render(<BulkImport
                hashList={hashesList}
            />, document.getElementById("renderModalDisplay"));

            if (hashesList.length === 0) {
                ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
                ReactDOM.render(<div className="btn-group">
                    <button type="button" className="btn btn-info" onClick={this.renderMainScreen}>Back</button>
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>, document.getElementById("modal-footer"));
                return;
            }

            ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
            ReactDOM.render(<div className="btn-group">
                <button type="button" className="btn btn-info" onClick={() => { this.importHashQueueFromShadow(shadow) }}>Back</button>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary" onClick={() => { this.sendToQueue(hashesList); }}>Save</button>
            </div>, document.getElementById("modal-footer"));

        }, () => {
            this.renderMainScreen();
        });
    };

    extractHashesFromShadow(shadow) {
        return new Promise((resolve) => {
            var hashes = [];
            shadow.split('\n').forEach((element, i) => {
                const line = element.split(':');
                if (line.length === 9 && line[1] !== '' && line[1] !== ' ' && line[1] !== '*' && line[1] !== '!' && line[1] !== '!!') {
                    hashes.push({
                        hash: line[1],
                        descripcio: element
                    });
                }
            });
            resolve(hashes);
        });
    };

    /*******************/
    /* IMPORT FROM SAM */
    /*******************/

    importHashQueueFromSAM(sam) {
        ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
        ReactDOM.unmountComponentAtNode(document.getElementById("renderModalDisplay"));
        ReactDOM.render(<ProcessingSettingsScreen
            defaultInitialTestCount={100000}
            defaultMsProcessingCount={5}
            defaultTimeUnitProcessingCount={'m'}
        />, document.getElementById("renderModalDisplay"));
        new Promise((resolve, reject) => {
            ReactDOM.render(<div className="btn-group">
                <button type="button" className="btn btn-warning" onClick={() => { reject(); }}>Back</button>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary" onClick={() => { resolve(); }}>Next</button>
            </div >, document.getElementById("modal-footer"));
        }).then(async () => {
            const hashes = await this.extractHashesFromSAM(sam);
            const hashDetails = this.readHashParametersDetails();

            const hashesList = hashes.map((element, i) => {
                return {
                    hash: element.hash,
                    descripcio: element.descripcio,
                    tipus: 0,

                    primerCaracterUnicode: hashDetails.primerCaracterUnicode,
                    ultimCaracterUnicode: hashDetails.ultimCaracterUnicode,
                    caracters: hashDetails.caracters,
                    caractersFinals: hashDetails.caractersFinals,
                    iteracions: hashDetails.iteracions,
                    salt: hashDetails.salt,
                    initialTestCount: hashDetails.initialTestCount,
                    msProcessingCount: hashDetails.msProcessingCount
                };
            });

            // remove the hashes that are already added to the hash queue or on the finished hashes list
            await this.removeDuplicates(hashesList);

            // render the list
            ReactDOM.unmountComponentAtNode(document.getElementById("renderModalDisplay"));
            ReactDOM.render(<BulkImport
                hashList={hashesList}
            />, document.getElementById("renderModalDisplay"));

            if (hashesList.length === 0) {
                ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
                ReactDOM.render(<div className="btn-group">
                    <button type="button" className="btn btn-warning" onClick={this.renderMainScreen}>Back</button>
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>, document.getElementById("modal-footer"));
                return;
            }

            ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
            ReactDOM.render(<div>
                <button type="button" className="btn btn-warning" onClick={() => { this.importHashQueueFromSAM(sam); }}>Back</button>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary" onClick={() => { this.sendToQueue(hashesList); }}>Save</button>
            </div>, document.getElementById("modal-footer"));

        }, () => {
            this.renderMainScreen();
        });
    };

    extractHashesFromSAM(sam) {
        return new Promise((resolve) => {
            var hashes = [];
            sam.split('\n').forEach((element, i) => {
                const line = element.split(':');
                if (line.length === 7 && line[3] !== '' && line[3] !== ' ') {
                    hashes.push({
                        hash: line[3],
                        descripcio: element
                    });
                }
            });
            resolve(hashes);
        });
    }

    /*********************/
    /* IMPORT FROM MYSQL */
    /*********************/

    importHashQueueFromMySQL(jsonToParse) {
        ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
        ReactDOM.unmountComponentAtNode(document.getElementById("renderModalDisplay"));
        ReactDOM.render(<ProcessingSettingsScreen
            defaultInitialTestCount={100000}
            defaultMsProcessingCount={5}
            defaultTimeUnitProcessingCount={'m'}
        />, document.getElementById("renderModalDisplay"));
        new Promise((resolve, reject) => {
            ReactDOM.render(<div className="btn-group">
                <button type="button" className="btn btn-warning" onClick={() => { reject(); }}>Back</button>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary" onClick={() => { resolve(); }}>Next</button>
            </div >, document.getElementById("modal-footer"));
        }).then(async () => {
            const hashes = await this.extractHashesFromMySQL(jsonToParse);
            const hashDetails = this.readHashParametersDetails();

            const hashesList = hashes.map((element, i) => {
                return {
                    hash: element.hash,
                    descripcio: element.descripcio,
                    tipus: 21,

                    primerCaracterUnicode: hashDetails.primerCaracterUnicode,
                    ultimCaracterUnicode: hashDetails.ultimCaracterUnicode,
                    caracters: hashDetails.caracters,
                    caractersFinals: hashDetails.caractersFinals,
                    iteracions: hashDetails.iteracions,
                    salt: hashDetails.salt,
                    initialTestCount: hashDetails.initialTestCount,
                    msProcessingCount: hashDetails.msProcessingCount
                };
            });

            // remove the hashes that are already added to the hash queue or on the finished hashes list
            await this.removeDuplicates(hashesList);

            // render the list
            ReactDOM.unmountComponentAtNode(document.getElementById("renderModalDisplay"));
            ReactDOM.render(<BulkImport
                hashList={hashesList}
            />, document.getElementById("renderModalDisplay"));

            if (hashesList.length === 0) {
                ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
                ReactDOM.render(<div className="btn-group">
                    <button type="button" className="btn btn-warning" onClick={this.renderMainScreen}>Back</button>
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>, document.getElementById("modal-footer"));
                return;
            }

            ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
            ReactDOM.render(<div>
                <button type="button" className="btn btn-warning" onClick={() => { this.importHashQueueFromMySQL(jsonToParse); }}>Back</button>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary" onClick={() => { this.sendToQueue(hashesList); }}>Save</button>
            </div>, document.getElementById("modal-footer"));

        }, () => {
            this.renderMainScreen();
        });

    }

    extractHashesFromMySQL(jsonToParse) {
        return new Promise((resolve) => {
            const users = JSON.parse(jsonToParse);
            var hashes = [];
            users.forEach((element, i) => {
                if (typeof element.Password === 'string'
                    && element.Password.length === 41
                    && element.Password[0] === '*') {
                    hashes.push({
                        hash: element.Password,
                        descripcio: JSON.stringify(element)
                    });
                }
            });
            resolve(hashes);
        });
    }

    /*************************/
    /* IMPORT FROM WORDPRESS */
    /*************************/

    importHashQueueFromWordpress(jsonToParse) {
        ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
        ReactDOM.unmountComponentAtNode(document.getElementById("renderModalDisplay"));
        ReactDOM.render(<ProcessingSettingsScreen
            defaultInitialTestCount={25000}
            defaultMsProcessingCount={2}
            defaultTimeUnitProcessingCount={'m'}
        />, document.getElementById("renderModalDisplay"));
        new Promise((resolve, reject) => {
            ReactDOM.render(<div className="btn-group">
                <button type="button" className="btn btn-warning" onClick={() => { reject(); }}>Back</button>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary" onClick={() => { resolve(); }}>Next</button>
            </div >, document.getElementById("modal-footer"));
        }).then(async () => {
            const hashes = await this.extractHashesFromWordpress(jsonToParse);
            const hashDetails = this.readHashParametersDetails();

            const hashesList = hashes.map((element, i) => {
                return {
                    hash: element.hash,
                    descripcio: element.descripcio,
                    tipus: 22,

                    primerCaracterUnicode: hashDetails.primerCaracterUnicode,
                    ultimCaracterUnicode: hashDetails.ultimCaracterUnicode,
                    caracters: hashDetails.caracters,
                    caractersFinals: hashDetails.caractersFinals,
                    iteracions: hashDetails.iteracions,
                    salt: hashDetails.salt,
                    initialTestCount: hashDetails.initialTestCount,
                    msProcessingCount: hashDetails.msProcessingCount
                };
            });

            // remove the hashes that are already added to the hash queue or on the finished hashes list
            await this.removeDuplicates(hashesList);

            // render the list
            ReactDOM.unmountComponentAtNode(document.getElementById("renderModalDisplay"));
            ReactDOM.render(<BulkImport
                hashList={hashesList}
            />, document.getElementById("renderModalDisplay"));

            if (hashesList.length === 0) {
                ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
                ReactDOM.render(<div className="btn-group">
                    <button type="button" className="btn btn-warning" onClick={this.renderMainScreen}>Back</button>
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>, document.getElementById("modal-footer"));
                return;
            }

            ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
            ReactDOM.render(<div>
                <button type="button" className="btn btn-warning" onClick={() => { this.importHashQueueFromWordpress(jsonToParse); }}>Back</button>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary" onClick={() => { this.sendToQueue(hashesList); }}>Save</button>
            </div>, document.getElementById("modal-footer"));

        }, () => {
            this.renderMainScreen();
        });
    }

    extractHashesFromWordpress(jsonToParse) {
        return new Promise((resolve) => {
            const users = JSON.parse(jsonToParse);
            var hashes = [];
            users.forEach((element, i) => {
                if (typeof element.user_pass === 'string'
                    && element.user_pass.length === 34
                    && (element.user_pass.substring(0, 3) === '$P$'
                        || element.user_pass.substring(0, 3) === '$H$')) {
                    hashes.push({
                        hash: element.user_pass,
                        descripcio: JSON.stringify(element)
                    });
                }
            });
            resolve(hashes);
        });
    }

    /**************************/
    /* HASH PARAMETERS SCREEN */
    /**************************/

    readHashParametersDetails() {
        const firstUnicodeCharacter = parseInt(document.getElementById('firstUnicodeCharacter').value);
        const lastUnicodeCharacter = parseInt(document.getElementById('lastUnicodeCharacter').value);
        const initialCharacters = parseInt(document.getElementById('initialCharacters').value);
        const caractersFinals = parseInt(document.getElementById('finalCharacters').value);
        const iteracions = parseInt(document.getElementById('rainbowIterations').value);
        const initialTestCount = parseInt(document.getElementById('initialTestCount').value);
        var msProcessingCount = parseInt(document.getElementById('msProcessingCount').value);
        const timeUnitProcessingCount = document.getElementById('timeUnitProcessingCount').value;
        const salt = document.getElementById('rainbowSalt').value;

        var characters = [];

        for (let i = 0; i < initialCharacters; i++) {
            characters.push(firstUnicodeCharacter);
        }

        msProcessingCount *= 1000;

        switch (timeUnitProcessingCount) {
            case 'm': {
                msProcessingCount *= 60;
                break;
            }
            case 'h': {
                msProcessingCount *= 3600;
                break;
            }
        }

        return {
            primerCaracterUnicode: firstUnicodeCharacter,
            ultimCaracterUnicode: lastUnicodeCharacter,
            caracters: characters,
            caractersFinals,
            iteracions,
            salt,
            initialTestCount,
            msProcessingCount
        };
    };

    render() {
        return (
            <div id="renderQueueModal">
                <div className="modal fade" tabIndex="-1" role="dialog" id="importQueueModal" aria-labelledby="importQueueModal" aria-hidden="false">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Import hashes to the queue</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body" id="renderModalDisplay"></div>
                            <div className="modal-footer" id="modal-footer"></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class TaskSelect extends Component {
    constructor({ setImportMethod }) {
        super();

        this.setImportMethod = setImportMethod;

        this.renderTabsFile = this.renderTabsFile.bind(this);
        this.renderTabsText = this.renderTabsText.bind(this);
        this.renderFile = this.renderFile.bind(this);
        this.renderTextInput = this.renderTextInput.bind(this);
    }

    componentDidMount() {
        this.renderFile();
    }

    renderTabsFile() {
        ReactDOM.unmountComponentAtNode(document.getElementById("renderModalTabs"));
        ReactDOM.render(<ul className="nav nav-tabs">
            <li className="nav-item">
                <a className="nav-link active" href="#" onClick={this.renderFile}>File</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="#" onClick={this.renderTextInput}>Text input</a>
            </li>
        </ul>, document.getElementById("renderModalTabs"));
    }

    renderTabsText() {
        ReactDOM.unmountComponentAtNode(document.getElementById("renderModalTabs"));
        ReactDOM.render(<ul className="nav nav-tabs">
            <li className="nav-item">
                <a className="nav-link" href="#" onClick={this.renderFile}>File</a>
            </li>
            <li className="nav-item">
                <a className="nav-link active" href="#" onClick={this.renderTextInput}>Text input</a>
            </li>
        </ul>, document.getElementById("renderModalTabs"));
    }

    renderFile() {
        this.renderTabsFile();
        this.setImportMethod(0);
        ReactDOM.unmountComponentAtNode(document.getElementById("importHashInput"));
        ReactDOM.render(<div>
            <label htmlFor="importQueueFile">Select a file</label>
            <input type="file" id="importQueueFile" className="form-control" />
        </div>, document.getElementById("importHashInput"));
    }

    renderTextInput() {
        this.renderTabsText();
        this.setImportMethod(1);
        ReactDOM.unmountComponentAtNode(document.getElementById("importHashInput"));
        ReactDOM.render(<div>
            <label htmlFor="importQueueText">Paste code below</label>
            <textarea id="importQueueText" className="form-control" rows="8" />
        </div>, document.getElementById("importHashInput"));
    }

    render() {
        return (
            <div>
                <div className="modal-body" id="renderModalTabs"></div>
                <div id="importHashInput"></div>
                <label htmlFor="importOperation">Enter file type</label>
                <select id="importOperation" className="form-control">
                    <option value="json">JSON export file</option>
                    <option value="sam">Windows SAM (NTLM)</option>
                    <option value="shadow">Linux SHADOW (MODULAR)</option>
                    <option value="mysql">MySQL mysql.user (JSON Export)</option>
                    <option value="wordpress">Wordpress wp_users (JSON Export)</option>
                </select>
            </div>
        )
    }
}

class BulkImport extends Component {

    constructor({ hashList }) {
        super();

        this.hashList = hashList;

        this.onRemoveHash = this.onRemoveHash.bind(this);
    }

    componentDidMount() {
        this.renderHashList();
    }

    renderHashList() {
        const componentsToRender = [];

        this.hashList.forEach((element, i) => {
            componentsToRender.push(<BulkImportHash
                key={i}

                objecte={element}
                handleRemove={this.onRemoveHash}
            />);
        });

        if (this.hashList.length !== 0) ReactDOM.render(componentsToRender, document.getElementById("bulkHashImportRender"));
    }

    onRemoveHash(hash) {
        for (let i = 0; i < this.hashList.length; i++) {
            if (this.hashList[i] === hash) {
                this.hashList.splice(i, 1);
                break;
            }
        }

        ReactDOM.unmountComponentAtNode(document.getElementById("bulkHashImportRender"));
        this.renderHashList();

    };

    render() {
        return (
            <div>
                <p>About to add {this.hashList.length} hashes to the queue.</p>
                {
                    this.hashList.length !== 0
                        ? <table className="HashCua">
                            <thead>
                                <tr>
                                    <th>Hash</th>
                                    <th>Type</th>
                                    <th>Range</th>
                                    <th>Test size</th>
                                    <th>Reconn. time</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody id="bulkHashImportRender"></tbody>
                        </table>
                        : <p>There are no unfinished hashes to import from this file.</p>
                }
            </div>
        )
    }
}

class BulkImportHash extends Component {

    constructor({ objecte, handleRemove }) {
        super();

        this.handleRemove = handleRemove;

        this.hash = objecte;
        this.tipus = tipusEnum[objecte.tipus];
        this.primerCaracterUnicode = this.numCaracterAString(objecte.primerCaracterUnicode);
        this.ultimCaracterUnicode = this.numCaracterAString(objecte.ultimCaracterUnicode);
        this.msProcessingCount = this.formatejarTemps(objecte.msProcessingCount);

        this.onRemove = this.onRemove.bind(this);
    }

    numCaracterAString(num) {
        return String.fromCharCode(num);
    }

    formatejarTemps(ms) {
        const unitats = ['ms', 's', 'min', 'hrs'];
        var unitat = 0;
        if (ms > 1000) {
            unitat++;
            ms /= 1000;
        }
        while (ms > 60 && unitat < unitats.length - 1) {
            ms /= 60;
            unitat++;
        }
        return Math.floor(ms) + " " + unitats[unitat];
    }

    onRemove() {
        this.handleRemove(this.hash);
    };

    render() {
        return (
            <tr>
                <td>
                    {this.hash.hash}
                </td>
                <td>
                    <span className="badge badge-pill badge-danger">
                        {this.tipus}
                    </span>
                </td>
                <td>
                    {this.primerCaracterUnicode + " - " + this.ultimCaracterUnicode}
                </td>
                <td>
                    {this.hash.initialTestCount}
                </td>
                <td>
                    {this.msProcessingCount}
                </td>
                <td>
                    <img src={trashICO} alt="trashICO" onClick={this.onRemove} />
                </td>
            </tr>
        )
    }
}

class ProcessingSettingsScreen extends Component {

    constructor({ defaultInitialTestCount, defaultMsProcessingCount, defaultTimeUnitProcessingCount }) {
        super();

        this.defaultInitialTestCount = defaultInitialTestCount;
        this.defaultMsProcessingCount = defaultMsProcessingCount;
        this.defaultTimeUnitProcessingCount = defaultTimeUnitProcessingCount;

        this.previewLastCharacter = this.previewLastCharacter.bind(this);
        this.ajustarRangCaracters = this.ajustarRangCaracters.bind(this);
        this.previewFirstCharacter = this.previewFirstCharacter.bind(this);
        this.previsualitzarCaracters = this.previsualitzarCaracters.bind(this);
    }

    componentDidMount() {
        this.previewFirstCharacter();
        this.previewLastCharacter();
    }

    /**
 * Previsualitza el codi del caracter de inici i refresca el rango
 * */
    previewFirstCharacter() {
        var firstCharacter = document.getElementById('firstUnicodeCharacter').value;
        firstCharacter = String.fromCharCode(firstCharacter);
        document.getElementById('firstCharacterPreview').innerText = firstCharacter;

        this.previsualitzarCaracters();
    }

    /**
     * Previsualitza el codi del ultim caracter i refresca el rango
     * */
    previewLastCharacter() {
        var lastCharacter = document.getElementById('lastUnicodeCharacter').value;
        lastCharacter = String.fromCharCode(lastCharacter);
        document.getElementById('lastCharacterPreview').innerText = lastCharacter;

        this.previsualitzarCaracters();
    }

    /**
     * Mostra per pantalla tots els caracters a ser carregats
     * */
    previsualitzarCaracters() {
        var primerCaracter = document.getElementById('firstUnicodeCharacter').value;
        var ultimCaracter = document.getElementById('lastUnicodeCharacter').value;
        if (!primerCaracter || !ultimCaracter) {
            return;
        } else {
            primerCaracter = parseInt(primerCaracter);
            ultimCaracter = parseInt(ultimCaracter);
        }
        if (ultimCaracter < primerCaracter) {
            const pegarLaVolta = ultimCaracter;
            ultimCaracter = primerCaracter;
            primerCaracter = pegarLaVolta;
        }
        var caracters = '';
        for (let i = primerCaracter; i <= ultimCaracter; i++) {
            caracters += String.fromCharCode(i);
        }
        document.getElementById('caractersPreview').innerText = caracters;
    }

    ajustarRangCaracters() {
        const preajust = document.getElementById('tipoBusqueda').value;
        var primerCaracter = 0;
        var ultimCaracter = 0;
        switch (preajust) {
            case 'majuscules': {
                primerCaracter = 65;
                ultimCaracter = 90;
                break;
            }
            case 'minuscules': {
                primerCaracter = 97;
                ultimCaracter = 122;
                break;
            }
            case 'alfabetic': {
                primerCaracter = 65;
                ultimCaracter = 122;
                break;
            }
            case 'alfanumeric': {
                primerCaracter = 48;
                ultimCaracter = 122;
                break;
            }
            case 'signes': {
                primerCaracter = 32;
                ultimCaracter = 126;
                break;
            }
            case 'personalitzar': {

                break;
            }
            default: {

            }
        }
        document.getElementById('firstUnicodeCharacter').value = primerCaracter;
        this.previewFirstCharacter();
        document.getElementById('lastUnicodeCharacter').value = ultimCaracter;
        this.previewLastCharacter();
    }

    render() {
        return (<div>
            <div className="form-row">
                <div className="col">
                    <label htmlFor="tipoBusqueda">Character set:</label>
                    <select name="tipoBusqueda" id="tipoBusqueda" className="form-control" onChange={this.ajustarRangCaracters} defaultValue="personalitzar">
                        <option value="majuscules">Uppercase</option>
                        <option value="minuscules">Lowercase</option>
                        <option value="alfabetic">Alphabetic + symbols</option>
                        <option value="alfanumeric">Alphanumeric + symbols</option>
                        <option value="signes">All characters</option>
                        <option value="personalitzar">Custom</option>
                    </select>
                </div>
                <div className="col">
                    <label htmlFor="firstUnicodeCharacter">First character:</label>
                    <input type="number" id="firstUnicodeCharacter" name="firstUnicodeCharacter" className="form-control"
                        defaultValue="48" min="32" onChange={this.previewFirstCharacter} />
                    <p id="firstCharacterPreview" className="previewCaracter"></p>
                </div>
                <div className="col">
                    <label htmlFor="lastUnicodeCharacter">Last character:</label>
                    <input type="number" id="lastUnicodeCharacter" name="lastUnicodeCharacter" className="form-control"
                        defaultValue="122" min="32" onChange={this.previewLastCharacter} />
                    <p id="lastCharacterPreview" className="previewCaracter"></p>
                </div>
            </div>
            <p id="caractersPreview" className="previewCaracters"></p>
            <div className="form-row">
                <div className="col">
                    <label htmlFor="rainbowIterations">Iterations:</label>
                    <input type="number" id="rainbowIterations" name="rainbowIterations" className="form-control" defaultValue="1" />
                </div>
                <div className="col">
                    <label htmlFor="rainbowSalt">Salt: </label>
                    <input type="text" id="rainbowSalt" name="rainbowSalt" className="form-control" />
                </div>
            </div>
            <div className="form-row">
                <div className="col">
                    <label htmlFor="initialCharacters">Initial character length:</label>
                    <input type="number" id="initialCharacters" name="initialCharacters" defaultValue="1" className="form-control" />
                </div>
                <div className="col">
                    <label htmlFor="finalCharacters">Final character length:</label>
                    <input type="number" id="finalCharacters" name="finalCharacters" defaultValue="12" className="form-control" />
                </div>
            </div>
            <div className="form-row">
                <div className="col">
                    <label htmlFor="initialTestCount">Number of combinations for testing:</label>
                    <input type="number" id="initialTestCount" name="initialTestCount"
                        defaultValue={this.defaultInitialTestCount ? this.defaultInitialTestCount : 75000} className="form-control" />
                </div>
                <div className="col">
                    <label htmlFor="msProcessingCount">Estimated time to process the part:</label>
                    <div className="input-group">
                        <input type="number" id="msProcessingCount" name="msProcessingCount"
                            defaultValue={this.defaultMsProcessingCount ? this.defaultMsProcessingCount : 60} className="form-control" />
                        <div className="input-group-append">
                            <select id="timeUnitProcessingCount" className="form-control"
                                defaultValue={this.defaultTimeUnitProcessingCount ? this.defaultTimeUnitProcessingCount : 's'}>
                                <option value="s">Segons</option>
                                <option value="m">Minuts</option>
                                <option value="h">Hores</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>)
    }
};

export default ImportHash;
