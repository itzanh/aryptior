import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// CSS
import './../../CSS/HashQueue.css';

// COMPONENTS
import { FormulariCua } from './FormulariCua';
import HashPerformanceTest from './HashPerformanceTest';



class HashesCua extends Component {
    constructor({ seleccionarFitxerJSON, validateHash, enviarHash, handleTest, getDictionaries, modalAlert }) {
        super();

        this.seleccionarFitxerJSON = seleccionarFitxerJSON;
        this.validateHash = validateHash;
        this.handleTest = handleTest;
        this.enviarHash = enviarHash;
        this.getDictionaries = getDictionaries;
        this.modalAlert = modalAlert;

        this.onSendHash = this.onSendHash.bind(this);
        this.openTestModal = this.openTestModal.bind(this);
        this.FormHashesButtons = this.FormHashesButtons.bind(this);

        this._child = React.createRef();
    }

    componentDidMount() {
        ReactDOM.render(<this.FormHashesButtons />,
            document.getElementById("formHashesButtons"));
    }

    onSendHash(objecte) {
        document.getElementById("btnSendHash").disabled = true;
        this.enviarHash(objecte).then(() => {
            ReactDOM.unmountComponentAtNode(document.getElementById('formHashesCua'));
            ReactDOM.render(
                <FormulariCua
                    validateHash={this.validateHash}
                    handleEnviar={this.onSendHash}
                    getDictionaries={this.getDictionaries}
                    modalAlert={this.modalAlert}
                    handleTest={this.handleTest}
                />
                , document.getElementById('formHashesCua'));
            document.getElementById("btnSendHash").disabled = false;
            ReactDOM.unmountComponentAtNode(document.getElementById("formHashesButtons"));
            ReactDOM.render(<this.FormHashesButtons />,
                document.getElementById("formHashesButtons"));
        }, () => {
            this.modalAlert('Error inserting hash.');
            document.getElementById("btnSendHash").disabled = false;
        });
    }

    openTestModal() {
        ReactDOM.unmountComponentAtNode(document.getElementById("renderModal"));
        ReactDOM.render(<HashPerformanceTest
            handleTest={this.handleTest}
        />, document.getElementById("renderModal"));
    }

    FormHashesButtons() {
        return (<div>
            <button className="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseFormHash"
                aria-expanded="false" aria-controls="collapseFormHash" onClick={(e) => {
                    if (e.target.innerText === 'Add a new hash') {
                        e.target.innerText = 'Hide';
                        document.getElementById("btnSendHash").style.visibility = 'visible';
                        document.getElementById("btnOpenTestModal").style.visibility = 'visible';
                    } else if (e.target.innerText === 'Hide') {
                        e.target.innerText = 'Add a new hash';
                        document.getElementById("btnSendHash").style.visibility = 'hidden';
                        document.getElementById("btnOpenTestModal").style.visibility = 'hidden';
                    }
                }}>
                Add a new hash
            </button>
            <button type="button" className="btn btn-outline-info" id="btnSendHash" onClick={() => {
                if (this._child !== null && this._child.current != null) {
                    this._child.current.enviarHash();
                }
            }} style={{ 'visibility': 'hidden' }}>
                Send
            </button>
            <button type="button" className="btn btn-outline-light" id="btnOpenTestModal" style={{ 'visibility': 'hidden' }}
                onClick={this.openTestModal}>
                Launch test
            </button>
        </div>)
    }

    render() {
        return (
            <div id="contenidorHashesCua">
                <div id="importOrExport">
                    <div id="export"></div>
                    <button type="button" className="btn btn-outline-info" onClick={this.seleccionarFitxerJSON}>Import</button>
                </div>
                <div id="renderModal"></div>
                <div className="llistaHashesCua">
                    <table className="HashCua table table-dark">
                        <thead>
                            <tr>
                                <th>Hash</th>
                                <th>Type</th>
                                <th>Range</th>
                                <th>Test size</th>
                                <th>Reconn. time</th>
                                <th>Combination</th>
                                <th>Details</th>
                            </tr>
                        </thead>
                        <tbody id="llistaHashesCua">

                        </tbody>
                    </table>
                </div>
                <div>
                    <div id="formHashesButtons">

                    </div>
                    <div id="formHashesCua">
                        <FormulariCua
                            validateHash={this.validateHash}
                            handleEnviar={this.onSendHash}
                            getDictionaries={this.getDictionaries}
                            modalAlert={this.modalAlert}
                            handleTest={this.handleTest}


                            ref={this._child}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default HashesCua;
