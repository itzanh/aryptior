// REACT
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import SeleccionarDiccionari from './SeleccionarDiccionari';
import HashPerformanceTest from './HashPerformanceTest';

class FormulariCua extends Component {
    constructor({ validateHash, handleEnviar, handleTest, getDictionaries, modalAlert }) {
        super();

        this.handleEnviar = handleEnviar;
        this.handleTest = handleTest;
        this.validateHash = validateHash;

        this.getDictionaries = getDictionaries;
        this.dictionary = null;

        this.modalAlert = modalAlert;

        this.enviarHash = this.enviarHash.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.ajustarTipusHash = this.ajustarTipusHash.bind(this);
        this.seleccionarDiccionari = this.seleccionarDiccionari.bind(this);
        this.obrirDialegDiccionari = this.obrirDialegDiccionari.bind(this);
    }

    componentDidMount() {
        this.renderitzarSeleccioForcaBruta();

    }

    enviarHash() {
        // recollir dades
        const tipus = this.refs.tipoHash.value;
        const hash = this.refs.hash.value;

        var primerCaracterUnicode;
        var ultimCaracterUnicode;
        const tipusBusqueda = this.refs.metodeBusqueda.value;
        if (tipusBusqueda === 'Diccionari') {
            primerCaracterUnicode = 0;
            ultimCaracterUnicode = 0;
        } else {
            primerCaracterUnicode = parseInt(this.refs.primerCaracter.value);
            ultimCaracterUnicode = parseInt(this.refs.ultimCaracter.value);
        }

        const iteracions = parseInt(this.refs.iteracions.value);
        const caractersInicials = parseInt(this.refs.caractersInicials.value);
        const caractersFinals = parseInt(this.refs.caractersFinals.value);
        const descripcio = this.refs.descripcio.value;
        const salt = this.refs.salt.value;

        const initialTestCount = this.refs.initialTestCount.value;
        const msProcessingCount = this.recollirTempsEnMs();

        var caracters = [];
        for (let i = 0; i < caractersInicials; i++) {
            caracters[i] = primerCaracterUnicode;
        }

        if (tipusBusqueda === 'Diccionari' && this.dictionary === null) {
            this.modalAlert("You must select a dictionary from the list first or upload one on the 'Dictionaries' tab.");
            return;
        } else if (tipusBusqueda !== 'Diccionari') {
            this.dictionary = null;
        }

        // enviar
        const newHash = ({
            tipus,
            hash,
            tipusBusqueda,
            diccionari: this.dictionary,
            primerCaracterUnicode,
            ultimCaracterUnicode,
            caracters,
            iteracions,
            salt,
            caractersFinals,
            descripcio,
            initialTestCount,
            msProcessingCount
        });

        if (!this.validateHash(newHash)) {
            this.modalAlert("Invalid data!");
            return;
        }
        this.handleEnviar(newHash);
    }

    /**
     * Intenta ajustar el tipus de hash segons el hash de entrada
     * */
    ajustarTipusHash() {
        const hash = this.refs.hash.value;
        const hashRegexp = /^[0-9A-F]{32,}$/ig;
        if (!hashRegexp.test(hash)) {
            return;
        }
        if (hash.length === 64) {
            this.refs.tipoHash.value = 'sha256';
        } else if (hash.length === 128) {
            this.refs.tipoHash.value = 'sha512';
        }
    }

    recollirTempsEnMs() {
        var timeLapse = parseInt(this.refs.timeLapseProcessingCount.value);
        const timeUnit = this.refs.timeUnitProcessingCount.selectedIndex;
        for (let i = 0; i < timeUnit; i++) {
            timeLapse *= 60;
        }

        return timeLapse * 1000;
    }

    seleccionarDiccionari() {
        const tipusBusqueda = this.refs.metodeBusqueda.value;
        ReactDOM.unmountComponentAtNode(this.refs.renderDictionaryModal);
        ReactDOM.unmountComponentAtNode(this.refs.renderDictionarySel);
        if (tipusBusqueda === 'Diccionari') {
            this.obrirDialegDiccionari();
            ReactDOM.unmountComponentAtNode(this.refs.renderBruteForce);
        } else {
            this.renderitzarSeleccioForcaBruta();
        }
    }

    obrirDialegDiccionari() {
        ReactDOM.unmountComponentAtNode(document.getElementById("renderModal"));
        ReactDOM.unmountComponentAtNode(this.refs.renderDictionaryModal);
        ReactDOM.render(<div>
            <SeleccionarDiccionari
                getDictionaries={this.getDictionaries}
                handleSelect={this.handleSelect}
            />
        </div>, this.refs.renderDictionaryModal);
        this.handleSelect(null);
    }

    renderitzarSeleccioForcaBruta() {
        ReactDOM.unmountComponentAtNode(this.refs.renderBruteForce);
        ReactDOM.render(<SeleccioForcaBruta
            ref={inst => {
                if (!inst) return;
                Object.keys(inst.refs).forEach((element) => {
                    this.refs[element] = inst.refs[element];
                });
            }}
        />, this.refs.renderBruteForce);

    }

    handleSelect(dictionary) {
        this.dictionary = dictionary;
        ReactDOM.unmountComponentAtNode(this.refs.renderDictionarySel);
        ReactDOM.render(<div>
            <button type="button" className="btn btn-outline-success" onClick={this.obrirDialegDiccionari}>Select dictionary</button>
            <table>
                <thead>
                    <tr>
                        <th>UUID</th>
                        <th>File name</th>
                        <th>Size</th>
                    </tr>
                </thead>
                {dictionary !== null ? (<tbody>
                    <tr>
                        <td>{dictionary.uuid}</td>
                        <td>{dictionary.fileName}</td>
                        <td>{this.formatSize(dictionary.sizeInBytes)}</td>
                    </tr>
                </tbody>)
                    : (<tbody>
                        <tr>
                            <td>SELECT DICTIONARY</td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>)}
            </table>
        </div>, this.refs.renderDictionarySel);
    }

    formatSize(sizeInBytes) {
        const units = ['B', 'Kb', 'Mb', 'Gb', 'Tb'];
        var unit = 0;
        while (sizeInBytes >= 1024) {
            sizeInBytes /= 1024;
            unit++;
        }
        return Math.floor(sizeInBytes) + ' ' + units[unit];
    }

    render() {
        return (
            <div id="formAddNewHash">
                <div id="renderAlertModal"></div>
                <form className="collapse" id="collapseFormHash">
                    <fieldset>
                        <legend>Search strategy:</legend>
                        <label htmlFor="metodeBusqueda">Search method:</label>
                        <select id="metodeBusqueda" className="form-control" ref="metodeBusqueda" onChange={this.seleccionarDiccionari}>
                            <option value="For&ccedil;aBruta">Brute force</option>
                            <option value="Diccionari">Dictionary</option>
                        </select>
                        <div id="renderDictionaryModal" ref="renderDictionaryModal">

                        </div>
                        <div id="renderDictionarySel" ref="renderDictionarySel">

                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Hash:</legend>
                        <label htmlFor="hash">Hash:</label>
                        <div className="input-group">
                            <input type="text" id="hash" ref="hash" className="form-control" onChange={this.ajustarTipusHash} />
                            <div className="input-group-append">
                                <select ref="tipoHash" id="tipoHash" className="form-control">
                                    <option value="NTLM">NTLM</option>
                                    <option value="MD4">MD4</option>
                                    <option value="MD5">MD5</option>
                                    <option value="SHA1">SHA-1</option>
                                    <option value="SHA256">SHA-256</option>
                                    <option value="SHA384">SHA-384</option>
                                    <option value="SHA512">SHA-512</option>
                                    <option value="BCRYPT">BCRYPT</option>
                                    <option value="RIPEMD128">RIPEMD-128</option>
                                    <option value="RIPEMD160">RIPEMD-160</option>
                                    <option value="RIPEMD256">RIPEMD-256</option>
                                    <option value="RIPEMD320">RIPEMD-320</option>
                                    <option value="BLAKE">BLAKE</option>
                                    <option value="BLAKE2b">BLAKE2b (512)</option>
                                    <option value="BLAKE2s">BLAKE2s (256)</option>
                                    <option value="WHIRLPOOL">WHIRLPOOL</option>
                                    <option value="TIGER">Tiger</option>
                                    <option value="TIGER2">Tiger2</option>
                                    <option value="TIGER3">Tiger3 192</option>
                                    <option value="TIGER4">Tiger4 192</option>
                                    <option value="MODULAR">Modular ($x$salt$hash)</option>
                                    <option value="MYSQL">MySQL (mysql.user)</option>
                                    <option value="WORDPRESS">Wordpress (wp_users)</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Character set:</legend>
                        <div id="renderBruteForce" ref="renderBruteForce">

                        </div>
                        <div className="row">
                            <div className="col">
                                <label htmlFor="iteracions">Interations:</label>
                                <input type="number" id="iteracions" className="form-control" ref="iteracions" min="1" defaultValue="1" />
                            </div>
                            <div className="col">
                                <label htmlFor="salt">Salt:</label>
                                <input type="text" id="salt" className="form-control" ref="salt" defaultValue="" />
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Options:</legend>
                        <label htmlFor="descripcio">Description:</label>
                        <br />
                        <textarea rows="2" cols="50" id="descripcio" className="form-control" ref="descripcio" ></textarea>
                        <br />
                        <div className="row">
                            <div className="col">
                                <label htmlFor="caractersInicials">Initial character length:</label>
                                <input type="number" id="caractersInicials" className="form-control" ref="caractersInicials" defaultValue="1" />
                            </div>
                            <div className="col">
                                <label htmlFor="caractersFinals">Final character length:</label>
                                <input type="number" id="caractersFinals" className="form-control" ref="caractersFinals" defaultValue="8" />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <label htmlFor="initialTestCount">Number of combinations for testing:</label>
                                <input type="number" id="initialTestCount" className="form-control" ref="initialTestCount" defaultValue="75000" />
                            </div>
                            <div className="col">
                                <label htmlFor="timeLapseProcessingCount">Estimated time to process the part:</label>
                                <div className="input-group">
                                    <input type="number" id="timeLapseProcessingCount" className="form-control"
                                        ref="timeLapseProcessingCount" defaultValue="5" />
                                    <div className="input-group-append">
                                        <select id="timeUnitProcessingCount" ref="timeUnitProcessingCount" className="form-control" defaultValue="m">
                                            <option value="s">Seconds</option>
                                            <option value="m">Minutes</option>
                                            <option value="h">Hours</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <br />
                </form>
            </div>
        )
    }
}

class SeleccioForcaBruta extends Component {
    constructor({ }) {
        super();


        this.previsualitzarPrimerCaracter = this.previsualitzarPrimerCaracter.bind(this);
        this.previsualitzarUltimCaracter = this.previsualitzarUltimCaracter.bind(this);
        this.previsualitzarCaracters = this.previsualitzarCaracters.bind(this);
        this.ajustarRangCaracters = this.ajustarRangCaracters.bind(this);

    }

    /**
     * Previsualitza el codi del caracter de inici i refresca el rango
     * */
    previsualitzarPrimerCaracter() {
        var primerCaracter = parseInt(this.refs.primerCaracter.value);
        if (primerCaracter >= 32) {
            primerCaracter = String.fromCharCode(primerCaracter);
            this.refs.primerCaracterPreview.innerText = primerCaracter;
        } else {
            this.refs.primerCaracterPreview.innerText = '';
        }

        this.previsualitzarCaracters();
    }

    /**
     * Previsualitza el codi del ultim caracter i refresca el rango
     * */
    previsualitzarUltimCaracter() {
        var ultimCaracter = parseInt(this.refs.ultimCaracter.value);
        if (ultimCaracter >= 32) {
            ultimCaracter = String.fromCharCode(ultimCaracter);
            this.refs.ultimCaracterPreview.innerText = ultimCaracter;
        } else {
            this.refs.ultimCaracterPreview.innerText = '';
        }

        this.previsualitzarCaracters();
    }

    /**
     * Mostra per pantalla tots els caracters a ser carregats
     * */
    previsualitzarCaracters() {
        var primerCaracter = this.refs.primerCaracter.value;
        var ultimCaracter = this.refs.ultimCaracter.value;
        if (!primerCaracter || !ultimCaracter) {
            return;
        } else {
            primerCaracter = parseInt(primerCaracter);
            ultimCaracter = parseInt(ultimCaracter);
        }
        if (primerCaracter < 32 || ultimCaracter < 32 || primerCaracter >= ultimCaracter) {
            this.refs.caractersPreview.innerText = '';
            return;
        }
        if (ultimCaracter < primerCaracter) {
            const pegarLaVolta = ultimCaracter;
            ultimCaracter = primerCaracter;
            primerCaracter = pegarLaVolta;
        }
        var caracters = '';
        for (let i = primerCaracter; i <= ultimCaracter; i++) {
            caracters += String.fromCharCode(i);
        }
        this.refs.caractersPreview.innerText = caracters;

        this.actualitzarRangCaracters();
    }

    /**
     * Modifica el desplegable amb la informacio dels caracters incials i finals una vegada modificats
     * */
    ajustarRangCaracters() {
        const preajust = this.refs.tipoBusqueda.value;
        var primerCaracter = 0;
        var ultimCaracter = 0;
        switch (preajust) {
            case 'majuscules': {
                primerCaracter = 65;
                ultimCaracter = 90;
                break;
            }
            case 'minuscules': {
                primerCaracter = 97;
                ultimCaracter = 122;
                break;
            }
            case 'alfabetic': {
                primerCaracter = 65;
                ultimCaracter = 122;
                break;
            }
            case 'alfanumeric': {
                primerCaracter = 48;
                ultimCaracter = 122;
                break;
            }
            case 'signes': {
                primerCaracter = 32;
                ultimCaracter = 126;
                break;
            }
            case 'numeric': {
                primerCaracter = 48;
                ultimCaracter = 57;
                break;
            }
            case 'personalitzar': {

                break;
            }
            default: {
                return;
            }
        }
        this.refs.primerCaracter.value = primerCaracter;
        this.previsualitzarPrimerCaracter();
        this.refs.ultimCaracter.value = ultimCaracter;
        this.previsualitzarUltimCaracter();
    }

    /**
     * Actualitza el desplegable de preajustos amb la informacio de caracter inicial i final
     * */
    actualitzarRangCaracters() {
        var primerCaracter = this.refs.primerCaracter.value;
        var ultimCaracter = this.refs.ultimCaracter.value;
        if (!primerCaracter || !ultimCaracter) {
            return;
        } else {
            primerCaracter = parseInt(primerCaracter);
            ultimCaracter = parseInt(ultimCaracter);
        }
        var preajust = 'personalitzar';
        switch (primerCaracter) {
            case 65: {
                if (ultimCaracter === 90) {
                    preajust = 'majuscules';
                } else if (ultimCaracter === 122) {
                    preajust = 'alfabetic';
                }
                break;
            }
            case 97: {
                if (ultimCaracter === 122) {
                    preajust = 'minuscules';
                }
                break;
            }
            case 48: {
                if (ultimCaracter === 122) {
                    preajust = 'alfanumeric';
                } else if (ultimCaracter === 57) {
                    preajust = 'numeric';
                }
                break;
            }
            case 32: {
                if (ultimCaracter === 126) {
                    preajust = 'signes';
                }
                break;
            }
            default: {
                return;
            }
        }
        this.refs.tipoBusqueda.value = preajust;
    }

    render() {
        return (<div>
            <div className="row">
                <div className="col">
                    <label>Character set:</label>
                    <select ref="tipoBusqueda" className="form-control" onChange={this.ajustarRangCaracters} defaultValue="personalitzar">
                        <option value="majuscules">Uppercase</option>
                        <option value="minuscules">Lowercase</option>
                        <option value="numeric">Numeric</option>
                        <option value="alfabetic">Alphabetic + symbols</option>
                        <option value="alfanumeric">Alphanumeric + symbols</option>
                        <option value="signes">All characters</option>
                        <option value="personalitzar">Custom</option>
                    </select>
                </div>
                <div className="col">
                    <label>First character:</label>
                    <input type="number" className="form-control primerCaracter" ref="primerCaracter" min="32" onChange={this.previsualitzarPrimerCaracter} />
                    <p ref="primerCaracterPreview" className="previewCaracter"></p>
                </div>
                <div className="col">
                    <label>Last character:</label>
                    <input type="number" className="form-control ultimCaracter" ref="ultimCaracter" min="32" onChange={this.previsualitzarUltimCaracter} />
                    <p ref="ultimCaracterPreview" className="previewCaracter"></p>
                </div>
            </div>
            <br />
            <p ref="caractersPreview" className="previewCaracters"></p>
        </div>)
    }
}

export { FormulariCua, SeleccioForcaBruta };


