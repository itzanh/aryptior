import React, { Component } from 'react';

// IMG
import loadingSpinner from './../IMG/loadingSpinner.svg';

class LoadingScreen extends Component {

    render() {
        return (
            <div id="loadingScreen">
                <img src={loadingSpinner} alt="loadingSpinner" />
                <br />
                <h3 style={{ 'color': 'white' }}>Loading, please wait...</h3>
            </div>
        )
    }
}

export default LoadingScreen;
