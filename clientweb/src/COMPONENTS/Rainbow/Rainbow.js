// REACT
import React, { Component } from 'react';

const tipusEnum = ['NTLM', 'MD4', 'MD5', 'SHA1', 'SHA-256', 'SHA-384', 'SHA-512', 'BCRYPT', 'RIPEMD128',
    'RIPEMD160', 'RIPEMD256', 'RIPEMD320', 'BLAKE', 'BLAKE2b (512)', 'BLAKE2s (256)', 'WHIRLPOOL', 'TIGER', 'TIGER2', 'TIGER3', 'TIGER4'];

class Rainbow extends Component {

    constructor({ rainbow, handleToggle, handleDetails }) {
        super();

        this.rainbow = rainbow;
        this.type = tipusEnum[rainbow.hash.tipus];
        this.firstUnicodeCharacter = String.fromCharCode(rainbow.hash.primerCaracterUnicode);
        this.lastUnicodeCharacter = String.fromCharCode(rainbow.hash.ultimCaracterUnicode);

        this.listStyle = rainbow.enabled ? 'list-group-item-dark' : 'list-group-item-danger';

        this.handleDetails = handleDetails;
        this.handleToggle = handleToggle;

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.handleToggle(this.rainbow.uuid);
    };

    render() {
        return (
            <div className={"card " + (this.rainbow.enabled ? 'text-white bg-dark mb-3' : 'text-white bg-danger mb-3')}>
                <div className="card-body" onClick={(e) => { e.stopPropagation(); this.handleDetails(this.rainbow); }}>
                    <h5 className="card-title">{this.rainbow.name}</h5>
                    <p className="card-text">{this.rainbow.hash.descripcio.substring(0, 150)}</p>
                    <h6 className="card-subtitle mb-2 text-muted">
                        <span className="badge badge-primary">
                            {this.type}
                        </span>
                        <span className="badge badge-info">
                            {this.rainbow.enabled ? 'Enabled' : 'Disabled'}
                        </span>
                        {this.rainbow.hash.estaResolt
                            && <span className="badge badge-success">Finished</span>}
                    </h6>
                    <ul className="list-group">
                        <li className={"list-group-item " + this.listStyle}>
                            Range: {this.firstUnicodeCharacter} - {this.lastUnicodeCharacter}
                        </li>
                        <li className={"list-group-item " + this.listStyle}>
                            <p>Iterations: {this.rainbow.hash.iteracions}</p>
                            <p>Salt: {this.rainbow.hash.salt}</p>
                        </li>
                        <li className={"list-group-item " + this.listStyle}>
                            Max length: {this.rainbow.hash.caractersFinals}
                        </li>
                    </ul>
                    {!this.rainbow.hash.estaResolt
                        && <a href="#" className="card-link" onClick={(e) => { e.stopPropagation(); this.toggle(); }}>
                            {this.rainbow.enabled ? 'Disable' : 'Enable'}
                        </a>}
                    <a href="#" className="card-link" onClick={(e) => { e.stopPropagation(); this.handleDetails(this.rainbow); }}>
                        Details
                    </a>
                </div>
            </div>
        )
    }
}

export default Rainbow;
