// REACT
import React, { Component } from 'react';

// CSS
import './../../CSS/Rainbows.css';

class RainbowFormModal extends Component {
    constructor({ handleCreateRainbow }) {
        super();

        this.handleCreateRainbow = handleCreateRainbow;

        this.handleSaveData = this.handleSaveData.bind(this);

        this.previewLastCharacter = this.previewLastCharacter.bind(this);
        this.ajustarRangCaracters = this.ajustarRangCaracters.bind(this);
        this.previewFirstCharacter = this.previewFirstCharacter.bind(this);
    }

    componentDidMount() {
        window.$('#changePwdModalCenter').modal({ show: true });

        this.previewFirstCharacter();
        this.previewLastCharacter();
    }

    componentWillUnmount() {
        window.$('#changePwdModalCenter').modal('hide');
    }

    handleSaveData() {
        const name = this.refs.rainbowName.value;
        const descripcio = this.refs.description.value;
        const tipus = this.refs.hashType.value;
        const firstUnicodeCharacter = parseInt(this.refs.firstUnicodeCharacter.value);
        const lastUnicodeCharacter = parseInt(this.refs.lastUnicodeCharacter.value);
        const initialCharacters = parseInt(this.refs.initialCharacters.value);
        const caractersFinals = parseInt(this.refs.finalCharacters.value);
        const iteracions = parseInt(this.refs.rainbowIterations.value);
        const initialTestCount = parseInt(this.refs.initialTestCount.value);
        var msProcessingCount = parseInt(this.refs.msProcessingCount.value);
        const timeUnitProcessingCount = this.refs.timeUnitProcessingCount.value;
        const salt = this.refs.rainbowSalt.value;

        var characters = [];

        for (let i = 0; i < initialCharacters; i++) {
            characters.push(firstUnicodeCharacter);
        }

        msProcessingCount *= 1000;

        switch (timeUnitProcessingCount) {
            case 'm': {
                msProcessingCount *= 60;
                break;
            }
            case 'h': {
                msProcessingCount *= 3600;
                break;
            }
        }

        const r = {
            name,
            hash: {
                tipus,
                descripcio,
                primerCaracterUnicode: firstUnicodeCharacter,
                ultimCaracterUnicode: lastUnicodeCharacter,
                caracters: characters,
                caractersFinals,
                iteracions,
                salt,
                initialTestCount,
                msProcessingCount
            }
        };
        if (!this.validateRainbow(r)) {
            alert("The form contains errors. Check the data and try again.");
            return;
        }
        this.handleCreateRainbow(r);

        window.$('#changePwdModalCenter').modal('hide');
    }

    validateRainbow(r) {
        return !(!typeof r.name === 'string' || r.name.length < 1 || r.name.length > 128
            || !typeof r.hash.descripcio === 'string' || !typeof r.hash === 'object' || !this.validateHash(r.hash));
    }

    validateHash(h) {
        return !(h.primerCaracterUnicode < 32 || h.ultimCaracterUnicode <= h.primerCaracterUnicode || h.caracters.length > 32
            || h.caractersFinals <= 1 || h.iteracions <= 0 || !typeof h.salt === 'string'
            || h.msProcessingCount <= 0 || h.initialTestCount <= 0 || h.descripcio.length > 3000
            || h.caractersFinals > 32 || h.salt.length > 128);
    }

    /**
 * Previsualitza el codi del caracter de inici i refresca el rango
 * */
    previewFirstCharacter() {
        var firstCharacter = this.refs.firstUnicodeCharacter.value;
        firstCharacter = String.fromCharCode(firstCharacter);
        this.refs.firstCharacterPreview.innerText = firstCharacter;

        this.previsualitzarCaracters();
    }

    /**
     * Previsualitza el codi del ultim caracter i refresca el rango
     * */
    previewLastCharacter() {
        var lastCharacter = this.refs.lastUnicodeCharacter.value;
        lastCharacter = String.fromCharCode(lastCharacter);
        this.refs.lastCharacterPreview.innerText = lastCharacter;

        this.previsualitzarCaracters();
    }

    /**
     * Mostra per pantalla tots els caracters a ser carregats
     * */
    previsualitzarCaracters() {
        var primerCaracter = this.refs.firstUnicodeCharacter.value;
        var ultimCaracter = this.refs.lastUnicodeCharacter.value;
        if (!primerCaracter || !ultimCaracter) {
            return;
        } else {
            primerCaracter = parseInt(primerCaracter);
            ultimCaracter = parseInt(ultimCaracter);
        }
        if (ultimCaracter < primerCaracter) {
            const pegarLaVolta = ultimCaracter;
            ultimCaracter = primerCaracter;
            primerCaracter = pegarLaVolta;
        }
        var caracters = '';
        for (let i = primerCaracter; i <= ultimCaracter; i++) {
            caracters += String.fromCharCode(i);
        }
        this.refs.caractersPreview.innerText = caracters;
    }

    ajustarRangCaracters() {
        const preajust = this.refs.tipoBusqueda.value;
        var primerCaracter = 0;
        var ultimCaracter = 0;
        switch (preajust) {
            case 'majuscules': {
                primerCaracter = 65;
                ultimCaracter = 90;
                break;
            }
            case 'minuscules': {
                primerCaracter = 97;
                ultimCaracter = 122;
                break;
            }
            case 'alfabetic': {
                primerCaracter = 65;
                ultimCaracter = 122;
                break;
            }
            case 'alfanumeric': {
                primerCaracter = 48;
                ultimCaracter = 122;
                break;
            }
            case 'signes': {
                primerCaracter = 32;
                ultimCaracter = 126;
                break;
            }
            case 'numeric': {
                primerCaracter = 48;
                ultimCaracter = 57;
                break;
            }
            case 'personalitzar': {

                break;
            }
            default: {

            }
        }
        this.refs.firstUnicodeCharacter.value = primerCaracter;
        this.previewFirstCharacter();
        this.refs.lastUnicodeCharacter.value = ultimCaracter;
        this.previewLastCharacter();
    }

    render() {
        return (
            <div id="rainbowModal">
                <div className="modal fade" id="changePwdModalCenter" tabIndex="-2" role="dialog" aria-labelledby="ChangePwdCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">New rainbow</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <label htmlFor="rainbowName">Rainbow name:</label>
                                <br />
                                <input type="text" id="rainbowName" ref="rainbowName" className="form-control" />
                                <label htmlFor="description">Rainbow description:</label>
                                <br />
                                <textarea id="description" ref="description" cols="30" rows="3" className="form-control"></textarea>
                                <label htmlFor="hashType">Algorithm:</label>
                                <select id="hashType" ref="hashType" className="form-control">
                                    <option value="NTLM">NTLM</option>
                                    <option value="MD4">MD4</option>
                                    <option value="MD5">MD5</option>
                                    <option value="SHA1">SHA-1</option>
                                    <option value="SHA256">SHA-256</option>
                                    <option value="SHA384">SHA-384</option>
                                    <option value="SHA512">SHA-512</option>
                                    <option value="BCRYPT">BCRYPT</option>
                                    <option value="RIPEMD128">RIPEMD-128</option>
                                    <option value="RIPEMD160">RIPEMD-160</option>
                                    <option value="RIPEMD256">RIPEMD-256</option>
                                    <option value="RIPEMD320">RIPEMD-320</option>
                                    <option value="BLAKE">BLAKE</option>
                                    <option value="BLAKE2b">BLAKE2b (512)</option>
                                    <option value="BLAKE2s">BLAKE2s (256)</option>
                                    <option value="WHIRLPOOL">WHIRLPOOL</option>
                                    <option value="TIGER">Tiger</option>
                                    <option value="TIGER2">Tiger2</option>
                                    <option value="TIGER3">Tiger3 192</option>
                                    <option value="TIGER4">Tiger4 192</option>
                                </select>
                                <div className="form-row">
                                    <div className="col">
                                        <label htmlFor="tipoBusqueda">Character set:</label>
                                        <select id="tipoBusqueda" ref="tipoBusqueda" className="form-control"
                                            onChange={this.ajustarRangCaracters} defaultValue="personalitzar">
                                            <option value="majuscules">Majuscules</option>
                                            <option value="minuscules">Minuscules</option>
                                            <option value="numeric">Num&eacute;ric</option>
                                            <option value="alfabetic">Alfabetic + signes</option>
                                            <option value="alfanumeric">Alfanumeric + signes</option>
                                            <option value="signes">Tots els caracters</option>
                                            <option value="personalitzar">Personalitzat</option>
                                        </select>
                                    </div>
                                    <div className="col">
                                        <label htmlFor="firstUnicodeCharacter">First character:</label>
                                        <input type="number" id="firstUnicodeCharacter" ref="firstUnicodeCharacter" className="form-control"
                                            defaultValue="65" min="32" onChange={this.previewFirstCharacter} />
                                        <p id="firstCharacterPreview" ref="firstCharacterPreview" className="previewCaracter"></p>
                                    </div>
                                    <div className="col">
                                        <label htmlFor="lastUnicodeCharacter">Last character:</label>
                                        <input type="number" id="lastUnicodeCharacter" ref="lastUnicodeCharacter" className="form-control"
                                            defaultValue="90" min="32" onChange={this.previewLastCharacter} />
                                        <p id="lastCharacterPreview" ref="lastCharacterPreview" className="previewCaracter"></p>
                                    </div>
                                </div>
                                <p id="caractersPreview" ref="caractersPreview" className="previewCaracters"></p>
                                <div className="form-row">
                                    <div className="col">
                                        <label htmlFor="rainbowIterations">Interations:</label>
                                        <input type="number" id="rainbowIterations" ref="rainbowIterations" className="form-control" defaultValue="1" />
                                    </div>
                                    <div className="col">
                                        <label htmlFor="rainbowSalt">Salt:</label>
                                        <input type="text" id="rainbowSalt" ref="rainbowSalt" className="form-control" />
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="col">
                                        <label htmlFor="initialCharacters">Initial character length:</label>
                                        <input type="number" id="initialCharacters" ref="initialCharacters" defaultValue="1" className="form-control" />
                                    </div>
                                    <div className="col">
                                        <label htmlFor="finalCharacters">Final character length:</label>
                                        <input type="number" id="finalCharacters" ref="finalCharacters" defaultValue="8" className="form-control" />
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="col">
                                        <label htmlFor="initialTestCount">Number of combinations for testing:</label>
                                        <input type="number" id="initialTestCount" ref="initialTestCount" defaultValue="75000" className="form-control" />
                                    </div>
                                    <div className="col">
                                        <label htmlFor="msProcessingCount">Estimated time to process the part:</label>
                                        <div className="input-group">
                                            <input type="number" id="msProcessingCount" ref="msProcessingCount"
                                                defaultValue="5" className="form-control" />
                                            <div className="input-group-append">
                                                <select id="timeUnitProcessingCount" ref="timeUnitProcessingCount" className="form-control" defaultValue="m">
                                                    <option value="s">Segons</option>
                                                    <option value="m">Minuts</option>
                                                    <option value="h">Hores</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-primary" onClick={this.handleSaveData}>Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default RainbowFormModal;


