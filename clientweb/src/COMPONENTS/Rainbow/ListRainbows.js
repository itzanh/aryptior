// REACT
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// CSS
import './../../CSS/Rainbows.css';
import RainbowFormModal from './RainbowFormModal';

class ListRainbows extends Component {
    constructor({ handleCreateRainbow, handleSearch }) {
        super();

        this.handleCreateRainbow = handleCreateRainbow;
        this.handleSearch = handleSearch;

        this.openModal = this.openModal.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    openModal() {
        ReactDOM.unmountComponentAtNode(document.getElementById("renderRainbowModal"));
        ReactDOM.render(<RainbowFormModal
            handleCreateRainbow={this.handleCreateRainbow}
        />, document.getElementById("renderRainbowModal"));
    }

    onSearch(e) {
        this.handleSearch(this.refs.search.value, parseInt(this.refs.searchType.value), parseInt(this.refs.sort.value));
    }

    render() {
        return (
            <div id="rainbowsTab">
                <div id="renderAlertModal">

                </div>
                <div id="renderRainbowModal">

                </div>
                <div id="rainbowOptions">
                    <nav className="navbar center navbar-dark bg-dark navbar-expand-lg">
                        <a className="navbar-brand" href="#" id="loadingSpinner">

                        </a>
                        <div className="collapse navbar-collapse" id="navbarText">
                            <ul className="navbar-nav mr-auto btn-group text-center mx-auto">
                                <li className="nav-item">
                                    <button type="button" className="btn btn-outline-danger" onClick={this.openModal}>New</button>
                                </li>
                                <li className="nav-item">
                                    <div className="input-group mb-3">
                                        <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"
                                            ref="search" onChange={this.onSearch} id="searchInput" />
                                        <div className="input-group-append">
                                            <div className="input-group mb-3">
                                                <select name="tipoHash" id="searchHash" className="form-control"
                                                    ref="searchType" onChange={this.onSearch}>
                                                    <option value="-1">Unfiltered</option>
                                                    <option value="0">NTLM</option>
                                                    <option value="1">MD4</option>
                                                    <option value="2">MD5</option>
                                                    <option value="3">SHA-1</option>
                                                    <option value="4">SHA-256</option>
                                                    <option value="5">SHA-384</option>
                                                    <option value="6">SHA-512</option>
                                                    <option value="7">BCRYPT</option>
                                                    <option value="8">RIPEMD-128</option>
                                                    <option value="9">RIPEMD-160</option>
                                                    <option value="10">RIPEMD-256</option>
                                                    <option value="11">RIPEMD-320</option>
                                                    <option value="12">BLAKE</option>
                                                    <option value="13">BLAKE2b (512)</option>
                                                    <option value="14">BLAKE2s (256)</option>
                                                    <option value="15">WHIRLPOOL</option>
                                                    <option value="16">Tiger</option>
                                                    <option value="17">Tiger2</option>
                                                    <option value="18">Tiger3 192</option>
                                                    <option value="19">Tiger4 192</option>
                                                </select>
                                                <select name="sort" id="sort" className="form-control"
                                                    ref="sort" onChange={this.onSearch}>
                                                    <option value="-1">Lastest</option>
                                                    <option value="0">A - Z</option>
                                                    <option value="1">Z - A</option>
                                                    <option value="2">Oldest</option>
                                                </select>
                                            </div>
                                            <button className="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <br />
                <div id="formRainbow"></div>
                <div id="cardContainer" className="card-deck">
                    <div id="listRainbows" className="card-deck">

                    </div>
                </div>
            </div>
        );
    }
}

export default ListRainbows;
