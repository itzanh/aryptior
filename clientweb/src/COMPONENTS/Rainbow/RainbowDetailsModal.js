// REACT
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Alert from '../Alert';

const tipusEnum = ['NTLM', 'MD4', 'MD5', 'SHA1', 'SHA-256', 'SHA-384', 'SHA-512', 'BCRYPT', 'RIPEMD128',
    'RIPEMD160', 'RIPEMD256', 'RIPEMD320', 'BLAKE', 'BLAKE2b (512)', 'BLAKE2s (256)', 'WHIRLPOOL', 'TIGER', 'TIGER2', 'TIGER3', 'TIGER4'];
const tipusBusquedaEnum = ['Brute force', 'Dictionary'];

class RainbowDetailsModal extends Component {
    constructor({ r, handleRemoveRainbow, handleToggle, handleSearchPwd, handleEdit, readOnly }) {
        super();

        this.r = r;
        this.tipus = tipusEnum[r.hash.tipus];
        this.tipusBusqueda = tipusBusquedaEnum[r.hash.tipusBusqueda];
        this.readOnly = (readOnly === undefined) ? false : readOnly;

        this.handleEdit = handleEdit;
        this.handleToggle = handleToggle;
        this.handleSearchPwd = handleSearchPwd;
        this.handleRemoveRainbow = handleRemoveRainbow;

        this.toggle = this.toggle.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
        this.openSearchModal = this.openSearchModal.bind(this);

        this.mainScreen = this.mainScreen.bind(this);
        this.modalAlert = this.modalAlert.bind(this);
        this.handleEditRainbow = this.handleEditRainbow.bind(this);
        this.renderMainScreen = this.renderMainScreen.bind(this);
    }

    componentDidMount() {
        window.$('#rainbowDetailsModal').modal({ show: true });

        window.$(() => {
            window.$('[data-toggle="tooltip"]').tooltip();
        });

        this.renderMainScreen();
    }

    componentWillUnmount() {
        window.$('#rainbowDetailsModal').modal('hide');

    }

    modalAlert(body, title) {
        ReactDOM.unmountComponentAtNode(this.refs.renderValidationErrorModal);
        ReactDOM.render(<Alert
            title={title}
            body={body}
        />, this.refs.renderValidationErrorModal);
    };

    renderMainScreen() {
        ReactDOM.render(<this.mainScreen />, this.refs.renderModalContent);
    }

    handleRemove() {
        this.handleRemoveRainbow(this.r.uuid);
        window.$('#rainbowDetailsModal').modal('hide');

    };

    toggle() {
        this.handleToggle(this.r.uuid);
    };

    openSearchModal() {
        ReactDOM.unmountComponentAtNode(this.refs.renderSearchPwdModal);
        ReactDOM.render(<SearchPwdModal
            uuid={this.r.uuid}
            handleSearchPwd={this.handleSearchPwd}
        />, this.refs.renderSearchPwdModal);
    }

    render() {
        return (
            <div>
                <div className="modal fade" id="rainbowDetailsModal" tabIndex="-2" role="dialog" aria-labelledby="rainbowDetailsModal" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">Details for {this.r.uuid}</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <data id="modalRainbowCurrentUUID" style={{ "display": "none" }}>{this.r.uuid}</data>
                            <div id="renderValidationErrorModal" ref="renderValidationErrorModal"></div>
                            <div id="renderModalContent" ref="renderModalContent"></div>

                        </div>
                    </div>
                </div >

                <div id="renderSearchPwdModal" ref="renderSearchPwdModal"></div>
            </div>
        )
    }

    formatDate(date) {
        const d = new Date(date);
        return d.getDate() + '/'
            + (d.getMonth() + 1) + '/'
            + d.getFullYear() + ' '
            + d.getHours() + ':'
            + d.getMinutes() + ':'
            + d.getSeconds();
    }

    formatTime(msTime) {
        const unitats = ['ms', 'sec', 'min', 'hour'];
        var unitat = 0;
        if (msTime >= 1000) {
            msTime /= 1000;
            unitat++;
        }
        while (msTime >= 60 && unitat < unitats.length - 1) {
            msTime /= 60;
            unitat++;
        }
        return Math.round(msTime) + " " + unitats[unitat];
    }

    mainScreen() {
        return (<div>
            <div className="modal-body" id="modal-body">
                <span className="badge badge-pill badge-danger">{this.tipus}</span>
                <span className="badge badge-pill badge-info">{this.tipusBusqueda}</span>
                <span className="badge badge-pill badge-warning">
                    {this.r.enabled ? 'Enabled' : 'Disabled'}
                </span>
                {this.r.hash.estaResolt
                    && <span className="badge badge-pill badge-success">Finished</span>}
                <br />
                <h6>Rainbow details</h6>
                <table className="table table-dark" id="rainbowDetailsTable">
                    <tbody>
                        <tr>
                            <td>UUID</td>
                            <td>{this.r.uuid}</td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>{this.r.name}</td>
                        </tr>
                        <tr>
                            <td>Enabled</td>
                            <td>{this.r.enabled ? 'Yes' : 'No'}</td>
                        </tr>
                        <tr>
                            <td>Date created</td>
                            <td data-toggle="tooltip" data-placement="top" title="DD/MM/YYYY hh:mm:ss">{this.formatDate(this.r.hash.dateCreated)}</td>
                        </tr>
                        {this.r.hash.estaResolt &&
                            <tr>
                                <td>Date finished</td>
                                <td data-toggle="tooltip" data-placement="top" title="DD/MM/YYYY hh:mm:ss">{this.formatDate(this.r.hash.dateFinished)}</td>
                            </tr>
                        }
                    </tbody>
                </table>
                <h6>Hash details</h6>
                <table className="table table-dark">
                    <tbody>
                        <tr>
                            <td>Initial test hash count</td>
                            <td>{this.r.hash.initialTestCount}</td>
                        </tr>
                        <tr>
                            <td>Time processing count</td>
                            <td>{this.formatTime(this.r.hash.msProcessingCount)}</td>
                        </tr>
                        <tr>
                            <td>Iterations</td>
                            <td>{this.r.hash.iteracions}</td>
                        </tr>
                        <tr>
                            <td>Salt</td>
                            <td>{this.r.hash.salt}</td>
                        </tr>
                        <tr>
                            <td>Final length</td>
                            <td>{this.r.hash.caractersFinals}</td>
                        </tr>
                        <tr>
                            <td>First character</td>
                            <td>{this.r.hash.primerCaracterUnicode}</td>
                        </tr>
                        <tr>
                            <td>Last character</td>
                            <td>{this.r.hash.ultimCaracterUnicode}</td>
                        </tr>
                        {this.r.hash.tipusBusqueda === 1
                            ? (<tr>
                                <td>Processing line number</td>
                                <td>{this.r.hash.numLinea}</td>
                            </tr>)
                            : null}
                    </tbody>
                </table>
                <h6>Description</h6>
                <pre>{this.r.hash.descripcio}</pre>
            </div>
            <div className="modal-footer" id="modal-footer">
                {!this.readOnly &&
                    <div className="btn-group mr-2">
                        {!this.r.hash.estaResolt
                            && <button type="button" className="btn btn-outline-success" onClick={this.handleEditRainbow}>Edit</button>}
                        {!this.r.hash.estaResolt
                            && <button type="button" className="btn btn-outline-info" onClick={this.toggle}>{this.r.enabled ? 'Disable' : 'Enable'}</button>}
                        {(!this.r.enabled || this.r.hash.estaResolt)
                            && <button type="button" className="btn btn-outline-danger" onClick={this.handleRemove}>Remove</button>}
                        <button type="button" className="btn btn-outline-light" onClick={this.openSearchModal}>Search password</button>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                }
                {this.readOnly &&
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>}
            </div>
        </div>)
    }

    handleEditRainbow() {
        ReactDOM.unmountComponentAtNode(this.refs.renderModalContent);
        ReactDOM.render(<EditRainbowDetailsModal
            r={this.r}
            handleEdit={this.handleEdit}
            modalAlert={this.modalAlert}
            renderMainScreen={this.renderMainScreen}
        />, this.refs.renderModalContent);
    }


}

class EditRainbowDetailsModal extends Component {
    constructor({ handleEdit, modalAlert, renderMainScreen, r }) {
        super();

        this.r = r;
        this.tipus = tipusEnum[r.hash.tipus];
        this.tipusBusqueda = tipusBusquedaEnum[r.hash.tipusBusqueda];

        this.handleEdit = handleEdit;
        this.modalAlert = modalAlert;
        this.renderMainScreen = renderMainScreen;

        this.handleSaveEditRainbow = this.handleSaveEditRainbow.bind(this);

    }

    componentDidMount() {
        ReactDOM.render(
            <div className="btn-group mr-2">
                <button type="button" className="btn btn-success" onClick={this.handleSaveEditRainbow}>Save</button>
                <button type="button" className="btn btn-secondary" onClick={this.renderMainScreen}>Cancel</button>
            </div>
            , document.getElementById("modal-footer"));
    }

    handleSaveEditRainbow() {
        const updatedSettings = { uuid: this.r.uuid };
        if (this.r.name !== this.refs.name.value) {
            updatedSettings.name = this.refs.name.value;
            if (updatedSettings.name.length > 128) {
                this.modalAlert('The name cannot be longer than 128 characters. Use the description field to write further details.', 'Validation error');
                return;
            }
        }

        if (this.r.hash.initialTestCount !== parseInt(this.refs.initialTestCount.value)) {
            updatedSettings.initialTestCount = parseInt(this.refs.initialTestCount.value);
            if (updatedSettings.initialTestCount <= 0) {
                this.modalAlert('Initial test count cannot be less or equal to zero.', 'Validation error');
                return;
            }
        }

        if (this.r.hash.msProcessingCount !== this.recollirTempsEnMs()) {
            updatedSettings.msProcessingCount = this.recollirTempsEnMs();
            if (updatedSettings.msProcessingCount <= 0) {
                this.modalAlert('Time processing count cannot be less or equal to zero.', 'Validation error');
                return;
            }
        }

        if (this.r.hash.caractersFinals !== parseInt(this.refs.caractersFinals.value)) {
            updatedSettings.caractersFinals = parseInt(this.refs.caractersFinals.value);
            if (updatedSettings.caractersFinals <= 1) {
                this.modalAlert('Final combination length cannot be less or equal to 1.', 'Validation error');
                return;
            }
        }

        if (this.r.hash.descripcio !== this.refs.descripcio.value) {
            updatedSettings.descripcio = this.refs.descripcio.value;
            if (updatedSettings.descripcio.length > 3000) {
                this.modalAlert('The description cannot be longer than 3.000 characters.', 'Validation error');
                return;
            }
        }

        if (Object.keys(updatedSettings).length > 1) {
            console.log(updatedSettings);
            ReactDOM.unmountComponentAtNode(document.getElementById("modal-footer"));
            ReactDOM.render(<small>
                Loading, please wait...
            </small>, document.getElementById("modal-footer"));
            this.handleEdit(updatedSettings).then(() => {
                window.$('#rainbowDetailsModal').modal('hide');
            }, () => {
                this.modalAlert('Error on the server side, check the input.', 'Server error');
                window.$('#rainbowDetailsModal').modal('hide');
            });
        } else {
            window.$('#rainbowDetailsModal').modal('hide');
        }
    }

    recollirTempsEnMs() {
        var timeLapse = parseInt(this.refs.timeLapseProcessingCount.value);
        const timeUnit = this.refs.timeUnitProcessingCount.selectedIndex;
        for (let i = 0; i < timeUnit; i++) {
            timeLapse *= 60;
        }

        return timeLapse * 1000;
    }

    render() {
        return (<div>
            <div className="modal-body" id="modal-body">
                <span className="badge badge-pill badge-danger">{this.tipus}</span>
                <span className="badge badge-pill badge-info">{this.tipusBusqueda}</span>
                <span className="badge badge-pill badge-warning">
                    {this.r.enabled ? 'Enabled' : 'Disabled'}
                </span>
                {this.r.hash.estaResolt
                    && <span className="badge badge-pill badge-success">Finished</span>}
                <br />
                <h6>Rainbow details</h6>
                <table className="table table-dark" id="rainbowDetailsTable">
                    <tbody>
                        <tr>
                            <td>UUID</td>
                            <td>{this.r.uuid}</td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>
                                <input type="text" id="name" ref="name" className="form-control" defaultValue={this.r.name}
                                    onChange={(e) => {
                                        if (e.target.value.length === 0) {
                                            e.target.style.borderColor = 'red';
                                        } else {
                                            e.target.style.borderColor = '';
                                        }
                                    }} />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <h6>Hash details</h6>
                <table className="table table-dark">
                    <tbody>
                        <tr>
                            <td>Initial test hash count</td>
                            <td>
                                <input type="number" id="initialTestCount" ref="initialTestCount" className="form-control" min="1"
                                    defaultValue={this.r.hash.initialTestCount} />
                            </td>
                        </tr>
                        <tr>
                            <td>Time processing count</td>
                            <td>
                                <div className="input-group">
                                    <input type="number" id="timeLapseProcessingCount" ref="timeLapseProcessingCount" className="form-control" min="1"
                                        name="timeLapseProcessingCount" defaultValue={(
                                            (msProcessingCount) => {
                                                msProcessingCount /= 1000;
                                                while (msProcessingCount >= 60) {
                                                    msProcessingCount /= 60;
                                                }
                                                return msProcessingCount;
                                            })(this.r.hash.msProcessingCount)} />
                                    <div className="input-group-append">
                                        <select id="timeUnitProcessingCount" ref="timeUnitProcessingCount" className="form-control" defaultValue={(
                                            (msProcessingCount) => {
                                                const units = ['s', 'm', 'h'];
                                                var unit = 0;
                                                msProcessingCount /= 1000;
                                                while (msProcessingCount >= 60) {
                                                    msProcessingCount /= 60;
                                                    unit++;
                                                }
                                                return units[unit];
                                            })(this.r.hash.msProcessingCount)} >
                                            <option value="s">Segons</option>
                                            <option value="m">Minuts</option>
                                            <option value="h">Hores</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Final length</td>
                            <td>
                                <input type="number" id="caractersFinals" ref="caractersFinals" className="form-control" min="1"
                                    defaultValue={this.r.hash.caractersFinals} />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br />
                <label htmlFor="isDiscoverable">Update description?</label>
                <h6>Description</h6>
                <textarea rows="5" className="form-control" id="descripcio" ref="descripcio" defaultValue={this.r.hash.descripcio}>
                </textarea>
            </div>
            <div className="modal-footer" id="modal-footer">

            </div>
        </div>)
    }
}

class SearchPwdModal extends Component {

    constructor({ uuid, handleSearchPwd }) {
        super();

        this.uuid = uuid;
        this.handleSearchPwd = handleSearchPwd;

        this.handleSearch = this.handleSearch.bind(this);
        this.mainFooterComponent = this.mainFooterComponent.bind(this);
    }

    componentDidMount() {
        window.$('#searchPwdModal').modal({ show: true });

        ReactDOM.render(<this.mainScreenComponent />, document.getElementById("modal-search-body"));
        ReactDOM.render(<this.mainFooterComponent />, document.getElementById("modal-search-footer"));
    }

    mainScreenComponent() {
        return (<div>
            <label htmlFor="hashInput">Insert hash</label>
            <input type="text" className="form-control" id="hashInput" placeholder="Hash" />
        </div>);
    }

    mainFooterComponent() {
        return (<div className="btn-group mr-2">
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" className="btn btn-primary" onClick={this.handleSearch}>Search</button>
        </div>);
    }

    async handleSearch() {
        const hashInput = document.getElementById("hashInput").value;

        ReactDOM.unmountComponentAtNode(document.getElementById("modal-search-body"));
        ReactDOM.render(<p>
            Searching, please wait...
        </p>, document.getElementById("modal-search-body"));
        ReactDOM.unmountComponentAtNode(document.getElementById("modal-search-footer"));
        ReactDOM.render(<this.closeFooterComponent />, document.getElementById("modal-search-footer"));


        const response = await this.handleSearchPwd(this.uuid, hashInput);

        if (response.resultCode === 0) {
            ReactDOM.render(<div>
                <h6>Results found!</h6>
                <p>{response.result}</p>
            </div>, document.getElementById("modal-search-body"));
        } else if (response.resultCode === 1) {
            ReactDOM.unmountComponentAtNode(document.getElementById("modal-search-body"));
            ReactDOM.render(this.errorOcurredComponent(), document.getElementById("modal-search-body"));
        } else if (response.resultCode === 2) {
            ReactDOM.unmountComponentAtNode(document.getElementById("modal-search-body"));
            ReactDOM.render(this.noResultFoundComponent(), document.getElementById("modal-search-body"));
        }
    }

    closeFooterComponent() {
        return (<div className="btn-group mr-2">
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>);
    }

    errorOcurredComponent() {
        return (<p>
            AN ERROR OCURRED AND THE PASSWORD COULD NOT BE FOUND!
            </p>);
    }

    noResultFoundComponent() {
        return (<p>
            Sorry, no results were found.
            </p>);
    }

    render() {
        return (
            <div>
                <div className="modal fade" id="searchPwdModal" tabIndex="-2" role="dialog" aria-labelledby="searchPwdModal" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">Search password in rainbow</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body" id="modal-search-body">

                            </div>
                            <div className="modal-footer" id="modal-search-footer">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default RainbowDetailsModal;
