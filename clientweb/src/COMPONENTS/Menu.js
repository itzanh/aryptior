// REACT
import React, { Component } from 'react';

// CSS
import './../CSS/Menu.css';

// IMG
import desarICO from './../IMG/desar.svg';
import dashboardICO from "./../IMG/dashboard.svg";
import hashICO from "./../IMG/hash.svg";
import finalitzatsICO from "./../IMG/finalitzats.svg";
import diccionarisICO from "./../IMG/diccionaris.svg";
import clientsICO from "./../IMG/clients.svg";
import ajustosICO from "./../IMG/ajustos.svg";
import tancarICO from "./../IMG/tancar.svg";
import logoutICO from "./../IMG/logout.svg";
import tokenICO from "./../IMG/token.svg";
import backupICO from "./../IMG/backup.svg";
import rainbowICO from "./../IMG/rainbow.svg";
import logsICO from "./../IMG/logs.svg";
import logo from './../logo.png';

class Menu extends Component {
    constructor(dades) {
        super();

        this.handleDashboard = dades.handleDashboard;
        this.handleCua = dades.handleCua;
        this.handleFinalitzats = dades.handleFinalitzats;
        this.handleDiccionaris = dades.handleDiccionaris;
        this.handleClients = dades.handleClients;
        this.handleBackups = dades.handleBackups;
        this.handleTokens = dades.handleTokens;
        this.handleRainbow = dades.handleRainbow;
        this.handleAjustos = dades.handleAjustos;
        this.handleLogs = dades.handleLogs;

        this.handleEixirGuardar = dades.handleEixirGuardar;
        this.handleEixirApagarSrv = dades.handleEixirApagarSrv;
        this.handleEixirApp = dades.handleEixirApp;
    }

    componentDidMount() {
        if (!this.state) {
            this.setState((state) => {
                return { currentTab: 0, hideTags: false };
            });
        }
    }

    render() {
        return (
            <div id="pantalla">
                <div id="menu">
                    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                        <a className="navbar-brand" href="#" onClick={() => {
                            this.setState({ hideTags: !this.state.hideTags })
                        }} >
                            <img src={logo} width="30" height="30" className="d-inline-block align-top imatgeMenu" alt="" />
                            aryptiOR
                        </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                            aria-controls="navbarNav" aria-expanded="true" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className={"collapse navbar-collapse " + ((this.state && this.state.hideTags) ? 'hideTags' : '')} id="navbarNav">
                            <ul className="navbar-nav">
                                <li className={"nav-item" + ((this.state && this.state.currentTab === 0) ? " active" : '')}
                                    onClick={() => { this.setState({ currentTab: 0 }); this.handleDashboard(); }}>
                                    <a className="nav-link" href="#">
                                        <img src={dashboardICO} alt="dashboardICO" />
                                        {((this.state !== null && !this.state.hideTags) || (this.setState === null)) &&
                                            <p>
                                                Dashboard < span className="sr-only">(current)</span>
                                            </p>}
                                    </a>
                                </li>
                                <li className={"nav-item" + ((this.state && this.state.currentTab === 1) ? " active" : '')}
                                    onClick={() => { this.setState({ currentTab: 1 }); this.handleCua(); }}>
                                    <a className="nav-link" href="#">
                                        <img src={hashICO} alt="hashICO" />
                                        {((this.state !== null && !this.state.hideTags) || (this.setState === null)) &&
                                            <p>
                                                Queue
                                            </p>}
                                    </a>
                                </li>
                                <li className={"nav-item" + ((this.state && this.state.currentTab === 2) ? " active" : '')}
                                    onClick={() => { this.setState({ currentTab: 2 }); this.handleFinalitzats(); }}>
                                    <a className="nav-link" href="#">
                                        <img src={finalitzatsICO} alt="finalitzatsICO" />
                                        {((this.state !== null && !this.state.hideTags) || (this.setState === null)) &&
                                            <p>
                                                Finished
                                            </p>}
                                    </a>
                                </li>
                                <li className={"nav-item" + ((this.state && this.state.currentTab === 3) ? " active" : '')}
                                    onClick={() => { this.setState({ currentTab: 3 }); this.handleDiccionaris(); }}>
                                    <a className="nav-link" href="#">
                                        <img src={diccionarisICO} alt="diccionarisICO" />
                                        {((this.state !== null && !this.state.hideTags) || (this.setState === null)) &&
                                            <p>
                                                Dictionaries
                                            </p>}
                                    </a>
                                </li>
                                <li className={"nav-item" + ((this.state && this.state.currentTab === 4) ? " active" : '')}
                                    onClick={() => { this.setState({ currentTab: 4 }); this.handleClients(); }}>
                                    <a className="nav-link" href="#">
                                        <img src={clientsICO} alt="clientsICO" />
                                        {((this.state !== null && !this.state.hideTags) || (this.setState === null)) &&
                                            <p>
                                                Clients
                                            </p>}
                                    </a>
                                </li>
                                <li className={"nav-item" + ((this.state && this.state.currentTab === 5) ? " active" : '')}
                                    onClick={() => { this.setState({ currentTab: 5 }); this.handleTokens(); }}>
                                    <a className="nav-link" href="#">
                                        <img src={tokenICO} alt="tokenICO" />
                                        {((this.state !== null && !this.state.hideTags) || (this.setState === null)) &&
                                            <p>
                                                Tokens
                                            </p>}
                                    </a>
                                </li>
                                <li className={"nav-item" + ((this.state && this.state.currentTab === 6) ? " active" : '')}
                                    onClick={() => { this.setState({ currentTab: 6 }); this.handleBackups(); }}>
                                    <a className="nav-link" href="#">
                                        <img src={backupICO} alt="backupICO" />
                                        {((this.state !== null && !this.state.hideTags) || (this.setState === null)) &&
                                            <p>
                                                Backup
                                            </p>}
                                    </a>
                                </li>
                                <li className={"nav-item" + ((this.state && this.state.currentTab === 7) ? " active" : '')}
                                    onClick={() => { this.setState({ currentTab: 7 }); this.handleRainbow(); }}>
                                    <a className="nav-link" href="#">
                                        <img src={rainbowICO} alt="rainbowICO" />
                                        {((this.state !== null && !this.state.hideTags) || (this.setState === null)) &&
                                            <p>
                                                Rainbow
                                            </p>}
                                    </a>
                                </li>
                                <li className={"nav-item" + ((this.state && this.state.currentTab === 8) ? " active" : '')}
                                    onClick={() => { this.setState({ currentTab: 8 }); this.handleAjustos(); }}>
                                    <a className="nav-link" href="#">
                                        <img src={ajustosICO} alt="ajustosICO" />
                                        {((this.state !== null && !this.state.hideTags) || (this.setState === null)) &&
                                            <p>
                                                Settings
                                            </p>}
                                    </a>
                                </li>
                                <li className={"nav-item" + ((this.state && this.state.currentTab === 9) ? " active" : '')}
                                    onClick={() => { this.setState({ currentTab: 9 }); this.handleLogs(); }}>
                                    <a className="nav-link" href="#">
                                        <img src={logsICO} alt="logsICO" />
                                        {((this.state !== null && !this.state.hideTags) || (this.setState === null)) &&
                                            <p>
                                                Logs
                                            </p>}
                                    </a>
                                </li>
                            </ul>
                            <div id="indicadorRefrescar">

                            </div>
                            <div className="dropdown show botoDesconectar">
                                <a className="btn btn-danger dropdown-toggle" href="#" role="button"
                                    id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src={tancarICO} alt="tancarICO" />
                                    Desconectar
                                    </a>

                                <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a className="dropdown-item" href="#" onClick={this.handleEixirGuardar}>
                                        <img src={desarICO} alt="desarICO" />
                                        Save
                                    </a>
                                    <a className="dropdown-item" href="#" onClick={this.handleEixirApagarSrv}>
                                        <img src={tancarICO} alt="tancarICO" />
                                        Save and shutdown server
                                    </a>
                                    <a className="dropdown-item" href="#" onClick={this.handleEixirApp}>
                                        <img src={logoutICO} alt="logoutICO" />
                                        Logout
                                    </a>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
                <div id="renderComfirmModal" className="">

                </div>
                <div id="renderitzarPestanya" className="">

                </div>
            </div >

        )
    }
}

export default Menu;