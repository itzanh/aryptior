// REACT
import React, { Component } from 'react';

// IMG
import trashICO from './../../IMG/trash.svg';

class Backup extends Component {

    constructor({ uuid, date, hash, size, handleRemove }) {
        super();

        this.uuid = uuid;
        this.date = this.formatejarFecha(date);
        this.hash = hash;
        this.size = this.formatejarTamany(size);

        this.handleRemove = handleRemove;

        this.removeBackup = this.removeBackup.bind(this);
    }

    formatejarTamany(tamany) {
        const unitats = ['B', 'Kb', 'Mb', 'Gb', 'Tb'];
        var unitat = 0;
        while (tamany > 1024) {
            tamany /= 1024;
            unitat++;
        }
        return Math.floor(tamany) + ' ' + unitats[unitat];
    }

    formatejarFecha(horaConnectat) {
        const fecha = new Date(horaConnectat);
        return fecha.getDate() + '/'
            + (fecha.getMonth() + 1) + '/'
            + fecha.getFullYear() + ' '
            + fecha.getHours() + ':'
            + fecha.getMinutes() + ':'
            + fecha.getSeconds();
    }

    removeBackup() {
        this.handleRemove(this.uuid);
    }

    render() {
        return (
            <tr>
                <td>{this.uuid}</td>
                <td>{this.date}</td>
                <td>{this.hash}</td>
                <td>{this.size}</td>
                <td><img src={trashICO} alt="trashICO" onClick={this.removeBackup} /></td>
            </tr>
        );
    }
};

export default Backup;

