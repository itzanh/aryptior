// REACT
import React, { Component } from 'react';

const BackupCronStates = ["Stopped", "Starting...", "Idle", "Backing Up...", "Error"];

class BackupCronState extends Component {

    constructor({ cronState, nextBackup }) {
        super();

        this.cronState = BackupCronStates[cronState];
        this.nextBackup = cronState === 0 ? '-' : this.formatejarFecha(nextBackup);
    }

    formatejarFecha(horaConnectat) {
        const fecha = new Date(horaConnectat);
        return fecha.getDate() + '/'
            + (fecha.getMonth() + 1) + '/'
            + fecha.getFullYear() + ' '
            + fecha.getHours() + ':'
            + fecha.getMinutes() + ':'
            + fecha.getSeconds();
    }

    render() {
        return (
            <tr id="CurrentBackupCronState">
                <td>
                    <p>State: {this.cronState}</p>
                </td>
                <td>
                    <p>Next backup: {this.nextBackup}</p>
                </td>
            </tr>
        );
    }
};

export default BackupCronState;

