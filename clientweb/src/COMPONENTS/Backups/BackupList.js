// REACT
import React, { Component } from 'react';

// IMG
import runBackupICO from './../../IMG/runBackup.svg';
import addBackupICO from './../../IMG/addBackup.svg';

// CSS
import './../../CSS/Backup.css';

class BackupList extends Component {

    constructor({ handleAddBackup, handleRunBackup }) {
        super();

        this.handleAddBackup = handleAddBackup;
        this.handleRunBackup = handleRunBackup;

        this.handleFileOpened = this.handleFileOpened.bind(this);
    }

    handleOpenFile() {
        document.getElementById("backupFileInput").click();
    }

    handleFileOpened() {
        const files = document.getElementById("backupFileInput").files;
        if (files.length === 1) {
            this.handleAddBackup(files[0]);
        }
    }

    render() {
        return (
            <div id="backupTab">
                <table id="backupOptions">
                    <tbody>
                        <tr>
                            <td>
                                <button type="button" className="btn btn-outline-primary" onClick={this.handleOpenFile}>
                                    <img src={addBackupICO} alt="addBackupICO" />
                                    Add backup
                                </button>
                            </td>
                            <td>
                                <button type="button" className="btn btn-outline-success" onClick={this.handleRunBackup}>
                                    <img src={runBackupICO} alt="runBackupICO" />
                                    Run backup
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="file" id="backupFileInput" onChange={this.handleFileOpened} />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tableBackupCronState">
                    <tbody id="backupCronState">

                    </tbody>
                </table>
                <div id="backupList">
                    <table className="table table-dark">
                        <thead>
                            <tr>
                                <th scope="col">UUID</th>
                                <th scope="col">Date</th>
                                <th scope="col">Hash (SHA-512)</th>
                                <th scope="col">Size</th>
                                <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody id="renderBackupList">

                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default BackupList;

