import React, { Component } from 'react';

// IMG
import refrescarICO from "./../../IMG/refrescar.svg";


class DashboardRefresh extends Component {
    constructor({ handleRefresh }) {
        super();

        this.refreshInterval = 1;
        this.handleRefresh = handleRefresh;

        this.refreshTimer = setInterval(() => {
            this.handleRefresh();
        }, 5000);

        this.stopTimer = this.stopTimer.bind(this);
        this.handleRefreshInterval = this.handleRefreshInterval.bind(this);
    }

    /**
     * When this component gets unmounted, stop the timer so the dashboard is shown no more
     * */
    componentWillUnmount() {
        this.stopTimer();
    }

    /**
     * Clears the current interval timer if it hasn't been cleared yet
     * */
    stopTimer() {
        if (this.refreshTimer !== null) {
            clearInterval(this.refreshTimer);
            // set to null for later detection that the timer has stopped
            this.refreshTimer = null;
        }
    }

    /**
     * Sets the timer interval from an enumerated value, and restart the timer with the new interval value.
     * @param {number} refreshInterval -1 = stop refresh, 0 = 1s, 1 = 5s, 2 = 10s, 3 = 1s, 4 = 30s, 5 = 1min
     */
    handleRefreshInterval(refreshInterval) {
        this.refreshInterval = refreshInterval;

        // clear the current interval timer if it was running
        this.stopTimer();

        // it an interval value was set, get the new interval in miliseconds and run again the timer
        if (refreshInterval !== -1) {
            var newInterval;
            switch (this.refreshInterval) {
                case 0: {
                    newInterval = 1000;
                    break;
                }
                case 1: {
                    newInterval = 5000;
                    break;
                }
                case 2: {
                    newInterval = 10000;
                    break;
                }
                case 3: {
                    newInterval = 15000;
                    break;
                }
                case 4: {
                    newInterval = 30000;
                    break;
                }
                case 5: {
                    newInterval = 60000;
                    break;
                }
            }
            this.refreshTimer = setInterval(() => {
                this.handleRefresh();
            }, newInterval);
        }
    }

    render() {
        return (
            <div id="dashboardControls">
                <div>
                    <button type="button" className="btn btn-outline-info" onClick={this.handleRefresh}>
                        <img src={refrescarICO} alt="refrescarICO" />
                        Refresh
                    </button>
                    <select id="refreshInterval" className="form-control" name="refreshInterval"
                        defaultValue={this.refreshInterval} onChange={(e) => { this.handleRefreshInterval(parseInt(e.target.value)); }}>
                        <option value="-1">Refresh manually</option>
                        <option value="0">1 second</option>
                        <option value="1">5 seconds</option>
                        <option value="2">10 seconds</option>
                        <option value="3">15 seconds</option>
                        <option value="4">30 seconds</option>
                        <option value="5">1 minute</option>
                    </select>
                </div>
            </div>
        )
    }
}

export default DashboardRefresh;


