import React, { Component } from 'react';

// IMG
import dashboardLoadingICO from "./../../IMG/dashboard_loading.svg";

class DashboardLoading extends Component {

    render() {
        return (
            <div id="dasboardLoading">
                <img src={dashboardLoadingICO} alt="dashboardLoadingICO" />
                <h1>Dashboard initializing...</h1>
            </div>
        );
    }
}

export default DashboardLoading;
