import React, { Component } from 'react';

import DashboardRefresh from './DashboardRefresh';

class DashboardContainer extends Component {
    constructor({ handleRefresh }) {
        super();
        this.handleRefresh = handleRefresh;

        this._child = React.createRef();
    }

    componentWillUnmount() {
        this._child.current.componentWillUnmount();
    }

    render() {
        return (<div>
            <div id="renderDashboardRefresh">
                <DashboardRefresh
                    ref={this._child}

                    handleRefresh={this.handleRefresh}
                />
            </div>
            <div id="renderDashboard"></div>
        </div>)
    }
}

export default DashboardContainer;


