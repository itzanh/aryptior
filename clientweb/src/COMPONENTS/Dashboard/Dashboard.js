import React, { Component } from 'react';

// CSS
import './../../CSS/Dashboard.css';

// IMG
import dashboardICO from "./../../IMG/dashboard.svg";
import hashICO from "./../../IMG/hash.svg";
import finalitzatsICO from "./../../IMG/finalitzats.svg";
import diccionarisICO from "./../../IMG/diccionaris.svg";
import clientsICO from "./../../IMG/clients.svg";
import ajustosICO from "./../../IMG/ajustos.svg";
import tokenICO from "./../../IMG/token.svg";
import rainbowICO from "./../../IMG/rainbow.svg";
import rainbow2ICO from "./../../IMG/rainbow2.svg";
import enmagatzenamentICO from "./../../IMG/enmagatzenament.svg";
import radialBarICO from "./../../IMG/radial_bar.svg";
import okICO from "./../../IMG/ok.svg";

class Dashboard extends Component {

    constructor({ data, handleRefresh, handleGetCpuInfo }) {
        super();

        this.memTotal = data.memTotal;
        this.memAvailable = data.memAvailable;
        this.clientsCount = data.clientsCount;
        this.hashQueueCount = data.hashQueueCount;
        this.finishedHashesCount = data.finishedHashesCount;
        this.nHashesS = data.nHashesS;
        this.tokensCount = data.tokensCount;
        this.dictionaryCount = data.dictionaryCount;
        this.rainbowsCount = data.rainbowsCount;
        this.computedHashesSize = data.computedHashesSize;
        this.totalDriveSpace = data.totalDriveSpace;
        this.availableDriveSpace = data.availableDriveSpace;
        this.enabledRainbows = data.enabledRainbows;
        this.solvedHashes = data.solvedHashes;
        this.cpuUsage = data.cpuUsage;
    }

    formatejarTamany(tamany) {
        const unitats = ['B', 'Kb', 'Mb', 'Gb', 'Tb'];
        var unitat = 0;
        while (tamany > 1024) {
            tamany /= 1024;
            unitat++;
        }
        return Math.floor(tamany) + ' ' + unitats[unitat];
    }

    render() {
        return (
            <div id="dashboard">
                <div className="card-columns">
                    <div className="card text-white bg-dark mb-3">
                        <div className="card-body">
                            <img src={ajustosICO} className="card-img-top" alt="ajustosICO" />
                            <h5 className="card-title">RAM</h5>
                            <h6 className="card-subtitle mb-2 text-muted">Available
                                <progress value={((this.memTotal - this.memAvailable) / this.memTotal) * 100} max="100">
                                    {this.memAvailable + " / " + this.memTotal}
                                </progress>
                            </h6>
                            <p className="card-text">{this.formatejarTamany(this.memAvailable) + " / " + this.formatejarTamany(this.memTotal)}</p>
                        </div>
                    </div>
                    <div className="card text-white bg-dark mb-3">
                        <div className="card-body">
                            <img src={clientsICO} className="card-img-top" alt="clientsICO" />
                            <h5 className="card-title">Clients</h5>
                            <p className="card-text">{this.clientsCount + " clients"}</p>
                        </div>
                    </div>
                    <div className="card text-white bg-dark mb-3">
                        <div className="card-body">
                            <img src={hashICO} className="card-img-top" alt="hashICO" />
                            <h5 className="card-title">Hashes in queue</h5>
                            <p className="card-text">{this.hashQueueCount + " hashes"}</p>
                        </div>
                    </div>
                    <div className="card text-white bg-dark mb-3">
                        <div className="card-body">
                            <img src={finalitzatsICO} className="card-img-top" alt="finalitzatsICO" />
                            <h5 className="card-title">Finished hashes</h5>
                            <p className="card-text">{this.finishedHashesCount + " hashes"}</p>
                        </div>
                    </div>
                    <div className="card text-white bg-dark mb-3">
                        <div className="card-body">
                            <img src={dashboardICO} className="card-img-top" alt="dashboardICO" />
                            <h5 className="card-title">Work rate</h5>
                            <h6 className="card-subtitle mb-2 text-muted">(hashes/second)</h6>
                            <p className="card-text">{this.nHashesS + " h/s"}</p>
                        </div>
                    </div>
                    <div className="card text-white bg-dark mb-3">
                        <div className="card-body">
                            <img src={diccionarisICO} className="card-img-top" alt="diccionarisICO" />
                            <h5 className="card-title">Dictionaries count</h5>
                            <p className="card-text">{this.dictionaryCount + " dictionaries"}</p>
                        </div>
                    </div>
                    <div className="card text-white bg-dark mb-3">
                        <div className="card-body">
                            <img src={tokenICO} className="card-img-top" alt="tokenICO" />
                            <h5 className="card-title">Tokens count</h5>
                            <p className="card-text">{this.tokensCount + " tokens"}</p>
                        </div>
                    </div>
                    <div className="card text-white bg-dark mb-3">
                        <div className="card-body">
                            <img src={rainbowICO} className="card-img-top" alt="rainbowICO" />
                            <h5 className="card-title">Rainbows count</h5>
                            <h6 className="card-subtitle mb-2 text-muted">enabled and disabled</h6>
                            <p className="card-text">{this.rainbowsCount + " rainbows"}</p>
                        </div>
                    </div>
                    <div className="card text-white bg-dark mb-3">
                        <div className="card-body">
                            <img src={enmagatzenamentICO} className="card-img-top" alt="enmagatzenamentICO" />
                            <h5 className="card-title">Size of computed hases</h5>
                            <h6 className="card-subtitle mb-2 text-muted">
                                used disk (in all rainbows) / total disk space
                                <br />
                                <progress value={this.totalDriveSpace - this.availableDriveSpace} max={this.totalDriveSpace} />
                            </h6>
                            <p className="card-text">
                                {this.formatejarTamany(this.computedHashesSize) + " / " + this.formatejarTamany(this.totalDriveSpace)}
                            </p>
                        </div>
                    </div>
                    <div className="card text-white bg-dark mb-3">
                        <div className="card-body">
                            <img src={radialBarICO} className="card-img-top" alt="radialBarICO" />
                            <h5 className="card-title">CPU Usage</h5>
                            <h6 className="card-subtitle mb-2 text-muted">
                                <progress value={this.cpuUsage} max="100">
                                    {this.cpuUsage}
                                </progress>
                            </h6>
                            <p className="card-text">{Math.floor(this.cpuUsage) + "%"}</p>
                        </div>
                    </div>
                    <div className="card text-white bg-dark mb-3">
                        <div className="card-body">
                            <img src={rainbow2ICO} className="card-img-top" alt="rainbow2ICO" />
                            <h5 className="card-title">Rainbows count</h5>
                            <h6 className="card-subtitle mb-2 text-muted">currently enabled</h6>
                            <p className="card-text">{this.enabledRainbows + " rainbows"}</p>
                        </div>
                    </div>
                    <div className="card text-white bg-dark mb-3">
                        <div className="card-body">
                            <img src={okICO} className="card-img-top" alt="okICO" />
                            <h5 className="card-title">Solved hases</h5>
                            <h6 className="card-subtitle mb-2 text-muted">(by brute force and dictionary)</h6>
                            <p className="card-text">{this.solvedHashes + " hashes"}</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Dashboard;


