import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class DictionaryDetails extends Component {
    constructor({ dictionary, handleRemove, handleEdit, readOnly }) {
        super();

        this.dictionary = dictionary;

        this.handleEdit = handleEdit;
        this.handleRemove = () => { handleRemove(dictionary.uuid); };

        this.readOnly = (readOnly === undefined) ? false : readOnly;

        this.showDescription = this.showDescription.bind(this);
        this.showMainScreen = this.showMainScreen.bind(this);
        this.editDictionary = this.editDictionary.bind(this);
        this.onEdit = this.onEdit.bind(this);
    }

    componentDidMount() {
        this.showMainScreen();
        window.$('#dictionaryDetails').modal({ show: true });
    }

    componentWillUnmount() {
        window.$('#dictionaryDetails').modal('hide');
    }

    showMainScreen() {
        ReactDOM.render(this.showDescription(), document.getElementById("showDescription"));
        ReactDOM.render(this.mainScreenButtons(), document.getElementById("modal-footer"));
    }

    formatSize(sizeInBytes) {
        const units = ['B', 'Kb', 'Mb', 'Gb', 'Tb'];
        var unit = 0;
        while (sizeInBytes >= 1024) {
            sizeInBytes /= 1024;
            unit++;
        }
        return Math.floor(sizeInBytes) + ' ' + units[unit];
    }

    showDescription() {
        return (<pre id="dictionaryDetailsDescription">{this.dictionary.description}</pre>);
    }

    mainScreenButtons() {
        if (this.readOnly) {
            return (<div>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>);
        }
        return (<div>
            <div className="btn-group" role="group">
                <button type="button" className="btn btn-outline-success" onClick={this.onEdit}>Edit</button>
                <button type="button" className="btn btn-outline-danger" onClick={this.handleRemove} data-dismiss="modal">Delete</button>
            </div>
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>);
    }

    onEdit() {
        ReactDOM.render(
            <textarea id="editDescription" defaultValue={this.dictionary.description} cols="60" rows="3"></textarea>
            , document.getElementById("showDescription"));
        ReactDOM.render(<div>
            <div className="btn-group" role="group">
                <button type="button" className="btn btn-outline-success" onClick={this.editDictionary}>Edit</button>
                <button type="button" className="btn btn-outline-light" onClick={this.showMainScreen}>Cancel</button>
            </div>
        </div>, document.getElementById("modal-footer"));
    }

    editDictionary() {
        this.handleEdit({
            uuid: this.dictionary.uuid, description: document.getElementById("editDescription").value
        }).then(() => {
            this.showMainScreen();
        });
    }

    render() {
        return (
            <div>
                <div className="modal fade" id="dictionaryDetails" tabIndex="-1" role="dialog" aria-labelledby="dictionaryDetails" aria-hidden="false">
                    <data id="currentDictionaryID">{this.dictionary.uuid}</data>
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">Dictionary details</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <h6>Details</h6>
                                <table className="table table-dark">
                                    <tbody>
                                        <tr>
                                            <td>UUID</td>
                                            <td>{this.dictionary.uuid}</td>
                                        </tr>
                                        <tr>
                                            <td>File name</td>
                                            <td>{this.dictionary.fileName}</td>
                                        </tr>
                                        <tr>
                                            <td>Size</td>
                                            <td>{this.formatSize(this.dictionary.sizeInBytes)}</td>
                                        </tr>
                                        <tr>
                                            <td>Progress</td>
                                            <td>{this.formatSize(this.dictionary.uploadProgress)}</td>
                                        </tr>
                                        <tr>
                                            <td>Is ready?</td>
                                            <td>{this.dictionary.isReady ? 'Yes' : 'No'}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h6>Description</h6>
                                <div id="showDescription"></div>
                            </div>
                            <div className="modal-footer" id="modal-footer">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DictionaryDetails;


