import React, { Component } from 'react';
import optionsICO from './../../IMG/options.svg';
import trashICO from './../../IMG/trash.svg'

class Diccionari extends Component {
    constructor({ diccionari, handleShowDetails, handleRemove }) {
        super();

        this.diccionari = diccionari;
        this.handleShowDetails = () => { handleShowDetails(diccionari); };
        this.handleRemove = () => { handleRemove(diccionari.uuid); };

        this.onClick = this.onClick.bind(this);

        this.progress = this.getProgress(this.diccionari.sizeInBytes, this.diccionari.uploadProgress);
    }

    formatSize(sizeInBytes) {
        const units = ['B', 'Kb', 'Mb', 'Gb', 'Tb'];
        var unit = 0;
        while (sizeInBytes >= 1024) {
            sizeInBytes /= 1024;
            unit++;
        }
        return Math.floor(sizeInBytes) + ' ' + units[unit];
    }

    getProgress(sizeInBytes, uploadProgress) {
        return (uploadProgress / sizeInBytes) * 100;
    }

    onClick(e) {
        e.stopPropagation();
        this.handleShowDetails();
    }

    render() {
        return (
            <tr className="diccionari" onClick={this.onClick}>
                <td scope="row">{this.diccionari.fileName}</td>
                <td>{this.progress === 100
                    ? <></>
                    : <div className="progress">
                        <div className="progress-bar progress-bar-striped bg-info" role="progressbar"
                            aria-valuenow={this.progress} aria-valuemin="0" aria-valuemax="100" style={{ width: this.progress + '%' }}></div>
                    </div>}</td>
                <td>{this.progress === 100
                    ? this.formatSize(this.diccionari.sizeInBytes)
                    : this.formatSize(this.diccionari.uploadProgress) + "/" + this.formatSize(this.diccionari.sizeInBytes)}</td>
                <td>
                    <img src={optionsICO} alt="optionsICO" onClick={(e) => { e.stopPropagation(); this.handleShowDetails(); }} />
                </td>
                <td>
                    <img src={trashICO} alt="trashICO" onClick={(e) => { e.stopPropagation(); this.handleRemove(); }} />
                </td>
            </tr>
        )
    }
}

export default Diccionari;
