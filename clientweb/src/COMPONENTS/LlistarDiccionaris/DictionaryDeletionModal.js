import React, { Component } from 'react';


class DictionaryDeletionModal extends Component {
    constructor({ hashesUsingDictionary }) {
        super();
        this.hashesUsingDictionary = hashesUsingDictionary;
    }

    componentDidMount() {
        window.$('#dictionaryDeletionErrorModal').modal({ show: true });
    }

    render() {
        return (
            <div className="modal fade" id="dictionaryDeletionErrorModal" tabindex="0" role="dialog" aria-labelledby="dictionaryDeletionErrorModal" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">Error deleting!</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>
                                There was an error deleting the dictionary because it is currently being used in hashes in the queue that has not finished
                                processing yet.
                            </p>
                            <br />
                            <p>Please, delete or resolve the following conflicting hashes:</p>
                            <table className="table table-dark">
                                <thead>
                                    <tr>
                                        <th scope="col">Hash</th>
                                        <th scope="col">UUID</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.hashesUsingDictionary.map((element, i) => {
                                            return <DictionaryDeletionModalHash
                                                hash={element.hash}
                                                uuid={element.uuid}
                                            />
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>)
    }
}

class DictionaryDeletionModalHash extends Component {
    constructor({ hash, uuid }) {
        super();
        this.hash = hash;
        this.uuid = uuid;
    }

    render() {
        return (<tr>
            <td>{this.hash}</td>
            <td>{this.uuid}</td>
        </tr>)
    }
}

export default DictionaryDeletionModal;
