import React, { Component } from 'react';
import './../../CSS/Dictionaries.css';

class Diccionaris extends Component {
    constructor(dades) {
        super();

        this.enviarFitxer = dades.handleEnviar;
        this.handleEnviar = this.handleEnviar.bind(this);
    }

    handleEnviar() {
        const inputFitxer = document.getElementById("fitxer").files;
        if (!inputFitxer) {
            return;
        }
        const fitxer = inputFitxer[0];
        const descripcio = document.getElementById("descripcio").value;
        if (typeof descripcio !== 'string' || descripcio.length > 3000) {
            alert("Description cannot more than 3000 characters long.");
            return;
        }
        this.enviarFitxer(fitxer, descripcio);
    }

    render() {
        return (
            <div id="diccionaris">
                <div id="renderModal"></div>
                <div id="renderAlertModal"></div>
                <table className="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">File name</th>
                            <th scope="col">Downloaded</th>
                            <th scope="col">Size</th>
                            <th scope="col">Details</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody id="llistaDiccionaris">
                    </tbody>
                </table>
                <div id="formDiccionaris">
                    <form>
                        <fieldset>
                            <legend>Upload new:</legend>
                            <label htmlFor="fitxer">File</label>
                            <br />
                            <input type="file" id="fitxer" name="fitxer" className="form-control" />
                            <br />
                            <label htmlFor="descripcio">Description</label>
                            <br />
                            <textarea id="descripcio" name="descripcio" className="form-control"></textarea>
                            <br />

                            <br />
                            <button type="button" className="btn btn-outline-danger" onClick={this.handleEnviar}>Send</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        )
    }
}

export default Diccionaris;
