import React, { Component } from 'react';

export class DeleteConfirm extends Component {
    constructor({ confirmName, customText, customBody, handleOk, handleCancel, okText, cancelText }) {
        super();

        this.customText = customText;
        this.customBody = customBody;
        this.confirmName = confirmName;
        this.handleOk = handleOk ? handleOk : () => { };
        this.handleCancel = handleCancel ? handleCancel : () => { };
        this.okText = okText ? okText : 'Continue';
        this.cancelText = cancelText ? cancelText : 'Cancel';
    }

    componentDidMount() {
        window.$('#confirmModal').modal({ show: true });
    }

    render() {
        return (<div>
            <div className="modal fade" id="confirmModal" tabIndex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">Confirm</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <h5>{this.confirmName ? 'Are you sure you want to delete ' + this.confirmName + '?'
                                : (this.customText ? this.customText : 'Are you sure you want to continue?')}</h5>
                            {this.customBody && <p>{this.customBody}</p>}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.handleCancel}>
                                {this.cancelText}
                            </button>
                            <button type="button" className="btn btn-danger" data-dismiss="modal" onClick={this.handleOk}>
                                {this.okText}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>);
    }
};
