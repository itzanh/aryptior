import React, { Component } from 'react';
import ReactDOM from 'react-dom';


// IMG
import exportICO from './../IMG/export.svg';
import Alert from './Alert';

const tipusEnum = ['NTLM', 'MD4', 'MD5', 'SHA1', 'SHA-256', 'SHA-384', 'SHA-512', 'BCRYPT', 'RIPEMD128',
    'RIPEMD160', 'RIPEMD256', 'RIPEMD320', 'BLAKE', 'BLAKE2b (512)', 'BLAKE2s (256)', 'WHIRLPOOL', 'TIGER',
    'TIGER2', 'TIGER3', 'TIGER4', 'MODULAR', 'MYSQL', 'WORDPRESS'];

const tipusBusquedaEnum = ['Brute force', 'Dictionary'];

const baseDate = '0001-01-01T00:00:00';

class HashDetailsModal extends Component {
    constructor({ hash, handleSurrender, handleRemove, handleEdit, handleExport, readOnly }) {
        super();

        this.hash = hash;
        this.tipus = tipusEnum[this.hash.tipus];
        this.tipusBusqueda = tipusBusquedaEnum[this.hash.tipusBusqueda];
        this.estaResolt = this.hash.estaResolt ? 'Yes' : 'No';
        this.solucio = this.hash.estaResolt ? this.hash.solucio : '';
        this.dateCreated = this.formatejarFecha(this.hash.dateCreated);
        this.dateFinished = this.hash.estaResolt ? this.formatejarFecha(this.hash.dateFinished) : '';
        this.processingTime = this.hash.estaResolt ? this.formatTime(new Date(this.hash.dateFinished) - new Date(this.hash.dateCreated)) : '';
        this.msProcessingCount = this.formatTime(this.hash.msProcessingCount);
        this.primerCaracterUnicode = String.fromCharCode(this.hash.primerCaracterUnicode);
        this.ultimCaracterUnicode = String.fromCharCode(this.hash.ultimCaracterUnicode);
        this.readOnly = (readOnly === undefined) ? false : readOnly;

        this.handleSurrender = handleSurrender;
        this.handleRemove = handleRemove;
        this.handleExport = handleExport;
        this.handleEdit = handleEdit;

        this.delete = this.delete.bind(this);
        this.surrender = this.surrender.bind(this);
        this.mainScreen = this.mainScreen.bind(this);
        this.exportarAJSON = this.exportarAJSON.bind(this);
        this.handleEditHash = this.handleEditHash.bind(this);
        this.renderMainScreen = this.renderMainScreen.bind(this);
    }

    componentDidMount() {
        window.$('#HashDetailsModal').modal({ show: true });

        this.renderMainScreen();
    }

    componentWillUnmount() {
        window.$('#HashDetailsModal').modal('hide');
    }

    modalAlert(body, title) {
        ReactDOM.unmountComponentAtNode(this.refs.renderAlertModal);
        ReactDOM.render(<Alert
            title={title}
            body={body}
        />, this.refs.renderAlertModal);
    };

    async renderMainScreen() {
        ReactDOM.unmountComponentAtNode(this.refs.renderModalContent);
        await ReactDOM.render(<this.mainScreen />, this.refs.renderModalContent);

        window.$(() => {
            window.$('[data-toggle="tooltip"]').tooltip();
        });
        this.previewCharacterRange();
    }

    previewCharacterRange() {
        if (this.hash.tipusBusqueda == 1) {
            return;
        }
        var caracters = '';
        for (let i = this.hash.primerCaracterUnicode; i <= this.hash.ultimCaracterUnicode; i++) {
            caracters += String.fromCharCode(i);
        }
        document.getElementById('caractersPreview').innerText = caracters;
    }

    formatejarFecha(horaConnectat) {
        const fecha = new Date(horaConnectat);
        return fecha.getDate() + '/'
            + (fecha.getMonth() + 1) + '/'
            + fecha.getFullYear() + ' '
            + fecha.getHours() + ':'
            + fecha.getMinutes() + ':'
            + fecha.getSeconds();
    }

    formatTime(msTime) {
        const unitats = ['ms', 'sec', 'min', 'hour'];
        var unitat = 0;
        if (msTime >= 1000) {
            msTime /= 1000;
            unitat++;
        }
        while (unitat > 0 && msTime >= 60 && unitat < unitats.length - 1) {
            msTime /= 60;
            unitat++;
        }
        return Math.round(msTime) + " " + unitats[unitat];
    }

    formatSize(sizeInBytes) {
        const units = ['B', 'Kb', 'Mb', 'Gb', 'Tb'];
        var unit = 0;
        while (sizeInBytes >= 1024) {
            sizeInBytes /= 1024;
            unit++;
        }
        return Math.floor(sizeInBytes) + ' ' + units[unit];
    }

    exportarAJSON() {
        this.handleExport(this.hash.uuid);
    }

    surrender() {
        this.handleSurrender(this.hash.uuid)
            .then(() => {
                window.$('#HashDetailsModal').modal('hide');
            }, () => {
                this.modalAlert("Error from the server side.");
            });
    }

    delete() {
        this.handleRemove(this.hash.uuid)
            .then(() => {
                window.$('#HashDetailsModal').modal('hide');
            }, (errCode) => {
                if (errCode === -1)
                    this.modalAlert("Error from the server side.");
            });
    }

    handleEditHash() {
        ReactDOM.unmountComponentAtNode(this.refs.renderModalContent);
        ReactDOM.render(<EditHashDetailsModal
            hash={this.hash}
            handleEdit={this.handleEdit}
            modalAlert={this.modalAlert}
            renderMainScreen={this.renderMainScreen}
        />, this.refs.renderModalContent);
    }

    render() {
        return (
            <div id="modalHashDetails">
                <div className="modal fade" id="HashDetailsModal" tabIndex="-2" role="dialog" aria-labelledby="hashDetailsModal" aria-hidden="true">
                    <data id="currentHashModalID">{this.hash.uuid}</data>
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Hash details</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div id="renderAlertModal" ref="renderAlertModal"></div>
                            <div id="renderModalContent" ref="renderModalContent"></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    mainScreen() {
        return (<div>
            <div className="modal-body" id="modal-body">
                <div>
                    <h5>{this.hash.hash}</h5>
                    <span className="badge badge-pill badge-danger">{this.tipus}</span>
                    <span className="badge badge-pill badge-info">{this.tipusBusqueda}</span>
                    {this.hash.surrender && <span className="badge badge-pill badge-warning">Surrender</span>}
                    {(!this.hash.surrender && this.hash.estaResolt && this.hash.solucio === null)
                        && <span className="badge badge-pill badge-secondary">FAILED</span>}
                    <br />
                    <br />
                    <table id="hashDetails" className="hashModalDetails">
                        <tbody>
                            <tr>
                                <td>UUID</td>
                                <td>{this.hash.uuid}</td>
                            </tr>
                            <tr>
                                <td>Solved?</td>
                                <td>{this.estaResolt}</td>
                            </tr>
                            <tr>
                                <td>Cleartext password</td>
                                <td>{this.solucio}</td>
                            </tr>
                            <tr>
                                <td>Date created</td>
                                <td data-toggle="tooltip" data-placement="top" title="DD/MM/YYYY hh:mm:ss">{this.dateCreated}</td>
                            </tr>
                            <tr>
                                <td>Date finished</td>
                                <td data-toggle="tooltip" data-placement="top" title="DD/MM/YYYY hh:mm:ss">{this.dateFinished}</td>
                            </tr>
                            <tr>
                                <td>Processing time</td>
                                <td>{this.processingTime}</td>
                            </tr>
                            <tr>
                                <td>Initial test hash count</td>
                                <td>{this.hash.initialTestCount}</td>
                            </tr>
                            <tr>
                                <td>Time processing count</td>
                                <td>{this.msProcessingCount}</td>
                            </tr>
                            <tr>
                                <td>Iterations</td>
                                <td>{this.hash.iteracions}</td>
                            </tr>
                            <tr>
                                <td>Salt</td>
                                <td>{this.hash.salt}</td>
                            </tr>
                            <tr>
                                <td>Final length</td>
                                <td>{this.hash.caractersFinals}</td>
                            </tr>
                            {this.hash.tipusBusqueda === 0
                                && <tr>
                                    <td>First character</td>
                                    <td>{this.primerCaracterUnicode}</td>
                                </tr>}
                            {this.hash.tipusBusqueda === 0
                                && <tr>
                                    <td>Last character</td>
                                    <td>{this.ultimCaracterUnicode}</td>
                                </tr>}
                            {this.hash.tipusBusqueda === 1
                                ? (<tr>
                                    <td>Processing line number</td>
                                    <td>{this.hash.numLinea}</td>
                                </tr>)
                                : null}
                        </tbody>
                    </table>
                    <br />
                    {
                        this.hash.tipusBusqueda === 0
                            ? (<p id="caractersPreview" className="previewCaracters"></p>)
                            : (<div>
                                <h6>Dictionary:</h6>
                                <table id="dictionaryDetails" className="hashModalDetails">
                                    <tbody>
                                        <tr>
                                            <td>UUID</td>
                                            <td>{this.hash.diccionari.uuid}</td>
                                        </tr>
                                        <tr>
                                            <td>File name</td>
                                            <td>{this.hash.diccionari.fileName}</td>
                                        </tr>
                                        <tr>
                                            <td>Size</td>
                                            <td>{this.formatSize(this.hash.diccionari.sizeInBytes)}</td>
                                        </tr>
                                        <tr>
                                            <td>Progress</td>
                                            <td>{this.formatSize(this.hash.diccionari.uploadProgress)}</td>
                                        </tr>
                                        <tr>
                                            <td>Is ready</td>
                                            <td>{this.hash.diccionari.isReady ? 'Yes' : 'No'}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p>Description</p>
                                <pre>{this.hash.diccionari.description}</pre>
                            </div>)
                    }
                    <br />
                    <h6>Description:</h6>
                    <pre>{this.hash.descripcio}</pre>
                </div>
            </div>
            <div className="modal-footer" id="modal-footer">
                {!this.readOnly && <div id="edition">
                    <div className="btn-group mr-2">
                        {(this.hash.estaResolt || this.hash.surrender)
                            && <button type="button" className="btn btn-outline-danger" data-dismiss="modal" onClick={this.delete}>Delete</button>}
                        {!this.hash.estaResolt
                            && <button type="button" className="btn btn-outline-success" onClick={this.handleEditHash}>Edit</button>}
                        {(!this.hash.estaResolt && !this.hash.surrender)
                            && <button type="button" className="btn btn-outline-warning" onClick={this.surrender}>Surrender</button>}
                        {(!this.hash.estaResolt && this.hash.surrender)
                            && <button type="button" className="btn btn-outline-warning" onClick={this.surrender}>Unsurrender</button>}
                        <button type="button" className="btn btn-outline-info exportToJSON" onClick={this.exportarAJSON}
                            data-toggle="tooltip" data-placement="top" title="Export to JSON">
                            <img src={exportICO} alt="exportICO" />
                        </button>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>}
                {this.readOnly &&
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>}
            </div>
        </div>)
    }


}

class EditHashDetailsModal extends Component {
    constructor({ hash, renderMainScreen, handleEdit, modalAlert }) {
        super();

        this.hash = hash;
        this.tipus = tipusEnum[this.hash.tipus];
        this.tipusBusqueda = tipusBusquedaEnum[this.hash.tipusBusqueda];
        this.renderMainScreen = renderMainScreen;
        this.handleEdit = handleEdit;
        this.modalAlert = modalAlert;

        this.handleSaveEditHash = this.handleSaveEditHash.bind(this);

    }

    componentDidMount() {
        ReactDOM.unmountComponentAtNode(this.refs.modalFooter);
        ReactDOM.render(<div className="btn-group mr-2">
            <button type="button" className="btn btn-success" onClick={this.handleSaveEditHash}>Save</button>
            <button type="button" className="btn btn-secondary" onClick={this.renderMainScreen}>Cancel</button>
        </div>, this.refs.modalFooter);
    }

    render() {
        return (<div>
            <div className="modal-body">
                <h5>{this.hash.hash}</h5>
                <span className="badge badge-pill badge-danger">{this.tipus}</span>
                <span className="badge badge-pill badge-info">{this.tipusBusqueda}</span>
                {this.hash.surrender && <span className="badge badge-pill badge-warning">Surrender</span>}
                <br />
                <br />
                <table id="editHashDetails" className="table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Property name</th>
                            <th scope="col">Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>UUID</td>
                            <td>{this.hash.uuid}</td>
                        </tr>
                        <tr>
                            <td>Initial test hash count</td>
                            <td>
                                <input type="number" className="form-control" id="initialTestCount"
                                    defaultValue={this.hash.initialTestCount} ref="initialTestCount" />
                            </td>
                        </tr>
                        <tr>
                            <td>Time processing count</td>
                            <td>
                                <div className="input-group">
                                    <input type="number" id="timeLapseProcessingCount" className="form-control"
                                        name="timeLapseProcessingCount" ref="timeLapseProcessingCount" defaultValue={(
                                            (msProcessingCount) => {
                                                msProcessingCount /= 1000;
                                                while (msProcessingCount >= 60) {
                                                    msProcessingCount /= 60;
                                                }
                                                return msProcessingCount;
                                            })(this.hash.msProcessingCount)} />
                                    <div className="input-group-append">
                                        <select id="timeUnitProcessingCount" ref="timeUnitProcessingCount"
                                            className="form-control" defaultValue={(
                                                (msProcessingCount) => {
                                                    const units = ['s', 'm', 'h'];
                                                    var unit = 0;
                                                    msProcessingCount /= 1000;
                                                    while (msProcessingCount >= 60) {
                                                        msProcessingCount /= 60;
                                                        unit++;
                                                    }
                                                    return units[unit];
                                                })(this.hash.msProcessingCount)} >
                                            <option value="s">Segons</option>
                                            <option value="m">Minuts</option>
                                            <option value="h">Hores</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Final length</td>
                            <td>
                                <input type="number" className="form-control" id="caractersFinals"
                                    ref="caractersFinals" defaultValue={this.hash.caractersFinals} />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br />
                <h6>Description:</h6>
                <textarea rows="5" className="form-control" id="descripcio" ref="descripcio"
                    defaultValue={this.hash.descripcio}></textarea>
            </div>
            <div className="modal-footer" id="modal-footer" ref="modalFooter">

            </div>
        </div>)
    }

    handleSaveEditHash() {
        const updatedSettings = { uuid: this.hash.uuid };
        if (this.hash.initialTestCount !== parseInt(this.refs.initialTestCount.value)) {
            updatedSettings.initialTestCount = parseInt(this.refs.initialTestCount.value);
            if (updatedSettings.initialTestCount <= 0) {
                this.modalAlert('Initial test count cannot be less or equal to zero.', 'Validation error');
                return;
            }
        }

        if (this.hash.msProcessingCount !== this.recollirTempsEnMs()) {
            updatedSettings.msProcessingCount = this.recollirTempsEnMs();
            if (updatedSettings.msProcessingCount <= 0) {
                this.modalAlert('Time processing count cannot be less or equal to zero.', 'Validation error');
                return;
            }
        }

        if (this.hash.caractersFinals !== parseInt(this.refs.caractersFinals.value)) {
            updatedSettings.caractersFinals = parseInt(this.refs.caractersFinals.value);
            if (updatedSettings.caractersFinals <= 1) {
                this.modalAlert('Final combination length cannot be less or equal to 1.', 'Validation error');
                return;
            }
        }

        if (this.hash.descripcio !== this.refs.descripcio.value) {
            updatedSettings.descripcio = this.refs.descripcio.value;
            if (updatedSettings.descripcio.length > 3000) {
                this.modalAlert('The description cannot be longer than 3.000 characters. What did you write there?.', 'Validation error');
                return;
            }
        }

        if (Object.keys(updatedSettings).length > 1) {
            ReactDOM.unmountComponentAtNode(this.refs.modalFooter);
            ReactDOM.render(<small>
                Loading, please wait...
            </small>, this.refs.modalFooter);
            this.handleEdit(updatedSettings).then(() => {
                window.$('#HashDetailsModal').modal('hide');
            }, () => {
                this.modalAlert('Error on the server side, check the input.', 'Server error.');
            });
        } else {
            window.$('#HashDetailsModal').modal('hide');
        }
    }

    recollirTempsEnMs() {
        var timeLapse = parseInt(this.refs.timeLapseProcessingCount.value);
        const timeUnit = this.refs.timeUnitProcessingCount.selectedIndex;
        for (let i = 0; i < timeUnit; i++) {
            timeLapse *= 60;
        }

        return timeLapse * 1000;
    }
}



export default HashDetailsModal;


