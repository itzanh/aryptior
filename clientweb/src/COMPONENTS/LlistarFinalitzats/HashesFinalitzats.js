import React, { Component } from 'react';
import './../../CSS/FinishedHashes.css';

class HashesFinalitzats extends Component {
    constructor({ renderFinishedHashes }) {
        super();

        this.renderFinishedHashes = renderFinishedHashes;

        this.handleFiltrar = this.handleFiltrar.bind(this);
    }

    handleFiltrar() {
        this.renderFinishedHashes(
            document.getElementById("searchMechanism").value,
            document.getElementById("tipoHash").value,
            document.getElementById("status").value);
    }

    render() {
        return (
            <div id="hashesFinalitzats">
                <div className="row">
                    <div className="col">
                        <div id="exportar">

                        </div>
                    </div>
                    <div className="col">
                        <label htmlFor="searchMechanism">Search method:</label>
                        <select id="searchMechanism" className="form-control" name="searchMechanism" onChange={this.handleFiltrar}>
                            <option value="">Unfiltered</option>
                            <option value="0">Brute force</option>
                            <option value="1">Dictionary</option>
                        </select>
                    </div>
                    <div className="col">
                        <label htmlFor="tipoHash">Hash algorithm:</label>
                        <select name="tipoHash" id="tipoHash" className="form-control" onChange={this.handleFiltrar}>
                            <option value="">Unfiltered</option>
                            <option value="0">NTLM</option>
                            <option value="1">MD4</option>
                            <option value="2">MD5</option>
                            <option value="3">SHA-1</option>
                            <option value="4">SHA-256</option>
                            <option value="5">SHA-384</option>
                            <option value="6">SHA-512</option>
                            <option value="7">BCRYPT</option>
                            <option value="8">RIPEMD-128</option>
                            <option value="9">RIPEMD-160</option>
                            <option value="10">RIPEMD-256</option>
                            <option value="11">RIPEMD-320</option>
                            <option value="12">BLAKE</option>
                            <option value="13">BLAKE2b (512)</option>
                            <option value="14">BLAKE2s (256)</option>
                            <option value="15">WHIRLPOOL</option>
                            <option value="16">Tiger</option>
                            <option value="17">Tiger2</option>
                            <option value="18">Tiger3 192</option>
                            <option value="19">Tiger4 192</option>
                            <option value="20">Modular ($x$salt$hash)</option>
                            <option value="21">MySQL (mysql.user)</option>
                            <option value="22">Wordpress (wp_users)</option>
                        </select>
                    </div>
                    <div className="col">
                        <label htmlFor="status">Status:</label>
                        <select id="status" className="form-control" name="status" onChange={this.handleFiltrar}>
                            <option value="">Unfiltered</option>
                            <option value="0">Solved</option>
                            <option value="1">Surrendered</option>
                            <option value="2">Failed</option>
                        </select>
                    </div>
                </div>
                <div id="renderModal"></div>
                <table className="HashFinalitzat table table-dark">
                    <thead>
                        <tr>
                            <th>Hash</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Cleartext password</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody id="llistaHashesFinalitzats">

                    </tbody>
                </table>
                <div id="renderNoReultsFound"></div>
            </div>
        )
    }
}

export default HashesFinalitzats;
