import React, { Component } from 'react';

// IMG
import optionsICO from './../../IMG/options.svg';

const tipusEnum = ['NTLM', 'MD4', 'MD5', 'SHA1', 'SHA-256', 'SHA-384', 'SHA-512', 'BCRYPT', 'RIPEMD128',
    'RIPEMD160', 'RIPEMD256', 'RIPEMD320', 'BLAKE', 'BLAKE2b (512)', 'BLAKE2s (256)', 'WHIRLPOOL', 'TIGER',
    'TIGER2', 'TIGER3', 'TIGER4', 'MODULAR', 'MYSQL', 'WORDPRESS'];

const tipusBusquedaEnum = ['Brute force', 'Dictionary'];

class HashFinalitzat extends Component {
    constructor({ objecte, handleDetails }) {
        super();

        this.hash = objecte;
        this.tipus = tipusEnum[objecte.tipus];
        this.tipusBusqueda = tipusBusquedaEnum[this.hash.tipusBusqueda];

        this.handleDetails = handleDetails;

        this.handleClickDetails = this.handleClickDetails.bind(this);
    }

    handleClickDetails() {
        this.handleDetails(this.hash);
    }

    render() {
        return (
            <tr onClick={this.handleClickDetails}>
                <td>{this.hash.hash}</td>
                <td>
                    <span className="badge badge-pill badge-danger">{this.tipus}</span>
                </td>
                <td>
                    <span className="badge badge-pill badge-info">{this.tipusBusqueda}</span>
                </td>
                <td>
                    {this.hash.surrender && <span className="badge badge-pill badge-warning">Surrender</span>}
                    {(!this.hash.surrender && this.hash.estaResolt && this.hash.solucio === null)
                        && <span className="badge badge-pill badge-secondary">FAILED</span>}
                </td>
                <td>{this.hash.solucio}</td>
                <td><img src={optionsICO} /></td>
            </tr>
        )
    }
}

export default HashFinalitzat;
