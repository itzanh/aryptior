import React, { Component } from 'react';

// CSS
import './../CSS/Login.css';
import './../CSS/Login.scss';

// IMG
import user from './../IMG/user.svg';
import logo from './../logo.png';

class Login extends Component {
    constructor({ enviarLogin, loginFailed }) {
        super();

        this.enviarLogin = enviarLogin;
        this.loginFailed = loginFailed ? "Login failed!" : "";
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleLogin() {
        const pwd = document.getElementById("pwd").value;
        this.enviarLogin(pwd, document.getElementById("recordarLogin").checked);
        document.getElementById("loggingIn").innerText = "Logging in...";
    }

    render() {
        return (
            <div id="loginScreen">
                <img src={logo} width="30" height="30" className="d-inline-block align-top imatgeMenu" alt="" />
                <header className="App-header">
                    <img src={user} className="App-logo" alt="logo" />
                    <input type="password" id="pwd" name="pwd" className="pwd" />
                    <a className="brk-btn" onClick={this.handleLogin}>
                        LOGIN
                    </a>
                    <br />
                    <small id="loggingIn"></small>
                    <small id="incorrectPassword"></small>
                    <br />
                    <div id="askRememberLogin">
                        <input type="checkbox" id="recordarLogin" name="recordarLogin" />
                        <label htmlFor="recordarLogin">Remember login?</label>
                    </div>
                </header>
            </div>
        )
    }
}

export default Login;
