import React, { Component } from 'react';

// IMG
import logo from './../logo.png';


class VersionMismatchError extends Component {
    render() {
        return (
            <div id="versionMismatchScreen">
                <img src={logo} width="30" height="30" className="d-inline-block align-top imatgeMenu" alt="" />
                <h5>aryptiOR</h5>
                <div id="versionMismatchError">
                    <h3>FATAL ERROR!</h3>
                    <h1>VERSION MISMATCH ERROR</h1>
                    <div id="description">
                        <p>The local version of this software (web client) does not use the same version of the protocol as the one that uses your remote server.</p>
                        <p>Please, upgrade both the static files of the website and the processing server to the same (or, if it's possible, to lastest) version of this software and try again.</p>
                    </div>
                    <small>Or waybe try to connect to another host.</small>
                </div>
            </div>
        )
    }
};

export default VersionMismatchError;


