// REACT
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class ConfigureAllModal extends Component {

    constructor({ handleConfigureAll }) {
        super();

        this.handleConfigureAll = handleConfigureAll;

        this.handleSave = this.handleSave.bind(this);
        this.handleSend = this.handleSend.bind(this);
    }

    componentDidMount() {
        window.$('#configureAllModal').modal({ show: true });
    }

    handleSave() {
        const settings = {};

        const changeThreads = this.refs.changeThreads.checked;
        const changeForceUpdateTime = this.refs.changeForceUpdateTime.checked;
        const changeProcessRainbowsOnIdle = this.refs.changeProcessRainbowsOnIdle.checked;
        const changeDiscoveryWaitTime = this.refs.changeDiscoveryWaitTime.checked;
        const changeDownloadBackups = this.refs.changeDownloadBackups.checked;
        const changeBackupMaxSize = this.refs.changeBackupMaxSize.checked;


        if (changeThreads) {
            settings.threads = parseInt(this.refs.threads.value);
        }
        if (changeForceUpdateTime) {
            var forceUpdateTime = parseInt(this.refs.forceUpdateTime.value);
            var timeUnitProcessingCount = this.refs.timeUnitProcessingCount.value;

            if (timeUnitProcessingCount === 's') {
                forceUpdateTime *= 1000;
            } else if (timeUnitProcessingCount === 'm') {
                forceUpdateTime *= 60000;
            }

            settings.forceUpdateTime = forceUpdateTime;
        }
        if (changeProcessRainbowsOnIdle) {
            settings.processRainbowsOnIdle = this.refs.processRainbowsOnIdle.checked;
        }
        if (changeDiscoveryWaitTime) {
            settings.discoveryWaitTime = parseInt(this.refs.discoveryWaitTime.value);
        }
        if (changeDownloadBackups) {
            settings.downloadBackups = this.refs.downloadBackups.checked;
        }
        if (changeBackupMaxSize) {
            const sizeLimitUnit = parseInt(this.refs.sizeLimitUnit.value);
            const backupMaxSize = this.convertSizeUnit(parseInt(this.refs.backupMaxSize.value), sizeLimitUnit);

            settings.backupMaxSize = backupMaxSize;
        }

        console.log(settings);
        this.handleSend(settings);
    };

    async handleSend(settings) {
        window.$('#configureAllModal').modal('hide');
        await ReactDOM.unmountComponentAtNode(document.getElementById("renderClientDetails"));
        const component = <ShowConfigureAllProgress />;
        await ReactDOM.render(component, document.getElementById("renderClientDetails"));
        this.showProgress = this.showProgress.bind(component);
        await this.handleConfigureAll(settings, this.showProgress);
    }

    showProgress(current, total) {

        const progress = (current / total) * 100;

        if (current === total) {
            window.$('#configureAllProgress').modal('hide');
            return;
        }

        ReactDOM.unmountComponentAtNode(document.getElementById("configureAllProgressRender"));
        ReactDOM.render(<div>
            <h6>{current} / {total}</h6>
            <div className="progress">
                <div className="progress-bar progress-bar-striped bg-danger" role="progressbar"
                    style={{ width: progress + '%' }} aria-valuenow={progress} aria-valuemin="0" aria-valuemax="100">{progress}%</div>
            </div>
        </div>, document.getElementById("configureAllProgressRender"));

    }

    convertSizeUnit(size, unit) {
        for (let i = 0; i < unit; i++) {
            size *= 1000;
        }
        return size;
    }

    render() {
        return (
            <div>
                <div className="modal fade" id="configureAllModal" tabIndex="-1" role="dialog" aria-labelledby="configureAllModal" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered  modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">Apply settings to all clients</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <table className="table table-dark">
                                    <thead>
                                        <tr>
                                            <th scope="col">Change?</th>
                                            <th scope="col">Property</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label className="switch">
                                                    <input type="checkbox" id="changeThreads" ref="changeThreads" />
                                                    <span className="slider"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label htmlFor="threads">Number of threads:</label>
                                                <input type="number" id="threads" ref="threads" className="form-control" min="1" max="64" defaultValue="4"
                                                    onChange={() => { this.refs.changeThreads.checked = true; }} />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label className="switch">
                                                    <input type="checkbox" id="changeForceUpdateTime" ref="changeForceUpdateTime" />
                                                    <span className="slider"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label htmlFor="forceUpdateTime">Force refresh timeout:</label>
                                                <div className="input-group">
                                                    <input type="number" id="forceUpdateTime" ref="forceUpdateTime" className="form-control" min="1" defaultValue="0"
                                                        onChange={() => { this.refs.changeForceUpdateTime.checked = true; }} />
                                                    <div className="input-group-append">
                                                        <select id="timeUnitProcessingCount" ref="timeUnitProcessingCount" className="form-control" defaultValue="s"
                                                            onChange={() => { this.refs.changeForceUpdateTime.checked = true; }} >
                                                            <option value="s">Seconds</option>
                                                            <option value="m">Minutes</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label className="switch">
                                                    <input type="checkbox" id="changeProcessRainbowsOnIdle" ref="changeProcessRainbowsOnIdle" />
                                                    <span className="slider"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label className="switch">
                                                    <input type="checkbox" id="processRainbowsOnIdle" ref="processRainbowsOnIdle" defaultChecked="true"
                                                        onChange={() => { this.refs.changeProcessRainbowsOnIdle.checked = true; }} />
                                                    <span className="slider"></span>
                                                </label>
                                                <label htmlFor="processRainbowsOnIdle">Procesar rainbows mentres esta procrastinant?</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label className="switch">
                                                    <input type="checkbox" id="changeDiscoveryWaitTime" ref="changeDiscoveryWaitTime" />
                                                    <span className="slider"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label htmlFor="discoveryWaitTime">Discovery wait time (ms):</label>
                                                <input type="number" id="discoveryWaitTime" ref="discoveryWaitTime" className="form-control" min="1"
                                                    defaultValue="3000" onChange={() => { this.refs.changeDiscoveryWaitTime.checked = true; }} />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label className="switch">
                                                    <input type="checkbox" id="changeDownloadBackups" ref="changeDownloadBackups" />
                                                    <span className="slider"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label className="switch">
                                                    <input type="checkbox" id="downloadBackups" ref="downloadBackups" defaultChecked="true"
                                                        onChange={() => { this.refs.changeDownloadBackups.checked = true; }} />
                                                    <span className="slider"></span>
                                                </label>
                                                <label htmlFor="downloadBackups">Download backups?</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label className="switch">
                                                    <input type="checkbox" id="changeBackupMaxSize" ref="changeBackupMaxSize" />
                                                    <span className="slider"></span>
                                                </label>
                                            </td>
                                            <td>
                                                <label htmlFor="backupMaxSize">Download max size:</label>
                                                <div className="input-group">
                                                    <input type="number" id="backupMaxSize" ref="backupMaxSize" className="form-control" min="0" defaultValue="0"
                                                        onChange={() => { this.refs.changeBackupMaxSize.checked = true; }} />
                                                    <div className="input-group-append">
                                                        <select id="sizeLimitUnit" ref="sizeLimitUnit" className="form-control" defaultValue="0"
                                                            onChange={() => { this.refs.changeBackupMaxSize.checked = true; }} >
                                                            <option value="0">Bytes</option>
                                                            <option value="1">Kb</option>
                                                            <option value="2">Mb</option>
                                                            <option value="3">Gb</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-primary" onClick={this.handleSave}>Apply</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class ShowConfigureAllProgress extends Component {

    constructor() {
        super();

    }

    componentDidMount() {
        window.$('#configureAllProgress').modal({ show: true });

        ReactDOM.render(<div>
            <h6>Applying settings...</h6>
            <div className="progress">
                <div className="progress-bar progress-bar-striped bg-danger" role="progressbar"
                    style={{ width: '0%' }} aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
            </div>
        </div>, this.refs.configureAllProgressRender);
    }

    render() {
        return (
            <div>
                <div className="modal" id="configureAllProgress" tabIndex="-1" role="dialog" aria-labelledby="configureAllProgress" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">Changing settings</h5>
                            </div>
                            <div className="modal-body" id="configureAllProgressRender" ref="configureAllProgressRender">

                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal" id="closeModal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
};

export default ConfigureAllModal;
