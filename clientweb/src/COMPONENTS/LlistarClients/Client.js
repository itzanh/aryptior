// REACT
import React, { Component } from 'react';

// IMG
import editICO from './../../IMG/edit.svg';

class Client extends Component {
    constructor({ uuid, addr, processing, timeConnected, showClientDetails, hashRatePerMs, lastNewPartRequested, lastReportedPart }) {
        super();
        this.uuid = uuid;
        this.addr = addr.split(':')[0];
        this.processing = processing ? 'Yes' : 'No';
        this.timeConnection = this.formatejarFecha(timeConnected);
        this.timeConnected = this.getTimeConnected(timeConnected);
        this.showClientDetails = showClientDetails;

        this.hashRatePerMs = hashRatePerMs === -1 ? "-" : hashRatePerMs;
        this.lastNewPartRequested = new Date(lastNewPartRequested).valueOf() === new Date('0001-01-01T00:00:00').valueOf() ?
            "-" : this.formatejarTemps(new Date() - new Date(lastNewPartRequested));

        this.lastReportedPart = new Date(lastReportedPart).valueOf() === new Date('0001-01-01T00:00:00').valueOf() ?
            "-" : this.formatejarTemps(new Date() - new Date(lastReportedPart));

        this.showDetails = this.showDetails.bind(this);
    }

    formatejarFecha(horaConnectat) {
        const fecha = new Date(horaConnectat);
        return fecha.getDate() + '/'
            + (fecha.getMonth() + 1) + '/'
            + fecha.getFullYear() + ' '
            + fecha.getHours() + ':'
            + fecha.getMinutes() + ':'
            + fecha.getSeconds();
    }

    getTimeConnected(horaConnectat) {
        return Math.floor(((new Date() - new Date(horaConnectat)) / 1000) / 60);
    }

    formatejarTemps(ms) {
        const unitats = ['ms', 's', 'min', 'hrs'];
        var unitat = 0;
        if (ms > 1000) {
            unitat++;
            ms /= 1000;
        }
        while (ms > 60 && unitat < unitats.length - 1) {
            ms /= 60;
            unitat++;
        }
        return Math.floor(ms) + " " + unitats[unitat];
    }

    showDetails() {
        this.showClientDetails(this.uuid);
    }

    render() {
        return (
            <tr>
                <td>{this.uuid}</td>
                <td>{this.addr}</td>
                <td>{this.processing}</td>
                <td>{this.timeConnection}</td>
                <td>{this.timeConnected} minutes.</td>
                <td>{this.hashRatePerMs}</td>
                <td>{this.lastNewPartRequested}</td>
                <td>{this.lastReportedPart}</td>
                <td>
                    <img src={editICO} alt="detalls..." onClick={this.showDetails} />
                </td>
            </tr>
        )
    }
}

export default Client;
