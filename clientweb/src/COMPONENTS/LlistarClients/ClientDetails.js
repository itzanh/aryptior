import React, { Component } from 'react';
import ReactDOM from 'react-dom'

class ClientDetails extends Component {
    constructor({ uuid, getClientSettings, getClientBackups, exportToJSON, saveData, handleKickClient, handleBanClient }) {
        super();

        this.uuid = uuid;
        this.getClientSettings = getClientSettings;
        this.getClientBackups = getClientBackups;
        this.exportToJSON = exportToJSON;
        this.saveData = saveData;
        this.handleKickClient = handleKickClient;
        this.handleBanClient = handleBanClient;

        this.renderConfig = this.renderConfig.bind(this);
        this.renderBackups = this.renderBackups.bind(this);
        this.handleJSONfile = this.handleJSONfile.bind(this);
        this.handleSaveData = this.handleSaveData.bind(this);
        this.handleExportBackups = this.handleExportBackups.bind(this);
        this.kickClient = this.kickClient.bind(this);
        this.banClient = this.banClient.bind(this);
    }

    componentDidMount() {
        window.$('#clientsModalCenter').modal({ show: true });

        this.renderConfig();
    }

    renderConfig() {
        // change the active tab
        ReactDOM.unmountComponentAtNode(document.getElementById('renderClientModalTabs'));
        ReactDOM.render(
            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <a className="nav-link active" href="#" onClick={this.renderConfig}>Settings</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="#" onClick={this.renderBackups}>Backups</a>
                </li>
            </ul>, document.getElementById('renderClientModalTabs'));

        // replace the action buttons on the footer of the modal window
        ReactDOM.unmountComponentAtNode(document.getElementById('clientModalFooter'));
        ReactDOM.render(
            <div>
                <button type="button" className="btn btn-danger" data-dismiss="modal">Tancar</button>
            </div>
            , document.getElementById('clientModalFooter'));

        // write loading message
        ReactDOM.unmountComponentAtNode(document.getElementById('renderClientModalTab'));
        ReactDOM.render(<p>Connecting with the remote client...</p>, document.getElementById('renderClientModalTab'));

        // request and render the client settings.
        this.getClientSettings(this.uuid).then((clientSettings) => {
            // render the settings component
            ReactDOM.unmountComponentAtNode(document.getElementById('renderClientModalTab'));
            ReactDOM.render(<ClientConfig
                clientSettings={clientSettings}
                handleJSONfile={this.handleJSONfile}
            />, document.getElementById('renderClientModalTab'));

            // replace the action buttons on the footer of the modal window
            ReactDOM.unmountComponentAtNode(document.getElementById('clientModalFooter'));
            ReactDOM.render(
                <div className="btn-group">
                    <button type="button" className="btn btn-outline-danger" onClick={this.banClient}>Ban IP</button>
                    <button type="button" className="btn btn-outline-light" onClick={this.kickClient}>Kick</button>
                    <div className="btn-group">
                        <button type="button" className="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Import/Export
                        </button>
                        <div className="dropdown-menu">
                            <a className="dropdown-item" href="#" onClick={() => { this.exportToJSON(clientSettings, "clientSettings.json"); }}>Export to JSON</a>
                            <a className="dropdown-item" href="#" onClick={() => { document.getElementById("importJSONfile").click(); }}>Import JSON</a>
                        </div>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="clientDetailsBtnSave" className="btn btn-primary" onClick={this.handleSaveData}>Save changes</button>
                    </div>
                </div>
                , document.getElementById('clientModalFooter'));
        }, () => {
            // handle the error in case the server responded with an error message
            alert("Error gathering client settings.");
            ReactDOM.unmountComponentAtNode(document.getElementById('renderClientModalTab'));
            ReactDOM.render(<p>
                Error gathering client settings. Could not properly communicate with the client.
                    </p>, document.getElementById('renderClientModalTab'));
        });
    }

    kickClient() {
        this.handleKickClient(this.uuid).then(() => {
            window.$('#clientsModalCenter').modal('hide');
        }, () => {
            alert('Error on the server side.');
        });
    };

    banClient() {
        this.handleBanClient(this.uuid).then(() => {
            window.$('#clientsModalCenter').modal('hide');
        }, () => {
            alert('Error on the server side.');
        });

    };

    async handleJSONfile() {
        if (!document.getElementById("importJSONfile")) return;
        const files = document.getElementById("importJSONfile").files;
        if (files.length === 1) {
            const file = files[0];
            const jsonToParse = await this.readBlobFileAsText(file);
            var clientSettings;

            try {
                clientSettings = JSON.parse(jsonToParse);
            } catch (e) {
                alert("Could not read the JSON file. " + e);
                return;
            }

            ReactDOM.unmountComponentAtNode(document.getElementById('renderClientModalTab'));
            ReactDOM.render(<ClientConfig
                clientSettings={clientSettings}
                handleJSONfile={this.handleJSONfile}
            />, document.getElementById('renderClientModalTab'));
        }
    }

    readBlobFileAsText(fitxer) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsText(fitxer);
            reader.onload = () => {
                resolve(reader.result);
            }
            reader.onerror = () => {
                reject();
            }
        })
    }

    renderBackups() {
        // change the active tab
        ReactDOM.unmountComponentAtNode(document.getElementById('renderClientModalTabs'));
        ReactDOM.render(
            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <a className="nav-link" href="#" onClick={this.renderConfig}>Settings</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link active" href="#" onClick={this.renderBackups}>Backups</a>
                </li>
            </ul>, document.getElementById('renderClientModalTabs'));

        // replace the action buttons on the footer of the modal window
        ReactDOM.unmountComponentAtNode(document.getElementById('clientModalFooter'));
        ReactDOM.render(
            <div>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            , document.getElementById('clientModalFooter'));

        // write loading message
        ReactDOM.unmountComponentAtNode(document.getElementById('renderClientModalTab'));
        ReactDOM.render(<p>Connecting with the remote client...</p>, document.getElementById('renderClientModalTab'));

        // request and render the list of client backups
        this.getClientBackups(this.uuid).then((clientBackups) => {
            // render the html table to hold the backup list
            ReactDOM.unmountComponentAtNode(document.getElementById('renderClientModalTab'));
            ReactDOM.render(<ClientBackupsList />, document.getElementById('renderClientModalTab'));

            // map the backup list to html elements to render
            ReactDOM.unmountComponentAtNode(document.getElementById('clientBackupsList'));
            ReactDOM.render(
                clientBackups.map((element, i) => {
                    return (<ClientBackup
                        key={i}

                        clientBackup={element}
                    />)
                }), document.getElementById('clientBackupsList'));

            // replace the action button to export to JSON
            ReactDOM.unmountComponentAtNode(document.getElementById('clientModalFooter'));
            ReactDOM.render(
                <div className="btn-group">
                    <button type="button" id="clientDetailsBtnSave" className="btn btn-outline-light" onClick={() => {
                        this.handleExportBackups(clientBackups);
                    }}>Export to JSON</button>
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                , document.getElementById('clientModalFooter'));
        }, () => {
            // handle the error in case the server responded with an error message
            alert("Error gathering client backups.");
            ReactDOM.unmountComponentAtNode(document.getElementById('renderClientModalTab'));
            ReactDOM.render(<p>
                Error gathering client backups. Could not properly communicate with the client.
                    </p>, document.getElementById('renderClientModalTab'));
        });
    }

    getFormData() {
        const threads = parseInt(document.getElementById("threads").value);
        var forceUpdateTime = parseInt(document.getElementById("forceUpdateTime").value);
        const timeUnitProcessingCount = document.getElementById("timeUnitProcessingCount").value;
        const processRainbowsOnIdle = document.getElementById("processRainbowsOnIdle").checked;
        const discoveryWaitTime = parseInt(document.getElementById("discoveryWaitTime").value);
        const downloadBackups = document.getElementById("downloadBackups").checked;
        const backupMaxSize = parseInt(document.getElementById("backupMaxSize").value);

        forceUpdateTime *= 1000; // from seconds to ms
        if (timeUnitProcessingCount === 'm') {
            forceUpdateTime *= 60;
        }

        return {
            threads,
            forceUpdateTime,
            processRainbowsOnIdle,
            discoveryWaitTime,
            downloadBackups,
            backupMaxSize
        }
    }

    handleSaveData() {
        const formData = this.getFormData();

        if (formData.threads <= 0 || formData.threads > 64) {
            alert("Threads must be between 1 and 64.");
            return;
        }
        if (formData.forceUpdateTime < 0) {
            alert("Reconnection time cannot be less than 0.");
            return;
        }
        if (formData.discoveryWaitTime <= 0) {
            alert("Discovery wait time cannot be less than 0.");
            return;
        }

        document.getElementById("clientDetailsBtnSave").disabled = true;

        this.saveData(this.uuid, formData).then((result) => {
            if (result) {
                window.$('#clientsModalCenter').modal('hide');
            } else {
                document.getElementById("clientDetailsBtnSave").disabled = false;
                alert("Error en the server side.");
            }
        });
    }

    handleExportBackups(clientBackups) {
        this.openExternalFile(JSON.stringify(clientBackups));
    }

    openExternalFile(text) {
        const fitxerTemp = new Blob([text]);
        const url = URL.createObjectURL(fitxerTemp);
        window.open(url, '_blank');
    }

    render() {
        return (
            <div id="clientDetails">
                <div className="modal fade" id="clientsModalCenter" tabIndex="-1" role="dialog" aria-labelledby="clientDetailsCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLongTitle">Details of the client {this.uuid}</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <data id="modalClientCurrentUUID">{this.uuid}</data>
                            </div>
                            <div className="modal-body">
                                <div id="renderClientModalTabs"></div>
                                <br />
                                <div id="renderClientModalTab"></div>
                            </div>
                            <div className="modal-footer" id="clientModalFooter">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class ClientConfig extends Component {
    constructor({ clientSettings, handleJSONfile }) {
        super();

        this.clientSettings = clientSettings;
        this.handleJSONfile = handleJSONfile;
    }

    render() {
        return (
            <div id="modalTabClientConfig">
                <div className="row">
                    <div className="col">
                        <label htmlFor="threads">Number of threads:</label>
                        <input type="number" id="threads" className="form-control" min="1" max="64" defaultValue={this.clientSettings.threads} />
                    </div>
                    <div className="col">
                        <label htmlFor="forceUpdateTime">Force refresh timeout:</label>
                        <div className="input-group">
                            <input type="number" id="forceUpdateTime" className="form-control" min="1" defaultValue={(this.clientSettings.forceUpdateTime / 1000)} />
                            <div className="input-group-append">
                                <select id="timeUnitProcessingCount" className="form-control" defaultValue="s">
                                    <option value="s">Seconds</option>
                                    <option value="m">Minutes</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <label className="switch">
                    <input type="checkbox" id="processRainbowsOnIdle" defaultChecked={this.clientSettings.processRainbowsOnIdle} />
                    <span className="slider"></span>
                </label>
                <label htmlFor="processRainbowsOnIdle">Process rainbows while is procrastinating?</label>
                <br />
                <label htmlFor="discoveryWaitTime">Discovery wait time (ms):</label>
                <input type="number" id="discoveryWaitTime" className="form-control" min="1" defaultValue={this.clientSettings.discoveryWaitTime} />
                <br />
                <label className="switch">
                    <input type="checkbox" id="downloadBackups" defaultChecked={this.clientSettings.downloadBackups} />
                    <span className="slider"></span>
                </label>
                <label htmlFor="downloadBackups">Download backups?</label>
                <br />
                <label htmlFor="backupMaxSize">Backup maximum size?</label>
                <input type="number" id="backupMaxSize" className="form-control" min="0" defaultValue={this.clientSettings.backupMaxSize} />
                <input type="file" id="importJSONfile" onChange={this.handleJSONfile} />
            </div>
        )
    }
}



class ClientBackupsList extends Component {
    constructor() {
        super();

    }

    render() {
        return (
            <table className="table table-dark">
                <thead>
                    <tr>
                        <th scope="col">UUID</th>
                        <th scope="col">Date</th>
                        <th scope="col">Size</th>
                        <th scope="col">Downloaded</th>
                        <th scope="col">Progress</th>
                    </tr>
                </thead>
                <tbody id="clientBackupsList"></tbody>
            </table>
        )
    }
}



class ClientBackup extends Component {
    constructor({ clientBackup }) {
        super();

        this.clientBackup = clientBackup;
        this.date = this.formatejarFecha(this.clientBackup.date);
        this.size = this.formatejarTamany(this.clientBackup.size);
        this.downloaded = this.formatejarTamany(this.clientBackup.downloaded);
        this.progress = this.getProgress(this.clientBackup.size, this.clientBackup.downloaded);
    }

    getProgress(size, downloaded) {
        return (downloaded / size) * 100;
    }

    formatejarFecha(horaConnectat) {
        const fecha = new Date(horaConnectat);
        return fecha.getDate() + '/'
            + (fecha.getMonth() + 1) + '/'
            + fecha.getFullYear() + ' '
            + fecha.getHours() + ':'
            + fecha.getMinutes() + ':'
            + fecha.getSeconds();
    }

    formatejarTamany(tamany) {
        const unitats = ['B', 'Kb', 'Mb', 'Gb', 'Tb'];
        var unitat = 0;
        while (tamany > 1024) {
            tamany /= 1024;
            unitat++;
        }
        return Math.floor(tamany) + ' ' + unitats[unitat];
    }

    render() {
        return (
            <tr>
                <td>{this.clientBackup.uuid}</td>
                <td>{this.date}</td>
                <td>{this.size}</td>
                <td>{this.downloaded}</td>
                <td>
                    <div className="progress">
                        <div className="progress-bar progress-bar-striped" role="progressbar" style={{ width: this.progress + '%' }}
                            aria-valuenow={this.progress} aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                </td>
            </tr>
        )
    }
}

export default ClientDetails;
