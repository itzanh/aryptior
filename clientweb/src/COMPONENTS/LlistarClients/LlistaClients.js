// REACT
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// CSS
import './../../CSS/Clients.css';

// IMG
import exportICO from './../../IMG/export.svg';

// COMPONENTS
import ConfigureAllModal from './ConfigureAllModal';

class LlistaClients extends Component {
    constructor({ handleRenderClients, handleConfigureAll, handleSearch, handleExport }) {
        super();

        this.handleRenderClients = handleRenderClients;
        this.handleConfigureAll = handleConfigureAll;
        this.handleExport = handleExport;
        this.handleSearch = handleSearch;
        this.t = null;

        this.handleOpenConfigureAll = this.handleOpenConfigureAll.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    componentDidMount() {
        window.$('[data-toggle="tooltip"]').tooltip();

        this.t = setInterval(() => {
            this.handleRenderClients();
        }, 60000);
    }

    componentWillUnmount() {
        if (this.t !== null) {
            clearInterval(this.t);
        }
        window.$('#clientsModalCenter').modal('hide');
    }

    handleOpenConfigureAll() {
        ReactDOM.unmountComponentAtNode(document.getElementById("renderClientDetails"));
        ReactDOM.render(
            <ConfigureAllModal
                handleConfigureAll={this.handleConfigureAll}
            />, document.getElementById("renderClientDetails"));
    }

    onSearch() {
        this.handleSearch(document.getElementById("searchInput").value);
    }

    render() {
        return (
            <div id="llistaClients">
                <div id="renderClientDetails"></div>
                <nav className="navbar center navbar-dark bg-dark navbar-expand-lg">
                    <div className="collapse navbar-collapse" id="navbarText">
                        <ul className="navbar-nav mr-auto btn-group text-center mx-auto">
                            <li className="nav-item">
                                <button type="button" className="btn btn-outline-danger" onClick={this.handleOpenConfigureAll}>Configure all</button>
                            </li>
                            <li className="nav-item">
                                <button type="button" className="btn btn-outline-light exportToJSON" onClick={this.handleExport}
                                    data-toggle="tooltip" data-placement="top" title="Export to JSON">
                                    <img src={exportICO} alt="exportICO" />
                                </button>
                            </li>
                            <li className="nav-item">
                                <div className="input-group mb-3">
                                    <input id="searchInput" className="form-control mr-sm-2" type="search"
                                        placeholder="Search by address or UUID" aria-label="Search" onChange={this.onSearch} />
                                    <div className="input-group-append">
                                        <button className="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <table className="TaulaClient table table-dark">
                    <thead>
                        <tr>
                            <th scope="col">UUID</th>
                            <th scope="col">Address</th>
                            <th scope="col">Processing?</th>
                            <th scope="col">Connection time</th>
                            <th scope="col">Time connected</th>
                            <th scope="col">Rate (hash/ms)</th>
                            <th scope="col">Last request</th>
                            <th scope="col">Last report</th>
                            <th scope="col">Details</th>
                        </tr>
                    </thead>
                    <tbody id="llistaClientsConnectats">

                    </tbody>
                    <div id="renderEmptyResultMessage">

                    </div>
                </table>
            </div>
        )
    }
}

export default LlistaClients;
