import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// CSS
import './../CSS/Login.css';
import './../CSS/Login.scss';

// IMG
import key from './../IMG/key.svg';
import logo from './../logo.png';

class ChangePWD extends Component {
    constructor({ handleChangePWD, handleCancel }) {
        super();

        this.handleChangePWD = handleChangePWD;
        this.handleCancel = handleCancel;

        this.onChangePWD = this.onChangePWD.bind(this);
        this.optionButtons = this.optionButtons.bind(this);
    }

    componentDidMount() {
        ReactDOM.render(<this.optionButtons />, document.getElementById("loginScreenChanging"));
    }

    generateSaltn(size = 32) {
        const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var str = '';
        for (var i = 0; i < size; i++) {
            str += (characters[Math.floor(Math.random() * characters.length)]);
        }
        return str;
    }

    onChangePWD() {
        const pwd = this.refs.pwd.value;
        if (pwd != this.refs.rpwd.value) {
            alert("Passwords do not match!");
        }
        this.handleChangePWD(pwd, 256000, this.generateSaltn());

        ReactDOM.unmountComponentAtNode(document.getElementById("loginScreenChanging"));
        ReactDOM.render(<p>Setting new password...</p>, document.getElementById("loginScreenChanging"));
    }

    optionButtons() {
        return (<div>
            <a className="brk-btn" onClick={this.onChangePWD}>
                CHANGE PASSWORD
            </a>
            <br />
            <a className="brk-btn cancel" onClick={this.handleCancel}>
                CANCEL
            </a>
        </div>);
    }

    render() {
        return (
            <div id="loginScreen">
                <img src={logo} width="30" height="30" className="d-inline-block align-top imatgeMenu" alt="" />
                <header className="App-header">
                    <img src={key} className="App-logo" alt="logo" />
                    <br />
                    <label htmlFor="pwd">New password:</label>
                    <input type="password" id="pwd" name="pwd" className="pwd" ref="pwd" />
                    <label htmlFor="rpwd">Repeat new password:</label>
                    <input type="password" id="rpwd" name="rpwd" className="pwd" ref="rpwd" />

                    <div id="loginScreenChanging">

                    </div>

                    <br />
                </header>
            </div>
        )
    }
}

export default ChangePWD;
