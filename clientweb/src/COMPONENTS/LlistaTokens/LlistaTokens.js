// REACT
import React, { Component } from 'react';
import './../../CSS/Tokens.css';

class LlistaTokens extends Component {
    constructor({ handleRevokeAll }) {
        super();

        this.handleRevokeAll = handleRevokeAll;
    }

    render() {
        return (
            <div id="tokensTab" className="table table-dark">
                <button type="button" className="btn btn-outline-danger" onClick={this.handleRevokeAll}>Revoke All</button>
                <table>
                    <thead>
                        <tr>
                            <th scope="col">IP</th>
                            <th scope="col">Token</th>
                            <th scope="col">Last used</th>
                            <th scope="col">Revocar</th>
                        </tr>
                    </thead>
                    <tbody id="renderTokens">

                    </tbody>
                </table>
            </div>
        )
    }
}

export default LlistaTokens;
