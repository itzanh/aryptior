// REACT
import React, { Component } from 'react';

// IMG
import trashICO from './../../IMG/trash.svg'

class Token extends Component {
    constructor({ addr, lastUsed, token, current, handleRevokeToken }) {
        super();

        this.addr = addr;
        this.lastUsed = this.formatejarFecha(lastUsed);
        this.current = current;
        this.token = token;

        this.handleRevokeToken = handleRevokeToken;

        this.handleRevoke = this.handleRevoke.bind(this);
    }

    handleRevoke() {
        this.handleRevokeToken(this.token);
    }

    formatejarFecha(horaConnectat) {
        const fecha = new Date(horaConnectat);
        return fecha.getDate() + '/'
            + (fecha.getMonth() + 1) + '/'
            + fecha.getFullYear() + ' '
            + fecha.getHours() + ':'
            + fecha.getMinutes() + ':'
            + fecha.getSeconds();
    }

    render() {
        return (
            <tr style={{ 'color': this.current ? 'red' : 'white' }}>
                <td>
                    {this.addr}
                    {this.current &&
                        (<div>
                            <span className="badge badge-danger">Current</span>
                        </div>)
                    }
                </td>
                <td>{this.token}</td>
                <td>{this.lastUsed}</td>
                <td>
                    <img src={trashICO} alt="trashICO" onClick={this.handleRevoke} />
                </td>
            </tr>
        )
    }
}

export default Token;
