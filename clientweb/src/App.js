import React, { Component } from 'react';

// CSS
import './App.css';

// IMG
import logo from './logo.png';

class App extends Component {
    constructor({ url }) {
        super();
        this.url = url;
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    {!this.url && <p>
                        Loading...
                </p>}
                    {this.url && <p>
                        Connecting to the server...
                </p>}
                    {this.url && <small>
                        {this.url}
                    </small>}
                    {!this.url && <small>
                        Allow up to one minute. If the connection to the server does not start, please refresh the page or check your Internet connection.
                </small>}
                </header>
            </div>
        );
    }
}
/*function App() {
    var url = "";
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                {!url && <p>
                    Loading...
                </p>}
                {url && <p>
                    Connecting to the server...
                </p>}
                {url && <small>
                    {url}
                </small>}
                {!url && <small>
                    Allow up to one minute. If the connection to the server does not start, please refresh the page or check your Internet connection.
                </small>}
            </header>
        </div>
    );
}*/

export default App;
