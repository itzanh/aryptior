/***********/
/* MODULES */
/***********/

// REACT
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import './CSS/Global.css';

// NETEVENTS.IO
import { NetEventIO_Client, NetEventIOOptions } from './netevent.io.js';

// IMG
import desar from './IMG/desar.svg';
import clientsICO from './IMG/clients.svg';
import loadingICO from './IMG/loading.svg';
import errorICO from './IMG/error.svg';

// COMPONENTS
import Login from './COMPONENTS/Login.js';
import Menu from './COMPONENTS/Menu';
import Dashboard from './COMPONENTS/Dashboard/Dashboard';
import HashFinalitzat from './COMPONENTS/LlistarFinalitzats/HashFinalitzat';
import HashCua from './COMPONENTS/LlistarCua/HashCua';
import Diccionaris from './COMPONENTS/LlistarDiccionaris/Diccionaris';
import Settings from './COMPONENTS/Settings/Settings';
import Diccionari from './COMPONENTS/LlistarDiccionaris/Diccionari';
import HashesCua from './COMPONENTS/LlistarCua/HashesCua';
import HashesFinalitzats from './COMPONENTS/LlistarFinalitzats/HashesFinalitzats';
import LlistaClients from './COMPONENTS/LlistarClients/LlistaClients';
import Client from './COMPONENTS/LlistarClients/Client';
import LlistaTokens from './COMPONENTS/LlistaTokens/LlistaTokens';
import Token from './COMPONENTS/LlistaTokens/Token';
import ListRainbows from './COMPONENTS/Rainbow/ListRainbows.js';
import Rainbow from './COMPONENTS/Rainbow/Rainbow';
import ClientDetails from './COMPONENTS/LlistarClients/ClientDetails';
import BackupList from './COMPONENTS/Backups/BackupList';
import Backup from './COMPONENTS/Backups/Backup';
import BackupCronState from './COMPONENTS/Backups/BackupCronState';
import HashDetailsModal from './COMPONENTS/HashDetailsModal';
import ImportHash from './COMPONENTS/LlistarCua/ImportHash';
import LoadingScreen from './COMPONENTS/LoadingScreen';
import DictionaryDetails from './COMPONENTS/LlistarDiccionaris/DictionaryDetails';
import RainbowDetailsModal from './COMPONENTS/Rainbow/RainbowDetailsModal';
import { DeleteConfirm } from './COMPONENTS/DeleteConfirm';
import Alert from './COMPONENTS/Alert';
import DictionaryDeletionModal from './COMPONENTS/LlistarDiccionaris/DictionaryDeletionModal';
import DashboardLoading from './COMPONENTS/Dashboard/DashboardLoading';
import DashboardContainer from './COMPONENTS/Dashboard/DashboardContainer';
import VersionMismatchError from './COMPONENTS/VersionMismatchError';
import LogsList from './COMPONENTS/Logs/LogsList';
import Logs from './COMPONENTS/Logs/Logs';
import LogsListError from './COMPONENTS/Logs/LogsListError';
import ChangePWD from './COMPONENTS/ChangePWD';


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

/*******/
/* APP */
/*******/



ReactDOM.render(<App />, document.getElementById('root'));

/**
 * Default port to bind to from the web app
 * */
const DEFAULT_PORT = 22216;
/**
 * Object that represents the WebSocket data connection with the server
 * */
var client;
/**
 * Integer indicating the current tab. Used in order to correctly automatically refresh.
 * */
var currentTab = -1;
/**
 * Web client settings.
 * */
var CONFIG = null;

async function main() {
    // load app settings from a saved cookie
    await carregarConfig();
    // get the server's port to bind to
    const PORT = await port();
    // choose between 'ws' and 'wss'
    const wsShema = await getWsSchema();
    const addr = await getServerAddr();
    // show connecting screen
    ReactDOM.unmountComponentAtNode(document.getElementById('root'));
    ReactDOM.render(<App
        url={wsShema + '://' + addr + ':' + PORT + '/'}
    />, document.getElementById('root'));
    // attemps a WebSocket connection, using the current URL's hostname 
    // as the server's host name, and the port obtained above
    const options = new NetEventIOOptions(onPasswordLogin, onTokenLogin);
    var ws;
    try {
        ws = new WebSocket(wsShema + '://' + addr + ':' + PORT + '/');
        console.log(wsShema + '://' + window.location.hostname + ':' + PORT + '/');
    } catch (e) {
        console.error(e);
    }
    client = new NetEventIO_Client(ws, options);
    // wait for the connection to open
    const connected = await client.connect();
    if (connected[0]) {
        if (wsShema === 'wss') {
            setInterval(() => {
                client.emit('ping', '', '', (_, __) => {

                });
            }, 50000);
        }

        console.log(client);
        if (document.getElementById("recordarLogin") && document.getElementById("recordarLogin").checked) {
            setTimeout(() => {
                console.log("recordant...");
                client.emit('token', 'get', '', (sender, message) => {
                    console.log("missatge " + message);
                    const result = message.split(" ");
                    if (result[0] === 'OK') {
                        CONFIG.token = result[1];
                        guardarConfig(CONFIG);
                    }

                });
            }, 1000);
        }

        await printMenu();
        printDashboard(true);

        //changePwdOnInit();

        setTimeout(() => {
            changePwdOnInit();
        }, 250);

        ws.onclose = (e) => {
            console.log('Disconnected', e);
            ws.close();
            currentTab = -1;
            ReactDOM.render(<App />, document.getElementById('root'));
            setTimeout(() => {
                main();
            }, 1000);
        };

    } else {
        if (connected[1] === 1) {
            ReactDOM.render(<VersionMismatchError />, document.getElementById('root'));
            return;
        }
        console.log('No connectat. ' + client.state);
        currentTab = -1;
        ReactDOM.render(<App />, document.getElementById('root'));
        setTimeout(() => {
            main();
        }, 1000);
    }
};

function onPasswordLogin() {
    return new Promise((resolve) => {
        ReactDOM.render(<Login
            loginFailed={false}
            enviarLogin={(pwd, recordar) => {
                resolve(pwd);
            }}
        />, document.getElementById('root'));
    });
};

function onTokenLogin() {
    return new Promise((resolve) => {
        if (CONFIG.token !== '') {
            console.log('reconnectant amb token ' + CONFIG.token);
            resolve(CONFIG.token);
        } else {
            console.log('no reconnectant amb token');
            resolve(null);
        }
    });
};

function port() {
    return new Promise((resolve) => {
        if (window.location.search === "") {
            resolve(DEFAULT_PORT);
        }
        const parametres = window.location.search.substring(1).split('&');
        parametres.forEach((element) => {
            const info = element.split('=');
            if (info[0] === 'port') {
                resolve(parseInt(info[1]));
            }
        });
        resolve(DEFAULT_PORT);
    });
};

function getWsSchema() {
    return new Promise((resolve) => {
        if (window.location.search === "") {
            resolve('wss');
        }
        const parametres = window.location.search.substring(1).split('&');
        parametres.forEach((element) => {
            const info = element.split('=');
            if (info[0] === 'insecure' && info[1] === 'true') {
                resolve('ws');
            }
        });
        resolve('wss');
    });
};

function getServerAddr() {
    return new Promise((resolve) => {
        if (window.location.search === "") {
            resolve(window.location.hostname);
        }
        const parametres = window.location.search.substring(1).split('&');
        parametres.forEach((element) => {
            const info = element.split('=');
            if (info[0] === 'host') {
                resolve(info[1]);
            }
        });
        resolve(window.location.hostname);
    });
};

function changePwdOnInit() {
    client.emit('pwd', 'init', '', (_, response) => {
        if (response === 'True') {
            ReactDOM.render(<ChangePWD
                handleCancel={() => {
                    client.emit('pwd', 'init', 'False', (_, response) => {

                    });
                    printMenu();
                    printDashboard(true);
                }}
                handleChangePWD={(pwd, iterations, salt) => {
                    canviarPWD('webSocketSettings', iterations, salt, pwd).then(() => {
                        printMenu();
                        printDashboard(true);
                    });
                }}
            />, document.getElementById('root'));
        }
    });
};

async function printMenu() {
    await ReactDOM.render(<Menu
        handleDashboard={printDashboard}
        handleCua={showHashQueue}
        handleFinalitzats={showFinishedHashes}
        handleDiccionaris={showDictionaries}
        handleClients={showClients}
        handleBackups={showBackups}
        handleTokens={showTokens}
        handleRainbow={showRainbows}
        handleAjustos={showSettings}
        handleLogs={showLogs}
        // opcions per a eixir
        handleEixirGuardar={guardarSrv}
        handleEixirApagarSrv={apagarSrv}
        handleEixirApp={eixir}
    />, document.getElementById('root'));
};

async function printDashboard(loadingScreen = false) {
    currentTab = 0;

    if (loadingScreen) {
        if (document.getElementById('renderitzarPestanya')) {
            await ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
        }
        if (document.getElementById('renderitzarPestanya')) {
            await ReactDOM.render(<DashboardLoading

            />, document.getElementById('renderitzarPestanya'));
        }
    }
    console.log('*');
    client.unsubscribeAll();
    console.log('**');

    const message = await getDashboard();
    console.log('***');

    if (document.getElementById('renderitzarPestanya')) {
        await ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
        await ReactDOM.render(<DashboardContainer
            handleRefresh={() => {
                getDashboard().then((message) => {
                    renderDashboard(message);
                });
            }}
        />, document.getElementById('renderitzarPestanya'));
    }

    console.log('****');
    renderDashboard(message);
};

function getDashboard() {
    return new Promise((resolve) => {
        client.emit("dashboard", "", "", (sender, message) => {
            resolve(JSON.parse(message));
        });
    });
};

function renderDashboard(message) {
    if (!document.getElementById('renderDashboard')) return;
    ReactDOM.unmountComponentAtNode(document.getElementById('renderDashboard'));
    ReactDOM.render(<Dashboard
        data={message}
        handleRefresh={getDashboard}
    />, document.getElementById('renderDashboard'));
};

async function showHashQueue() {
    currentTab = 1;

    client.unsubscribeAll();

    ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
    ReactDOM.render(<LoadingScreen />, document.getElementById('renderitzarPestanya'));

    var hashQueueResponse;
    await new Promise((resolve, reject) => {
        client.emit("hash", "get", JSON.stringify({ offset: 0, limit: -1 }), (sender, message) => {
            hashQueueResponse = JSON.parse(message);
            resolve();
        });
    });

    renderScreenHashQueue();

    ReactDOM.render(<div id="loadingSpinner">
        <img src={loadingICO} alt="loadingICO" />
        <h4>Loading...</h4>
    </div>, document.getElementById('export'));

    var arrayHashesInQueue = [];
    const CHUNK_SIZE = 10;
    for (let i = 0; i < hashQueueResponse.totalLength; i += CHUNK_SIZE) {
        await new Promise((resolve, reject) => {
            client.emit("hash", "get", JSON.stringify({ offset: i, limit: CHUNK_SIZE }), (sender, message) => {
                arrayHashesInQueue = arrayHashesInQueue.concat(JSON.parse(message).response);
                console.log(JSON.parse(message).response);
                renderHashQueue(arrayHashesInQueue);
                resolve();
            });
        });
    }

    ReactDOM.unmountComponentAtNode(document.getElementById('export'));
    ReactDOM.render(
        <button type="button" className="btn btn-outline-light" onClick={() => {
            client.emit('hash', 'export', 'queue', (_, response) => {
                if (response === 'ERR') {
                    alert("Error exporting hash queue!");
                }
                openExternalFile(response, 'queue.json');
            });
        }}>Export to JSON</button>
        , document.getElementById('export'));

    var finestraModal = null;
    renderHashQueue(arrayHashesInQueue, (newWindow) => {
        finestraModal = newWindow;
    });

    client.subscribe('hash', (sender, topicName, changeType, pos, value) => {
        const newHash = JSON.parse(value);
        console.log('Ha passat algo ', changeType, pos, newHash);
        switch (changeType) {
            case 0: {
                if (pos === -1) {
                    break;
                }
                arrayHashesInQueue[pos] = newHash;
                break;
            }
            case 1: {
                if (pos === -1) {
                    break;
                }
                arrayHashesInQueue[pos] = newHash;
                if (finestraModal !== null) {
                    finestraModal.current.hash = newHash;
                    finestraModal.current.renderMainScreen();
                }

                break;
            }
            case 2: {
                if (pos >= 0) {
                    if (finestraModal !== null
                        && finestraModal.current.hash.uuid === arrayHashesInQueue[pos].uuid) {
                        window.$('#HashDetailsModal').modal('hide');
                    }
                    arrayHashesInQueue.splice(pos, 1);
                }

                break;
            }

        }

        renderHashQueue(arrayHashesInQueue, (newWindow) => {
            finestraModal = newWindow;
        });
    });
};

function renderScreenHashQueue() {
    ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
    ReactDOM.render(
        <HashesCua
            getDictionaries={getDictionaries}
            seleccionarFitxerJSON={selectJSONFile}
            validateHash={validateHash}
            enviarHash={enviarHash}
            modalAlert={modalAlert}
            handleTest={testHashPerformance}
        />
        , document.getElementById('renderitzarPestanya'));
};

function renderHashQueue(arrayHashesInQueue, windowOpened) {
    const componentsToRender = [];

    arrayHashesInQueue.forEach((element, i) => {
        componentsToRender.push(<HashCua
            key={i}

            objecte={element}
            handleShowDetails={(hash) => {
                if (windowOpened) {
                    windowOpened(renderHashQueueModalDetails(hash));
                } else {
                    renderHashQueueModalDetails(hash);
                }
            }}
        />);
    });

    ReactDOM.unmountComponentAtNode(document.getElementById('llistaHashesCua'));
    ReactDOM.render(
        componentsToRender, document.getElementById('llistaHashesCua'));
};

function renderHashQueueModalDetails(hash) {
    ReactDOM.unmountComponentAtNode(document.getElementById("renderModal"));
    var _child = React.createRef();
    const window = <HashDetailsModal
        hash={hash}

        handleSurrender={surrenderHashToggle}
        handleEdit={handleEditHash}
        handleExport={exportHashToJSON}

        ref={_child}
    />;
    ReactDOM.render(window, document.getElementById("renderModal"));
    return _child;
};

function surrenderHashToggle(uuid) {
    return new Promise((resolve, reject) => {
        client.emit('hash', 'surrender', uuid, (sender, respose) => {
            if (respose === 'ERR') {
                reject();
            } else {
                resolve();
            }
        });
    });
};

function finishedHashRemove(uuid) {
    return new Promise((resolve, reject) => {
        modalConfirm(uuid, () => {
            client.emit('hash', 'remove', uuid, (sender, respose) => {
                if (respose === 'ERR') {
                    reject(-1);
                } else {
                    resolve();
                }
            });
        }, () => {
            reject(0);
            return;
        }, null, null, 'Delete');
    });
};

function exportHashToJSON(uuid) {
    client.emit('hash', 'export', uuid, (_, response) => {
        if (response === 'ERR') {
            alert("Error exporting hash!");
        }
        openExternalFile(response, uuid + '.json');
    });
};

function exportarAJSON(objectOrArray = [], fileName = 'export.json') {
    openExternalFile(JSON.stringify(objectOrArray), fileName);
}

function selectJSONFile() {
    ReactDOM.unmountComponentAtNode(document.getElementById('renderModal'));
    ReactDOM.render(
        <ImportHash
            getHashQueue={getHashQueue}
            getFinishedHashes={getFinishedHashes}
            validateHash={validateHash}
            sendHash={enviarHash}
        />
        , document.getElementById('renderModal'));
}

function getHashQueue() {
    return new Promise((resolve) => {
        client.emit('hash', 'get', '', (sender, message) => {
            const arrayHashesInQueue = JSON.parse(message);
            resolve(arrayHashesInQueue);
        });
    });
}

function getFinishedHashes() {
    return new Promise((resolve) => {
        client.emit('hash', 'finished', '', (sender, message) => {
            const arrayHashesInQueue = JSON.parse(message);
            resolve(arrayHashesInQueue);
        });
    });
}

/**
 * Crea un fitxer blob amb el string especificat, i el obri com a fitxer per URL en una nova pestanya.
 * @param {string} text
 */
function openExternalFile(text, fileName = 'download') {
    const fitxerTemp = new Blob([text]);
    const url = URL.createObjectURL(fitxerTemp);
    //window.open(url, '_blank');
    var tempLink = document.createElement('a');
    tempLink.href = url;
    tempLink.setAttribute('download', fileName);
    tempLink.click();
}

function enviarHash(objecte) {
    return new Promise((resolve, reject) => {
        client.emit('hash', 'insert', JSON.stringify(objecte), (sender, message) => {
            if (message === 'ERR') {
                reject();
            }
            resolve();
        });
    });
};

function validateHash(hash) {
    console.log(hash);
    const tipusEnum = ['NTLM', 'MD4', 'MD5', 'SHA1', 'SHA-256', 'SHA-384', 'SHA-512', 'BCRYPT', 'RIPEMD128',
        'RIPEMD160', 'RIPEMD256', 'RIPEMD320', 'BLAKE', 'BLAKE2b (512)', 'BLAKE2s (256)', 'WHIRLPOOL', 'TIGER',
        'TIGER2', 'TIGER3', 'TIGER4', 'MODULAR', 'MYSQL', 'WORDPRESS'];

    if (hash.tipus != null && typeof hash.tipus === 'number' && hash.tipus >= 0 && hash.tipus < tipusEnum.length) {
        hash.tipus = tipusEnum[hash.tipus];
    } else if (hash.tipus != null && tipusEnum.indexOf(hash.tipus) < 0) {
        return false;
    }
    if (typeof hash.hash !== 'string' || hash.initialTestCount <= 100 || hash.descripcio === undefined
        || hash.descripcio === null || hash.msProcessingCount <= 5000 || hash.caractersFinals <= 1
        || hash.iteracions <= 0 || typeof hash.tipus !== 'string' || isNaN(hash.iteracions)
        || isNaN(hash.initialTestCount) || isNaN(hash.caractersFinals) || isNaN(hash.msProcessingCount)
        || hash.hash.length > 256 || hash.salt.length > 128 || hash.descripcio.length > 3000) {
        return false;
    }

    if (hash.tipusBusqueda !== 'Diccionari'
        && (hash.primerCaracterUnicode < 32 || hash.ultimCaracterUnicode <= hash.primerCaracterUnicode) || isNaN(hash.primerCaracterUnicode)
        || isNaN(hash.ultimCaracterUnicode) || hash.caractersFinals <= 1 || hash.caractersFinals > 32 || hash.caracters.length > 32) {
        return false;
    }

    switch (hash.tipus) {
        case "MD4":
        case "MD5":
        case "RIPEMD128":
        case "NTLM": {
            if (hash.hash.length !== 32) return false;
            break;
        }
        case "RIPEMD160":
        case "SHA1": {
            if (hash.hash.length !== 40) return false;
            break;
        }
        case "TIGER":
        case "TIGER2":
        case "TIGER3":
        case "TIGER4": {
            if (hash.hash.length !== 48) return false;
            break;
        }
        case "BLAKE": {
            if (hash.hash.length !== 56) return false;
            break;
        }
        case "BCRYPT": {
            if (hash.hash.length !== 60) return false;
            break;
        }
        case "RIPEMD256":
        case "BLAKE2s":
        case "SHA256": {
            if (hash.hash.length !== 64) return false;
            break;
        }
        case "RIPEMD320": {
            if (hash.hash.length !== 80) return false;
            break;
        }
        case "SHA384": {
            if (hash.hash.length !== 96) return false;
            break;
        }
        case "BLAKE2b":
        case "WHIRLPOOL":
        case "SHA512": {
            if (hash.hash.length !== 128) return false;
            break;
        }
        case "MODULAR":
        case "MYSQL":
        case "WORDPRESS":
            {
                break;
            }
        default: {
            return false;
        }
    }

    if (hash.tipus === 'BCRYPT') {
        if (!/^[$][2]([]|[a]|[b]|[x]|[y])[$]\d\d[$]\S{53}$/.test(hash.hash)) return false;
    } else if (hash.tipus === 'NTLM') {
        if (!/^[A-F0-9]{32}$/.test(hash.hash)) return false;
    } else if (hash.tipus === 'MODULAR') {
        if (!validateModularHash(hash.hash)) return false;
    } else if (hash.tipus === 'MYSQL') {
        if (!mySqlHashIsValid(hash.hash)) return false;
    } else if (hash.tipus === 'WORDPRESS') {
        if (!wordpressHashIsValid(hash.hash)) return false;
    } else {
        if (!/^[a-f0-9]+$/.test(hash.hash)) return false;
    }

    if (hash.tipusBusqueda === 'Diccionari' && hash.diccionari === null || hash.diccionari === undefined) {
        return false;
    }

    return true;
};

function mySqlHashIsValid(hash) {
    console.log(hash)
    return !(hash.length !== 41 || hash[0] !== '*');
};

function wordpressHashIsValid(hash) {
    return !(hash.length !== 34 || (hash.substring(0, 3) !== "$P$" && hash.substring(0, 3) !== "$H$"));
};

function validateModularHash(hash) {
    if (hash[0] !== '_' && hash[0] !== '$') {
        // DES hash detected
        if (hash.Length != 13) return false;
    }
    else if (hash[0] === '_') {
        // BSDi hash detected
        if (hash.Length != 20) return false;
    }
    else if (hash[0] === '$') {
        const hashData = hash.split('$');
        if (hashData.length !== 4) return false;
        switch (hashData[1]) {
            case "1":
            case "md5":
                {
                    if (hashData[3].length != 22) return false;
                    break;
                }
            case "2":
            case "2a":
            case "2x":
            case "2y":
                {
                    if (hashData[3].length != 53) return false;
                    break;
                }
            case "3":
                {
                    if (hashData[3].length != 32) return false;
                    break;
                }
            case "5":
                {
                    if (hashData[3].length != 43) return false;
                    break;
                }
            case "6":
                {
                    if (hashData[3].length != 86) return false;
                    break;
                }
            case "sha1":
                {
                    if (hashData[3].length != 37) return false;
                    break;
                }
            default:
                {
                    return false;
                }
        }
    }

    return true;
};

function testHashPerformance(testHash) {
    return new Promise((resolve, reject) => {
        client.emit('hash', 'test', JSON.stringify(testHash), (_, response) => {
            if (response === 'ERR') {
                reject();
            } else {
                resolve(JSON.parse(response));
            }
        });
    });
};

async function showFinishedHashes() {
    var refreshing = false;
    if (currentTab === 2) {
        refreshing = true;
    }
    currentTab = 2;

    client.unsubscribeAll();

    if (!refreshing) {
        ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
        ReactDOM.render(<LoadingScreen />, document.getElementById('renderitzarPestanya'));
    }

    var finishedHashesResponse;
    await new Promise((resolve) => {
        client.emit('hash', 'finished', JSON.stringify({ offset: 0, limit: -1 }), (sender, message) => {
            finishedHashesResponse = JSON.parse(message);
            resolve();
        });
    });

    if (!refreshing) {
        ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
        ReactDOM.render(
            <HashesFinalitzats
                renderFinishedHashes={(searchMechanism, algorithm, status) => {
                    renderFinishedHashes(arrayFinishedHashes, searchMechanism, algorithm, status);
                }}
            />
            , document.getElementById('renderitzarPestanya'));
    }

    ReactDOM.render(<div id="loadingSpinner">
        <img src={loadingICO} alt="loadingICO" />
        <h4>Loading...</h4>
    </div>, document.getElementById('exportar'));

    var modalWindow = null;
    var arrayFinishedHashes = [];
    const CHUNK_SIZE = 10;

    for (let i = 0; i <= finishedHashesResponse.totalLength; i += CHUNK_SIZE) {
        await new Promise((resolve) => {
            client.emit('hash', 'finished', JSON.stringify({ offset: i, limit: CHUNK_SIZE }), (sender, message) => {
                arrayFinishedHashes = arrayFinishedHashes.concat(JSON.parse(message).response);
                renderFinishedHashes(arrayFinishedHashes, -1, -1, -1, false, (window) => {
                    modalWindow = window;
                });
                resolve();
            });
        });
    }

    // export to JSON button
    ReactDOM.unmountComponentAtNode(document.getElementById('exportar'));
    ReactDOM.render(<button type="button" className="btn btn-outline-light" onClick={() => {
        exportFinishedHashesToJson(arrayFinishedHashes,
            document.getElementById("searchMechanism").value, document.getElementById("tipoHash").value, document.getElementById("status").value,
            "finishedHashes.json");
    }}>Exportar a JSON</button>
        , document.getElementById('exportar'));

    client.subscribe('hash', (sender, topicName, changeType, pos, value) => {
        const newHash = JSON.parse(value);
        console.log('Ha passat algo ', changeType, pos, newHash);
        switch (changeType) {
            case 0: {
                if (pos === -1) {
                    arrayFinishedHashes.unshift(newHash);
                }
            }
            case 1: {
                if (pos === -1) {
                    for (let i = 0; i < arrayFinishedHashes.length; i++) {
                        if (arrayFinishedHashes[i].uuid === newHash.uuid) {
                            console.log(modalWindow);
                            if (modalWindow !== null) {
                                modalWindow.current.hash = newHash;
                                modalWindow.current.renderMainScreen();
                            }

                            arrayFinishedHashes[i] = newHash;
                            break;
                        }
                    }
                }
                break;
            }
            case 2: {
                if (pos === -1) {
                    if (document.getElementById("currentHashModalID")
                        && document.getElementById("currentHashModalID").innerText === newHash.uuid) {
                        window.$('#HashDetailsModal').modal('hide');
                    }

                    for (let i = 0; i < arrayFinishedHashes.length; i++) {
                        if (arrayFinishedHashes[i].uuid === newHash.uuid) {
                            arrayFinishedHashes.splice(i, 1);
                            break;
                        }
                    }
                }

                break;
            }

        }

        console.log(arrayFinishedHashes);
        renderFinishedHashes(arrayFinishedHashes, -1, -1, -1, true, (window) => {
            modalWindow = window;
        });
    });
};

function exportFinishedHashesToJson(arrayFinishedHashes, searchMechanism = -1, algorithm = -1, status = -1, fileName) {
    if (searchMechanism != -1 || algorithm != -1 || status != -1) {
        arrayFinishedHashes = filterFinishedHashes(arrayFinishedHashes, searchMechanism, algorithm, status);

        const ids = arrayFinishedHashes.map((element) => {
            return element.uuid;
        });

        client.emit('hash', 'export', JSON.stringify(ids), (_, response) => {
            if (response === 'ERR') {
                alert("Error exporting hash queue!");
            }
            openExternalFile(response, fileName);
        });
    } else {
        client.emit('hash', 'export', 'finished', (_, response) => {
            if (response === 'ERR') {
                alert("Error exporting hash queue!");
            }
            openExternalFile(response, fileName);
        });
    }
};

function renderFinishedHashes(arrayFinishedHashes, searchMechanism = -1, algorithm = -1, status = -1, unmount = true, onNewModal = null) {
    const filtered = filterFinishedHashes(arrayFinishedHashes, searchMechanism, algorithm, status);
    const componentsToRender = [];

    filtered.forEach((element, i) => {
        componentsToRender.push(<HashFinalitzat
            key={i}

            objecte={element}
            handleDetails={(hash) => {
                const window = showFinishedHashDetails(hash);
                if (onNewModal !== null) {
                    onNewModal(window);
                }
            }}
        />);
    });

    if (unmount) ReactDOM.unmountComponentAtNode(document.getElementById('llistaHashesFinalitzats'));
    if (componentsToRender.length === 0 && (searchMechanism !== -1 || algorithm !== -1 || status !== -1)) {
        ReactDOM.render(
            <h3>NO RESULTS FOUND!</h3>
            , document.getElementById('renderNoReultsFound'));
    } else {
        ReactDOM.unmountComponentAtNode(document.getElementById('renderNoReultsFound'));
        ReactDOM.render(componentsToRender, document.getElementById('llistaHashesFinalitzats'));
    }
};

function filterFinishedHashes(arrayFinishedHashes, searchMechanism = -1, algorithm = -1, status = -1) {
    const filtered = [];

    if (algorithm === '' || algorithm === undefined) {
        algorithm = -1;
    } else {
        algorithm = parseInt(algorithm);
    }

    if (searchMechanism === '' || searchMechanism === undefined) {
        searchMechanism = -1;
    } else {
        searchMechanism = parseInt(searchMechanism);
    }

    if (status === '' || status === undefined) {
        status = -1;
    } else {
        status = parseInt(status);
    }

    arrayFinishedHashes.forEach((element, i) => {
        if (algorithm !== -1 && algorithm !== element.tipus)
            return;
        if (searchMechanism !== -1 && searchMechanism !== element.tipusBusqueda)
            return;
        if (status !== -1) {
            switch (status) {
                case 0: {
                    if (!element.estaResolt || element.surrender || element.solucio === null)
                        return;
                    break;
                }
                case 1: {
                    if (!element.surrender)
                        return;
                    break;
                }
                case 2: {
                    if (!element.estaResolt || element.surrender || element.solucio !== null)
                        return;
                    break;
                }
            }
        }

        filtered.push(element);
    });

    return filtered;
};

function showFinishedHashDetails(hash) {
    ReactDOM.unmountComponentAtNode(document.getElementById('renderModal'));
    var _child = React.createRef();
    const window = <HashDetailsModal
        hash={hash}

        handleSurrender={surrenderHashToggle}
        handleRemove={finishedHashRemove}
        handleEdit={handleEditHash}
        handleExport={exportHashToJSON}

        ref={_child}
    />;
    ReactDOM.render(window, document.getElementById('renderModal'));
    return _child;
};

function handleEditHash(updatedSettings) {
    return new Promise((resolve, reject) => {
        client.emit('hash', 'edit', JSON.stringify(updatedSettings), (sender, message) => {
            if (message === 'OK') {
                resolve();
            } else if (message === 'ERR') {
                reject();
            }
        });
    });
};

async function showDictionaries() {
    currentTab = 3;

    client.unsubscribeAll();

    ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
    ReactDOM.render(<LoadingScreen />, document.getElementById('renderitzarPestanya'));

    const dictionaries = await getDictionaries();

    ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
    ReactDOM.render(<Diccionaris
        handleEnviar={inicialitzarPutjada}
    />, document.getElementById('renderitzarPestanya'));

    var modalWindow = null;

    renderDictionaries(dictionaries, (window) => {
        modalWindow = window;
    });

    client.subscribe('dictionary', (sender, topicName, changeType, pos, value) => {
        console.log('Ha passat algo ', changeType, pos, value);
        switch (changeType) {
            case 0: {
                dictionaries[pos] = JSON.parse(value);

                break;
            }
            case 1: {
                const dictionaryChanges = JSON.parse(value);

                var dictionarySel = null;
                for (let i = 0; i < dictionaries.length; i++) {
                    if (dictionaries[i].uuid === dictionaryChanges.uuid) {
                        Object.keys(dictionaryChanges).forEach((key) => {
                            dictionaries[i][key] = dictionaryChanges[key];
                        });
                        dictionarySel = dictionaries[i];
                        break;
                    }
                }

                console.log('editant', dictionaryChanges);
                if (dictionarySel !== null && modalWindow !== null
                    && modalWindow.current.dictionary.uuid === dictionaryChanges.uuid) {
                    modalWindow.current.dictionary = dictionarySel;
                    modalWindow.current.showMainScreen();
                }

                break;
            }
            case 2: {
                if (modalWindow !== null && modalWindow.current.dictionary.uuid === dictionaries[pos].uuid) {
                    window.$('#dictionaryDetails').modal('hide');
                }
                dictionaries.splice(pos, 1);

                break;
            }

        }

        renderDictionaries(dictionaries, (window) => {
            modalWindow = window;
        });
    });
};

function getDictionaries() {
    return new Promise((resolve, reject) => {
        client.emit('dictionaries', 'get', '', (sender, message) => {
            try {
                const dictionaries = JSON.parse(message);
                resolve(dictionaries);
                return;
            } catch (_) {

            }
            reject();

        });
    });
};

function renderDictionaries(dictionaries, onWindowOpen) {
    const componentsToRender = [];

    dictionaries.forEach((element, i) => {
        componentsToRender.push(
            <Diccionari
                key={i}

                diccionari={element}
                handleShowDetails={(dictionary) => {
                    const window = showDictionaryDetails(dictionary);
                    onWindowOpen(window);
                }}
                handleRemove={removeDictionary}
            />);
    });

    ReactDOM.unmountComponentAtNode(document.getElementById('llistaDiccionaris'));
    ReactDOM.render(
        componentsToRender, document.getElementById('llistaDiccionaris'));
};

function showDictionaryDetails(dictionary) {
    ReactDOM.unmountComponentAtNode(document.getElementById('renderModal'));
    const _child = React.createRef();
    ReactDOM.render(<DictionaryDetails
        dictionary={dictionary}

        handleEdit={editDictionary}
        handleRemove={removeDictionary}

        ref={_child}
    />, document.getElementById('renderModal'));

    return _child;
};

function removeDictionary(uuid) {
    modalConfirm(uuid, () => {
        client.emit('dictionaries', 'delete', uuid, (sender, message) => {
            const deleteResult = JSON.parse(message);
            if (deleteResult.operationResult === 1) {
                if (deleteResult.hashesUsingDictionary === null) {
                    modalAlert("There was an Input/Output error deleting the dictionary file. Please check the permissions or refresh.", "Error deleting!");
                } else {
                    ReactDOM.unmountComponentAtNode(document.getElementById("renderComfirmModal"));
                    ReactDOM.render(<DictionaryDeletionModal
                        hashesUsingDictionary={deleteResult.hashesUsingDictionary}
                    />, document.getElementById("renderComfirmModal"));
                }
            }
        });
    }, null, "This operation cannot be undone.", null, 'Delete');
};

function editDictionary(dictionary) {
    console.log(dictionary);
    return new Promise((resolve) => {
        client.emit('dictionaries', 'edit', JSON.stringify(dictionary), (_, message) => {
            resolve();
            if (message === 'ERR') {
                modalAlert('There was an error editing the dictionary.', 'Server error.');
            }
        });
    });
};

function inicialitzarPutjada(fitxer, description) {
    client.on('dictionary', (sender, message) => {
        if (message.command === 'chunkUpload') {
            const part = JSON.parse(message.message);


            const trosFitxer = fitxer.slice(part.i, (part.i + part.chunkSize));
            client.emitBinary('dictionaryChunkUpload', '', trosFitxer);
            console.log("i " + part.i + " tamany " + trosFitxer.size);
            // comprovar si ja se ha putjat tot
            if ((part.i + part.chunkSize) >= fitxer.size) {
                fitxer = null;
            }
        }
    });
    client.emit('dictionaries', 'upload', JSON.stringify({
        fileName: fitxer.name,
        sizeInBytes: fitxer.size,
        description
    }), (_, message) => {
        if (message === 'ERR') {
            modalAlert('An unexpected error has occurred on the server side.', 'Upload error!');
        } else {
            showDictionaries();
        }
    });
};

function showClients() {
    currentTab = 4;

    client.unsubscribeAll();

    ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
    ReactDOM.render(<LoadingScreen />, document.getElementById('renderitzarPestanya'));

    client.emit('clients', '', '', (sender, message) => {
        // object (used as a dictionary) that contains the list of clients on the remote server
        const serversClients = (JSON.parse(message));
        // list of react components to render
        console.log('clients ', serversClients);

        // clear and show loading message
        ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
        ReactDOM.render(<p>Loading...</p>, document.getElementById('renderitzarPestanya'));

        // render the list of clients
        renderClients(serversClients);

        // subscribe to client changes
        client.subscribe('client', (sender, topicName, changeType, pos, value) => {
            switch (changeType) {
                case 0: {
                    const newClient = JSON.parse(value);
                    serversClients[newClient.uuid] = newClient.client;

                    break;
                }
                case 2: {
                    delete serversClients[value];

                    if (document.getElementById("modalClientCurrentUUID")
                        && document.getElementById("modalClientCurrentUUID").innerText === value) {
                        window.$('#clientsModalCenter').modal('hide');
                        ReactDOM.unmountComponentAtNode(document.getElementById('renderClientDetails'));
                    }

                    break;
                }

            }

            if (document.getElementById("searchInput") && document.getElementById("searchInput").value !== '') {
                searchClients(serversClients, document.getElementById("searchInput").value);
            } else {
                renderClients(serversClients);
            }

        });
    });
};

async function renderClients(serversClients, filtered = false) {
    const clientsToRender = [];

    // create the react components for each client on the list
    var i = 0;
    var element;
    for (var key of Object.keys(serversClients)) {
        element = serversClients[key];
        clientsToRender.push(
            <Client
                key={i}

                uuid={key}
                addr={element.addr}
                timeConnected={element.timeConnected}
                processing={element.processing}
                hashRatePerMs={element.hashRatePerMs}
                lastNewPartRequested={element.lastNewPartRequested}
                lastReportedPart={element.lastReportedPart}

                showClientDetails={showClientDetails}
            />
        );
        i++;
    }

    // show empty client list message
    if (Object.keys(serversClients).length === 0) {
        if (!filtered) {
            ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
            ReactDOM.render(<div id="emptyClientList">
                <img src={clientsICO} alt="clientsICO" />
                <h5>There are no clients currently connected to this server.</h5>
            </div>, document.getElementById('renderitzarPestanya'));
            return;
        } else {
            ReactDOM.unmountComponentAtNode(document.getElementById('renderEmptyResultMessage'));
            ReactDOM.render(<div>
                <h5>No result we found.</h5>
            </div>, document.getElementById('renderEmptyResultMessage'));
        }
    } else if (document.getElementById("llistaClients")) {
        ReactDOM.unmountComponentAtNode(document.getElementById('renderEmptyResultMessage'));

    }

    // render tab
    if (!document.getElementById("llistaClients")) {
        ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
        await ReactDOM.render(<LlistaClients
            handleRenderClients={() => { renderClients(serversClients); }}
            handleConfigureAll={configureAllClients}
            handleSearch={(search) => { searchClients(serversClients, search) }}
            handleExport={() => { exportarAJSON(serversClients, "clients.json"); }}
        />, document.getElementById('renderitzarPestanya'));
    }

    // render components
    ReactDOM.unmountComponentAtNode(document.getElementById('llistaClientsConnectats'));
    ReactDOM.render(
        clientsToRender
        , document.getElementById('llistaClientsConnectats'));


};

function searchClients(serversClients, search) {
    if (search === '')
        renderClients(serversClients, true);

    const filetedElements = {};
    var element;
    for (var key of Object.keys(serversClients)) {
        element = serversClients[key];
        if (element.addr.includes(search) || key.includes(search)) {
            filetedElements[key] = element;
        }
    }

    renderClients(filetedElements, true);
};

function showClientDetails(uuid) {
    ReactDOM.unmountComponentAtNode(document.getElementById('renderClientDetails'));
    ReactDOM.render(<ClientDetails
        uuid={uuid}

        getClientSettings={getClientSettings}
        getClientBackups={getClientBackups}
        exportToJSON={exportarAJSON}

        handleKickClient={kickClient}
        handleBanClient={banClient}
        saveData={sendClientSettings}
    />, document.getElementById('renderClientDetails'));
};

function getClientSettings(uuid) {
    return new Promise((resolve, reject) => {
        client.emit('clients', 'read', uuid, (sender, message) => {
            if (message === 'ERR') {
                reject();
                return;
            }
            const clientSettings = JSON.parse(message);
            resolve(clientSettings);
        });
    });
};

function getClientBackups(uuid) {
    return new Promise((resolve, reject) => {
        client.emit('clients', 'listBackup', uuid, (sender, message) => {
            if (message === 'ERR') {
                reject();
                console.log(message);
                return;
            }
            const clientBackups = JSON.parse(message);
            resolve(clientBackups);
        });
    });
};

function sendClientSettings(uuid, newSettings) {
    return new Promise((resolve) => {
        client.emit('clients', 'write', JSON.stringify({ uuid, newSettings }), (sender, message) => {
            if (message === 'OK') {
                resolve(true);
            }
            resolve(false);
        });
    });
};

function configureAllClients(settings, callbackFn) {
    return new Promise((resolve) => {
        client.on('configAllProgress', (sender, message) => {
            const data = message.message.split(':');

            const current = parseInt(data[0]);
            const total = parseInt(data[1]);

            callbackFn(current, total);

            if (current === total) {
                client.removeEvent('configAllProgress');
                return;
            }
        });

        client.emit('clients', 'configAll', JSON.stringify(settings), (sender, message) => {
            if (message === 'OK') {
                resolve(true);
            }
            resolve(false);
        });
    });
};

function kickClient(uuid) {
    return new Promise((resolve, reject) => {
        client.emit('clients', 'kick', uuid, (sender, message) => {
            if (message === 'OK') {
                resolve();
            }
            reject();
        });
    });
};

function banClient(uuid) {
    return new Promise((resolve, reject) => {
        client.emit('clients', 'ban', uuid, (sender, message) => {
            if (message === 'OK') {
                resolve();
            }
            reject();
        });
    });
};

function showSettings() {
    currentTab = 8;

    client.unsubscribeAll();

    ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
    ReactDOM.render(<LoadingScreen />, document.getElementById('renderitzarPestanya'));

    client.emit('settings', 'readConf', '', (sender, message) => {
        console.log(JSON.parse(message));
        const serverSettings = JSON.parse(message);
        ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
        ReactDOM.render(<Settings
            ConfigClient={CONFIG}
            ConfigServer={serverSettings}
            modalAlert={modalAlert}

            handleRevokeToken={revocarToken}

            guardarServer={enviarAjustos}

            canviarPWD={(subcommand, iterations, salt, newPWD) => {
                canviarPWD(subcommand, iterations, salt, newPWD).then(() => {
                    showSettings();
                });
            }}

            exportarAJSON={() => {
                exportarAJSON(serverSettings, "settings.json");
            }}
            importarJSON={importarConfigJSON}

            handleListCerts={listServerCertificates}
            handleRemoveCert={removeServerCertificate}
            handleCertUpload={uploadServerCertificate}
            handleCertDownload={downloadServerCertificate}
        />, document.getElementById('renderitzarPestanya'));

        client.emit('settings', 'configPath', '', (sender, message) => {
            document.getElementById('settingsTabMenuFilename').innerText = message;
        });
    });
};

function listServerCertificates() {
    return new Promise((resovle, reject) => {
        client.emit('cert', '', '', (sender, message) => {
            if (message === 'ERR') {
                reject();
            } else {
                resovle(JSON.parse(message));
            }
        });
    });
};

function removeServerCertificate(fileName) {
    return new Promise((resolve, reject) => {
        client.emit('cert', 'remove', fileName, (sender, message) => {
            if (message === 'ERR') {
                reject();
            } else {
                resolve();
            }
        });
    });
};

function uploadServerCertificate(file) {
    return new Promise((resolve, reject) => {
        client.on('certUpload', (_, message) => {
            client.emitBinary('certUpload', '', file.slice(0));
            client.removeEvent('certUpload');
        });
        client.emit('cert', 'upload', file.name, (_, message) => {
            if (message === 'OK') {
                resolve();
            } else {
                reject();
            }
        });
    });
};

function downloadServerCertificate(uuid, certificateFilename = 'certificate.pfx') {
    return new Promise((resolve, reject) => {
        client.on('certDownload', async (_, message) => {
            const url = URL.createObjectURL(message.message);
            ReactDOM.unmountComponentAtNode(document.getElementById("renderDownloads"));
            await ReactDOM.render(<a download={certificateFilename} id={uuid} href={url} />
                , document.getElementById("renderDownloads"));
            document.getElementById(uuid).click();
            ReactDOM.unmountComponentAtNode(document.getElementById("renderDownloads"));
            client.removeEvent('certDownload');
        });
        client.emit('cert', 'get', uuid, (_, message) => {
            if (message === 'OK') {
                resolve();
            } else {
                reject();
            }
        });
    });
};

function canviarPWD(subcommand, iterations, salt, newPWD) {
    return new Promise((resolve) => {
        client.emit('pwd', subcommand, JSON.stringify({
            iterations,
            salt,
            pwd: newPWD
        }), (_, message) => {
            resolve();
            if (message === 'ERR') {
                modalAlert('Check that the settings file on the server is writeable, and that the server is running properly.',
                    'There was an error saving changes!');
            }
        });
    });
}

function showTokens() {
    var refreshing = false;
    if (currentTab === 5) {
        refreshing = true;
    } else {
        currentTab = 5;
    }

    client.unsubscribeAll();

    client.emit('token', 'list', '', (sender, message) => {
        const serversTokenList = (JSON.parse(message));
        if (!refreshing) {
            ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
            ReactDOM.render(<LlistaTokens
                handleRevokeAll={handleRevokeAllTokens}
            />, document.getElementById('renderitzarPestanya'));
        }

        renderTokens(serversTokenList);

        client.subscribe('token', (sender, topicName, changeType, pos, value) => {
            switch (changeType) {
                case 0: {
                    serversTokenList.push(JSON.parse(value));

                    break;
                }
                case 1: {
                    const updatedToken = JSON.parse(value);

                    for (let i = 0; i < serversTokenList.length; i++) {
                        if (serversTokenList[i].token === updatedToken.token) {
                            serversTokenList[i] = updatedToken;
                        }
                    }

                    break;
                }
                case 2: {
                    for (let i = 0; i < serversTokenList.length; i++) {
                        if (serversTokenList[i].token === value) {
                            serversTokenList.splice(i, 1);
                        }
                    }

                    break;
                }

            }

            renderTokens(serversTokenList);

        });
    });
};

function renderTokens(serversTokenList) {
    const componentsToRender = [];

    serversTokenList.forEach((element, i) => {
        componentsToRender.push(
            <Token
                key={i}

                addr={element.addr}
                lastUsed={element.lastUsed}
                token={element.token}
                current={(element.token === CONFIG.token)}

                handleRevokeToken={revocarToken}
            />);
    });

    ReactDOM.unmountComponentAtNode(document.getElementById('renderTokens'));
    ReactDOM.render(
        componentsToRender, document.getElementById('renderTokens'));
};

function revocarToken(token) {
    modalConfirm(null, () => {
        if (!token || typeof token !== 'string') {
            if (CONFIG.token) {
                token = CONFIG.token;
            } else {
                return;
            }
        }
        client.emit('token', 'revoke', token, (sender, message) => {
            if (message === 'ERR') {
                alert('Error al revocar el token');
                return;
            }
        });
    });
};

function handleRevokeAllTokens() {
    ReactDOM.unmountComponentAtNode(document.getElementById("renderComfirmModal"));
    ReactDOM.render(<DeleteConfirm
        customText={"Delete all tokens?"}
        customBody={"This will delete all the tokens the server, so users who remembered the password will have to input it again."}
        handleOk={revokeAllTokens}
    />, document.getElementById("renderComfirmModal"));
};

function revokeAllTokens() {
    client.emit('token', 'revokeall', '', (_, msg) => {
        if (msg === 'ERR') {
            modalAlert('There was an error revoking the tokens.', 'Server error.');
        }
    });
};

async function showRainbows() {
    var refreshing = false;
    if (currentTab === 6) {
        refreshing = true;
    } else {
        currentTab = 6;
    }

    client.unsubscribeAll();

    if (!refreshing) {
        ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
        ReactDOM.render(<LoadingScreen />, document.getElementById('renderitzarPestanya'));
    }


    var rainbowsResponse;
    await new Promise((resolve, reject) => {
        client.emit('rainbow', 'list', JSON.stringify({ offset: -1, limit: -1 }), (sender, message) => {
            rainbowsResponse = JSON.parse(message);
            resolve();
        });
    });

    ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
    ReactDOM.render(<ListRainbows
        handleCreateRainbow={sendRainbow}
        handleSearch={(search, algorithm, sort) => { renderRainbows(searchRainbows(rainbowsFromServer, search, algorithm, sort)); }}
    />, document.getElementById('renderitzarPestanya'));

    ReactDOM.unmountComponentAtNode(document.getElementById('loadingSpinner'));
    ReactDOM.render(<div>
        <img src={loadingICO} alt="loadingICO" />
    </div>, document.getElementById('loadingSpinner'));

    var rainbowsFromServer = [];
    const CHUNK_SIZE = 10;
    var modalWindow = null;

    for (let i = 0; i <= (rainbowsResponse.totalLength); i += CHUNK_SIZE) {
        await new Promise((resolve, reject) => {
            client.emit('rainbow', 'list', JSON.stringify({ offset: i, limit: CHUNK_SIZE }), (sender, message) => {
                rainbowsFromServer = rainbowsFromServer.concat(JSON.parse(message).response);
                renderRainbows(rainbowsFromServer, (window) => {
                    modalWindow = window;
                });
                resolve();
            });
        });
    }

    console.log(rainbowsFromServer);
    ReactDOM.unmountComponentAtNode(document.getElementById('loadingSpinner'));

    client.subscribe('rainbow', (sender, topicName, changeType, pos, value) => {
        try {
            switch (changeType) {
                case 0: {
                    const r = JSON.parse(value);

                    rainbowsFromServer.push(r);

                    break;
                }
                case 1: {
                    const r = JSON.parse(value);
                    var rainbow = null;

                    for (let i = 0; i < rainbowsFromServer.length; i++) {
                        if (rainbowsFromServer[i].uuid === r.uuid) {
                            Object.keys(r).forEach((prop) => {
                                if (prop !== 'hash') {
                                    rainbowsFromServer[i][prop] = r[prop];
                                }
                            });
                            if (r.hash) {
                                Object.keys(r.hash).forEach((prop) => {
                                    rainbowsFromServer[i].hash[prop] = r.hash[prop];
                                });
                            }

                            rainbow = rainbowsFromServer[i];
                            break;
                        }
                    }

                    if (rainbow !== null && modalWindow !== null && modalWindow.current.r.uuid === r.uuid) {
                        modalWindow.current.r = rainbow;
                        modalWindow.current.renderMainScreen();
                    }

                    break;
                }
                case 2: {
                    for (let i = 0; i < rainbowsFromServer.length; i++) {
                        if (rainbowsFromServer[i].uuid === value) {
                            rainbowsFromServer.splice(i, 1);
                            break;
                        }
                    }

                    if (modalWindow !== null && modalWindow.current.r.uuid === value) {
                        window.$('#rainbowDetailsModal').modal('hide');
                        ReactDOM.unmountComponentAtNode(document.getElementById('renderRainbowModal'));
                    }

                    break;
                }
            } //switch

            if (document.getElementById("searchInput") && document.getElementById("searchInput").value !== '') {
                renderRainbows(
                    searchRainbows(rainbowsFromServer, document.getElementById("searchInput").value, document.getElementById("searchHash").value)
                    , (window) => {
                        modalWindow = window;
                    });
            } else {
                renderRainbows(searchRainbows(rainbowsFromServer), (window) => {
                    modalWindow = window;
                });
            }

        } catch (e) { console.log(e); }

    });
};

function renderRainbows(rainbowsFromServer, onModalWindowOpen) {
    const componentsToRender = [];

    rainbowsFromServer.forEach((element, i) => {
        componentsToRender.push(<Rainbow
            key={i}

            rainbow={element}

            handleRemoveRainbow={removeRainbow}
            handleToggle={handleToggleRainbow}
            handleDetails={(rainbow) => {
                const window = showRainbowDetails(rainbow);
                if (onModalWindowOpen !== null) {
                    onModalWindowOpen(window);
                }
            }}
        />);
    });

    ReactDOM.unmountComponentAtNode(document.getElementById('listRainbows'));
    ReactDOM.render(
        componentsToRender, document.getElementById('listRainbows'));
};

function sendRainbow(rainbow) {
    client.emit('rainbow', 'new', JSON.stringify(rainbow), (sender, message) => {
        if (message === 'ERR') {
            alert("Error per part del servidor!");
        }
    });
};

function removeRainbow(uuid) {
    modalConfirm(uuid, () => {
        client.emit('rainbow', 'remove', uuid, (sender, message) => {
            if (message === 'ERR') {
                modalAlert("Error on the server side!", 'Error!');
            }
        });
    }, null, null, null, 'Delete');
};

function handleToggleRainbow(uuid) {
    client.emit('rainbow', 'toggle', uuid, (sender, message) => {
        if (message === 'ERR') {
            modalAlert('Error per part del servidor!', 'Error!');
        }
    });
};

function searchRainbows(rainbowsFromServer, search = '', algorithm = -1, sort = -1) {
    const filteredRainbows = rainbowsFromServer;
    if (search !== '' && algorithm !== -1) {
        filteredRainbows = rainbowsFromServer.filter((element) => {
            return (element.name.includes(search) || element.hash.descripcio.includes(search))
                && (algorithm === -1 || element.hash.tipus === algorithm);
        });
    }
    filteredRainbows.sort((i, j) => {
        switch (sort) {
            case -1: {
                const iDate = new Date(i.hash.dateCreated);
                const jDate = new Date(j.hash.dateCreated);
                if (iDate < jDate) {
                    return 1;
                } else if (iDate > jDate) {
                    return -1;
                } else {
                    return 0;
                }
            }
            case 0: {
                if (i.name < j.name) {
                    return -1;
                } else if (i.name > j.name) {
                    return 1;
                } else {
                    return 0;
                }
            }
            case 1: {
                if (i.name > j.name) {
                    return -1;
                } else if (i.name < j.name) {
                    return 1;
                } else {
                    return 0;
                }
            }
            case 2: {
                const iDate = new Date(i.hash.dateCreated);
                const jDate = new Date(j.hash.dateCreated);
                if (iDate < jDate) {
                    return -1;
                } else if (iDate > jDate) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    });

    return filteredRainbows;
};

function showRainbowDetails(rainbow) {
    ReactDOM.unmountComponentAtNode(document.getElementById("renderRainbowModal"));
    var _child = React.createRef();
    ReactDOM.render(<RainbowDetailsModal
        r={rainbow}

        handleRemoveRainbow={removeRainbow}
        handleToggle={handleToggleRainbow}
        handleSearchPwd={searchPwdInRainbow}
        handleEdit={handleEditRainbow}
        modalAlert={modalAlert}

        ref={_child}
    />, document.getElementById("renderRainbowModal"));
    return _child;
};

function searchPwdInRainbow(uuid, hash) {
    return new Promise((resolve) => {
        client.emit('rainbow', 'search', JSON.stringify(
            { uuid, hash }
        ), (sender, message) => {
            const result = JSON.parse(message);
            resolve(result);
        });
    });
};

function handleEditRainbow(updatedSettings) {
    return new Promise((resolve, reject) => {
        client.emit('rainbow', 'edit', JSON.stringify(updatedSettings), (sender, message) => {
            if (message === 'OK') {
                resolve();
            } else if (message === 'ERR') {
                reject();
            }
        });
    });
};

function enviarAjustos(novaConf) {
    if (!verificarIP(novaConf.socketSettings.host)) {
        alert('La adre�a IP del servidor no es valida.');
        if (document.getElementById('guardatConfSrv'))
            ReactDOM.unmountComponentAtNode(document.getElementById('guardatConfSrv'));
        return;
    }
    if (novaConf.socketSettings.port <= 0 || novaConf.socketSettings.port >= 65535) {
        alert('El port no es valid. Deu ser un numero major a 0 i menor a 65535.');
        if (document.getElementById('guardatConfSrv'))
            ReactDOM.unmountComponentAtNode(document.getElementById('guardatConfSrv'));
        return;
    }
    client.emit('settings', 'writeConf', JSON.stringify(novaConf), (sender, message) => {
        if (message === 'OK') {
            // show the save icon on the save button
            ReactDOM.unmountComponentAtNode(document.getElementById('guardatConfSrv'));
            ReactDOM.render(
                <img
                    id="desar"
                    src={desar}
                    alt="desat"
                />
                , document.getElementById('guardatConfSrv'));
            // remove the button after 1.000 ms
            setTimeout(() => {
                if (document.getElementById('guardatConfSrv'))
                    ReactDOM.unmountComponentAtNode(document.getElementById('guardatConfSrv'));
            }, 1000);
        } else if (message === 'ERR') {
            modalAlert("There was a problem on the server side!\n Settings couldn't be saved!", "Server error!");
            if (document.getElementById('guardatConfSrv'))
                ReactDOM.unmountComponentAtNode(document.getElementById('guardatConfSrv'));
        }
    });
};

function showBackups() {
    var refreshing = false;
    if (currentTab === 7) {
        refreshing = true;
    } else {
        currentTab = 7;
    }

    client.unsubscribeAll();

    client.emit('backup', 'list', '', (sender, message) => {
        if (message === 'ERR') {
            ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
            ReactDOM.render(<div id="errorBackupList">
                <div>
                    <table id="tableBackupCronState">
                        <tbody id="backupCronState">

                        </tbody>
                    </table>
                </div>
                <img src={errorICO} alt="errorICO" />
                <h5>The backup service is currently unavailable on this server.</h5>
                <p>Check your settings in the settings tab and enable the backup service if you need it.</p>
            </div>, document.getElementById('renderitzarPestanya'));
            return;
        }
        const backupsFromServer = JSON.parse(message);

        if (!refreshing) {
            ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
            ReactDOM.render(<BackupList
                handleAddBackup={addBackup}
                handleRunBackup={runBackup}
            />, document.getElementById('renderitzarPestanya'));
        }

        renderBackupList(backupsFromServer);

        client.subscribe('backup', (sender, topicName, changeType, pos, value) => {
            switch (changeType) {
                case 0: {
                    backupsFromServer.push(JSON.parse(value));

                    break;
                }
                case 2: {
                    for (let i = 0; i < backupsFromServer.length; i++) {
                        if (backupsFromServer[i].uuid === value) {
                            backupsFromServer.splice(i, 1);
                            break;
                        }
                    }

                    break;
                }
            }

            renderBackupList(backupsFromServer);
        });
    });

    client.emit('backup', 'status', '', (sender, message) => {
        var state = JSON.parse(message);

        renderBackupCronState(state);

        client.subscribe('backupCron', (sender, topicName, changeType, pos, value) => {
            if (changeType === 1) {
                state = JSON.parse(value);

                renderBackupCronState(state);
            }
        });
    });
};

function renderBackupCronState(state) {
    ReactDOM.unmountComponentAtNode(document.getElementById('backupCronState'));
    ReactDOM.render(<BackupCronState
        cronState={state.cronState}
        nextBackup={state.nextBackup}
    />, document.getElementById('backupCronState'));
};

function renderBackupList(backupsFromServer) {
    const componentsToRender = [];

    backupsFromServer.forEach((element, i) => {
        componentsToRender.push(<Backup
            key={i}

            uuid={element.uuid}
            date={element.date}
            hash={element.hash}
            size={element.size}

            handleRemove={removeBackup}
        />);
    });

    ReactDOM.unmountComponentAtNode(document.getElementById('renderBackupList'));
    ReactDOM.render(
        componentsToRender, document.getElementById('renderBackupList'));
};

function runBackup() {
    client.emit('backup', 'start', '', (_, message) => {
        if (message === 'OK') {
            client.emit('backup', 'status', '', (_, msg) => {
                const state = JSON.parse(msg);

                renderBackupCronState(state);
            });
        } else {
            alert("Error running a backup on the server side");
        }
    });
};

/**
 * 
 * @param {blob} fitxer
 */
function addBackup(fitxer) {
    uploadBackup(fitxer);
};

/**
 * 
 * @param {blob} fitxer
 */
function uploadBackup(fitxer) {
    client.on('backup', (sender, message) => {
        if (message.command === 'chunkUpload') {
            const part = JSON.parse(message.message);


            const trosFitxer = fitxer.slice(part.i, (part.i + part.tamanyPart));
            client.emitBinary('backupChunkUpload', '', trosFitxer);
            console.log("i " + part.i + " tamany " + trosFitxer.size);
            if ((part.i + part.tamanyPart) >= fitxer.size) {
                fitxer = null;
            }
        }
    });
    client.emit('backup', 'upload', fitxer.size, (sender, message) => {
        if (message.message === 'ERR') {
            alert("Error per el costat del servidor!");
        }
    });

};

function removeBackup(uuid) {
    modalConfirm(uuid, () => {
        client.emit('backup', 'remove', uuid, (_, message) => {
            if (message === 'ERR') {
                alert("Error om the server side removing backup. Please refresh.");
            }
        });
    });
};

/**
 * Prints a list of the logs downloaded by the server, or prints an error screen if there is nothing to download.
 * */
async function showLogs() {
    currentTab = 8;

    ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
    ReactDOM.render(<LoadingScreen />, document.getElementById('renderitzarPestanya'));

    client.unsubscribeAll();

    var logsFromServer = [];
    var rows = 500;

    ReactDOM.unmountComponentAtNode(document.getElementById('renderitzarPestanya'));
    ReactDOM.render(<LogsList
        handleFilter={(event) => {
            filterLogs(logsFromServer, event);
        }}
        handleChangeLimit={(start, limitRows) => {
            rows = limitRows;
            showLogsChangeLimit(logsFromServer, start, limitRows);
        }}
    />, document.getElementById('renderitzarPestanya'));

    ReactDOM.unmountComponentAtNode(document.getElementById('exportLogs'));
    ReactDOM.render(<div>
        <img src={loadingICO} alt="loadingICO" id="loadingSpinner" />
        <p style={{ 'color': 'white' }}>Loading...</p>
    </div>, document.getElementById('exportLogs'));

    // subscribe to automatically update logs from now
    client.subscribe('logs', (sender, topicName, changeType, pos, value) => {
        switch (changeType) {
            case 0: {
                // add the new log at the beggining of the list
                const logs = JSON.parse(value);
                logsFromServer.unshift(logs);

                // remove the excess of rows ocurred when adding new rows to a limited list
                if (logsFromServer.length > rows) {
                    logsFromServer = logsFromServer.splice(0, logsFromServer.length - rows);
                }

                break;
            }
            case 2: {
                logsFromServer = [];
            }
        }

        renderLogs(logsFromServer, true);
    });

    const CHUNK_SIZE = 10;
    var cont = true;

    // progressively download logs on arrays of 10 positions
    while (logsFromServer.length <= rows && cont) {
        await new Promise((resolve, reject) => {
            client.emit('logs', 'list', JSON.stringify({ offset: logsFromServer.length, limit: CHUNK_SIZE }), (sender, message) => {
                const newElements = JSON.parse(message);
                if (newElements.length === 0) {
                    cont = false;
                }
                logsFromServer = logsFromServer.concat(newElements);
                if (newElements === null || logsFromServer === null) {
                    cont = false;
                }
                if (renderLogs(logsFromServer) === false) {
                    errorListingLogs();
                    cont = false;
                }
                resolve();
            });
        });
    }

    cont = true;
    if (document.getElementById('exportLogs')) {
        ReactDOM.unmountComponentAtNode(document.getElementById('exportLogs'));
        ReactDOM.render(<div>
            <button type="button" className="btn btn-outline-light" onClick={() => {
                exportarAJSON(logsFromServer, "logs.json");
            }}>Export to JSON</button>
        </div>, document.getElementById('exportLogs'));
    }

    if (document.getElementById('clearLogs')) {
        ReactDOM.render(<div>
            <button type="button" className="btn btn-outline-danger" onClick={clearLogs}>Clear logs</button>
        </div>, document.getElementById('clearLogs'));
    }
};

function errorListingLogs() {
    ReactDOM.render(<LogsListError />
        , document.getElementById('renderitzarPestanya'));
};

function renderLogs(logs, unmount = false) {
    if (!document.getElementById('renderLogsList')) {
        return false;
    }
    if (unmount)
        ReactDOM.unmountComponentAtNode(document.getElementById('renderLogsList'));

    const components = [];
    logs.forEach((element, i) => {
        components.push(<Logs
            log={element}

            key={i}
        />);
    });

    ReactDOM.render(components, document.getElementById('renderLogsList'));
};

function filterLogs(logs, event = -1) {
    if (event === -1) {
        renderLogs(logs, true);
        return;
    }

    const filteredResults = logs.filter((element) => {
        return (element.type === event);
    });

    renderLogs(filteredResults, true);

};

async function showLogsChangeLimit(logsFromServer, start, rows) {

    if (rows > logsFromServer.length) {
        const CHUNK_SIZE = 10;
        var cont = true;
        const query = { offset: 0, limit: CHUNK_SIZE };

        if (start !== "") {
            query.start = new Date(start);
        }

        while (logsFromServer.length <= rows && cont) {
            query.offset = logsFromServer.length;
            query.limit = Math.min(rows - logsFromServer.length, CHUNK_SIZE);
            if (query.limit <= 0) {
                break;
            }

            await new Promise((resolve, reject) => {
                client.emit('logs', 'list', JSON.stringify(query), (sender, message) => {
                    const newElements = JSON.parse(message);
                    if (newElements === null || newElements.length === 0) {
                        cont = false;
                    }
                    logsFromServer = logsFromServer.concat(newElements);
                    resolve();
                });
            });
        }

    } else if (rows < logsFromServer.length) {
        logsFromServer = logsFromServer.splice(0, logsFromServer.length - rows);
    }

    renderLogs(logsFromServer, true);
};

function clearLogs() {
    return new Promise((resolve, reject) => {
        modalConfirm('', () => {
            client.emit('logs', 'clear', '', (_, msg) => {
                if (msg === 'OK') {
                    resolve();
                } else {
                    modalAlert('Could not delete the logs from the server. Refresh or try again later...', 'Server error');
                    reject();
                }
            });
        }, () => {
            reject();
        }, 'Are you sure you want to delete all the logs from the server?', 'Clear logs?');
    });
};

function modalConfirm(confirmName = null, handleOk = null, handleCancel = null, customBody = null, customText = null, okText = null, cancelText = null) {
    ReactDOM.unmountComponentAtNode(document.getElementById("renderComfirmModal"));
    ReactDOM.render(<DeleteConfirm
        confirmName={confirmName}
        handleOk={handleOk}
        handleCancel={handleCancel}
        customBody={customBody}
        customText={customText}
        okText={okText}
        cancelText={cancelText}
    />, document.getElementById("renderComfirmModal"));
};

function modalAlert(body, title) {
    ReactDOM.unmountComponentAtNode(document.getElementById("renderAlertModal"));
    ReactDOM.render(<Alert
        title={title}
        body={body}
    />, document.getElementById("renderAlertModal"));
};

function verificarIP(ipaddress) {
    return (
        /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
            .test(ipaddress));
}

function importarConfigJSON() {
    if (document.getElementById("fitxerJSON").files.length === 0) {
        return;
    }
    const fitxer = document.getElementById("fitxerJSON").files[0];
    var configCarregada;
    try {
        configCarregada = JSON.parse(fitxer);
    } catch (_) {
        return;
    }
    enviarAjustos(configCarregada);
}

function carregarConfig() {
    return new Promise((resolve) => {
        // carregar les cookies i buscar la cookie de configuracio del client
        const cookies = document.cookie.split(';');
        cookies.forEach((cookie) => {
            const valorsCookieSel = cookie.trim().split('=');
            // si la hem trovat, parsejarla altra vegada a objecte.
            if (valorsCookieSel[0] === 'config') {
                CONFIG = JSON.parse(valorsCookieSel[1]);
                resolve();
            }
        });
        // ja que no existeix, crarla per primera vegada
        if (CONFIG === null) {
            CONFIG = { token: '' };
            resolve();
            console.log('Generant la nova config', CONFIG);
            guardarConfig(CONFIG);
        }
    });
}

function guardarConfig(novaConf) {
    // save the new configuration permanently in a cookie
    document.cookie = "config=" + JSON.stringify(novaConf);
}

function guardarSrv() {
    return new Promise((resolve, reject) => {
        client.emit('save', '', '', (sender, message) => {
            if (message === 'OK') {
                resolve();
            } else {
                reject();
            }
        });
    });
}

async function apagarSrv() {
    await guardarSrv();
    ReactDOM.unmountComponentAtNode(document.getElementById("renderComfirmModal"));
    ReactDOM.render(<DeleteConfirm
        customText={"Are you sure?"}
        customBody={"This will completely stop the processing server. You won't be able to log in nor process in background till you restart it."}
        handleOk={() => { client.emit('exit'); }}
        okText="Shutdown"
    />, document.getElementById("renderComfirmModal"));
}

function eixir() {
    ReactDOM.unmountComponentAtNode(document.getElementById("renderComfirmModal"));
    ReactDOM.render(<DeleteConfirm
        customText={"Are you sure?"}
        customBody={"This will log you out of the server."}
        handleOk={logout}
    />, document.getElementById("renderComfirmModal"));
};

async function logout() {
    currentTab = -1;
    if (CONFIG.token !== '') {
        await new Promise((resolve) => {
            client.emit('token', 'revoke', CONFIG.token, (_, __) => {
                resolve();
            });
        });
        CONFIG = { token: '' };
        guardarConfig(CONFIG);
    }
    client.client.close();
    client = null;

};

main();
