# aryptiOR

aryptiOR is an open-source software that takes advantage of distributed computing to recover passwords by a given hash. This can be managed with an easy graphical interface, and an easy installation.

## Webpage

```
http://izcloud.tk/
```

## Installation

### Docker (Linux)

Use the docker image [(repo)](https://hub.docker.com/r/itzanchan/aryptior) to install the server.

```bash
sudo docker run -p 22215-22216:22215-22216 -d itzanchan/aryptior:server
```

Pull and run the image for the web page, used as a GUI, for example, in the port 80.
```bash
sudo docker run -p 80:80 -d itzanchan/aryptior:web
```

Install the client in such many terminals as you can to speed up the processing. You can pull the image, run in a container and connect to a host via command line arguments in a single line.

```bash
sudo docker run -d itzanchan/aryptior:client host=izcloud.tk
```

### Binaries

Unless you are running this software under a Windows machine, you will need to install the [mono](https://www.mono-project.com/download/stable/) compiler in order to run code written in .NET, which is the case of server and the client.
Take a look at the [download section](http://izcloud.tk/index.php/descargas/). If you get lost installing and running the software, please, take a look at the [install page](http://izcloud.tk/index.php/instalacion/).

## Usage

If you haven't yet, take a look at the [first steps](http://izcloud.tk/index.php/instalacion/#PRIMEROS_PASOS) on the web.

Start the server using the binaries or using the Docker image. To start the server normally using the default settings: (can be configured later through the web interface)
```
sudo docker run -p 22215-22216:22215-22216 -d itzanchan/aryptior:server
```
Example of modifying the default settings through command line arguments. Modifying every parameter:
```
sudo docker run -p 22215-22216:22215-22216 -d itzanchan/aryptior:server configPath=config.yaml host=192.168.1.3 port=22214 hostws=192.168.1.3 portws=22222 requireClientPassword=true useDatabase=true mongohost=127.0.0.1 mongoport=27017 userName=app password=password databaseName=aryptior mongoDbAuthMechanism=SCRAM-SHA-256 isDiscoverable=false discoverableName=PEPESERVER
```
If you are connecting to a MongoDB using a *mongodb://* or *mongodb+srv://* URI, for example, from [MongoDB Atlas](https://www.mongodb.com/cloud/atlas), you don't need to specify every field on the URI manually, instead use the **dburl** argument:
```
sudo docker run -p 22215-22216:22215-22216 -d itzanchan/aryptior:server useDatabase=true dburl=mongodb+srv://app:password@blahblah.blah.mongodb.net/test?authSource=admin"&"replicaSet=blah-shard-0"&"readPreference=primary"&"ssl=true databaseName=aryptior
```

Access the webpage you have running in docker, or in an existing web server, on a browser. For example:
```
http://localhost/?insecure=true
```
The default password is **1234**. After logging in by the first time, you will be prompted to change the password. If you have an SSL certificate, please upload it in the settings tab. If you don't have one, the '?insecure=true' parameter will be always necessary to connect to the server.

Install and connect some processing clients, running them with parameters "service host=[address]". You can check at any time the clients that are already connected on the clients tab on the web.

```bash
sudo docker run -d itzanchan/aryptior:client host=[address]
```

If you have some clients ready to process, it's a good moment to add the hash to the queue. Go to the queue tab, click on the 'Add new hash' button, fill the form with the hash data and the combinations you want to try, and save. If you have a file with a format to store hashes (for example, a copy of /etc/shadow) you can add it automaticallý using a wizard you can launch using the 'Import' button at the top of the page.

## Examples of the hash format

| ALGORITHM | HASH                                                                                                                             | CLEARTEXT PASSWORD |
|-----------|----------------------------------------------------------------------------------------------------------------------------------|--------------------|
| NTLM      | DE3410A470D9BA49C02838B66089BF2C                                                                                                 | ALPHA13            |
| MD4       | dc1f1098cea95c11e9b126789d12d373                                                                                                 | aryptiOR           |
| MD5       | 1378c2dc269b38c9d7849409a667a370                                                                                                 | aryptiOR           |
| SHA1      | 265392dc2782778664cc9d56c8e3cd9956661bb                                                                                          | pepe               |
| SHA256    | 6f975e731fd58baa79482d05e252456a4b4df9a17b6dc1c81b4ca15cba77a486                                                                 | aryptiOR           |
| SHA384    | bfa5267a466afdebe456e60c4f83803109b75400bd4c1dddb948985e57e2fd2c6a491c747ea2e27fc24ba2d94e4b94d8                                 | pepe               |
| SHA512    | aa0b8d4339f9f145719d8119250237664d1410c31c1d7a97f79d68f21c906d01bbd54cd05298ad380168159475bba9ba585423fcde4809b5d429d665c137f113 | william11          |
| BCRYPT    | $2a$04$E14Mm0q7C6t0sUpnfO8M4ebrjYlTtaxe14fdiJgwtuae/XOTx/x0.                                                                     | pepe               |
| RIPEMD128 | 43978a115c4fe1591ab438f841151929                                                                                                 | pepe               |
| RIPEMD160 | 27e7c3e457f8a7796aa4915b0afdd8831000f9b4                                                                                         | pepe               |
| RIPEMD256 | 69aa31d2e16058d593d672cfb865d55084db85e534b19e754341565093ab00da                                                                 | pepe               |
| RIPEMD320 | ed43feb760e065d8e687326b8f875a2355258cea8ef560f98a6df135d9174d35d3b74473811e6064                                                 | pepe               |
| BLAKE     | 220674ed7e94497a26acf49d689ded4bb78bf5d1f7194351253371d3                                                                         | pepe               |
| BLAKE2b   | bac9b93e4d1fdcc021698ce839ecd56e0e3ac044fa8eb84e1cc5498e079c029a82514667a8a9834e771e0ddaf8eca6330c34d85b7c98404fb32ad639b81b9202 | pepe               |
| BLAKE2s   | cbc4fa893a052d50ccd97ed7519614652270ea8a7beb23add1a4b35bba3b74be                                                                 | pepe               |
| WHIRLPOOL | 8b0a6345d2c819bcbdde154f72d5e00229276802dc05ef91d5e837d9a90abdeddd4ba26e2f94ec40097661a9b23229c5a776207e51e3f074638afea86da80994 | pepe               |
| TIGER     | c8fc69a10c814cc259fecc9254bdcddd1ef1c1b14be173e5                                                                                 | pep                |
| TIGER2    | adc661876ba3105c64a1f2764c664b71cf4e89a7d44d61b1                                                                                 | pepe               |
| TIGER3    | bd8a0cbfb109c6177d6fe5a436f2e2aedff4aa754492db29                                                                                 | pepe               |
| TIGER4    | a46c83571b7e00dbf0c24bd018d42a3f23b03899f006c25d                                                                                 | pepe               |
| MODULAR   | $6$pepepepe$TfCF7JzGhb.2jHMl3qctH8Z7VqYThWr.BN5sXqTxuUrJMi/Fz6po0TrzopU/oTNmycuZIjjWeenDdP19FsCuX.                               | pepe               |
| MYSQL     | *A4B6157319038724E3560894F7F932C8886EBFCF                                                                                        | 1234               |
| WORDPRESS | $P$BR7Mhnlbwhzpln05mYhUFSTK.lm6yx0                                                                                               | 1234               |

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)