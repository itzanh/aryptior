﻿# -*- coding: <utf-8> -*-


import hashlib
import json


class Hash:
    def __init__(self, hash, tipus, primerCaracterUnicode, ultimCaracterUnicode, caractersInicials, caractersFinals,
                 iteracions, salt, initialTestCount, msProcessingCount, descripcio):
        self.hash = hash
        self.tipus = tipus
        self.primerCaracterUnicode = primerCaracterUnicode
        self.ultimCaracterUnicode = ultimCaracterUnicode
        self.caractersFinals = caractersFinals
        self.iteracions = iteracions
        self.salt = salt
        self.initialTestCount = initialTestCount
        self.msProcessingCount = msProcessingCount
        self.descripcio = descripcio

        self.caracters = list()
        for i in range(caractersInicials):
            self.caracters.append(primerCaracterUnicode)


def main():
    salir = False
    while not salir:
        print("""************************************
-- FERRAMENTA DE CALCUL DE HASHES --
************************************""")
        print("Introdueix el text\n> ", end='')
        text = input()
        print("Introdueix el algorisme [NTLM] (MD4/NTLM/MD5/SHA1/SHA256/SHA384/SHA512/BLAKE2b/BLAKE2s)\n> ", end='')
        algorisme = input().upper()
        if algorisme == '':
            algorisme = 'NTLM'
        print("Introdueix les iteracions (>1)\n> ", end='')
        try:
            iteracions = int(input())
        except:
            print("ASI VA UN NUMERO. ERES MU TONTO!!!")
            return
        if iteracions <= 0:
            print("Error, deus introduir un numero de iteracions positiu i major a 0")
            return
        hash = calcularHash(text, algorisme, iteracions)
        if hash != '':
            mostrarResultats(text, algorisme, iteracions, hash)
            guardarResultats(text, algorisme, iteracions, hash)
        print("Vols exportar a un bon Jotason [N] (S/N)")
        exportar = input()
        if (exportar.upper()[0] == 'S'):
            exportarJSON(text, algorisme, iteracions, hash)
        print("Vols continuar executant [N] (S/N)")
        repetir = input()
        if (repetir == '') or (repetir.upper()[0] == 'N'):
            salir = True


def calcularHash(text, algorisme, iteracions):
    if algorisme == 'NTLM':
        hashActual = ''
        for char in text:
            hashActual += char + '\x00'
    else:
        hashActual = text

    for i in range(iteracions):
        if algorisme == 'SHA256':
            hash = hashlib.sha256()
            hash.update(hashActual.encode('utf-8'))
            hashActual = (hash.hexdigest())
        elif algorisme == 'SHA512':
            hash = hashlib.sha512()
            hash.update(hashActual.encode('utf-8'))
            hashActual = (hash.hexdigest())
        elif algorisme == 'MD5':
            hash = hashlib.md5()
            hash.update(hashActual.encode('utf-8'))
            hashActual = (hash.hexdigest())
        elif algorisme == 'MD4':
            hash = hashlib.new("md4", hashActual.encode('utf-8'))
            hashActual = (hash.hexdigest())
        elif algorisme == 'NTLM':
            hash = hashlib.new("md4", hashActual.encode('utf-8'))
            hashActual = (hash.hexdigest()).upper()
        elif algorisme == 'SHA1':
            hash = hashlib.sha1()
            hash.update(hashActual.encode('utf-8'))
            hashActual = (hash.hexdigest())
        elif algorisme == 'SHA384':
            hash = hashlib.sha3_384()
            hash.update(hashActual.encode('utf-8'))
            hashActual = (hash.hexdigest())
        elif algorisme == 'BLAKE2B':
            hash = hashlib.blake2b()
            hash.update(hashActual.encode('utf-8'))
            hashActual = (hash.hexdigest())
        elif algorisme == 'BLAKE2S':
            hash = hashlib.blake2s()
            hash.update(hashActual.encode('utf-8'))
            hashActual = (hash.hexdigest())
        else:
            print("ALGORISME NO RECONEGUT!!!" + algorisme + "!")
            return ''
    return (hashActual)


def mostrarResultats(text, algorisme, iteracions, hash):
    print("""************************************
--        CALCUL FINALITZAT       --
************************************""")
    print("(" + algorisme + ") >> " + text + " =>> " + hash)
    print("Iteracions: " + str(iteracions))
    print("************************************")


def guardarResultats(text, algorisme, iteracions, hash):
    try:
        fitxer = open("registre.log", "at")
        fitxer.write(algorisme + ":" + str(iteracions) + ":" + hash + ":" + text + "\n")
        fitxer.close()
    except:
        print("ERROR! No se ha pogut escriure a registre.log")


def exportarJSON(text, algorisme, iteracions, hash):
    print('Primer caracter unicode? (97)')
    primerCaracterUnicode = input()
    if (primerCaracterUnicode == ''):
        primerCaracterUnicode = 97
    else:
        primerCaracterUnicode = int(primerCaracterUnicode)
    print('Ultim caracter unicode? (122)')
    ultimCaracterUnicode = input()
    if (ultimCaracterUnicode == ''):
        ultimCaracterUnicode = 122
    else:
        ultimCaracterUnicode = int(ultimCaracterUnicode)
    print('Caracters inicials? (1)')
    caractersInicials = input()
    if (caractersInicials == ''):
        caractersInicials = 1
    else:
        caractersInicials = int(caractersInicials)
    print('Caracters finals? (12)')
    caractersFinals = input()
    if (caractersFinals == ''):
        caractersFinals = 12
    else:
        caractersFinals = int(caractersFinals)
    print('Tamany de test inicial? (75000)')
    initialTestCount = input()
    if (initialTestCount == ''):
        initialTestCount = 75000
    else:
        initialTestCount = int(initialTestCount)
    print('Temps de processament (ms)? (60000)')
    msProcessingCount = input()
    if (msProcessingCount == ''):
        msProcessingCount = 60000
    else:
        msProcessingCount = int(msProcessingCount)
    h = Hash(hash, algorisme, primerCaracterUnicode, ultimCaracterUnicode, caractersInicials, caractersFinals,
             iteracions, '', initialTestCount, msProcessingCount, text)
    print("************************************")
    print(json.dumps(h.__dict__))


main()
