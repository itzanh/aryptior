﻿using System;
using System.Collections.Concurrent;

namespace ProvaFils
{

    class DictionaryThread : BruteForceThread
    {
        protected ConcurrentQueue<string> combinaciones;

        public DictionaryThread(Hash hash, int contarCaracters, ConcurrentQueue<string> combinaciones) : base(hash, contarCaracters)
        {
            this.combinaciones = combinaciones;
        }

        public new void Run()
        {
            this.searchHash();
        }

        protected new void searchHash()
        {
            for (int i = 0; i < this.charactersCount; i++)
            {
                // generate string from current character combination

                ClientThread.dictionary_produced.WaitOne();
                if (!combinaciones.TryDequeue(out string combination))
                {
                    i--;
                    ClientThread.dictionary_consumed.Release();
                }
                ClientThread.dictionary_consumed.Release();

                // compute the hash of the generated combination

                // check if it's the hash we are looking for
                if (!isProcessingRainbow && Utilities.verifyHash(this.hash.hash, combination, this.hash.tipus, this.hash.iteracions))
                {
                    Console.WriteLine("PASSWORD FOUND :D " + combination);
                    found = true;
                    passwordFound = combination;
                    return;
                }
                else if (isProcessingRainbow)
                {
                    string currentHash = Utilities.computeHash(combination, this.hash.tipus, this.hash.iteracions);
                    this.computed.Enqueue(new ComputedHash((this.hash.salt + combination), currentHash));
                }
            }
        }
    }
}
