﻿using HashLib;
using SauceControl.Blake2Fast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ProvaFils
{
    public static class Utilities
    {

        /// <summary>
        /// Computes the hash o a string with the specified algorithm and iterations.
        /// </summary>
        /// <param name="combination">cleartext password to be computed</param>
        /// <param name="type">Hash algorithm: NTLM, MD4, MD5, SHA1, SHA256, SHA512</param>
        /// <param name="interations">how many times is it necessary to compute the hash. minimum 1.</param>
        /// <returns>return the combinarion hashed with the specified settings</returns>
        public static bool verifyHash(string hash, string cleartextPwd, Tipus type, int iterations = 1)
        {
            switch (type)
            {
                case Tipus.BCRYPT:
                    {
                        return BCrypt.Net.BCrypt.Verify(cleartextPwd, hash);
                    }
                case Tipus.BLAKE:
                    {
                        IHash ihash = HashFactory.Crypto.SHA3.CreateBlake((HashSize)(hash.Length / 2));
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.BLAKE2b:
                    {
                        return hashToHex(Blake2b.ComputeHash(new Span<byte>(Encoding.UTF8.GetBytes(cleartextPwd))))
                    .Equals(hash);
                    }
                case Tipus.BLAKE2s:
                    {
                        return hashToHex(Blake2s.ComputeHash(new Span<byte>(Encoding.UTF8.GetBytes(cleartextPwd))))
                    .Equals(hash);
                    }
                case Tipus.WHIRLPOOL:
                    {
                        IHash ihash = HashFactory.Crypto.CreateWhirlpool();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.RIPEMD128:
                    {
                        IHash ihash = HashFactory.Crypto.CreateRIPEMD128();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        if (cleartextPwd.Equals("pepe")) Console.WriteLine(hashToHex(hashResult.GetBytes()));
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.RIPEMD256:
                    {
                        IHash ihash = HashFactory.Crypto.CreateRIPEMD256();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.RIPEMD320:
                    {
                        IHash ihash = HashFactory.Crypto.CreateRIPEMD320();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.TIGER:
                    {
                        IHash ihash = HashFactory.Crypto.CreateTiger((HashRounds)iterations);
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.TIGER2:
                    {
                        IHash ihash = HashFactory.Crypto.CreateTiger2();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.TIGER3:
                    {
                        IHash ihash = HashFactory.Crypto.CreateTiger_3_192();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        if (cleartextPwd.Equals("pepe")) Console.WriteLine(hashToHex(hashResult.GetBytes()));
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
                case Tipus.TIGER4:
                    {
                        IHash ihash = HashFactory.Crypto.CreateTiger_4_192();
                        HashResult hashResult = ihash.ComputeString(cleartextPwd, Encoding.UTF8);
                        if (cleartextPwd.Equals("pepe")) Console.WriteLine(hashToHex(hashResult.GetBytes()));
                        return hashToHex(hashResult.GetBytes()).Equals(hash);
                    }
            }

            string currentHash = cleartextPwd;
            if (type == Tipus.NTLM)
            {
                // generar NTLM
                string hashInNTLM = "";
                char[] combinationCharacters = currentHash.ToCharArray();
                for (int j = 0; j < combinationCharacters.Length; j++)
                {
                    hashInNTLM += combinationCharacters[j];
                    hashInNTLM += '\x00';
                }
                currentHash = hashInNTLM;
            }
            ;
            for (int i = 0; i < iterations; i++)
            {
                switch (type)
                {
                    case Tipus.NTLM:
                        {

                            currentHash = computeMD4(currentHash).ToUpper();
                            break;
                        }
                    case Tipus.MD4:
                        {
                            currentHash = computeMD4(currentHash);
                            break;
                        }
                    case Tipus.MD5:
                        {
                            MD5 md5 = MD5.Create();
                            byte[] hashBinari = md5.ComputeHash(Encoding.UTF8.GetBytes(currentHash));
                            currentHash = hashToHex(hashBinari);
                            md5.Dispose();
                            break;
                        }
                    case Tipus.SHA1:
                        {
                            SHA1 sha1 = SHA1.Create();
                            byte[] hashBinari = sha1.ComputeHash(Encoding.UTF8.GetBytes(currentHash));
                            currentHash = hashToHex(hashBinari).Substring(0, 39);
                            sha1.Dispose();
                            break;
                        }
                    case Tipus.SHA256:
                        {
                            SHA256 sha256 = SHA256.Create();
                            byte[] hashBinari = sha256.ComputeHash(Encoding.UTF8.GetBytes(currentHash));
                            currentHash = hashToHex(hashBinari);
                            sha256.Dispose();
                            break;
                        }
                    case Tipus.SHA384:
                        {
                            SHA384 sha384 = SHA384.Create();
                            byte[] hashBinari = sha384.ComputeHash(Encoding.UTF8.GetBytes(currentHash));
                            currentHash = hashToHex(hashBinari);
                            break;
                        }
                    case Tipus.SHA512:
                        {
                            SHA512 sha512 = SHA512.Create();
                            byte[] hashBinari = sha512.ComputeHash(Encoding.UTF8.GetBytes(currentHash));
                            currentHash = hashToHex(hashBinari);
                            sha512.Dispose();
                            break;
                        }
                    case Tipus.RIPEMD160:
                        {
                            RIPEMD160 rIPEMD160 = RIPEMD160.Create();
                            byte[] hashBinari = rIPEMD160.ComputeHash(Encoding.UTF8.GetBytes(currentHash));
                            currentHash = hashToHex(hashBinari);
                            rIPEMD160.Dispose();
                            break;
                        }
                    case Tipus.MODULAR:
                        {
                            return CryptSharp.Crypter.CheckPassword(cleartextPwd, hash);
                        }
                    case Tipus.MYSQL:
                        {
                            return (GenerateMySQL5PasswordHash(cleartextPwd)).Equals(hash);
                        }
                    case Tipus.WORDPRESS:
                        {
                            return (WordPressHashEncode(cleartextPwd, hash)).Equals(hash);
                        }
                }
            }
            return currentHash.Equals(hash);
        }

        public static string computeHash(string cleartextPwd, Tipus type, int iterations = 1)
        {
            if (type == Tipus.BCRYPT)
            {
                return BCrypt.Net.BCrypt.HashPassword(cleartextPwd, iterations);
            }

            string currentHash = cleartextPwd;
            if (type == Tipus.NTLM)
            {
                // generar NTLM
                string hashInNTLM = "";
                char[] combinationCharacters = currentHash.ToCharArray();
                for (int j = 0; j < combinationCharacters.Length; j++)
                {
                    hashInNTLM += combinationCharacters[j];
                    hashInNTLM += '\x00';
                }
                currentHash = hashInNTLM;
            }
            ;
            for (int i = 0; i < iterations; i++)
            {
                switch (type)
                {
                    case Tipus.NTLM:
                        {

                            currentHash = computeMD4(currentHash).ToUpper();
                            break;
                        }
                    case Tipus.MD4:
                        {
                            currentHash = computeMD4(currentHash);
                            break;
                        }
                    case Tipus.MD5:
                        {
                            MD5 md5 = MD5.Create();
                            byte[] hashBinari = md5.ComputeHash(Encoding.UTF8.GetBytes(currentHash));
                            currentHash = hashToHex(hashBinari);
                            md5.Dispose();
                            break;
                        }
                    case Tipus.SHA1:
                        {
                            SHA1 sha1 = SHA1.Create();
                            byte[] hashBinari = sha1.ComputeHash(Encoding.UTF8.GetBytes(currentHash));
                            currentHash = hashToHex(hashBinari);
                            sha1.Dispose();
                            break;
                        }
                    case Tipus.SHA256:
                        {
                            SHA256 sha256 = SHA256.Create();
                            byte[] hashBinari = sha256.ComputeHash(Encoding.UTF8.GetBytes(currentHash));
                            currentHash = hashToHex(hashBinari);
                            sha256.Dispose();
                            break;
                        }
                    case Tipus.SHA512:
                        {
                            SHA512 sha512 = SHA512.Create();
                            byte[] hashBinari = sha512.ComputeHash(Encoding.UTF8.GetBytes(currentHash));
                            currentHash = hashToHex(hashBinari);
                            sha512.Dispose();
                            break;
                        }
                }
            }
            return currentHash;
        }

        private static string GenerateMySQL5PasswordHash(string password)
        {
            return GenerateMySQL5PasswordHash(password, Encoding.UTF8);
        }

        private static string GenerateMySQL5PasswordHash(string password, Encoding textEncoding)
        {
            byte[] passBytes = textEncoding.GetBytes(password);
            byte[] hash;
            using (var sha1 = SHA1.Create())
            {
                hash = sha1.ComputeHash(passBytes);
                hash = sha1.ComputeHash(hash);
            }

            var sb = new StringBuilder();
            sb.Append("*");
            for (int i = 0; i < hash.Length; i++)
                sb.AppendFormat("{0:X2}", hash[i]);

            return sb.ToString();
        }

        private static string WordPressHashEncode(string password, string hash)
        {
            const string itoa64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            string output = "*0";
            if (hash == null)
            {
                return output;
            }

            if (hash.StartsWith(output))
                output = "*1";

            string id = hash.Substring(0, 3);
            // We use "$P$", phpBB3 uses "$H$" for the same thing
            if (id != "$P$" && id != "$H$")
                return output;

            // get who many times will generate the hash
            int count_log2 = itoa64.IndexOf(hash[3]);
            if (count_log2 < 7 || count_log2 > 30)
                return output;

            int count = 1 << count_log2;

            string salt = hash.Substring(4, 8);
            if (salt.Length != 8)
                return output;

            byte[] hashBytes = { };
            using (MD5 md5Hash = MD5.Create())
            {
                hashBytes = md5Hash.ComputeHash(Encoding.ASCII.GetBytes(salt + password));
                byte[] passBytes = Encoding.ASCII.GetBytes(password);
                do
                {
                    hashBytes = md5Hash.ComputeHash(hashBytes.Concat(passBytes).ToArray());
                } while (--count > 0);
            }

            output = hash.Substring(0, 12);
            string newHash = Encode64(hashBytes, 16);

            return output + newHash;
        }

        private static string Encode64(byte[] input, int count)
        {
            const string itoa64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            StringBuilder sb = new StringBuilder();
            int i = 0;
            do
            {
                int value = (int)input[i++];
                sb.Append(itoa64[value & 0x3f]); // to uppercase
                if (i < count)
                    value = value | ((int)input[i] << 8);
                sb.Append(itoa64[(value >> 6) & 0x3f]);
                if (i++ >= count)
                    break;
                if (i < count)
                    value = value | ((int)input[i] << 16);
                sb.Append(itoa64[(value >> 12) & 0x3f]);
                if (i++ >= count)
                    break;
                sb.Append(itoa64[(value >> 18) & 0x3f]);
            } while (i < count);

            return sb.ToString();
        }

        /// <summary>
        /// Converts a given hash in binary format and returns a string 
        /// with the hash represented in hexadecimal format in lower case.
        /// </summary>
        /// <param name="hash">hash result to convert</param>
        /// <returns></returns>
        private static string hashToHex(byte[] hash)
        {
            StringBuilder str = new StringBuilder();
            foreach (byte bSel in hash)
            {
                str.Append(bSel.ToString("x2"));
            }
            return str.ToString();
        }

        /// <summary>
        /// Calculates the hash in MD4 algorithm from a specified cleartext password.
        /// </summary>
        /// <param name="entrada"></param>
        /// <returns></returns>
        private static string computeMD4(string entrada)
        {
            // get padded uints from bytes
            List<byte> bytes = Encoding.ASCII.GetBytes(entrada).ToList();
            uint bitCount = (uint)(bytes.Count) * 8;
            bytes.Add(128);
            while (bytes.Count % 64 != 56) bytes.Add(0);
            var uints = new List<uint>();
            for (int i = 0; i + 3 < bytes.Count; i += 4)
                uints.Add(bytes[i] | (uint)bytes[i + 1] << 8 | (uint)bytes[i + 2] << 16 | (uint)bytes[i + 3] << 24);
            uints.Add(bitCount);
            uints.Add(0);

            // run rounds
            uint a = 0x67452301, b = 0xefcdab89, c = 0x98badcfe, d = 0x10325476;
            Func<uint, uint, uint> rol = (x, y) => x << (int)y | x >> 32 - (int)y;
            for (int q = 0; q + 15 < uints.Count; q += 16)
            {
                var chunk = uints.GetRange(q, 16);
                uint aa = a, bb = b, cc = c, dd = d;
                Action<Func<uint, uint, uint, uint>, uint[]> round = (f, y) =>
                {
                    foreach (uint i in new[] { y[0], y[1], y[2], y[3] })
                    {
                        a = rol(a + f(b, c, d) + chunk[(int)(i + y[4])] + y[12], y[8]);
                        d = rol(d + f(a, b, c) + chunk[(int)(i + y[5])] + y[12], y[9]);
                        c = rol(c + f(d, a, b) + chunk[(int)(i + y[6])] + y[12], y[10]);
                        b = rol(b + f(c, d, a) + chunk[(int)(i + y[7])] + y[12], y[11]);
                    }
                };
                round((x, y, z) => (x & y) | (~x & z), new uint[] { 0, 4, 8, 12, 0, 1, 2, 3, 3, 7, 11, 19, 0 });
                round((x, y, z) => (x & y) | (x & z) | (y & z), new uint[] { 0, 1, 2, 3, 0, 4, 8, 12, 3, 5, 9, 13, 0x5a827999 });
                round((x, y, z) => x ^ y ^ z, new uint[] { 0, 2, 1, 3, 0, 8, 4, 12, 3, 9, 11, 15, 0x6ed9eba1 });
                a += aa; b += bb; c += cc; d += dd;
            }

            // return hex encoded string
            byte[] outBytes = new[] { a, b, c, d }.SelectMany(BitConverter.GetBytes).ToArray();
            return BitConverter.ToString(outBytes).Replace("-", "").ToLower();
        }

        public static void PadRight(bool lessOne)
        {
            PadRight(1, String.Empty, lessOne);
        }

        public static void PadRight(string message, bool lessOne = false)
        {
            PadRight(1, message, lessOne);
        }

        public static void PadRight(int times = 1, string message = "", bool lessOne = false)
        {
            for (int i = 0; i < times; i++)
            {
                Console.Write(message.PadRight(
                    (Console.WindowWidth > 0 ? Console.WindowWidth : 100)
                    -
                    (lessOne ? Console.CursorLeft + 1 : Console.CursorLeft)
                    ));
            }
        }

        public static bool showHelp()
        {
            if (Program.parameters == null || !(Program.parameters.ContainsKey("--help") || Program.parameters.ContainsKey("-help") || Program.parameters.ContainsKey("help")
                    || Program.parameters.ContainsKey("-h") || Program.parameters.ContainsKey("h")))
            {
                return false;
            }

            Console.WriteLine("                         _   _  ____  _____  \n"
                + "                        | | (_)/ __ \\|  __ \\ \n"
                + "   __ _ _ __ _   _ _ __ | |_ _| |  | | |__) |\n"
                + "  / _` | '__| | | | '_ \\| __| | |  | |  _  / \n"
                + " | (_| | |  | |_| | |_) | |_| | |__| | | \\ \\ \n"
                + "  \\__,_|_|   \\__, | .__/ \\__|_|\\____/|_|  \\_\\\n"
                + "              __/ | |                        \n"
                + "             |___/|_|                        \n");

            Console.WriteLine(">>>> AYUDA\t\t[http://izcloud.tk/index.php/documentacion-cliente-parametros/]\n"
                + "===\n\n"
                + "La introducción de parámetros se hará como si fuera un diccionario, utilizando el siguiente formato:\n\n"
                + "clave = valor clave2 = valor2\n\n"
                + "EJEMPLOS\n");

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("$ aryptiOR_client.exe service host=192.168.1.3 port=22215 requiresPassword=false passwd= useServerSettings=true options.encryption=false options.compression=true ignoreSecurityWarnings=true\n"
                + "$ aryptiOR_client.exe service url=aryptior://pepe@izcloud.tk/?useServerSettings=true\"&\"options.encryption=true\"&\"options.compression=true\n");
            Console.ResetColor();

            Console.WriteLine("\t>> SERVICIO\n" +
                "\t==\n\n" +
                "Es posible que no quieras lanzar en cliente en modo interactivo para introducir los datos de la conexión y los ajustes por el menú de la consola, sino que quieras iniciar el programa de manera desatendida, a modo de servicio.\n\n" +
                "Para ello, se debe pasar siempre el parámetro «service», sin ningún valor, en la consola, seguido del resto de parámetros de conexión definidos a continuación. Si el parámetro «service» no está presente, los argumentos de conexión directa serán ignorados.\n\n" +
                "url: (string) Si se especifica este parámetro, el resto de parámetros explicados en este punto se ignorarán, ya que se introducirán a través de la URL. En este argumento, se debe especificar una dirección URL de conexión al servidor, con el esquema «aryptior», como en el ejemplo siguiente:\n" +
                "aryptior://izcloud.tk/\n" +
                "De no especificarse un puerto, se utilizará el de por defecto (22215). Si se requiere autenficicación se realizará indicando la contraseña del servidor en la información de usuario de la URL, por ejemplo, para la contraseña «pepe»:\n" +
                "aryptior://pepe@izcloud.tk/\n" +
                "Si no se especifica ninguna contraseña, se intentará conectar sin autetificación. Los demás parámetros, se pueden especificar en la parte de la consulta de la URL. Se especificarán con los mismos valores que si se pasaran por parámetros separados. Por ejemplo:\n" +
                "aryptior://izcloud.tk/?useServerSettings=true\"&\"options.encryption=true\"&\"options.compression=true\n\n" +
                "host: (string) Contiene la dirección IP o nombre del servidor al que conectarse. De no especificarse, «127.0.0.1» será el valor tomado por defecto.\n" +
                "port: (int) Puerto del servidor indicado en la dirección, en el campo «host». Se tomará 22215 por defecto si no se especifica.\n" +
                "requiresPassword: (bool) Indica si se requiere contraseña. Se ajustará a false por defecto.\n" +
                "passwd: (string) Contraseña de acceso. No es necesario si «requiresPassword» toma el valor false. El valor es la cadena vacía por defecto.\n" +
                "useServerSettings: (bool) Si usar o no los ajustes del servidor y no los locales. Se toma true por defecto.\n" +
                "options.encryption: (bool) Indica si usar cifrado SSL entre servidor y cliente. Se toma true por defecto.\n" +
                "options.compression: (bool) Define si el tráfico entre servidor y cliente debe comprimirse para ahorrar ancho de banda. Se toma true por defecto.\n\n" +
                "\t>> OTROS PARAMETROS\n" +
                "\t==\n\n" +
                "ignoreSecurityWarnings: (bool) Cuando se ajuste a true, no se mostrará la pantalla de advertencia al conectarse con un servidor sin seguridad, y se conectará directamente.\n" +
                "configPath: (string) nombre del fichero de configuración que se usará para guardar los ajustes y los servidores guardados. Si se especifica un fichero con extensión .yaml o .yml, se creará o leerá el fichero de configuración (dependiendo de si exista o no) con serialización YAML. En caso de que la extensión sea .json, se creará o leerá con serialización JSON formateada. En caso de que la extensión no sea ninguna de esas tres, el cliente fallará al arrancar. Por defecto: config.yaml.\n" +
                "backupPath: (string) nombre del fichero donde el cliente guardará la lista de copias de seguridad descargadas desde distintos servidores y su progreso de descarga. Si se especifica un fichero con extensión .yaml o .yml, se creará o leerá el fichero con serialización YAML. En caso de que la extensión sea .json, se creará o leerá con serialización JSON formateada. En caso de que la extensión no sea ninguna de esas tres, el cliente fallará al arrancar. Por defecto, backup.yaml.\n");
            return true;
        }
    }
}
