﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace ProvaFils
{
    class BackupCron
    {
        // attributes
        private readonly NetEventIO_Client client;
        private Semaphore dlMutex = new Semaphore(1, 1);
        private Thread t;
        public long sizeLimit;

        // constants
        public const string BACKUP_TMP_DIR = "./backup/temp/";

        public BackupCron(NetEventIO_Client client, long sizeLimit)
        {
            this.client = client;
            this.sizeLimit = sizeLimit;
        }

        public void Run()
        {
            if (!initializeBackupStorage()) return;

            this.client.subscribe("backup", this.onNewBackup);
            t = new Thread(new ThreadStart(this.syncBackups));
            t.Start();
        }

        private bool initializeBackupStorage()
        {
            try
            {
                if (!Directory.Exists("./backup")) Directory.CreateDirectory("./backup");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void syncBackups()
        {
            this.client.emit("backup", "list", "", (NetEventIO_Client sender, string message) =>
            {
                if (message.Equals("ERR")) return;
                List<Backup> remoteBackups = (List<Backup>)JsonConvert.DeserializeObject(message, typeof(List<Backup>));
                List<Backup> localBackups = LocalStorage.loadBackups(Program.currentServer.host);

                bool found;
                Backup localBackupSel = null;
                foreach (Backup remoteBackup in remoteBackups)
                {
                    found = false;
                    foreach (Backup localBackup in localBackups)
                    {
                        if (localBackup.uuid.Equals(remoteBackup.uuid))
                        {
                            found = true;
                            localBackupSel = localBackup;
                            break;
                        }
                    }
                    if (!found)
                    {
                        remoteBackup.downloaded = 0;
                        localBackups.Add(remoteBackup);
                    }
                }

                this.dlMutex.WaitOne();

                for (int i = 0; i < localBackups.Count; i++)
                {
                    Backup localBackup = localBackups[i];
                    if (localBackup.downloaded >= localBackup.size) continue;

                    Console.WriteLine("DOWNLOADING... " + localBackup.uuid + " - " + localBackup.size);
                    this.downloadBackup(sender, localBackup, Program.currentServer.host, (Backup b) =>
                    {
                        localBackups[i] = b;
                        if (!LocalStorage.saveBackups(localBackups, Program.currentServer.host)) Console.WriteLine("NO ES POT DESAR!!");
                    });

                    localBackups[i] = localBackup;
                    LocalStorage.saveBackups(localBackups, Program.currentServer.host);
                    this.clearOlderBackups(Program.currentServer.host);
                }

                this.dlMutex.Release();

            });
        }

        private void onNewBackup(NetEventIO_Client sender, OnSubscriptionPushEventArgs e)
        {
            this.dlMutex.WaitOne();

            Console.WriteLine("NeW BACKUP!!!!!");
            List<Backup> localBackups = LocalStorage.loadBackups(Program.currentServer.host);
            Backup localBackup = (Backup)JsonConvert.DeserializeObject(e.newValue, typeof(Backup));

            localBackups.Add(localBackup);
            int i = localBackups.IndexOf(localBackup);

            this.downloadBackup(sender, localBackup, Program.currentServer.host, (Backup b) =>
            {
                localBackups[i] = b;
                LocalStorage.saveBackups(localBackups, Program.currentServer.host);
            });

            localBackups[i] = localBackup;
            LocalStorage.saveBackups(localBackups, Program.currentServer.host);
            this.clearOlderBackups(Program.currentServer.host);

            this.dlMutex.Release();

        }

        private delegate void OnBackupChunkDownloaded(Backup b);

        private void downloadBackup(NetEventIO_Client sender, Backup b, string addr, OnBackupChunkDownloaded onChunkDownloaded)
        {

            FileStream fs = null;
            try
            {
                if (!Directory.Exists("./backup/" + addr)) Directory.CreateDirectory("./backup/" + addr);
                fs = new FileStream("./backup/" + addr + "/" + b.toFileName(), FileMode.OpenOrCreate);
            }
            catch (Exception)
            {
                return;
            }
            const int MAX_BLOCK_SIZE = 65535;
            long blockSize;

            for (long i = b.downloaded; i < b.size; i += blockSize)
            {
                Semaphore s = new Semaphore(0, 1);
                BinaryMessage msg = null;

                blockSize = Math.Min(MAX_BLOCK_SIZE, (b.size - i));
                Console.WriteLine("*");
                sender.on("backup", (NetEventIO_Client client, BinaryMessage bm) =>
                {
                    Console.WriteLine("****");
                    msg = bm;
                    try
                    {
                        Console.WriteLine("*****");
                        s.Release();
                        Console.WriteLine("****** soltat el semafor");
                    }
                    catch (Exception e) { Console.WriteLine("ELOL!" + e.ToString()); }
                });
                Console.WriteLine("**");
                sender.emit("backup", MAX_BLOCK_SIZE + "-" + i, b.uuid);
                Console.WriteLine("***");
                s.WaitOne();
                Console.WriteLine("******* espera del semafor");
                fs.Write(msg.message, 0, msg.message.Length);
                s.Dispose();

                b.downloaded = i + blockSize;
                onChunkDownloaded(b);

            }

            fs.Dispose();
        }

        private void clearOlderBackups(string addr)
        {
            // if the cron was set to elete the backups in a size bound, compute the current size of the backup directory
            if (this.sizeLimit > 0)
            {
                // get the total size of the directory
                long dirSize = 0;
                foreach (FileInfo file in new DirectoryInfo("./backup/" + addr).GetFiles())
                {
                    dirSize += file.Length;
                }

                // clear older backups if the file size has been exceeded
                if (dirSize > this.sizeLimit)
                {
                    FileInfo[] fileInfo = new DirectoryInfo("./backup/" + addr).GetFiles();

                    // sort file names by date
                    this.sortBackupNamesByDate(ref fileInfo);

                    // delete until the enough space is freed
                    for (int i = 0; (i < fileInfo.Length) && (dirSize > this.sizeLimit); i++)
                    {
                        FileInfo file = fileInfo[i];

                        dirSize -= file.Length;
                        file.Delete();
                        this.deleteBackupFromList(addr, file.Name.Split('_')[0]);
                    }
                }
            }
        }

        private bool sortBackupNamesByDate(ref FileInfo[] fileInfo)
        {
            for (int i = 0; i < fileInfo.Length; i++)
            {
                for (int j = (i % 2); j < fileInfo.Length; j += 2)
                {
                    if ((j + 1) >= fileInfo.Length) continue;

                    // get the dates
                    DateTime dateTime1;
                    if (!this.dateFromBackupFileName(fileInfo[j].Name, out dateTime1)) return false;
                    DateTime dateTime2;
                    if (!this.dateFromBackupFileName(fileInfo[(j + 1)].Name, out dateTime2)) return false;

                    // check the date order
                    if (DateTime.Compare(dateTime1, dateTime2) > 0)
                    {
                        // flip the order
                        FileInfo aux = fileInfo[j];
                        fileInfo[j] = fileInfo[(j + 1)];
                        fileInfo[(j + 1)] = aux;
                    }
                }
            }
            return true;
        }

        private bool dateFromBackupFileName(string fileName, out DateTime dateTimeOutput)
        {
            try
            {
                string formattedDate = fileName.Split('_')[1];
                string[] date = formattedDate.Split(',')[0].Split('-');
                string[] time = formattedDate.Split(',')[1].Split('-');

                dateTimeOutput = new DateTime(Int32.Parse(date[0]), Int32.Parse(date[1]), Int32.Parse(date[2]),
                    Int32.Parse(time[0]), Int32.Parse(time[1]), Int32.Parse(time[2]));
                return true;
            }
            catch (Exception)
            {
                dateTimeOutput = new DateTime();
                return false;
            }
        }

        private bool deleteBackupFromList(string addr, string uuid)
        {
            try
            {
                List<Backup> localBackups = LocalStorage.loadBackups(Program.currentServer.host);
                if (localBackups == null) return false;
                for (int i = 0; i < localBackups.Count; i++)
                {
                    Backup backupSel = localBackups[i];
                    if (backupSel.uuid.Equals(uuid))
                    {
                        localBackups.Remove(backupSel);
                        break;
                    }
                }
                return LocalStorage.saveBackups(localBackups, addr);
            }
            catch (Exception) { return false; }
        }

        public bool Stop()
        {
            try
            {
                this.client.unsubscribe("backup");
                if (this.t != null && t.IsAlive)
                {
                    t.Abort();
                }
                return true;
            }
            catch (Exception) { return false; }
        }
    }
}
