﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

namespace ProvaFils
{
    class ClientThread
    {
        /// <summary>
        /// Hash (combinations, algorithm, hash, etc.) of the password being currently processed by the remote server.
        /// </summary>
        private Hash currentHash;
        /// <summary>
        /// In case of being processing a password by dictionary tecnique, this contains the lines currently being processed.
        /// </summary>
        private ConcurrentQueue<string> linees = new ConcurrentQueue<string>();
        /// <summary>
        /// Sets whatever the server is IDLE.
        /// </summary>
        private bool estaIdle;
        /// <summary>
        /// Subprocess that keeps the local backups synced with the remote server's backups.
        /// In the event that a new backup is made on the server, this thread downloads it automatically.
        /// </summary>
        public static BackupCron backupCron;
        /// <summary>
        /// Connnection with the remote server through NetEventIO.
        /// </summary>
        private static NetEventIO_Client client;

        public ClientThread()
        {
            this.estaIdle = true;
        }

        public void Run()
        {
            // network connection
            NetworkConnection connection;
            if (!connectToServer(out connection))
            {
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Red;
                Utilities.PadRight("ERROR! COULD NOT CONNECT TO THE SERVER!");
                Console.ResetColor();
                Console.ReadKey(true);
                return;
            }

            NetEventIOClient_Options options = new NetEventIOClient_Options(Program.currentServer.requiresPassword, Program.currentServer.passwd);
            client = new NetEventIO_Client(connection, options);
            NetEventIO_Client.NetEventIOConnectionResult connectionResult = client.connect();
            if (connectionResult == NetEventIO_Client.NetEventIOConnectionResult.InitErr)
            {
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Red;
                Utilities.PadRight("FATAL ERROR!");
                Console.WriteLine("VERSION MISMATCH ERROR");
                Console.ResetColor();
                Console.WriteLine("The local version of this software does not use the same version of the protocol as " +
                    "the one that uses your remote server.");
                Console.WriteLine("Please, upgrade both the static files of the website and the processing server to the same (or, i" +
                    "f it's possible, to lastest) version of this software and try again.");
                return;
            }
            else if (connectionResult == NetEventIO_Client.NetEventIOConnectionResult.AuthErr)
            {
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Red;
                if (Program.parameters != null && Program.parameters.ContainsKey("service"))
                {
                    Utilities.PadRight("ERROR! INCORRECT PASSWORD!");
                    return;
                }
                do
                {
                    Utilities.PadRight("ERROR! INCORRECT PASSWORD!");
                    Console.ResetColor();
                    Console.WriteLine("Input password and try again!");
                    Console.Write("> ");
                    string pwd = Console.ReadLine();
                    if (pwd.Equals(String.Empty))
                        return;
                    options = new NetEventIOClient_Options(true, pwd);
                    client = new NetEventIO_Client(connection, options);
                    connectionResult = client.connect();
                }
                while (connectionResult == NetEventIO_Client.NetEventIOConnectionResult.AuthErr);
                return;
            }
            Console.Title = client.id;

            // ping and pong cron (keeps the connection alive)
            startPingInterval();

            // function that adds to the NetEventIO object all the handler functions necessary to answer the server's requests
            this.addEvents(client);

            Thread eventHandler = new Thread(new ThreadStart(client.Run));
            eventHandler.Start();

            runBackupCron();

            while (connection.isConnected())
            {
                Semaphore s = new Semaphore(0, 1);
                // initialize tasks from the server
                bool res = client.emit("initialization", (NetEventIO_Client sender, string response) =>
                {
                    initializeConnection(response);

                    if (estaIdle)
                    {
                        Console.WriteLine("The server's status is IDLE");
                        if (!processRainbow(client))
                        {
                            Thread forceUpdateTimer = this.forceUpdateTimer(s);
                            client.subscribe("hash", (NetEventIO_Client sneder, OnSubscriptionPushEventArgs e) =>
                            {
                                if (e.changeType == SubscriptionChangeType.insert)
                                {
                                    client.unsubscribe("hash");
                                    client.unsubscribe("rainbow");
                                    if (forceUpdateTimer != null)
                                    {
                                        forceUpdateTimer.Abort();
                                        forceUpdateTimer = null;
                                    }
                                    s.Release();
                                }
                            });
                            client.subscribe("rainbow", (NetEventIO_Client sneder, OnSubscriptionPushEventArgs e) =>
                            {
                                client.unsubscribe("rainbow");
                                client.unsubscribe("hash");
                                if (forceUpdateTimer != null)
                                {
                                    forceUpdateTimer.Abort();
                                    forceUpdateTimer = null;
                                }
                                s.Release();

                            });
                        }
                        else
                        {
                            s.Release();
                        }
                    }
                    else
                    {
                        Console.WriteLine("*** SEARCHING " + currentHash.tipus + " HASH " + currentHash.hash + " ***");
                        searchPassword(client);
                        s.Release();
                    }
                    //s.Release();
                });

                s.WaitOne();
            }
        }

        private static void startPingInterval()
        {
            new Thread(new ThreadStart(() =>
            {
                AutoResetEvent are = new AutoResetEvent(false);
                int pingInterval = 0;
                client.emit("ping", "interval", "", (NetEventIO_Client _, string message) =>
                {
                    are.Set();
                    pingInterval = Int32.Parse(message);
                });
                are.WaitOne();
                if (pingInterval > 0)
                {
                    while (client.client.isConnected())
                    {
                        client.emit("ping");
                        Thread.Sleep(pingInterval);
                    }
                }
            })).Start();
        }

        private Thread forceUpdateTimer(Semaphore s)
        {
            if (Program.config.clientSettings.forceUpdateTime > 0)
            {
                Thread t = new Thread(new ThreadStart(() =>
                {
                    int currentCursorLine = Console.CursorTop;
                    for (int i = Program.config.clientSettings.forceUpdateTime; i >= 0; i -= 1000)
                    {
                        Console.SetCursorPosition(0, currentCursorLine);
                        Console.WriteLine("Sleeping " + (i / 1000) + " seconds.");
                        Thread.Sleep(1000);
                    }

                    client.unsubscribe("hash");
                    client.unsubscribe("rainbow");
                    s.Release();
                }));
                t.Start();
                return t;
            }
            else
            {
                return null;
            }
        }

        public static void runBackupCron()
        {
            if (Program.config.clientSettings.downloadBackups)
            {
                backupCron = new BackupCron(client, Program.config.clientSettings.backupMaxSize);
                backupCron.Run();
            }
        }

        private void addEvents(NetEventIO_Client eventEmmiter)
        {
            eventEmmiter.on("config", configEvent);
            eventEmmiter.on("configAll", configAllEvent);
            eventEmmiter.on("backup", configBackup);
            eventEmmiter.on("test", testPerformance);
        }

        private static void configEvent(NetEventIO_Client sender, OnMessageEventArgs e, NetEventIO_Client.Callback callback)
        {
            if (e.message.command.Equals("read"))
            {
                callback(JsonConvert.SerializeObject(Program.config.clientSettings));
            }
            else if (e.message.command.Equals("write"))
            {
                ClientSettings clientSettings = (ClientSettings)JsonConvert.DeserializeObject(e.message.message, typeof(ClientSettings));
                if (clientSettings == null || !clientSettings.isValid())
                {
                    callback("ERR");
                    return;
                }
                Program.config.clientSettings = clientSettings;
                callback("OK");
                LocalStorage.saveSettings(Program.config);
            }
        }

        private static void configAllEvent(NetEventIO_Client sender, OnMessageEventArgs e, NetEventIO_Client.Callback callback)
        {
            Dictionary<string, dynamic> newGlobalSettings = (Dictionary<string, dynamic>)
                JsonConvert.DeserializeObject(e.message.message, typeof(Dictionary<string, dynamic>));

            if (newGlobalSettings == null || !ClientSettings.isValid(newGlobalSettings))
            {
                callback("ERR");
                return;
            }

            Program.config.clientSettings.fromGlobalSettings(newGlobalSettings);

            if (Program.config.clientSettings.isValid())
            {
                LocalStorage.saveSettings(Program.config);
                callback("OK");
            }
            else
            {
                callback("ERR");
            }
        }

        private static void configBackup(NetEventIO_Client sender, OnMessageEventArgs e, NetEventIO_Client.Callback callback)
        {
            if (e.message.command.Equals("list"))
            {
                callback(JsonConvert.SerializeObject(LocalStorage.loadBackups(Program.currentServer.host)));
                return;
            }
            callback("ERR");

        }

        private void testPerformance(NetEventIO_Client sender, OnMessageEventArgs e, NetEventIO_Client.Callback callback)
        {
            new Thread(new ThreadStart(() =>
            {
                Console.WriteLine("TESTING PERFORMANCE!!!");
                try
                {
                    BruteForceTest bruteForceTest = (BruteForceTest)JsonConvert.DeserializeObject(e.message.message, typeof(BruteForceTest));
                    if (bruteForceTest == null || !bruteForceTest.isValid())
                    {
                        callback("ERR");
                        return;
                    }

                    int hashCount = 0;
                    int[] combination = new int[] { bruteForceTest.firstUnicodeCharacter };
                    DateTime operationStartTime = DateTime.Now;

                    int numberOfThreads = Program.config.clientSettings.threads;
                    Thread[] threads = new Thread[numberOfThreads];
                    for (int i = 0; i < numberOfThreads; i++)
                    {
                        threads[i] = new Thread(new ThreadStart(() =>
                        {
                            int hashCountPerThread = 0;
                            while ((DateTime.Now - operationStartTime).TotalMilliseconds <= bruteForceTest.timeout)
                            {
                                string currentCombination = BruteForceThread.combinationToString(combination);
                                Utilities.verifyHash(String.Empty,
                                    (bruteForceTest.algorithm + currentCombination),
                                    bruteForceTest.algorithm,
                                    bruteForceTest.iterations);
                                BruteForceThread.incrementCombination(ref combination, bruteForceTest.firstUnicodeCharacter, bruteForceTest.lastUnicodeCharacter);
                                hashCountPerThread++;
                            }
                            Interlocked.Add(ref hashCount, hashCountPerThread);
                        }));
                        threads[i].Start();
                    }
                    for (int i = 0; i < numberOfThreads; i++)
                    {
                        threads[i].Join();
                        threads[i] = null;
                    }

                    callback(JsonConvert.SerializeObject(new BruteForceTestResponse(hashCount, numberOfThreads)));
                    return;
                }
                catch (Exception)
                {
                    callback("ERR");
                }
            })).Start();
        }

        private static bool connectToServer(out NetworkConnection connection)
        {
            connection = new NetworkConnection(Program.currentServer);
            NetworkConnection.ConnectResult connectResult = connection.connect(Program.currentServer.useServerSettings, Program.currentServer.options);
            while (connectResult == NetworkConnection.ConnectResult.IOEerror)
            {
                LocalStorage.MUTEXconfig.WaitOne();
                int reconnectionWaitTime =
                    Program.config.clientSettings.forceUpdateTime > 0
                    ? Program.config.clientSettings.forceUpdateTime
                    : 15000;
                LocalStorage.MUTEXconfig.Release();
                Console.WriteLine("Could not connect to the specified remote server (" + Program.currentServer.host + ":" + Program.currentServer.port + ")."
                    + " Retrying in " + (reconnectionWaitTime / 1000) + " seconds...");
                Thread.Sleep(reconnectionWaitTime);

                connectResult = connection.connect(Program.currentServer.useServerSettings, Program.currentServer.options);
            }
            if (connectResult == NetworkConnection.ConnectResult.SSLError || !connection.isSecure)
            {
                if (Program.parameters != null
                    && Program.parameters.TryGetValue("service", out string service)
                    && service == null)
                {
                    if (Program.parameters.TryGetValue("ignoreSecurityWarnings", out string ignoreSecurityWarnings)
                        && ignoreSecurityWarnings.Equals("true"))
                    {
                        Console.WriteLine("WARNING: AN SSL ERROR HAS OCCURRED AND A SECURE CONNECTION COULD NOT BE DONE! " +
                            "CONNECTING IN INSECURE MODE BY DEFAULT AS ignoreSecurityWarnings IS SET TO TRUE!");
                        if (!connection.isSecure && connectResult != NetworkConnection.ConnectResult.SSLError)
                            return true;

                        connection = new NetworkConnection(Program.currentServer);
                        Program.currentServer.options.encryption = false;
                        connectResult = connection.connect(false, Program.currentServer.options);
                        if (connectResult == NetworkConnection.ConnectResult.OK || connectResult == NetworkConnection.ConnectResult.AlreadyConnected)
                        {
                            return true;
                        }
                        connection.disconnect();
                        return false;
                    }
                    else
                    {
                        Console.WriteLine("ERROR: COULD NOT CONNECT TO THE SERVER! AN SSL ERROR HAS OCCURRED AND A SECURE CONNECTION COULD NOT BE DONE!" +
                            "IN ORDER TO CONTINUE ANYWAYS, PLEASE SPECIFY ignoreSecurityWarnings=true IN THE PARAMETERS!");
                        return false;
                    }
                }

                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Red;
                Utilities.PadRight();
                Utilities.PadRight("\tAN SSL ERROR HAS OCCURRED AND A SECURE CONNECTION COULD NOT BE DONE!");
                Utilities.PadRight("\t**********");
                Console.ForegroundColor = ConsoleColor.Black;
                Utilities.PadRight(2);
                Utilities.PadRight("Information not properly encrypted could be readed and altered by anyone who has access to your network and " +
                    "can be a really high risk for your security.");
                Utilities.PadRight(2);
                Utilities.PadRight("If you own the server, please check out the security settings and add a security" +
                " certificate to the server in order to make the network connections more reliable.");
                Utilities.PadRight(2);
                Utilities.PadRight("If you really do trust this host and still" +
                " want to continue, you can do it at your own risk.");
                Utilities.PadRight(2);
                Console.ForegroundColor = ConsoleColor.Red;
                Utilities.PadRight("Do you want to DISABLE THE SECURITY and continue anyways?");
                Utilities.PadRight(2);
                Console.ResetColor();
                if (Program.menuYesOrNo(false))
                {
                    Console.Clear();
                    if (!connection.isSecure && connectResult != NetworkConnection.ConnectResult.SSLError)
                        return true;

                    connection = new NetworkConnection(Program.currentServer);
                    Program.currentServer.options.encryption = false;
                    connectResult = connection.connect(false, Program.currentServer.options);
                    if (connectResult == NetworkConnection.ConnectResult.OK || connectResult == NetworkConnection.ConnectResult.AlreadyConnected)
                    {
                        return true;
                    }
                    connection.disconnect();
                    return false;
                }
                else
                {
                    Console.Clear();
                    connection.disconnect();
                    return false;
                }
            }
            else if (connectResult == NetworkConnection.ConnectResult.OK || connectResult == NetworkConnection.ConnectResult.AlreadyConnected)
            {
                return true;
            }
            else
            {
                if (connectResult == NetworkConnection.ConnectResult.HandshakeError)
                    Console.WriteLine("ERROR DURING HANDSHAKE!!! CORRECT SETTINGS WOULD BE " + JsonConvert.SerializeObject(connection.options));
                return false;
            }
        }

        private bool processRainbow(NetEventIO_Client client)
        {
            LocalStorage.MUTEXconfig.WaitOne();
            if (!Program.config.clientSettings.processRainbowsOnIdle)
            {
                LocalStorage.MUTEXconfig.Release();
                return false;
            }
            LocalStorage.MUTEXconfig.Release();

            bool resultToReturn = false;
            string rId = "";
            ConcurrentQueue<ComputedHash> computed = new ConcurrentQueue<ComputedHash>();
            BruteForceChunk bruteForceChunk = null;
            Semaphore s = new Semaphore(0, 1);

            client.emit("getRainbow", (NetEventIO_Client sender, string receivedMessage) =>
            {
                RainbowPart rainbowPart = (RainbowPart)JsonConvert.DeserializeObject(receivedMessage, typeof(RainbowPart));
                rId = rainbowPart.uuid;
                if (rainbowPart == null || !rainbowPart.isValid() || rainbowPart.uuid.Equals("false"))
                {
                    s.Release();
                    return;
                }
                Console.WriteLine("PROCESSING RAINBOW ID " + rainbowPart.uuid);

                Hash h = rainbowPart.hash;
                if (h == null)
                {
                    s.Release();
                    return;
                }

                currentHash = h;
                bruteForceChunk = rainbowPart.bruteForceChunk;
                currentHash.caracters = bruteForceChunk.characters;

                computed_produced = new Semaphore(0, MAX_BUFFER_SIZE);
                computed_consumed = new Semaphore(MAX_BUFFER_SIZE, MAX_BUFFER_SIZE);
                DateTime inici = DateTime.Now;
                Thread sendRainbowResultsThread = new Thread(new ThreadStart(() =>
                {
                    resultToReturn = sendRainbowResults(rId, ref computed, client, bruteForceChunk.charactersCount);
                    Console.WriteLine(">>> He acabat");
                }));
                sendRainbowResultsThread.Start();

                runThreads(bruteForceChunk.charactersCount, ref h, ref computed, true);

                sendRainbowResultsThread.Join();
                Console.WriteLine(">>>> He acabat");
                Console.WriteLine("TEMPS DE PROCESSAMENT " + (DateTime.Now - inici).TotalMilliseconds);
                computed_produced.Dispose();
                computed_consumed.Dispose();


                if (computed == null)
                {
                    s.Release();
                    return;
                }

                resultToReturn = true;
                s.Release();
                return;
            });

            s.WaitOne();
            if (!resultToReturn) return false;

            return resultToReturn;
        }

        public const int MAX_BUFFER_SIZE = 40000;
        public static Semaphore computed_produced;
        public static Semaphore computed_consumed;

        private static bool sendRainbowResults(string rainbowId, ref ConcurrentQueue<ComputedHash> computed, NetEventIO_Client client, int charactersCount)
        {
            bool result;
            // counts the number of computedhashes written to the queue
            int currentCount = 0;
            // maximum number of computedhashes per block sent to the server
            int MAX_BLOCK_SIZE = 128;
            // maximum number of computedhashes to allow to be queued before start sending them to the server again
            MAX_BLOCK_SIZE = Math.Min(MAX_BLOCK_SIZE, (charactersCount - currentCount));

            double lastMessageTransferTime = -1;

            Dictionary<string, string> computedBlock;
            // repeat until the entire character set has been processed
            while (currentCount < charactersCount)
            {
                // wait for some hash to be produced
                computed_produced.WaitOne();

                // if the block maximum size has been reached by the current size of the queue, read the queue and send to the server
                if (computed.Count >= MAX_BLOCK_SIZE)
                {
                    MAX_BLOCK_SIZE = Math.Min(MAX_BLOCK_SIZE, (charactersCount - currentCount));

                    // transform the current queue into an array
                    computedBlock = new Dictionary<string, string>();

                    // clear the queue using the size of the extracted array
                    ComputedHash aux;
                    for (int i = 0; i < MAX_BLOCK_SIZE; i++)
                    {
                        if (computed.TryDequeue(out aux))
                            computedBlock.Add(aux.hash, aux.clearPwd);
                    }

                    // send the array to the server
                    Console.WriteLine(">> A informar! " + computed.Count + "/" + MAX_BLOCK_SIZE + " : " + currentCount);
                    DateTime inici = DateTime.Now;
                    result = sendRainbowResults(rainbowId, computedBlock, client);
                    double msTransferTime = (DateTime.Now - inici).TotalMilliseconds;
                    MAX_BLOCK_SIZE = getMessageSize(lastMessageTransferTime, msTransferTime, MAX_BLOCK_SIZE);
                    lastMessageTransferTime = msTransferTime;
                    Console.WriteLine(">> Informat!");
                    if (!result) return false;

                    // increment the current count, calculate the new block maximum size and continue
                    currentCount += 1;
                    MAX_BLOCK_SIZE = Math.Min(MAX_BLOCK_SIZE, (charactersCount - currentCount));

                }
                else // if the block maximum size has been reached yet, increment the current count and continue
                {
                    currentCount += 1;
                }

                computed_consumed.Release();
            }

            computedBlock = new Dictionary<string, string>();
            foreach (var item in computed)
            {
                computedBlock.Add(item.hash, item.clearPwd);
            }
            result = sendRainbowResults(rainbowId, computedBlock, client);
            Console.WriteLine(">> He acabat");
            return result;
        }

        private static int getMessageSize(double lastMessageTransferTime, double msTransferTime, int currentSize)
        {
            if (msTransferTime > lastMessageTransferTime && currentSize - 1 > 0)
            {
                return currentSize - 1;
            }
            else
            {
                return currentSize + 1;

            }
        }

        private static bool sendRainbowResults(string rainbowId, Dictionary<string, string> computed, NetEventIO_Client client)
        {
            bool result = false;
            Semaphore s = new Semaphore(0, 1);
            client.emit("reportRainbow", "", JsonConvert.SerializeObject(new RainbowComputedPart(rainbowId, computed)),
                (NetEventIO_Client sender, string receivedMessage) =>
            {
                result = receivedMessage.Equals("OK");
                s.Release();
            });
            s.WaitOne();

            return result;
        }

        public static Semaphore dictionary_produced;
        public static Semaphore dictionary_consumed;

        /// <summary>
        /// Gets a new part (a new combination) from the server, and calls to run all the threads and start processing.
        /// When the processing is done, reports the results to the server and gets a new part, till the password is found.
        /// </summary>
        /// <param name="connection"></param>
        private void searchPassword(NetEventIO_Client client)
        {

            // SEARCH THE PASSWORD
            string passwordFound = null;
            do
            {
                Semaphore s = new Semaphore(0, 1);
                // request a new combination from the server
                client.emit("newPart", "", this.currentHash.uuid, (NetEventIO_Client sender, string receivedMessage) =>
                {
                    if (receivedMessage.Equals("IDLE"))
                    {
                        passwordFound = "";
                        s.Release();
                        return;
                    }
                    int countCharsLength = 0;

                    if (currentHash.tipusBusqueda == TipusBusqueda.ForçaBruta)
                    {
                        // a brute force combination is made of a list of integers,
                        // where every value represents the decimal code of an unicode character.
                        BruteForceChunk bruteForceChunk = (BruteForceChunk)JsonConvert.DeserializeObject(receivedMessage, typeof(BruteForceChunk));
                        if (bruteForceChunk == null || !bruteForceChunk.isValid()) return;
                        currentHash.caracters = bruteForceChunk.characters;
                        countCharsLength = bruteForceChunk.charactersCount;

                        passwordFound = runThreads(countCharsLength);

                    }
                    else if (currentHash.tipusBusqueda == TipusBusqueda.Diccionari)
                    {
                        // a dictionary combination is made of a list of lines (strings),
                        // where every value represents a cleartext password to try
                        countCharsLength = Int32.Parse(receivedMessage);
                        Console.WriteLine("Ens han dit que processem " + countCharsLength);
                        dictionary_produced = new Semaphore(0, MAX_BUFFER_SIZE);
                        dictionary_consumed = new Semaphore(MAX_BUFFER_SIZE, MAX_BUFFER_SIZE);

                        linees = new ConcurrentQueue<string>();

                        Thread threadConsumidor = new Thread(new ThreadStart(() =>
                        {
                            passwordFound = runThreads(countCharsLength);

                        }));
                        threadConsumidor.Start();

                        Thread threadProductor = new Thread(new ThreadStart(() =>
                        {
                            int MAX_BLOCK_SIZE = 128;
                            double lastMessageTransferTime = -1;

                            for (long i = 0; i < countCharsLength;)
                            {

                                Semaphore waitForResponse = new Semaphore(0, 1);
                                DateTime inici = DateTime.Now;

                                long blockSize = Math.Min(MAX_BLOCK_SIZE, (countCharsLength - i));
                                client.emit("dictionaryChuck", "", blockSize + "", (NetEventIO_Client _, string response) =>
                                {
                                    double msTransferTime = (DateTime.Now - inici).TotalMilliseconds;
                                    MAX_BLOCK_SIZE = getMessageSize(lastMessageTransferTime, msTransferTime, MAX_BLOCK_SIZE);
                                    lastMessageTransferTime = msTransferTime;

                                    string[] combinacionsAEnviar = (string[])JsonConvert.DeserializeObject(response, typeof(string[]));
                                    if (combinacionsAEnviar == null) return;
                                    foreach (var item in combinacionsAEnviar)
                                    {
                                        Console.Title = i + " / " + countCharsLength;
                                        dictionary_consumed.WaitOne();
                                        linees.Enqueue(item);
                                        dictionary_produced.Release();
                                    }
                                    combinacionsAEnviar = null;

                                    waitForResponse.Release();
                                });
                                waitForResponse.WaitOne();

                                i += blockSize;
                            }
                        }));

                        threadProductor.Start();
                        threadConsumidor.Join();

                        if (threadProductor.IsAlive)
                            threadProductor.Abort();
                    }

                    if (passwordFound == null)
                    {
                        client.emit("reportPart", "false", "", (NetEventIO_Client _, string responseFalse) =>
                        {
                            s.Release();
                            if (!responseFalse.Equals("OK")) return;
                        });
                    }
                    else
                    {
                        Console.WriteLine(">> " + passwordFound);
                        client.emit("reportPart", "true", passwordFound, (NetEventIO_Client _, string responseFalse) =>
                        {
                            s.Release();
                            if (!responseFalse.Equals("OK")) return;
                        });
                    }
                });
                s.WaitOne();
            }
            while (passwordFound == null);
        }

        /// <summary>
        /// Gets the current state from the server.
        /// </summary>
        /// <param name="connection"></param>
        private bool initializeConnection(string initializationData)
        {
            // initialize from the server
            Estat currentStatus = (Estat)JsonConvert.DeserializeObject(initializationData, typeof(Estat));
            if (currentStatus == null) return false;
            estaIdle = currentStatus.estaIdle;
            // if the server is idle, don't set the hash info (is undefined)
            if (estaIdle == true)
            {
                return true;
            }
            // else, save the details of the hash that's being processed
            currentHash = currentStatus.hashActual;
            return true;
        }

        private string runThreads(int countCharsLength)
        {
            ConcurrentQueue<ComputedHash> computed = null;
            return runThreads(countCharsLength, ref currentHash, ref computed, false);
        }

        /// <summary>
        /// Divides the work of processing the password in x parts, and runs 4 threads to process it.
        /// When the processing is done, checks and returns the result.
        /// </summary>
        /// <returns>
        /// returns the password or null if the password was not found
        /// </returns>
        private string runThreads(int countCharsLength, ref Hash h, ref ConcurrentQueue<ComputedHash> computed, bool isProcessingRainbow)
        {
            //computed = null; // out

            LocalStorage.MUTEXconfig.WaitOne();
            int numberOfThreads = Program.config.clientSettings.threads;
            LocalStorage.MUTEXconfig.Release();

            int countCharactersPart = (countCharsLength / numberOfThreads);
            int restCharactersCount = countCharsLength % numberOfThreads;

            BruteForceThread[] threadObjects = new BruteForceThread[numberOfThreads];
            Thread[] threads = new Thread[numberOfThreads];

            // if it's a brute force attack, create BruteForceThread objects
            if (h.tipusBusqueda == TipusBusqueda.ForçaBruta)
            {
                // CREATE OBJECTS
                for (int i = 0; i < numberOfThreads; i++)
                {
                    // copy the array that contains the current combination
                    int[] objThreadCharacters = new int[h.caracters.Length];
                    Array.Copy(h.caracters, 0, objThreadCharacters, 0, h.caracters.Length);

                    // check if it's the first and add the rest of the hash division
                    if (i == 0)
                    {
                        if (!isProcessingRainbow)
                        {
                            threadObjects[i] = new BruteForceThread(h, objThreadCharacters, countCharactersPart + restCharactersCount);
                        }
                        else
                        {
                            threadObjects[i] = new BruteForceThread(h, objThreadCharacters, countCharactersPart + restCharactersCount, ref computed);
                        }

                        incrementCombination(ref h, countCharactersPart + restCharactersCount);
                    }
                    else
                    {
                        if (!isProcessingRainbow)
                        {
                            threadObjects[i] = new BruteForceThread(h, objThreadCharacters, countCharactersPart);
                        }
                        else
                        {
                            threadObjects[i] = new BruteForceThread(h, objThreadCharacters, countCharactersPart, ref computed);
                        }

                        incrementCombination(ref h, countCharactersPart);
                    }

                    Console.WriteLine("." + BruteForceThread.combinationToString(threadObjects[i].combination));
                } //for

                // CREATE THREADS
                for (int i = 0; i < numberOfThreads; i++)
                {
                    threads[i] = new Thread(new ThreadStart(threadObjects[i].Run));
                }
            }
            // if it's a dictionary attack, create DictionaryThread objects
            else if (currentHash.tipusBusqueda == TipusBusqueda.Diccionari)
            {
                DictionaryThread[] threadDictionaries = new DictionaryThread[numberOfThreads];
                // CREATE OBJECTS
                for (int i = 0; i < numberOfThreads; i++)
                {
                    threadDictionaries[i] =
                        new DictionaryThread(currentHash, countCharactersPart, linees);
                }
                // CREATE THREADS
                for (int i = 0; i < numberOfThreads; i++)
                {
                    threads[i] = new Thread(new ThreadStart(threadDictionaries[i].Run));
                }
                threadObjects = threadDictionaries;
            }

            // run all threads
            for (int i = 0; i < numberOfThreads; i++)
            {
                threads[i].Start();
            }

            // wait for the threads to finish
            for (int i = 0; i < numberOfThreads; i++)
            {
                threads[i].Join();
                // if the password was found in one thread
                if (threadObjects[i].found)
                {
                    // abort the other threads
                    for (int j = (i + 1); j < numberOfThreads; j++)
                    {
                        threads[j].Abort();
                    }
                    // and return the result
                    Console.WriteLine(threadObjects.Length + " " + i);
                    return (threadObjects[i].passwordFound);
                }
            }

            for (int i = 0; i < numberOfThreads; i++)
            {
                threads[i] = null;
                threadObjects[i] = null;
            }

            return null;
        }

        /// <summary>
        /// Increments the combination as times as specified by the parameter.
        /// </summary>
        /// <param name="nIncrements"></param>
        private static void incrementCombination(ref Hash currentHash, int nIncrements)
        {
            for (int i = 0; i < nIncrements; i++)
            {
                incrementCombination(ref currentHash);
            }
        }

        /// <summary>
        /// Increments the current combination once. 
        /// Ej. A -> B
        /// Ej. AAB -> AAC
        /// </summary>
        private static void incrementCombination(ref Hash currentHash)
        {
            // augmentar el prrimer caracter
            currentHash.caracters[0]++;
            for (int i = 0; i < currentHash.caracters.Length; i++)
            {
                // comprovar si el caracter actual es mes gran que el maxim
                if (currentHash.caracters[i] > currentHash.ultimCaracterUnicode)
                {
                    // reinicialitzar al primer caracter la posicio actual
                    currentHash.caracters[i] = currentHash.primerCaracterUnicode;
                    // si la seguent posicio esta incialitzada, augmentar, si no, inicialitzar al primer caracter
                    if ((i + 2) > currentHash.caracters.Length)
                    {
                        int[] longerCombination = new int[(currentHash.caracters.Length + 1)];
                        for (int j = 0; j < currentHash.caracters.Length; j++)
                        {
                            longerCombination[j + 1] = currentHash.caracters[j];
                        }
                        longerCombination[0] = currentHash.primerCaracterUnicode;
                        currentHash.caracters = longerCombination;
                    }
                    else
                    {
                        currentHash.caracters[(i + 1)]++;
                    }
                }
            }
        }
    }
}
