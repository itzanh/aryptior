﻿using System;

namespace ProvaFils
{
    [Serializable]
    public enum TipusBusqueda
    {
        ForçaBruta,
        Diccionari
    }
}
