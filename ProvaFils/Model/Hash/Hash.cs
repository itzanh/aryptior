﻿using System;

namespace ProvaFils
{
    [Serializable]
    public class Hash
    {
        public string uuid;
        public string hash;
        public TipusBusqueda tipusBusqueda;
        public Tipus tipus;
        public int primerCaracterUnicode;
        public int ultimCaracterUnicode;
        public int caractersFinals;
        public int[] caracters;
        public int iteracions;
        public string salt;

        public Hash()
        {
            this.tipusBusqueda = TipusBusqueda.ForçaBruta;
            this.caracters = new int[1] { 65 };
            this.caractersFinals = 12;
            this.iteracions = 1;
            this.salt = "";
        }

        public bool check()
        {
            switch (this.tipus)
            {
                case Tipus.NTLM:
                    {

                        break;
                    }
                default:
                    {
                        return false;
                    }
            }
            return true;
        }
    }
}
