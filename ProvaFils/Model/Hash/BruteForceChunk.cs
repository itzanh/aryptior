﻿using System;

namespace ProvaFils
{
    [Serializable]
    public class BruteForceChunk
    {
        public int charactersCount;
        public int totalCharactersCount;
        public int[] characters;

        public BruteForceChunk()
        {
        }

        public BruteForceChunk(int charactersCount, int[] characters, int totalCharactersCount)
        {
            this.charactersCount = charactersCount;
            this.characters = characters;
            this.totalCharactersCount = totalCharactersCount;
        }

        public bool isValid()
        {
            return !(this.charactersCount <= 0 || this.totalCharactersCount <= 0 || this.characters == null);
        }
    }

    [Serializable]
    public class BruteForceTest
    {
        public int timeout;
        public Tipus algorithm;
        public int iterations;
        public string salt;
        public int firstUnicodeCharacter;
        public int lastUnicodeCharacter;

        public BruteForceTest()
        {
            this.timeout = -1;
        }

        public bool isValid()
        {
            return (this.timeout >= 0 && this.timeout <= 60000 && this.iterations >= 0
                && this.salt != null && this.firstUnicodeCharacter >= 32 && this.lastUnicodeCharacter >= 0
                && (this.lastUnicodeCharacter > this.firstUnicodeCharacter));
        }
    }

    [Serializable]
    public class BruteForceTestResponse
    {
        public long hashCount;
        public int threads;

        public BruteForceTestResponse(long hashCount, int threads)
        {
            this.hashCount = hashCount;
            this.threads = threads;
        }
    }
}
