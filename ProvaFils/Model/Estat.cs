﻿using System;

namespace ProvaFils
{
    [Serializable]
    public class Estat
    {
        public bool estaIdle;
        public Hash hashActual;

        public Estat()
        {
        }

        public Estat(bool estaIdle, Hash hashActual)
        {
            this.estaIdle = estaIdle;
            this.hashActual = hashActual;
        }
    }
}

