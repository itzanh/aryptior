﻿using System;
using System.Collections.Generic;

namespace ProvaFils
{
    [Serializable]
    public class ClientSettings
    {
        /// <summary>
        /// Number of concurrent threads used to process the hashes.
        /// </summary>
        public int threads;
        /// <summary>
        /// Time to wait between attempts to reconnect to the server. (ms)
        /// </summary>
        public int forceUpdateTime;
        /// <summary>
        /// Sets if this client will attempt to prcess rainbows from the server when it's idle.
        /// </summary>
        public bool processRainbowsOnIdle;
        /// <summary>
        /// Time to wait for the reposonses when trying to discover server on the local network. (ms)
        /// </summary>
        public int discoveryWaitTime;
        /// <summary>
        /// Sets whether to download or not the remote server backups.
        /// </summary>
        public bool downloadBackups;
        /// <summary>
        /// Indicates the maximum size in bytes that can be used by each backup folder by each individual server.
        /// If this property is set to zero, no bounds in backup folders size will be applied.
        /// </summary>
        public long backupMaxSize;

        public ClientSettings()
        {
            this.threads = Environment.ProcessorCount;
            this.forceUpdateTime = 0;
            this.processRainbowsOnIdle = true;
            this.discoveryWaitTime = 3000;
            this.downloadBackups = false;
            this.backupMaxSize = 0;
        }

        public bool isValid()
        {
            // check number of threads
            if (this.threads <= 0 || this.threads > 64) return false;

            // check reconnection wait time
            if (this.forceUpdateTime < 0) return false;

            if (this.discoveryWaitTime <= 0) return false;

            if (this.backupMaxSize < 0) return false;

            return true;
        }

        public static bool isValid(Dictionary<string, dynamic> settings)
        {
            foreach (string key in settings.Keys)
            {
                dynamic value = settings[key];
                switch (key)
                {
                    case "threads":
                        {
                            if (!(value is long) || value <= 0 || value > 64) return false;
                            break;
                        }
                    case "forceUpdateTime":
                        {
                            if (!(value is long) || value < 0) return false;
                            break;
                        }
                    case "discoveryWaitTime":
                        {
                            if (!(value is long) || value <= 0) return false;
                            break;
                        }
                    case "backupMaxSize":
                        {
                            if (!(value is long) || value < 0) return false;
                            break;
                        }
                    case "processRainbowsOnIdle":
                    case "downloadBackups":
                        {
                            if (!(value is bool)) return false;
                            break;
                        }
                    default:
                        {
                            return false;
                        }
                }
            }

            return true;
        }

        public bool fromGlobalSettings(Dictionary<string, dynamic> settings)
        {
            foreach (string key in settings.Keys)
            {
                dynamic value = settings[key];
                switch (key)
                {
                    case "threads":
                        {
                            this.threads = (int)value;
                            break;
                        }
                    case "forceUpdateTime":
                        {
                            this.forceUpdateTime = (int)value;
                            break;
                        }
                    case "discoveryWaitTime":
                        {
                            this.discoveryWaitTime = (int)value;
                            break;
                        }
                    case "backupMaxSize":
                        {
                            this.backupMaxSize = (int)value;
                            break;
                        }
                    case "processRainbowsOnIdle":
                        {
                            this.processRainbowsOnIdle = (bool)value;
                            break;
                        }
                    case "downloadBackups":
                        {
                            this.downloadBackups = (bool)value;
                            break;
                        }
                    default:
                        {
                            return false;
                        }
                }
            }

            return true;
        }
    }
}
