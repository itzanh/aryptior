﻿using System.Collections.Generic;

namespace ProvaFils
{
    public class Settings
    {
        public List<Server> servers;
        public ClientSettings clientSettings;

        public Settings()
        {
            this.servers = new List<Server>();
            this.clientSettings = new ClientSettings();
        }

        public bool isValid()
        {
            // make sure every server is valid
            foreach (Server s in this.servers)
                if (!s.isValid()) return false;

            // check client settings
            if (!this.clientSettings.isValid()) return false;

            return true;
        }
    }
}
