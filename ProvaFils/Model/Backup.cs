﻿using System;

namespace ProvaFils
{
    [Serializable]
    public class Backup
    {
        public string uuid;
        public DateTime date;
        public string hash;
        public long size;
        public long downloaded;

        public Backup()
        {
            this.uuid = Guid.NewGuid().ToString();
            this.date = DateTime.Now;
            this.hash = "";
            this.size = 0;
            this.downloaded = 0;
        }

        public Backup(string uuid, DateTime date, string hash, long size)
        {
            this.uuid = uuid;
            this.date = date;
            this.hash = hash;
            this.size = size;
        }

        public string toFileName()
        {
            return this.uuid + "_" + this.formatDate(this.date) + "_" + this.hash + ".backup";
        }

        private string formatDate(DateTime date)
        {
            return date.Year + "-" + date.Month + "-" + date.Day + "," + date.Hour + "-" + date.Minute + "-" + date.Second;
        }
    }
}
