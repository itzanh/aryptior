﻿using System;
using System.Text;

namespace ProvaFils
{
    [Serializable]
    public class Server
    {
        /// <summary>
        /// Contains the host name or IP adress of the server.
        /// </summary>
        public string host;
        /// <summary>
        /// Sets the port of the server to bind to.
        /// </summary>
        public int port;
        /// <summary>
        /// Set this propierty to true in order to use the password specfied below to connect to the server.
        /// If this is set to false, the atttribute 'passwd' that appears below will be ignored.
        /// </summary>
        public bool requiresPassword;
        /// <summary>
        /// If a password is required to connect to the server, specify the cleartext password here.
        /// </summary>
        public string passwd;
        /// <summary>
        /// If this is set to true, the settings under the 'options' property will be ignored, and the remote server's settings will be used.
        /// On the other hand, if this property is set to false, the settings on the 'options' tag will be used to connect to the server.
        /// If you are using specific settings, the server has specified that custom settings will not be allowed, and the local 
        /// settings differ from the server's ones, the connection will not be possible.
        /// </summary>
        public bool useServerSettings;
        /// <summary>
        /// This settings contains the specific options used in the socket connection with the remote server. 
        /// In case that this client does not use the server's settings, this settings will be applied.
        /// </summary>
        public SocketOptions options;

        public Server()
        {
            this.host = "127.0.0.1";
            this.port = 22215;
            this.passwd = String.Empty;
            this.requiresPassword = false;

            this.useServerSettings = true;
            this.options = new SocketOptions();
        }

        public Server(string host, int port) : this()
        {
            this.host = host;
            this.port = port;
        }

        public Server(string host, int port, bool requiresPassword, string passwd) : this()
        {
            this.host = host;
            this.port = port;
            this.requiresPassword = requiresPassword;
            this.passwd = passwd;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str
                .Append(this.host).Append("\t").Append(this.port)
                .Append("\t");
            if (this.requiresPassword)
            {
                for (int i = 0; i < this.passwd.Length; i++)
                {
                    str.Append("*");
                }
            }
            else
            {
                str.Append("<no password>");
            }

            return str.ToString();
        }

        public bool isValid()
        {
            // check port number
            if (this.port <= 0 || this.port > 65535) return false;

            if (this.requiresPassword && (this.passwd == null || this.passwd.Equals(""))) return false;
            if (this.options == null) return false;

            return true;
        }

        public string GetDetails()
        {
            StringBuilder str = new StringBuilder();
            str
                .Append("HOST\t").Append(this.host).Append("\n")
                .Append("PORT\t").Append(this.port).Append("\n")
                .Append("AUTH\t").Append((this.requiresPassword ? "REQUIRES PASSWORD" : "NO AUTHENTICATION")).Append("\n");
            if (this.requiresPassword)
            {
                str.Append("PWD\t").Append(this.passwd).Append("\n");
            }
            str.Append("Use server settings?\t").Append(this.useServerSettings ? "Yes" : "No").Append("\n");
            if (!this.useServerSettings)
            {
                str
                    .Append("\tEncryption:\t").Append(this.options.encryption ? "Yes" : "No").Append("\n")
                    .Append("\tCompression:\t").Append(this.options.compression ? "Yes" : "No").Append("\n");
            }
            return str.ToString();
        }
    }
}
