﻿using System;

namespace ProvaFils
{
    [Serializable]
    public class SocketOptions
    {
        public bool encryption;
        public bool compression;

        public SocketOptions()
        {
            this.encryption = true;
            this.compression = true;
        }

        public SocketOptions(bool encryption, bool compression)
        {
            this.encryption = encryption;
            this.compression = compression;
        }

    }
}
