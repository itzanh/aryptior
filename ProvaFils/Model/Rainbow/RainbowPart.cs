﻿using System;

namespace ProvaFils
{
    [Serializable]
    public class RainbowPart
    {
        public string uuid;
        public Hash hash;
        public BruteForceChunk bruteForceChunk;

        public RainbowPart()
        {
        }

        public RainbowPart(string uuid, Hash hash, BruteForceChunk bruteForceChunk)
        {
            this.uuid = uuid;
            this.hash = hash;
            this.bruteForceChunk = bruteForceChunk;
        }

        public bool isValid()
        {
            return !(this.uuid == null || !Guid.TryParse(this.uuid, out _) || this.hash == null
                || this.bruteForceChunk == null || !this.bruteForceChunk.isValid());
        }
    }
}
