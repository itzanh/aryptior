﻿using System;
using System.Collections.Generic;

namespace ProvaFils
{
    [Serializable]
    public class RainbowComputedPart
    {
        public string id;
        public Dictionary<string, string> computedHashes;

        public RainbowComputedPart()
        {
            this.id = "";
            this.computedHashes = new Dictionary<string, string>();
        }

        public RainbowComputedPart(string id, Dictionary<string, string> computedHashes)
        {
            this.id = id;
            this.computedHashes = computedHashes;
        }
    }
}
