﻿using System;

namespace ProvaFils
{
    [Serializable]
    public class ComputedHash
    {
        public string hash;
        public string clearPwd;

        public ComputedHash(string hash, string clearPwd)
        {
            this.hash = hash;
            this.clearPwd = clearPwd;
        }
    }
}
