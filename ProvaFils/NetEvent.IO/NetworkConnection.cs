﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ProvaFils
{
    public class NetworkConnection : NetworkConnectionAbstract
    {
        public enum ConnectResult
        {
            AlreadyConnected,
            IOEerror,
            HandshakeError,
            SSLError,
            OK
        }

        /// <summary>
        /// TCP connection with a processing server.
        /// </summary>
        private TcpClient client;
        /// <summary>
        /// NetworkStream with the client.
        /// </summary>
        private NetworkStream ns;
        /// <summary>
        /// IP address of the remote endpoint
        /// </summary>
        private readonly string host;
        /// <summary>
        /// IP port of the remote endpoint
        /// </summary>
        private readonly int port;
        /// <summary>
        /// Indicates if this client is currently connected with the server
        /// </summary>
        private bool connected;

        public bool isSecure
        {
            get { return this.ssl != null; }
        }
        /// <summary>
        /// Secure sockets layer connection with the server, if it's set to use it.
        /// </summary>
        private SslStream ssl;
        /// <summary>
        /// The NetworkStream or SslStream to send the information through, as the connection may be secure or insecure and may not change.
        /// </summary>
        public Stream s;
        /// <summary>
        /// Sets the socket options to use, encryption, compression, etc.
        /// </summary>
        public SocketOptions options;
        /// <summary>
        /// object used to lock the receiveData method and use the socket thread-safely
        /// </summary>
        private readonly object lockReceiveData = new object();
        /// <summary>
        /// object used to lock the sendData method and use the socket thread-safely
        /// </summary>
        private readonly object lockSendData = new object();



        public NetworkConnection(string host, int port)
        {
            this.port = port;
            this.host = host;

            this.client = null;
            this.ns = null;
            this.ssl = null;
            connected = false;
        }

        public NetworkConnection(Server s) : this(s.host, s.port) { }

        /// <summary>
        /// Tries to connect over TCP to the server using the host and port attributes.
        /// </summary>
        /// <returns>
        ///     
        /// </returns>
        public ConnectResult connect(bool useServerSettings, SocketOptions options)
        {
            // check if it's already connected to a server
            if (this.connected || this.client != null)
            {
                return ConnectResult.AlreadyConnected;
            }

            // attempt connection
            try
            {
                // attempt connection
                this.client = new TcpClient(this.host, this.port);
                this.ns = client.GetStream();
                this.client.NoDelay = true;

                // attempt handshake
                ConnectResult handshakeResult = this.handshake(useServerSettings, options);
                if (handshakeResult != ConnectResult.OK) return handshakeResult;
                if (this.options == null) return ConnectResult.HandshakeError;

                if (this.options.encryption)
                    this.s = this.ssl;
                else
                    this.s = this.ns;

                return ConnectResult.OK;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return ConnectResult.IOEerror;
            }
        }

        public override bool isConnected()
        {
            try
            {
                return this.client.Connected;
            }
            catch (Exception e) { Console.WriteLine(e.ToString()); return false; }
        }


        /// <summary>
        /// Reads binary data from the network connection
        /// </summary>
        /// <returns>
        /// received binary data or null if it's not possible to read data
        /// </returns>
        public override bool receiveData(out byte[] receivedDataout, out bool isBinaryout)
        {
            isBinaryout = false;
            lock (lockReceiveData)
                try
                {
                    // integer to receive which contains el size of the information it's about to be sent
                    byte[] sizeToReceive = new byte[4];
                    int readData = 0;
                    while (readData < sizeToReceive.Length)
                        readData += this.s.Read(sizeToReceive, 0, (sizeToReceive.Length - readData));

                    byte[] parameters = new byte[1];
                    readData = 0;
                    while (readData < 1)
                        readData += this.s.Read(parameters, 0, 1);
                    if (parameters[0] == 1)
                        isBinaryout = true;

                    // the information that is about to be received through the socket is read with the previously specified size
                    uint dataSize = BitConverter.ToUInt32(sizeToReceive, 0);
                    readData = 0;
                    byte[] receivedData = new byte[dataSize];
                    // the packet can be received fragmented, so the program keeps reading and appending to the current position
                    // till the entire message is read
                    while (readData < dataSize)
                        readData = this.s.Read(receivedData, readData, (receivedData.Length - readData));

                    // optionally dencrypt or uncompress
                    MemoryStream ms = new MemoryStream(receivedData);
                    if (this.options.compression)
                        ms = uncompressData(ms);

                    receivedDataout = ms.ToArray();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    receivedDataout = null;
                    isBinaryout = false;
                    return false;
                }
        }

        /// <summary>
        /// Reads text from the network connection
        /// </summary>
        /// <returns>
        /// received binary data converted to text
        /// </returns>
        public string receiveData()
        {
            try
            {
                byte[] informacioRebuda;
                bool isBinary;
                // read the network data
                if (!receiveData(out informacioRebuda, out isBinary) || informacioRebuda == null) return null;

                // it is retrieved a utf-8 string from the received bytes
                string textRebut = Encoding.UTF8.GetString(informacioRebuda);
                return textRebut;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        /// <summary>
        /// Send the data contained by the string in the parameter to the server.
        /// </summary>
        /// <param name="dataToSend"></param>
        /// <returns>
        ///     true if the data was sent successfully to the remote endpoint
        ///     false if there was a problem while sending
        /// </returns>
        public override bool sendData(byte[] dataToSend, bool isBinary)
        {
            lock (lockSendData)
                try
                {
                    MemoryStream dataToBeSent = new MemoryStream(dataToSend);
                    if (this.options.compression)
                        dataToBeSent = compressData(dataToBeSent);

                    dataToBeSent.Position = 0;

                    // calculate the size of what is going to be sent
                    byte[] sizeInfo = BitConverter.GetBytes((uint)dataToBeSent.Length);

                    // make know how many information it's about to be sent
                    this.s.Write(sizeInfo, 0, sizeInfo.Length);
                    this.s.Flush();

                    // append the binary parameter
                    if (isBinary)
                        this.s.Write(new byte[] { 1 }, 0, 1);
                    else
                        this.s.Write(new byte[] { 0 }, 0, 1);

                    // send the information through the socket
                    dataToBeSent.WriteTo(this.s);
                    this.ns.Flush();

                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    return false;
                }
        }

        /// <summary>
        /// Encodes the string received through the parameter in UTF-8 and sends it in binary
        /// </summary>
        /// <param name="dataToSend"></param>
        /// <returns></returns>
        public bool sendData(string dataToSend)
        {
            return sendData(Encoding.UTF8.GetBytes(dataToSend), false);
        }

        /*********************/
        /* COMPRESSION LAYER */
        /*********************/

        private MemoryStream compressData(MemoryStream input)
        {
            try
            {
                return new MemoryStream(IronSnappy.Snappy.Encode(input.ToArray()));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        private MemoryStream uncompressData(MemoryStream input)
        {
            input.Position = 0;

            try
            {
                return new MemoryStream(IronSnappy.Snappy.Decode(input.ToArray()));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        /************************/
        /* SECURE SOCKETS LAYER */
        /************************/

        private SslStream initializeSSL(TcpClient client, string host)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                SslStream sslStream = new SslStream(client.GetStream(), true,
                    new RemoteCertificateValidationCallback(ValidateServerCertificate), null);
                sslStream.AuthenticateAsClient(host);
                return sslStream;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        private static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            Console.WriteLine("Certificate error: {0}", sslPolicyErrors);


            // Do not allow this client to communicate with unauthenticated servers.
            return false;
        }

        /// <summary>
        /// Closes the TCP connection with the remote socket.
        /// </summary>
        /// <returns>
        ///     true if the connection was successfully closed
        ///     false if the connection wasn't open yet
        /// </returns>
        public override void disconnect()
        {
            try
            {
                connected = false;
                if (this.client == null || this.ns == null)
                {
                    Console.WriteLine("ASO ES NULL TIO");
                    return;
                }
                this.client.GetStream().Flush();
                this.client.GetStream().Close();
                this.client.Close();
                this.client.Dispose();
                this.client = null;
                this.ns = null;
            }
            catch (Exception e) { 
                Console.WriteLine(e.ToString());
            }
        }

        private ConnectResult handshake(bool useServerSettings, SocketOptions options)
        {
            try
            {
                this.connected = false;
                this.options = new SocketOptions(false, false);

                this.s = this.ns;

                if (useServerSettings)
                {
                    this.sendData("DEFAULT");
                    string response = this.receiveData();

                    if (response.Split('$')[0].Equals("OK"))
                    {
                        SocketOptions remoteOpions = (SocketOptions)JsonConvert.DeserializeObject(response.Split('$')[1], typeof(SocketOptions));
                        if (remoteOpions == null)
                            return ConnectResult.HandshakeError;
                        this.options = remoteOpions;
                        this.connected = true;
                    }
                }
                else
                {
                    this.sendData(JsonConvert.SerializeObject(options));
                    string response = this.receiveData();
                    if (response.Split('$')[0].Equals("OK"))
                    {
                        this.options = options;
                        this.connected = true;
                    }
                    else if (response.Split('$')[0].Equals("ERR"))
                    {
                        this.options = (SocketOptions)JsonConvert.DeserializeObject(response.Split('$')[1], typeof(SocketOptions));
                        this.connected = false;
                        if (options.encryption && !this.options.encryption)
                            return ConnectResult.SSLError;
                        return ConnectResult.HandshakeError;
                    }
                }

                if (!this.connected) return ConnectResult.HandshakeError;

                if (this.options.encryption)
                    if ((this.ssl = this.initializeSSL(this.client, this.host)) == null)
                        return ConnectResult.SSLError;

                return ConnectResult.OK;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            this.options = null;
            return ConnectResult.IOEerror;
        }

    }


}
