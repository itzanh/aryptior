﻿using System;

namespace ProvaFils
{
    public class OnSubscriptionPushEventArgs : EventArgs
    {
        public string topicName;
        public SubscriptionChangeType changeType;
        public int position;
        public string newValue;

        public OnSubscriptionPushEventArgs()
        {
        }

        public OnSubscriptionPushEventArgs(string topicName, SubscriptionChangeType changeType, int position, string newValue)
        {
            this.topicName = topicName;
            this.changeType = changeType;
            this.position = position;
            this.newValue = newValue;
        }
    }
}
