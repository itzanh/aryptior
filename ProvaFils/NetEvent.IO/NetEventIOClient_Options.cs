﻿namespace ProvaFils
{
    public class NetEventIOClient_Options
    {
        public bool requiresPassword;
        public string passwd;

        public NetEventIOClient_Options()
        {
        }

        public NetEventIOClient_Options(bool requiresPassword, string passwd)
        {
            this.requiresPassword = requiresPassword;
            this.passwd = passwd;
        }
    }
}
