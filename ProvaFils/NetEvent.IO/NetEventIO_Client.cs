﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace ProvaFils
{

    public class NetEventIO_Client
    {
        public enum NetEventIOConnectionResult
        {
            Ok,
            InitErr,
            AuthErr
        }

        public const int CURRENT_VERSION = 1;

        public string id;
        public readonly NetworkConnectionAbstract client;
        private NetEventIOClientState state;
        private NetEventIOClient_Options options;
        public OnDisconnect onDisconnect;

        private readonly ConcurrentDictionary<string, OnMessage> messageListeners;
        private readonly ConcurrentDictionary<string, OnMessageWithCallback> messageListenersWithCallback;
        private readonly ConcurrentDictionary<string, OnCallback> callbackQueue;
        private readonly ConcurrentDictionary<string, OnSubscriptionPush> subscriptionListeners;
        private readonly ConcurrentDictionary<string, OnBinaryMessage> binaryMessageListeners;

        public delegate void OnSubscriptionPush(NetEventIO_Client sender, OnSubscriptionPushEventArgs e);

        public delegate void OnMessage(NetEventIO_Client sender, OnMessageEventArgs e);
        public delegate void OnBinaryMessage(NetEventIO_Client sender, BinaryMessage message);
        public delegate void Callback(string messageToCallback);
        public delegate void OnCallback(NetEventIO_Client sender, string response);
        public delegate void OnMessageWithCallback(NetEventIO_Client sender, OnMessageEventArgs e, Callback callback);
        public delegate void OnDisconnect(NetEventIO_Client sender);

        private NetEventIO_Client()
        {
            this.messageListeners = new ConcurrentDictionary<string, OnMessage>();
            this.messageListenersWithCallback = new ConcurrentDictionary<string, OnMessageWithCallback>();
            this.callbackQueue = new ConcurrentDictionary<string, OnCallback>();
            this.subscriptionListeners = new ConcurrentDictionary<string, OnSubscriptionPush>();
            this.binaryMessageListeners = new ConcurrentDictionary<string, OnBinaryMessage>();

            state = NetEventIOClientState.Connected;
            this.onDisconnect = (NetEventIO_Client client) => { };
        }

        public NetEventIO_Client(NetworkConnectionAbstract client, NetEventIOClient_Options options) : this()
        {
            this.client = client;
            this.options = options;
        }

        public void Run()
        {
            byte[] message;
            bool isBinary;

            while (this.client.isConnected())
            {
                if (!receiveMessage(out message, out isBinary) || message == null)
                {
                    this.disconnected();
                    return;
                }
                if (isBinary)
                {
                    this.handleBinaryEvent(this.parseMessage(message));
                    continue;
                }
                Message newMessage = this.parseMessage(Encoding.UTF8.GetString(message));
                if (newMessage != null)
                {
                    switch (newMessage.packetType)
                    {
                        case PacketType.S_sendEvent:
                            {
                                this.handleEvent(newMessage);
                                break;
                            }
                        case PacketType.S_sendCallbackForEvent:
                            {
                                this.handleCallbackResponse(newMessage);
                                break;
                            }
                        case PacketType.S_subscriptionPush:
                            {
                                this.handleSubscriptionPush(newMessage);
                                break;
                            }
                        case PacketType.S_sendEventForCallback:
                            {
                                this.handleEventWithCallback(newMessage);
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("Possible communication error with the server, one message was dropped.");
                                break;
                            }
                    }
                }
            }

            this.disconnected();
        }

        private void disconnected()
        {
            Console.WriteLine("connection was closed");
            this.client.disconnect();
            if (this.onDisconnect != null) onDisconnect(this);

            this.callbackQueue.Clear();
            this.messageListeners.Clear();
            this.subscriptionListeners.Clear();
            this.binaryMessageListeners.Clear();
            this.messageListenersWithCallback.Clear();
        }

        public NetEventIOConnectionResult connect()
        {
            if (!this.handleInit())
            {
                Console.WriteLine("Could not init! VERSION MISMATCH!");
                this.state = NetEventIOClientState.Disconnected;
                return NetEventIOConnectionResult.InitErr;
            }
            if (!this.handleAuth())
            {
                Console.WriteLine("Could not auth!");
                this.state = NetEventIOClientState.Disconnected;
                return NetEventIOConnectionResult.AuthErr;
            }
            return NetEventIOConnectionResult.Ok;
        }

        private Message parseMessage(string message)
        {
            try
            {
                // initial check
                if (message[message.Length - 1] != '$') return null;

                // get UUID
                int uuidEnd = message.IndexOf('$');
                if (uuidEnd == -1) return null;
                string uuid = message.Substring(0, uuidEnd);

                message = message.Substring(uuidEnd + 1);

                // get metadata
                int packetInitializationEnd = message.IndexOf('$');
                if (packetInitializationEnd == -1) return null;
                string[] metadata = message.Substring(0, packetInitializationEnd).Split(':');
                if (metadata.Length != 3) return null;

                // get message
                message = message.Substring(packetInitializationEnd + 1, (message.Length - (packetInitializationEnd + 2)));

                Message newMessage = new Message();

                newMessage.id = uuid;

                newMessage.packetType = (PacketType)Int32.Parse(metadata[0]);
                newMessage.eventName = metadata[1];
                newMessage.command = metadata[2];

                newMessage.message = message;

                return newMessage;
            }
            catch (Exception) { return null; }
        }

        private BinaryMessage parseMessage(byte[] message)
        {
            try
            {
                // get the header size
                byte[] headerLengthInt = new byte[4];
                Array.Copy(message, 0, headerLengthInt, 0, headerLengthInt.Length);
                int headerLength = BitConverter.ToInt32(message, 0);

                // get the string header
                byte[] binaryHeader = new byte[headerLength];
                Array.Copy(message, 4, binaryHeader, 0, headerLength);
                string header = Encoding.UTF8.GetString(binaryHeader);

                // get the message length
                byte[] messageLengthInt = new byte[4];
                Array.Copy(message, headerLength + 4, messageLengthInt, 0, messageLengthInt.Length);
                int messageLength = BitConverter.ToInt32(messageLengthInt, 0);

                // get the binary message
                byte[] binaryMessage = new byte[messageLength];
                Array.Copy(message, headerLength + 8, binaryMessage, 0, binaryMessage.Length);

                // initial check
                if (header[header.Length - 1] != '$') return null;

                // get UUID
                int uuidEnd = header.IndexOf('$');
                if (uuidEnd == -1) return null;
                string uuid = header.Substring(0, uuidEnd);

                header = header.Substring(uuidEnd + 1);

                // get metadata
                int packetInitializationEnd = header.IndexOf('$');
                if (packetInitializationEnd == -1) return null;
                string[] metadata = header.Substring(0, packetInitializationEnd).Split(':');
                if (metadata.Length != 3) return null;

                BinaryMessage newMessage = new BinaryMessage();

                newMessage.id = uuid;

                newMessage.packetType = (PacketType)Int32.Parse(metadata[0]);
                newMessage.eventName = metadata[1];
                newMessage.command = metadata[2];

                newMessage.message = binaryMessage;

                return newMessage;
            }
            catch (Exception) { return null; }
        }

        private string serializeMessage(Message m)
        {
            try
            {
                StringBuilder str = new StringBuilder();
                str
                    .Append(m.id)
                    .Append('$')
                    .Append((int)m.packetType).Append(':')
                    .Append(m.eventName).Append(':')
                    .Append(m.command)
                    .Append('$').Append(m.message).Append('$');
                return str.ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error serialitzant el missatge " + e.ToString());
                return null;
            }
        }

        private byte[] serializeMessage(BinaryMessage m)
        {
            try
            {
                MemoryStream ms = new MemoryStream();

                StringBuilder str = new StringBuilder();
                str
                    .Append(m.id)
                    .Append('$')
                    .Append((int)m.packetType).Append(':')
                    .Append(m.eventName).Append(':')
                    .Append(m.command)
                    .Append('$');

                // write header
                byte[] header = Encoding.UTF8.GetBytes(str.ToString());
                ms.Write(BitConverter.GetBytes(header.Length), 0, 4);
                ms.Write(header, 0, header.Length);

                // write message
                ms.Write(BitConverter.GetBytes(m.message.Length), 0, 4);
                ms.Write(m.message, 0, m.message.Length);

                // write verification char
                ms.Write(Encoding.UTF8.GetBytes("$"), 0, 1);
                return ms.ToArray();
            }
            catch (Exception) { return null; }
        }

        private bool receiveMessage(out byte[] receivedDataout, out bool isBinaryout)
        {
            byte[] receivedData;
            bool isBinary;
            if (!this.client.receiveData(out receivedData, out isBinary))
            {
                receivedDataout = null;
                isBinaryout = false;
                return false;
            }

            receivedDataout = receivedData;
            isBinaryout = isBinary;
            return true;
        }

        private Message receiveMessage()
        {
            byte[] receivedData = null;
            bool isBinary;
            if (!this.client.receiveData(out receivedData, out isBinary) || receivedData == null || isBinary)
            {
                return null;
            }

            return this.parseMessage(Encoding.UTF8.GetString(receivedData));
        }

        private bool sendMessage(Message m)
        {
            string serializedMessage = this.serializeMessage(m);
            return this.client.sendData(Encoding.UTF8.GetBytes(serializedMessage), false);
        }

        private bool sendMessage(BinaryMessage bm)
        {
            byte[] serializedMessage = this.serializeMessage(bm);
            return this.client.sendData(serializedMessage, true);
        }

        private bool handleInit()
        {
            Message m = new Message(PacketType.C_init, "", "", CURRENT_VERSION.ToString());
            this.sendMessage(m);

            Message response = this.receiveMessage();
            if (response == null) return false;
            if (response.packetType == PacketType.S_initOk)
            {
                this.id = response.message;
                this.state = NetEventIOClientState.Initialized;
                return true;
            }
            return false;
        }

        private bool handleAuth()
        {
            if (this.options.requiresPassword)
            {
                this.sendMessage(new Message(PacketType.C_authSend, "", "", this.options.passwd));
            }
            else
            {
                this.sendMessage(new Message(PacketType.C_authSend, "", "", ""));
            }

            Message response = this.receiveMessage();
            if (response == null) return false;
            if (response.packetType == PacketType.S_authOk)
            {
                this.state = NetEventIOClientState.Authenticated;
                return true;
            }
            return false;
        }

        private void handleEvent(Message m)
        {
            if (this.state != NetEventIOClientState.Authenticated) return;
            // search event by name in the dictionary
            OnMessage listener = null;
            try
            {
                listener = this.messageListeners[m.eventName];
            }
            catch (Exception) { return; }
            if (listener == null) return;

            // emit event
            listener(this, new OnMessageEventArgs(m));
        }

        public void handleEventWithCallback(Message m)
        {
            if (this.state != NetEventIOClientState.Authenticated) return;
            OnMessageWithCallback listener = null;
            try
            {
                listener = this.messageListenersWithCallback[m.eventName];
            }
            catch (Exception) { return; }
            if (listener == null) return;

            // emit event
            listener(this, new OnMessageEventArgs(m), (string messageToCallback) =>
            {
                this.sendMessage(new Message(m.id, PacketType.C_sendCallbackForEvent, m.eventName, m.command, messageToCallback));
            });
        }

        public bool on(string eventName, OnMessage listener)
        {
            if (eventName.Contains(':') || eventName.Contains('$') || listener == null) return false;
            try
            {
                return this.messageListeners.TryAdd(eventName, listener);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool on(string eventName, OnMessageWithCallback listenerWithCallback)
        {
            if (eventName.Contains(':') || eventName.Contains('$') || listenerWithCallback == null) return false;
            try
            {
                return this.messageListenersWithCallback.TryAdd(eventName, listenerWithCallback);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool on(string eventName, OnBinaryMessage listener)
        {
            if (eventName.Contains(':') || eventName.Contains('$') || listener == null) return false;
            try
            {
                this.binaryMessageListeners[eventName] = listener;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error nene " + e.ToString());
                return false;
            }
            return true;
        }

        public bool removeEvent(string eventName)
        {
            if (eventName.Contains(':') || eventName.Contains('$')) return false;
            try
            {
                return this.messageListeners.TryRemove(eventName, out _);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool emit(Message m)
        {
            return this.emit(m.eventName, m.command, m.message);
        }

        public bool emit(string eventName)
        {
            return this.emit(eventName, "", "");
        }

        public bool emit(string eventName, string command)
        {
            return this.emit(eventName, command, "");
        }

        public bool emit(string eventName, string command, string message)
        {
            Message m = new Message(PacketType.C_sendEvent, eventName, command, message);
            return this.sendMessage(m);
        }

        public bool emit(string eventName, string command, byte[] message)
        {
            if (this.state != NetEventIOClientState.Authenticated) return false;

            BinaryMessage m = new BinaryMessage(PacketType.C_binaryEvent, eventName, command, message);
            return this.sendMessage(m);
        }

        public bool handleCallbackResponse(Message m)
        {
            try
            {
                if (!this.callbackQueue.TryGetValue(m.id, out OnCallback callback)) return false;
                new Thread(new ThreadStart(() =>
                {
                    callback(this, m.message);
                })).Start();

                return this.callbackQueue.TryRemove(m.id, out _);
            }
            catch (Exception) { return false; }
        }

        public bool handleBinaryEvent(BinaryMessage m)
        {
            if (this.state != NetEventIOClientState.Authenticated || m.packetType != PacketType.S_binaryEvent) return false;
            OnBinaryMessage listener = null;
            try
            {
                listener = this.binaryMessageListeners[m.eventName];
            }
            catch (Exception) { return false; }
            if (listener == null) return false;

            // emit event
            listener(this, m);
            return true;
        }

        public bool emit(string eventName, OnCallback callback)
        {
            return this.emit(eventName, "", "", callback);
        }

        public bool emit(string eventName, string command, string message, OnCallback callback)
        {
            try
            {
                Message m = new Message(PacketType.C_sendEventForCallback, eventName, command, message);
                if (m == null || m.id == null || callback == null) return false;

                if (this.callbackQueue == null)
                    return false;

                if (!this.callbackQueue.TryAdd(m.id, callback))
                    return false;

                return this.sendMessage(m);
            }
            catch (Exception) { return false; }
        }

        public bool subscribe(string topicName, OnSubscriptionPush listener)
        {
            if (topicName.Contains(':') || topicName.Contains('$') || listener == null) return false;
            try
            {
                this.subscriptionListeners.TryAdd(topicName, listener);
                Message m = new Message(PacketType.C_subscribe, topicName, "", "");
                return this.sendMessage(m);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool unsubscribe(string topicName)
        {
            if (topicName.Contains(':') || topicName.Contains('$')) return false;
            try
            {
                if (this.subscriptionListeners.TryRemove(topicName, out _))
                {
                    Message m = new Message(PacketType.C_unsubscribe, topicName, "", "");
                    return this.sendMessage(m);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void handleSubscriptionPush(Message m)
        {
            if (this.state != NetEventIOClientState.Authenticated) return;
            // search event by name in the dictionary
            OnSubscriptionPush listener;
            try
            {
                listener = this.subscriptionListeners[m.eventName];

                // emit event
                SubscriptionChangeType changeType = (SubscriptionChangeType)Int32.Parse(m.command.Split(',')[0]);
                int pos = Int32.Parse(m.command.Split(',')[1]);

                new Thread(new ThreadStart(() =>
                {
                    listener(this, new OnSubscriptionPushEventArgs(m.eventName, changeType, pos, m.message));
                })).Start();
            }
            catch (Exception) { return; }
            if (listener == null) return;
        }
    }
}
