﻿namespace ProvaFils
{
    public enum NetEventIOClientState
    {
        Connected,
        Initialized,
        Authenticated,
        Disconnected
    }
}
