﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using YamlDotNet.Serialization;

namespace ProvaFils
{
    public static class LocalStorage
    {
        /// <summary>
        /// Specifies the default name of the attached configuration file. (YAML)
        /// </summary>
        public const string CONFIGURATION_FILENAME = "config.yaml";
        /// <summary>
        /// Sets the default name of the file where the dictionary of downloaded backups is going to be stored. (YAML)
        /// </summary>
        public const string BACKUP_FILENAME = "backup.yaml";

        /// <summary>
        /// Specifies the name of the attached configuration file.
        /// </summary>
        public static string configPath;
        /// <summary>
        /// Sets the name of the file where the dictionary of downloaded backups is going to be stored.
        /// </summary>
        public static string backupPath;

        public static Semaphore MUTEXconfig = new Semaphore(1, 1);

        public enum SettingsInitializationResult
        {
            Ok,
            ReadError,
            InvalidSettings
        }

        /// <summary>
        /// Tries to load the configuration file (JSON) from disk, 
        /// or creates a new one with the default parameters if the configuration file does not exist.
        /// </summary>
        /// <returns>
        /// true: configuration file was loaded or created by default successfully
        /// false: could not initialize the client settings
        /// </returns>
        public static SettingsInitializationResult initializeSettings()
        {
            try
            {
                if (!File.Exists(configPath))
                {
                    // create a new configuration object, with the default parameters (see default constructor)
                    Settings configuration = new Settings();
                    //configuration.servers.Add(new Server());
                    saveSettings(configuration);
                    // set as the current configuration
                    MUTEXconfig.WaitOne();
                    Program.config = configuration;
                    MUTEXconfig.Release();
                }
                else
                {
                    Settings configuration = loadSettings();
                    if (configuration == null) return SettingsInitializationResult.ReadError;
                    if (!configuration.isValid()) return SettingsInitializationResult.InvalidSettings;
                    // set as the current configuration
                    MUTEXconfig.WaitOne();
                    Program.config = configuration;
                    MUTEXconfig.Release();
                }
                return SettingsInitializationResult.Ok;
            }
            catch (Exception)
            {
                return SettingsInitializationResult.ReadError;
            }
        }

        public static Settings loadSettings()
        {
            FileStream fs = null;
            try
            {
                // deserialize from disk
                fs = new FileStream(configPath, FileMode.Open);
                MemoryStream ms = new MemoryStream();
                fs.CopyTo(ms);

                string content = Encoding.UTF8.GetString(ms.ToArray());
                Settings c;
                if (configPath.EndsWith(".json"))
                    c = (Settings)JsonConvert.DeserializeObject(content, typeof(Settings));
                else if (configPath.EndsWith(".yaml") || configPath.EndsWith(".yml"))
                {
                    Deserializer deserializer = new Deserializer();
                    c = deserializer.Deserialize<Settings>(content);
                }
                else
                    return null;

                ms.Dispose();
                return c;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
            }
        }

        public static bool saveSettings(Settings configuration)
        {
            try
            {
                string content;

                if (configPath.EndsWith(".json"))
                    content = JsonConvert.SerializeObject(configuration, Formatting.Indented);
                else if (configPath.EndsWith(".yaml") || configPath.EndsWith(".yml"))
                {
                    Serializer serializer = new Serializer();
                    content = serializer.Serialize(configuration);
                }
                else
                    return false;

                byte[] serializedSettings = Encoding.UTF8.GetBytes(content);
                FileStream fs = new FileStream(configPath, FileMode.OpenOrCreate, FileAccess.Write);
                fs.SetLength(0);
                fs.Write(serializedSettings, 0, serializedSettings.Length);
                fs.Flush();
                fs.Close();
                fs.Dispose();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        /// <summary>
        /// adds an event in order to watch for changes on the settings file (JSON).
        /// when the file is modified on the file system, a function is called in order 
        /// to refresh automatically the on-memory settings
        /// </summary>
        /// <returns></returns>
        public static bool watchSettingsFile()
        {
            try
            {
                FileSystemWatcher fsw = new FileSystemWatcher();
                fsw.Path = Directory.GetCurrentDirectory();
                fsw.Filter = Path.GetFileName(configPath);
                fsw.Changed += fswConfigChanged;
                fsw.EnableRaisingEvents = true;
            }
            catch (Exception)
            {
                Console.WriteLine("Changes in the settings file could not be under watch. " +
                "Updating the settings will require a manual restart.");
                return false;
            }
            return true;
        }

        /// <summary>
        /// this functions gells called when file settings is modified (it gets written) on the file system.
        /// first, attemps to read the settings, if it's able to to so, checks the integrity of the 
        /// readed configuration and finally, saves the settings and calls to do hot changes on it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void fswConfigChanged(object sender, FileSystemEventArgs e)
        {
            Settings s = loadSettings();

            if (s != null && s.isValid())
            {
                Console.WriteLine("Changes detected! Reloading settings... ");
                perforHotConfigChanges(s);
                Program.config = s;
            }
            else if (s != null)
            {
                MUTEXconfig.WaitOne();
                MUTEXconfig.Release();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("THE NEW CONFIGURATION IS INVALID! THE LAST VALID SETTINGS PREVIOUSLY LOADED TO MEMORY WILL NOT BE UPDATED.");
                Console.ResetColor();
            }
        }

        /// <summary>
        /// compares the settings given by parameter and the current ones.
        /// then, attempts to perform the necessary changes to apply the new 
        /// settings while the server is running, without requiring a restart.
        /// </summary>
        /// <param name="s"></param>
        private static void perforHotConfigChanges(Settings s)
        {
            // checks if it's necessary to start or stop the backup background job
            if (s.clientSettings.downloadBackups != Program.config.clientSettings.downloadBackups)
            {
                if (s.clientSettings.downloadBackups)
                {
                    ClientThread.runBackupCron();
                }
                else
                {
                    ClientThread.backupCron.Stop();
                }
            }

            // refreshes the maximum backup file size
            if (s.clientSettings.backupMaxSize != Program.config.clientSettings.backupMaxSize)
            {
                ClientThread.backupCron.sizeLimit = s.clientSettings.backupMaxSize;
            }
        }

        /***********/
        /* BACKUPS */
        /***********/

        public static bool saveBackups(List<Backup> backups, string addr)
        {
            FileStream fsRead = null;
            FileStream fsWrite = null;
            try
            {
                // deserialize from disk
                fsRead = new FileStream(backupPath, FileMode.Open);
                MemoryStream ms = new MemoryStream();
                fsRead.CopyTo(ms);
                fsRead.Close();
                fsRead.Dispose();

                string content = Encoding.UTF8.GetString(ms.ToArray());
                Dictionary<string, List<Backup>> backupStorage;
                if (configPath.EndsWith(".json"))
                    backupStorage = (Dictionary<string, List<Backup>>)
                    JsonConvert.DeserializeObject(content, typeof(Dictionary<string, List<Backup>>));
                else if (configPath.EndsWith(".yaml") || configPath.EndsWith(".yml"))
                {
                    Deserializer deserializer = new Deserializer();
                    backupStorage = deserializer.Deserialize<Dictionary<string, List<Backup>>>(content);
                }
                else
                    return false;

                if (backupStorage.ContainsKey(addr)) backupStorage.Remove(addr);
                backupStorage.Add(addr, backups);

                string serializedBackups;
                if (backupPath.EndsWith(".json"))
                    serializedBackups = JsonConvert.SerializeObject(backupStorage, Formatting.Indented);
                else if (backupPath.EndsWith(".yaml") || backupPath.EndsWith(".yml"))
                {
                    Serializer serializer = new Serializer();
                    serializedBackups = serializer.Serialize(backupStorage);
                }
                else
                    return false;

                byte[] serializedContent = Encoding.UTF8.GetBytes(serializedBackups);
                fsWrite = new FileStream(backupPath, FileMode.Open, FileAccess.Write);
                fsWrite.SetLength(0);
                fsWrite.Write(serializedContent, 0, serializedContent.Length);
                fsWrite.Flush();


                fsWrite.Close();
                fsWrite.Dispose();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                if (fsRead != null)
                {
                    fsRead.Close();
                    fsRead.Dispose();
                }
                if (fsWrite != null)
                {
                    fsWrite.Close();
                    fsWrite.Dispose();
                }
            }
            return false;
        }

        public static List<Backup> loadBackups(string addr)
        {
            if (!File.Exists(backupPath))
            {
                if (initializeBackups())
                    return new List<Backup>();
                else
                    return null;
            }

            FileStream fs = null;
            try
            {
                // deserialize from disk
                fs = new FileStream(backupPath, FileMode.Open);
                MemoryStream ms = new MemoryStream();
                fs.CopyTo(ms);

                string content = Encoding.UTF8.GetString(ms.ToArray());
                Dictionary<string, List<Backup>> backupStorage;
                if (backupPath.EndsWith(".json"))
                    backupStorage = (Dictionary<string, List<Backup>>)JsonConvert.DeserializeObject(content, typeof(Dictionary<string, List<Backup>>));
                else if (backupPath.EndsWith(".yaml") || backupPath.EndsWith(".yml"))
                {
                    Deserializer deserializer = new Deserializer();
                    backupStorage = deserializer.Deserialize<Dictionary<string, List<Backup>>>(content);
                }
                else
                    return null;

                List<Backup> backups;
                if (!backupStorage.TryGetValue(addr, out backups))
                {
                    ms.Dispose();
                    fs.Close();
                    fs.Dispose();
                    return new List<Backup>();
                }

                ms.Dispose();
                fs.Close();
                fs.Dispose();
                return backups;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
                return null;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
            }
        }

        private static bool initializeBackups()
        {
            if (!File.Exists(backupPath))
            {
                FileStream fsWrite = null;
                try
                {
                    Dictionary<string, List<Backup>> backupStorage = new Dictionary<string, List<Backup>>();

                    string serializedBackups;
                    if (backupPath.EndsWith(".json"))
                        serializedBackups = JsonConvert.SerializeObject(backupStorage, Formatting.Indented);
                    else if (backupPath.EndsWith(".yaml") || backupPath.EndsWith(".yml"))
                    {
                        Serializer serializer = new Serializer();
                        serializedBackups = serializer.Serialize(backupStorage);
                    }
                    else
                        return false;

                    byte[] serializedContent = Encoding.UTF8.GetBytes(serializedBackups);
                    fsWrite = new FileStream(backupPath, FileMode.Create, FileAccess.Write);
                    fsWrite.SetLength(0);
                    fsWrite.Write(serializedContent, 0, serializedContent.Length);
                    fsWrite.Flush();

                    fsWrite.Close();
                    fsWrite.Dispose();

                    return true;
                }
                catch (Exception)
                {

                }
                finally
                {
                    if (fsWrite != null)
                    {
                        fsWrite.Close();
                        fsWrite.Dispose();
                    }
                }

            }
            return false;
        }
    }
}
