﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;
using System.Net.NetworkInformation;

namespace ProvaFils
{
    public class NetworkDiscovery
    {
        public const int discoveryPort = 22217;
        public const int discovertResponsePort = 22218;
        public const string discoveryMessage = "$ARYPTIOR$9d09fba2-eff2-400a-9e84-04e7660b9348$";
        private readonly int discoveryWaitTime;

        public NetworkDiscovery(int discoveryWaitTime)
        {
            this.discoveryWaitTime = discoveryWaitTime;
        }

        public List<NetworkDiscoveryData> dicover()
        {
            // prepare the udp connection that is going to be used to send the beacon data to discover servers
            UdpClient udpBroadcast = new UdpClient
            {
                EnableBroadcast = true
            };
            byte[] data = Encoding.UTF8.GetBytes(discoveryMessage);

            List<NetworkDiscoveryData> servers = new List<NetworkDiscoveryData>();
            // prepare a background thread to listen to all the responses from all the possible servers
            UdpClient udpClient = new UdpClient(new IPEndPoint(IPAddress.Any, discovertResponsePort));
            Thread listenToServers = new Thread(new ThreadStart(() =>
            {
                IPEndPoint from = new IPEndPoint(0, 0);
                while (true)
                {
                    byte[] recvBuffer = udpClient.Receive(ref from);
                    string message = Encoding.UTF8.GetString(recvBuffer);

                    if (message[0] == '$' && message[message.Length - 1] == '$' && message.StartsWith("$ARYPTIOR$1$") && message.Count(f => f == '$') == 4)
                    {
                        string[] msgData = message.Split('$');

                        string host = from.Address.ToString();
                        NetworkDiscoveryData ndData = (NetworkDiscoveryData)JsonConvert.DeserializeObject(msgData[3], typeof(NetworkDiscoveryData));
                        ndData.host = host;

                        servers.Add(ndData);
                    }
                }
            }));

            listenToServers.Start();
            foreach (IPAddress addr in listBroadcastIpAdresses())
            {
                udpBroadcast.Send(data, data.Length, addr.ToString(), discoveryPort);
            }
            listenToServers.Join(this.discoveryWaitTime);
            listenToServers.Abort();

            udpBroadcast.Close();
            if (udpClient != null) udpClient.Close();
            return servers;
        }

        private List<IPAddress> listBroadcastIpAdresses()
        {
            // list of all broadcast addresses from all the local interfaces
            List<IPAddress> ipBroadcastAddrList = new List<IPAddress>();

            // get all the unicast adresses for each local network interface and get the broadcast addresses
            foreach (NetworkInterface nInterface in NetworkInterface.GetAllNetworkInterfaces())
            {
                foreach (UnicastIPAddressInformation ip in nInterface.GetIPProperties().UnicastAddresses)
                {
                    if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        byte[] ipAdressBytes = ip.Address.GetAddressBytes();
                        byte[] subnetMaskBytes = ip.IPv4Mask.GetAddressBytes();

                        byte[] broadcastAddress = new byte[ipAdressBytes.Length];
                        for (int i = 0; i < broadcastAddress.Length; i++)
                        {
                            broadcastAddress[i] = (byte)(ipAdressBytes[i] | (subnetMaskBytes[i] ^ 255));
                        }
                        ipBroadcastAddrList.Add(new IPAddress(broadcastAddress));
                    }
                }
            }

            return ipBroadcastAddrList;
        }

    }

    [Serializable]
    public class NetworkDiscoveryData
    {
        public string host;
        public int portSocket;
        public int portWS;
        public string name;

        public NetworkDiscoveryData()
        {
            this.portSocket = 22215;
            this.portWS = 22216;
            this.name = "";
        }

        public NetworkDiscoveryData(string host, int port_socket, int port_ws, string name)
        {
            this.host = host;
            this.portSocket = port_socket;
            this.portWS = port_ws;
            this.name = name;
        }

        public override string ToString()
        {
            return this.name + "\t" + this.host + "\t" + this.portSocket;
        }
    }
}
