﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace ProvaFils
{
    class Program
    {
        /// <summary>
        /// This property stores this client's current configuration.
        /// </summary>
        public static Settings config
        {
            get
            {
                lock (configMUTEX)
                {
                    return _config;
                }
            }
            set
            {
                lock (configMUTEX)
                {
                    _config = value;
                }
            }
        }
        /// <summary>
        /// Current configuration of the client
        /// </summary>
        private static Settings _config;
        /// <summary>
        /// Parsed command line arguments
        /// </summary>
        public static Dictionary<string, string> parameters;
        /// <summary>
        /// MUTEX for mutual exclusion acces to the current configuration
        /// </summary>
        private static readonly Semaphore configMUTEX = new Semaphore(1, 1);

        /// <summary>
        /// Contains the server's host and port that is currently bind to.
        /// </summary>
        public static Server currentServer;

        static void Main(string[] args)
        {
            parameters = parseArgs(args);

            if (Utilities.showHelp())
            {
                return;
            }
            initializeLocalStoragePaths();

            // load settings or load the default ones in case the configuration file is not present
            LocalStorage.SettingsInitializationResult initializationResult = LocalStorage.initializeSettings();
            if (initializationResult == LocalStorage.SettingsInitializationResult.ReadError)
            {
                config = new Settings();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ERROR! Could not read or create the configuration file. Check if you have enought permissions and disk space.");
                Console.ResetColor();
            }
            else if (initializationResult == LocalStorage.SettingsInitializationResult.InvalidSettings)
            {
                config = new Settings();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ERROR! INVALID SETTINGS DETECTED! Could not load the configuration file. Exiting...");
                Console.ResetColor();
            }

            // prompt the user to select a server from the list, add or search one

            if (!getServerFromParams(out currentServer))
            {
                Console.WriteLine("COULD NOT PARSE SERVICE PARAMETERS!");
                return;
            }

            if (currentServer == null)
                currentServer = selectCurrentServer();

            // wait for changes on the settings file to refresh automatically the on-memory settings
            LocalStorage.watchSettingsFile();

            // start the clien's main thread
            ClientThread c = new ClientThread();
            Thread t = new Thread(new ThreadStart(c.Run));
            t.Start();
            t.Join();

            Console.WriteLine("******************");
            Console.ReadKey(true);
        }

        private static void initializeLocalStoragePaths()
        {
            string configPath;
            if (parameters != null && parameters.TryGetValue("configPath", out configPath))
            {
                if (!configPath.EndsWith(".yaml") && !configPath.EndsWith(".yml") && !configPath.EndsWith(".json"))
                {
                    Console.WriteLine("ERROR! THE SERVER ONLY SUPPORTS JSON AND YAML SERIALIZATION FOR THE SETTINGS FILE.\n" +
                        "THE ONY ALLOWED EXTENSIONS FOR THE CONFIG FILE ARE .yaml, .yml and .json");
                    return;
                }
                LocalStorage.configPath = configPath;
            }
            else
            {
                LocalStorage.configPath = LocalStorage.CONFIGURATION_FILENAME;
            }

            string backupPath;
            if (parameters != null && parameters.TryGetValue("backupPath", out backupPath))
            {
                if (!backupPath.EndsWith(".yaml") && !backupPath.EndsWith(".yml") && !backupPath.EndsWith(".json"))
                {
                    Console.WriteLine("ERROR! THE SERVER ONLY SUPPORTS JSON AND YAML SERIALIZATION FOR THE BACKUPS FILE.\n" +
                        "THE ONY ALLOWED EXTENSIONS FOR THE CONFIG FILE ARE .yaml, .yml and .json");
                    return;
                }
                LocalStorage.backupPath = backupPath;
            }
            else
            {
                LocalStorage.backupPath = LocalStorage.BACKUP_FILENAME;
            }
        }

        private static Dictionary<string, string> parseArgs(string[] args)
        {
            if (args.Length == 0)
                return null;

            Dictionary<string, string> parameters = new Dictionary<string, string>();

            foreach (string arg in args)
            {
                int value = arg.IndexOf('=');
                parameters[(value == -1 ? arg : arg.Substring(0, value))] = (value > -1 ? arg.Substring(value + 1) : null);
            }

            return parameters;
        }

        private static bool getServerFromParams(out Server s)
        {

            if (parameters == null
                || (parameters != null && !parameters.TryGetValue("service", out string service) && service == null))
            {
                s = null;
                return true;
            }

            if (parameters != null && parameters.TryGetValue("url", out string url) && url != null)
            {
                s = getServerFromUrl(url);
            }
            else
            {
                s = parseServerFromParams();
            }

            return (s != null);
        }

        private static Server parseServerFromParams()
        {
            if (parameters != null && !parameters.TryGetValue("service", out string service) && service == null)
                return null;
            if (parameters != null && parameters.TryGetValue("url", out string url) && url != null)
                return getServerFromUrl(url);

            Server srv = new Server();

            try
            {
                foreach (string arg in parameters.Keys)
                {
                    switch (arg)
                    {
                        case "host":
                            {
                                srv.host = parameters[arg];
                                break;
                            }
                        case "port":
                            {
                                srv.port = Int32.Parse(parameters[arg]);
                                break;
                            }
                        case "requiresPassword":
                            {
                                srv.requiresPassword = Boolean.Parse(parameters[arg]);
                                break;
                            }
                        case "passwd":
                            {
                                srv.passwd = parameters[arg];
                                break;
                            }
                        case "useServerSettings":
                            {
                                srv.useServerSettings = Boolean.Parse(parameters[arg]);
                                break;
                            }
                        case "options.encryption":
                            {
                                srv.options.encryption = Boolean.Parse(parameters[arg]);
                                break;
                            }
                        case "options.compression":
                            {
                                srv.options.compression = Boolean.Parse(parameters[arg]);
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }

            return srv;
        }

        private static Server getServerFromUrl(string url)
        {
            Uri uri;
            try
            {
                uri = new Uri(url);
                if (!uri.Scheme.Equals("aryptior", StringComparison.InvariantCultureIgnoreCase))
                {
                    return null;
                }
                Server srv = new Server();

                srv.host = uri.Host;
                srv.port = uri.IsDefaultPort ? 22215 : uri.Port;
                srv.requiresPassword = !uri.UserInfo.Equals(String.Empty);
                srv.passwd = uri.UserInfo;

                if (!uri.Query.Equals(String.Empty) && uri.Query.Length > 1)
                {
                    string[] query = uri.Query.Substring(1).Split('&');
                    foreach (var querySel in query)
                    {
                        string[] option = querySel.Split('=');
                        if (option.Length != 2)
                        {
                            return null;
                        }
                        switch (option[0])
                        {
                            case "useServerSettings":
                                {
                                    srv.useServerSettings = Boolean.Parse(option[1]);
                                    break;
                                }
                            case "options.encryption":
                                {
                                    srv.options.encryption = Boolean.Parse(option[1]);
                                    break;
                                }
                            case "options.compression":
                                {
                                    srv.options.compression = Boolean.Parse(option[1]);
                                    break;
                                }
                            default:
                                {
                                    return null;
                                }
                        }
                    }
                }

                return srv;
            }
            catch (Exception)
            {
                return null;
            }

        }

        /// <summary>
        /// A menu is opened to let the user to create or select a server from the list, or search one on the network.
        /// </summary>
        private static Server selectCurrentServer()
        {
            int itemSelected = 0;
            ConsoleKeyInfo keyPressed;

            while (true)
            {
                Console.SetCursorPosition(0, 0);
                Console.Clear();

                Console.BackgroundColor = ConsoleColor.Yellow;
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("\tSELECT BETWEEN " + config.servers.Count + " SERVERS:\n\n");
                Console.ResetColor();

                Console.BackgroundColor = ConsoleColor.DarkGray;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("HOST\t\tPORT\tPASSWORD");
                Utilities.PadRight();
                Console.ResetColor();

                for (int i = 0; i < config.servers.Count + 2; i++)
                {
                    if (i == itemSelected)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                    }
                    if (i == 0)
                    {
                        Utilities.PadRight("Add server...", true);
                    }
                    else if (i == 1)
                    {
                        Utilities.PadRight("Search servers...", true);
                    }
                    else
                    {
                        Server s = config.servers[(i - 2)];

                        Console.Write(s.ToString());
                        Utilities.PadRight(true);
                    }
                    Console.Write("\n");
                    if (i == itemSelected)
                    {
                        Console.ResetColor();
                    }
                }

                keyPressed = Console.ReadKey(true);
                switch (keyPressed.Key)
                {
                    case ConsoleKey.Enter:
                        {
                            Console.SetCursorPosition(0, 0);
                            Console.Clear();
                            if (itemSelected > 1)
                            {
                                return config.servers[(itemSelected - 2)];
                            }
                            else if (itemSelected == 0)
                            {
                                menuAddServer();
                            }
                            else if (itemSelected == 1)
                            {
                                NetworkDiscoveryData discovered = searchServers();
                                if (discovered == null) break;
                                return new Server(discovered.host, discovered.portSocket);
                            }
                            break;
                        }
                    case ConsoleKey.DownArrow:
                        {
                            if ((itemSelected) > config.servers.Count) break;
                            itemSelected++;
                            break;
                        }
                    case ConsoleKey.UpArrow:
                        {
                            if ((itemSelected) <= 0) break;
                            itemSelected--;
                            break;
                        }
                    case ConsoleKey.D:
                        {
                            if (itemSelected > 1)
                            {
                                config.servers.RemoveAt(itemSelected - 2);
                                LocalStorage.saveSettings(config);
                                itemSelected = 0;
                            }
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Opens a menu the lists all the servers discovered on the network and
        /// prompts the user to select one or go back.
        /// </summary>
        /// <returns></returns>
        private static NetworkDiscoveryData searchServers()
        {
            // loading screen
            Console.SetCursorPosition(0, 0);
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("\tDISCOVERING SERVERS...\n\n");
            Console.ResetColor();

            // starts a new thread to update the remaining time of the discovery
            new Thread(new ThreadStart(() =>
            {
                for (int i = (config.clientSettings.discoveryWaitTime / 1000); i > 0; i--)
                {
                    Console.SetCursorPosition(0, 0);
                    Console.Clear();

                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("\tDISCOVERING SERVERS... " + i + "\n\n");
                    Console.ResetColor();

                    Thread.Sleep(1000);
                }
            })).Start();

            // discover local devices using the maximum discovery timeout
            NetworkDiscovery discovery = new NetworkDiscovery(config.clientSettings.discoveryWaitTime);
            List<NetworkDiscoveryData> discovered = discovery.dicover();

            // arrows menu
            int itemSelected = 0;
            ConsoleKeyInfo keyPressed;
            while (true)
            {
                // menu header
                Console.SetCursorPosition(0, 0);
                Console.Clear();

                Console.BackgroundColor = ConsoleColor.Yellow;
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("\tSELECT BETWEEN " + discovered.Count + " SERVERS:\n\n");
                Console.ResetColor();

                Console.BackgroundColor = ConsoleColor.DarkGray;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("NAME\t\t\tHOST\t\tPORT");
                Utilities.PadRight();
                Console.ResetColor();

                for (int i = 0; i <= discovered.Count; i++)
                {
                    if (i == itemSelected)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                    }
                    if (i == 0)
                    {
                        Utilities.PadRight("Fuck go back");
                    }
                    else
                    {
                        NetworkDiscoveryData d = discovered[(i - 1)];

                        Console.Write(d.ToString());
                        Utilities.PadRight(true);
                    }
                    Console.Write("\n");
                    if (i == itemSelected)
                    {
                        Console.ResetColor();
                    }
                }

                keyPressed = Console.ReadKey(true);
                switch (keyPressed.Key)
                {
                    case ConsoleKey.Enter:
                        {
                            Console.SetCursorPosition(0, 0);
                            Console.Clear();
                            if (itemSelected > 0)
                            {
                                return discovered[(itemSelected - 1)];
                            }
                            else if (itemSelected == 0)
                            {
                                return null;
                            }
                            break;
                        }
                    case ConsoleKey.DownArrow:
                        {
                            if ((itemSelected) == discovered.Count) break;
                            itemSelected++;
                            break;
                        }
                    case ConsoleKey.UpArrow:
                        {
                            if ((itemSelected) <= 0) break;
                            itemSelected--;
                            break;
                        }
                }
            }
        }


        /// <summary>
        /// This method opens a menu on the terminal to let the user input the data of the new server on a form.
        /// Then, the new server is appended to the existing list and is saved to disk.
        /// </summary>
        private static void menuAddServer()
        {
            bool loop = true;
            while (loop)
            {
                Console.SetCursorPosition(0, 0);
                Console.Clear();
                Console.BackgroundColor = ConsoleColor.Yellow;
                Console.ForegroundColor = ConsoleColor.Cyan;
                Utilities.PadRight("ADD NEW SERVER");
                Console.ResetColor();

                Console.Write("Input URL [leave blank to go back]\n> ");
                string url = Console.ReadLine();

                if (url.Equals(String.Empty))
                    break;

                Server s = getServerFromUrl(url);
                if (s != null && s.isValid())
                {
                    Console.WriteLine("\n*** SERVER DETAILS:");
                    Console.WriteLine(s.GetDetails());
                    Console.WriteLine("Add server?");
                    if (menuYesOrNo())
                    {
                        config.servers.Add(s);
                        LocalStorage.saveSettings(config);
                        loop = false;
                    }
                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\n\nTHE NEW SERVER IS NOT VALID!!! Try again?");
                    Console.ResetColor();
                    loop = menuYesOrNo();
                }
            }
        }

        /// <summary>
        /// Prompt the userr with a yes or no question that can be selected with the keyboard arrows.
        /// </summary>
        /// <param name="defaultSelected"></param>
        /// <returns></returns>
        public static bool menuYesOrNo(bool defaultSelected = true)
        {
            int cursorLines = Console.CursorTop;
            bool loop = true;
            bool selected = defaultSelected;
            ConsoleKeyInfo keyPressed;
            while (loop)
            {
                Console.SetCursorPosition(0, cursorLines);
                // YES
                if (selected)
                {
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                Utilities.PadRight("YES");
                if (selected)
                {
                    Console.ResetColor();
                }
                // NO
                if (!selected)
                {
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                Utilities.PadRight("NO");
                if (!selected)
                {
                    Console.ResetColor();
                }
                // PRESS KEY
                keyPressed = Console.ReadKey(true);
                switch (keyPressed.Key)
                {
                    case ConsoleKey.Enter:
                        {
                            return selected;
                        }
                    case ConsoleKey.DownArrow:
                        {
                            if (!selected) break;
                            selected = false;
                            break;
                        }
                    case ConsoleKey.UpArrow:
                        {
                            if (selected) break;
                            selected = true;
                            break;
                        }
                }
            }
            return true;
        }
    }
}

