﻿using System;
using System.Collections;
using System.Collections.Concurrent;

namespace ProvaFils
{
    class BruteForceThread
    {
        public Hash hash;
        public int[] combination;
        public int charactersCount;
        public bool found;
        public string passwordFound;
        protected bool isProcessingRainbow;
        public ConcurrentQueue<ComputedHash> computed;

        protected BruteForceThread(Hash hash, int charactersCount)
        {
            this.hash = hash;
            this.charactersCount = charactersCount;
        }

        public BruteForceThread(Hash hash, int[] combination, int charactersCount)
        {
            this.hash = hash;
            this.combination = combination;
            this.charactersCount = charactersCount;
            this.found = false;
            this.isProcessingRainbow = false;
        }

        public BruteForceThread(Hash hash, int[] combination, int charactersCount, ref ConcurrentQueue<ComputedHash> computed)
            : this(hash, combination, charactersCount)
        {
            this.isProcessingRainbow = true;
            this.computed = computed;
        }

        public void Run()
        {
            this.searchHash();
        }

        public void Run(Object o)
        {
            this.computed = (ConcurrentQueue<ComputedHash>)o;
            this.searchHash();
        }

        protected void searchHash()
        {
            for (int i = 0; i < charactersCount; i++)
            {
                // generate string from current character combination
                string currentCombination = combinationToString(combination);

                if (currentCombination.Length > hash.caractersFinals)
                {
                    break;
                }

                // check if it's the hash we are looking for
                if (!isProcessingRainbow && Utilities.verifyHash(hash.hash, this.hash.salt + currentCombination, hash.tipus, hash.iteracions))
                {
                    Console.WriteLine("PASSWORD FOUND :D " + this.hash.salt + currentCombination);
                    found = true;
                    passwordFound = currentCombination;
                    return;
                }
                else if (isProcessingRainbow)
                {
                    ClientThread.computed_consumed.WaitOne();
                    string currentHash = Utilities.computeHash(currentCombination, hash.tipus, hash.iteracions);
                    this.computed.Enqueue(new ComputedHash(currentHash, currentCombination));
                    ClientThread.computed_produced.Release();
                }

                // increment the current character combination
                incrementCombination(ref this.combination, this.hash.primerCaracterUnicode, this.hash.ultimCaracterUnicode);
            }
        }

        public static void incrementCombination(ref int[] combination, int primerCaracterUnicode, int ultimCaracterUnicode)
        {
            // augmentar el prrimer caracter
            combination[0]++;
            for (int i = 0; i < combination.Length; i++)
            {
                // comprovar si el caracter actual es mes gran que el maxim
                if (combination[i] > ultimCaracterUnicode)
                {
                    // reinicialitzar al primer caracter la posicio actual
                    combination[i] = primerCaracterUnicode;
                    // si la seguent posicio esta incialitzada, augmentar, si no, inicialitzar al primer caracter
                    if ((i + 2) > combination.Length)
                    {
                        int[] longerCombination = new int[(combination.Length + 1)];
                        for (int j = 0; j < combination.Length; j++)
                        {
                            longerCombination[j + 1] = combination[j];
                        }
                        longerCombination[0] = primerCaracterUnicode;
                        combination = longerCombination;
                    }
                    else
                    {
                        combination[(i + 1)]++;
                    }
                }
            }
        }

        public static string combinationToString(int[] combination)
        {
            string combinacioCaracters = "";
            for (int i = (combination.Length - 1); i >= 0; i--)
            {
                combinacioCaracters += (char)combination[i];
            }
            return combinacioCaracters;
        }

    }
}

