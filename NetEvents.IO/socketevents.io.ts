enum PacketType {
    S_initOk,
    C_init,
    S_initErr,
    C_authSend,
    S_authOk,
    C_sendEvent,
    S_authErr,
    C_sendEventForCallback,
    S_sendEvent,
    C_sendCallbackForEvent,
    S_sendEventForCallback,
    C_subscribe,
    S_sendCallbackForEvent,
    C_unsubscribe,
    S_subscriptionPush,
    C_unsubscribeAll,
    S_binaryEvent,
    C_binaryEvent
}

enum NetEventIOClientState {
    Connected,
    Initialized,
    Authenticated,
    Disconnected
}

enum SubscriptionChangeType {
    insert,
    update,
    delete
}

class Message {
    public id: string;
    public packetType: PacketType;
    public eventName: string;
    public command: string;
    public message: string;

    constructor(id?: string, packetType?: PacketType, eventName?: string, command?: string, message?: string) {
        this.id = id || this.generateMessageID();
        this.packetType = packetType;
        this.eventName = eventName;
        this.command = command;
        this.message = message;
    }

    generateMessageID(): string {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}

class BinaryMessage {
    public id: string;
    public packetType: PacketType;
    public eventName: string;
    public command: string;
    public message: Blob;

    constructor(id?: string, packetType?: PacketType, eventName?: string, command?: string, message?: Blob) {
        this.id = id || this.generateMessageID();
        this.packetType = packetType;
        this.eventName = eventName;
        this.command = command;
        this.message = message;
    }

    generateMessageID(): string {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}

class NetEventIOOptions {
    public onPasswordLogin = (): Promise<string> => {
        return new Promise((resolve) => {
            resolve("");
        });
    };
    public onTokenLogin = (): Promise<string> => {
        return new Promise((resolve) => {
            resolve(null);
        });
    };

    constructor(onPasswordLogin: () => Promise<string>, onTokenLogin: () => Promise<string>) {
        this.onPasswordLogin = onPasswordLogin;
        this.onTokenLogin = onTokenLogin;
    }
}

class NetEventIO_Client {
    readonly CURRENT_VERSION: number = 1;

    public id: string;
    public client: WebSocket;
    public state: NetEventIOClientState;

    private messageListeners: object;
    private callbackQueue: object;
    private subscriptionListeners: object;

    private onPasswordLogin: () => Promise<string>;
    private onTokenLogin: () => Promise<string>;

    constructor(client: WebSocket, options: NetEventIOOptions) {
        this.client = client;

        this.state = NetEventIOClientState.Connected;
        this.messageListeners = {};
        this.callbackQueue = {};
        this.subscriptionListeners = {};

        this.onPasswordLogin = options.onPasswordLogin;
        this.onTokenLogin = options.onTokenLogin;
    }

    public connect(): Promise<Array<boolean | number>> {
        return new Promise(async (resolve) => {
            var result: boolean = await new Promise((connResolve) => {
                this.client.onopen = () => {
                    connResolve(true);
                }
                this.client.onclose = () => {
                    connResolve(false);
                }
                this.client.onerror = () => {
                    connResolve(false);
                }
            });

            if (!result) {
                resolve([false, 0]);
                return;
            }

            if (!await this.handleInit()) {
                this.state = NetEventIOClientState.Disconnected;
                resolve([false, 1]);
                return;
            }
            this.state = NetEventIOClientState.Initialized;
            if (!await this.handleAuth()) {
                this.state = NetEventIOClientState.Disconnected;
                resolve([false, 2]);
                return;
            }

            this.state = NetEventIOClientState.Authenticated;
            this.client.binaryType = "blob";
            resolve([true]);

            setTimeout(() => {
                this.run();
            }, 0);
        });
    }

    public async run(): Promise<void> {
        while (this.client.readyState === this.client.OPEN) {
            var o: any = await this.getNewMessage();
            if (o instanceof Blob) {
                console.log('Rebent un mensajico en binari');
                var bm: BinaryMessage = await this.parseBinaryMessage(o);
                switch (bm.packetType) {
                    case PacketType.S_sendEvent: {
                        this.handleBinaryEvent(bm);
                        break;
                    }
                }
                continue;
            }
            var m: Message = await this.parseMessage(o);

            if (m != null) {
                switch (m.packetType) {
                    case PacketType.S_sendEvent: {
                        this.handleEvent(m);
                        break;
                    }
                    case PacketType.S_sendCallbackForEvent: {
                        this.handleCallbackResponse(m);
                        break;
                    }
                    case PacketType.S_subscriptionPush: {
                        this.handleSubscriptionPush(m);
                        break;
                    }
                }
            }
        }
        console.log('ASO HA ACABAT AAAAAAAAA!!!!');
    }

    private parseMessage(message: string): Message {
        try {
            // initial check
            if (message[message.length - 1] != '$') return null;

            // get UUID
            var uuidEnd: number = message.indexOf('$');
            if (uuidEnd == -1) return null;
            var uuid: string = message.substring(0, uuidEnd);

            message = message.substring(uuidEnd + 1);

            // get metadata
            var packetInitializationEnd: number = message.indexOf('$');
            if (packetInitializationEnd == -1) return null;
            var metadata: string[] = message.substring(0, packetInitializationEnd).split(':');
            if (metadata.length != 3) return null;

            // get message
            message = message.substring(packetInitializationEnd + 1, (message.length - 1));

            var newMessage: Message = new Message();

            newMessage.id = uuid;

            newMessage.packetType = parseInt(metadata[0]);
            newMessage.eventName = metadata[1];
            newMessage.command = metadata[2];

            newMessage.message = message;

            return newMessage;
        } catch (e) {
            console.log(e);
            return null;
        }
    } //parseMessage

    private async parseBinaryMessage(message: Blob): Promise<BinaryMessage> {
        try {
            var dv: DataView = new DataView(await new Response(message).arrayBuffer());
            var array: ArrayBuffer = await new Response(message).arrayBuffer();

            // get the header size
            var headerLength: number = dv.getInt32(0, true);

            // get the string header
            var dec: TextDecoder = new TextDecoder();
            var header: string = dec.decode(array.slice(4, (headerLength + 4)));

            // get the message length
            var messageLength: number = dv.getInt32(headerLength + 4, true);

            // get the binary message
            var binaryMessage: ArrayBuffer = array.slice((headerLength + 8), (headerLength + 8 + messageLength));

            /**/

            // initial check
            if (header[header.length - 1] != '$') return null;

            // get UUID
            var uuidEnd: number = header.indexOf('$');
            if (uuidEnd == -1) return null;
            var uuid: string = header.substring(0, uuidEnd);

            header = header.substring(uuidEnd + 1);

            // get metadata
            var packetInitializationEnd: number = header.indexOf('$');
            if (packetInitializationEnd == -1) return null;
            var metadata: string[] = header.substring(0, packetInitializationEnd).split(':');
            if (metadata.length != 3) return null;


            var newMessage: BinaryMessage = new BinaryMessage();

            newMessage.id = uuid;

            newMessage.packetType = parseInt(metadata[0]);
            newMessage.eventName = metadata[1];
            newMessage.command = metadata[2];

            newMessage.message = new Blob([binaryMessage]);

            return newMessage;
        } catch (e) { console.log(e); return null; }
    } //parseBinaryMessage

    private serializeMessage(m: Message): string {
        try {
            var str: string = "";
            str += m.id
                + '$'
                + m.packetType + ':'
                + m.eventName + ':'
                + m.command
                + '$' + m.message + '$';
            return str;
        }
        catch (e) { console.log(e); return null; }
    } //serializeMessage

    private getNewMessage(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.client.onmessage = (data) => {
                resolve(data.data);
            };
            this.client.onerror = () => {
                reject();
            };
        });
    };

    private receiveMessage(): Promise<Message> {
        return new Promise((resolve, reject) => {
            this.client.onmessage = (data) => {
                resolve(this.parseMessage(data.data));
            };
            this.client.onerror = () => {
                reject();
            };
        });
    };

    private sendMessage(m: Message): void {
        var serializedMessage: string = this.serializeMessage(m);
        this.client.send(serializedMessage);
    }

    private async handleInit(): Promise<boolean> {
        var m: Message = new Message(null, PacketType.C_init, "", "", this.CURRENT_VERSION.toString());
        this.sendMessage(m);

        var response: Message = await this.receiveMessage();
        if (response == null) return false;
        if (response.packetType == PacketType.S_initOk) {
            this.id = response.message;
            this.state = NetEventIOClientState.Initialized;
            return true;
        }
        return false;
    }

    private async handleAuth(): Promise<boolean> {
        var token: string = await this.onTokenLogin();
        if (token !== null && token !== '') {
            this.sendMessage(new Message(null, PacketType.C_authSend, "", "token", token));
            var response: Message = await this.receiveMessage();

            if (response == null) return false;
            if (response.packetType == PacketType.S_authOk) {
                this.state = NetEventIOClientState.Authenticated;
                return true;
            }
        }

        var pwd: string = await this.onPasswordLogin();
        this.sendMessage(new Message(null, PacketType.C_authSend, "", "", pwd));

        var response: Message = await this.receiveMessage();
        if (response == null) return false;
        if (response.packetType == PacketType.S_authOk) {
            this.state = NetEventIOClientState.Authenticated;
            return true;
        }
        return false;
    }

    private handleEvent(m: Message): void {
        if (this.state != NetEventIOClientState.Authenticated) return;
        // search event by name in the dictionary
        try {
            (this.messageListeners[m.eventName])(this, m);
        }
        catch (e) { console.log(e); return; }
    }

    private handleBinaryEvent(bm: BinaryMessage): void {
        if (this.state != NetEventIOClientState.Authenticated) return;
        // search event by name in the dictionary
        try {
            (this.messageListeners[bm.eventName])(this, bm);
        }
        catch (e) { console.log(e); return; }
    }

    public on(eventName: string, listener: Function): boolean {
        if (eventName.indexOf(':') != -1 || eventName.indexOf('$') != -1 || listener == null) return false;
        try {
            this.messageListeners[eventName] = listener;
        }
        catch (e) {
            console.log(e);
            return false;
        }
        return true;
    }

    public removeEvent(eventName: string): boolean {
        if (eventName.indexOf(':') != -1 || eventName.indexOf('$') != -1) return false;
        try {
            delete this.messageListeners[eventName];
            return true;
        }
        catch (e) {
            console.log(e);
            return false;
        }
    }

    public handleCallbackResponse(m: Message): void {
        var f: Function = this.callbackQueue[m.id];
        if (f == null || f == undefined) return;
        delete this.callbackQueue[m.id];
        f(this, m.message);
    }

    public emit(eventName: string = '', command: string = '', message: string = '', callback: Function): void {
        if (this.state != NetEventIOClientState.Authenticated) return;
        if (callback == undefined || callback == null) {
            var m: Message = new Message(null, PacketType.C_sendEvent, eventName, command, message);
            var mSerialized: string = this.serializeMessage(m);
            this.client.send(mSerialized);
        } else {
            var m: Message = new Message(null, PacketType.C_sendEventForCallback, eventName, command, message);
            var mSerialized: string = this.serializeMessage(m);

            this.callbackQueue[m.id] = callback;

            this.client.send(mSerialized);
        }
    }

    public emitBinary(eventName: string = '', command: string = '', message: Blob): void {
        if (this.state != NetEventIOClientState.Authenticated) return;

        var bm: BinaryMessage = new BinaryMessage(null, PacketType.C_binaryEvent, eventName, command, message);
        var mSerialized: Blob = this.serializeBinaryMessage(bm);

        this.client.send(mSerialized);
    }

    public serializeBinaryMessage(bm: BinaryMessage): Blob {
        // get the text header
        var header: string = '';
        header += bm.id + '$' + bm.packetType + ':' + bm.eventName + ':' + bm.command + '$';

        // get the footer
        var footer: string = '$';

        // get the header and message lengths
        var headerLength: number = header.length;
        var bufHeaderLength: ArrayBuffer = new ArrayBuffer(4);
        var viewHeaderLength: DataView = new DataView(bufHeaderLength);
        viewHeaderLength.setUint32(0, headerLength, false);
        var messageLength: number = bm.message.size;

        var bufMessageLength: ArrayBuffer = new ArrayBuffer(4);
        var viewMessageLength: DataView = new DataView(bufMessageLength);
        viewMessageLength.setUint32(0, messageLength, false);

        // append on a new blob
        var b: Blob = new Blob([this.toBytesInt32(headerLength), header, this.toBytesInt32(messageLength), bm.message, footer]);

        // return the result
        return b;
    }

    private toBytesInt32(num: number) {
        var arr: Uint8Array = new Uint8Array([
            (num & 0x000000ff),
            (num & 0x0000ff00) >> 8,
            (num & 0x00ff0000) >> 16,
            (num & 0xff000000) >> 24
        ]);
        return arr.buffer;
    }

    public subscribe(topicName: string, listener: Function): void {
        if (topicName.indexOf(':') >= 0 || topicName.indexOf('$') >= 0 || listener == null) return;
        try {
            this.subscriptionListeners[topicName] = listener;
            const m: Message = new Message(null, PacketType.C_subscribe, topicName, "", "");
            this.sendMessage(m);
        }
        catch (Exception) {
            return;
        }
    }

    public unsubscribe(topicName: string): void {
        if (topicName.indexOf(':') >= 0 || topicName.indexOf('$') >= 0) return;
        try {
            delete this.subscriptionListeners[topicName];
            var m: Message = new Message(null, PacketType.C_unsubscribe, topicName, "", "");
            this.sendMessage(m);
        }
        catch (e) {
            console.log(e);
            return;
        }
    }

    public unsubscribeAll(): void {
        this.subscriptionListeners = {};
        try {
            var m: Message = new Message(null, PacketType.C_unsubscribeAll, "", "", "");
            this.sendMessage(m);
        }
        catch (e) {
            console.log(e);
            return;
        }
    }

    private handleSubscriptionPush(m: Message): void {
        if (this.state != NetEventIOClientState.Authenticated) return;
        // search event by name in the dictionary
        var listener: Function;
        try {
            listener = this.subscriptionListeners[m.eventName];

            // emit event
            const changeType: SubscriptionChangeType = parseInt(m.command.split(',')[0]);
            const pos: number = parseInt(m.command.split(',')[1]);
            listener(this, m.eventName, changeType, pos, m.message);
        }
        catch (e) {
            console.log(e);
            return;
        }
        if (listener == null) return;
    }
}

export { PacketType, NetEventIOClientState, SubscriptionChangeType, Message, NetEventIOOptions, NetEventIO_Client }
